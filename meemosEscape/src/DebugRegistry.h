#ifndef _DebugRegistry_h_
#define _DebugRegistry_h_

#include <cassert>
#include <list>
#include <utility>
#include "Debugable.h"

namespace avt{
	/**
	 * Singleton pattern 
	 */
	class DebugRegistry
	{	
	private:
		DebugRegistry(){};
		static DebugRegistry* _instance;
		static std::list<Debugable*>* _debugables;

	public:
		static DebugRegistry* getInstance();
		static void deleteInstance();
		static bool isInstantiated();

		static void add(Debugable* debugable);
		static void remove(Debugable* debugable);
		static void clearList();

		static void toggleDebugMode();
	};
}

#endif