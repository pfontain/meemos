#include "StatePhysicsMeemoMinionHit.h"

namespace avt{
	void StatePhysicsMeemoMinionHit::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		statePhysicsMeemoMinionVisitor->start(this);
	}

	void StatePhysicsMeemoMinionHit::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionHit::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){}

	bool StatePhysicsMeemoMinionHit::canInterrupt(){
		return false;
	}
}