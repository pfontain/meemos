#ifndef _Behaviour_h_
#define _Behaviour_h_

#include "EmotionalAgent.h"
#include "Map.h"
#include "EventMemoryRetrieval.h"
#include "EventWitness.h"
#include "Timer.h"
#include "ThoughtBalloonStateMonitor.h"

namespace meemo{
	class Behaviour{
	public:
		Behaviour(EmotionalAgent* emotionalAgent);
		void init();

	protected:
		EmotionalAgent* _emotionalAgent;
		appraisal::EmotionalState* _emotionalState;
		avt::Physics* _physics;
		Timer _thoughtBalloonTimer;
		avt::ThoughtBalloonStateMonitor _thoughtBalloonStateMonitor;
		double _thoughtBalloonDuration;

	public:
		virtual void update(double elapsedSeconds);

	protected:
		virtual void updateMoodColour();
		void updateFace();
		void updateFace(appraisal::EmotionType::Type emotionType);
		virtual void updateThoughtBalloon(double elapsedSeconds);
		virtual void activateThoughtBalloon(avt::EventMemoryRetrieval* event);
		virtual void setStateThoughtBalloonMonitored(avt::ThoughtBalloonState::State stateThoughtBalloon);
	};
}

#endif // _Behaviour_h_