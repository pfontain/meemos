#ifndef _Eyes_h_
#define _Eyes_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Physical.h"
#include "Texture.h"
#include "Utilities.h"

namespace avt{

	namespace EyesState{
		enum State{
			NEUTRAL,
			JOY,
			SAD,
			ANGRY};
	}

	class Eyes{
	public:
		Eyes(Physical* physical);
		~Eyes();
		void init();

	private:
		int _modelDLNeutral;
		int _modelDLJoy;
		int _modelDLSad;
		int _modelDLAngry;
		float _radius;
		unsigned short _numberEdges;

		Material* _material;
		Texture _textureNeutral;
		Texture _textureJoy;
		Texture _textureSadLeft;
		Texture _textureSadRight;
		Texture _textureAngryLeft;
		Texture _textureAngryRight;

		cg::Vector3d _relativePosition;
		Physical* _physical;

		EyesState::State _state;

	private:
		void initTextures();
		void makeModels();
		void makeModelNeutral();
		void makeModelJoy();
		void makeModelSad();
		void makeModelAngry();
		void makeModelCircle(const cg::Vector3d& position);
		void makeModelCircleNonTextured(const cg::Vector3d& position);

	public:
		void draw();
		void neutralOn();
		void joyOn();
		void sadOn();
		void angryOn();
	};
}


#endif // _Eyes_h_