#ifndef _MaterialDynamic_h_
#define _MaterialDynamic_h_

#include "Material.h"

namespace avt{
	class MaterialDynamic: public Material{
	public:
		void init();
		void draw();
	};
}

#endif // _MaterialDynamic_h_