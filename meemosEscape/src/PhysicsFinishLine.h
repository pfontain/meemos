#ifndef _PhysicsFinishLine_h
#define _PhysicsFinishLine_h

#include "Physics.h"
#include "EventCollisionFinishLine.h"

namespace avt {

	class PhysicsFinishLine : public Physics {

	public:
		virtual EventCollision* getCollisionEvent(){
			return new EventCollisionFinishLine();
		}

		bool isColliding(Physics* physics){
			return defaultIsColliding(this,physics);
		}
	};
}

#endif