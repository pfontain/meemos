#ifndef _ReactiveProcessMemoryRetrieval_h_
#define _ReactiveProcessMemoryRetrieval_h_

#include "ReactiveProcess.h"
#include "MemoryParameters.h"
#include "MemoryStorage.h"

namespace meemo{
	class ReactiveProcessMemoryRetrieval: public appraisal::ReactiveProcess{
	public:
		ReactiveProcessMemoryRetrieval(const std::string& nameSelf,appraisal::EmotionalState* emotionalState,MemoryStorage* memoryStorage);

	private:
		MemoryStorage* _memoryStorage;

	private:
		void generateActionBasedEmotions(appraisal::Event* event, int praiseworthiness);
		void generateFortuneForAll(appraisal::Event* event, int desirability, int desirabilityForOther, std::string other);
		void generateFortuneOfOtherEmotions(appraisal::Event* event, int desirability, int desirabilityForOther,std::string target);
		void generateWellBeingEmotions(appraisal::Event* event, int desirability);
	};
}

#endif // _ReactiveProcessMemoryRetrieval_h_