#ifndef _ThoughtBalloonStateMonitor_h_
#define _ThoughtBalloonStateMonitor_h_

#include "ThoughtBalloon.h"

namespace avt{
	class ThoughtBalloonStateMonitor{
	public:
		ThoughtBalloonStateMonitor(void);

	private:
		ThoughtBalloonState::State _state;

	public:
		ThoughtBalloonState::State getState(void);

		// returns true if the state was changed, and false otherwise
		bool update(ThoughtBalloonState::State state);
	};
}

#endif // _ThoughtBalloonStateMonitor_h_