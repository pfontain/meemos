#ifndef _Timer_h_
#define _Timer_h_

namespace meemo{
	class Timer{
	public:
		Timer(void);

	private:
		double _value;
		bool _on;

	public:
		void setTimer(double value);
		void resetTimer(double value);
		void update(double elapsedSeconds);
		bool isOn(void);
	};
}

#endif // _Timer_h_