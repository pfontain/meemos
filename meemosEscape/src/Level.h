#ifndef _Level_avt_
#define _Level_avt_

#include "AbstractLevel.h"
#include "CameraFree.h"
#include "MeemoCaptain.h"
#include "MeemoMinion.h"
#include "InterfaceController.h"
#include "CameraController.h"
#include "DebugConsole.h"
#include "LevelLoader.h"
#include "GameLogic.h"
#include "MeemoApplication.h"

namespace avt{
	class MeemoApplication;
	class LevelLoader;

	class Level: public AbstractLevel
	{
	public:
		Level(GameApplication* application);
		~Level(void);

	private:
		MeemoCaptain* _meemoCaptain;
		std::list<MeemoMinion*> _meemoMinions;
		std::list<Meemo*> _meemos;
		GameLogic* _gameLogic;

	public:
		GameLogic* getGameLogic() const;
		void add(Camera* camera);
		void add(Scene* scene);
		void add(cg::Entity* entity);
		void add(MeemoCaptain* meemoCaptain);
		void add(MeemoMinion* meemoMinion);

	protected:
		void createCameras(void);
		void createScene(void);
		void createEntities(void);
		void registerEntities(void);
	};
}

#endif