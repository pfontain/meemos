#include "AbstractLevel.h"

namespace avt{
	AbstractLevel::AbstractLevel(std::string id, GameApplication* application){
		_id = id;
		_application = application;
	}

	AbstractLevel::~AbstractLevel(){
		delete _map;
		destroyAll();
	}

	void AbstractLevel::init(){
		createCameras();
		_activeCamera = _cameras.begin();
		createScene();
		createEntities();
		registerEntities();
	}

	const std::string AbstractLevel::getId(){
		return _id;
	}

	Camera* AbstractLevel::getActiveCamera(){
		return *_activeCamera;
	}

	Scene* AbstractLevel::getActiveScene(){
		return _activeScene;
	}

	void AbstractLevel::setMap(Map* map){
		_map = map;
	}

	Map* AbstractLevel::getMap() const{
		return _map;
	}

	void AbstractLevel::destroyAll(){
		destroyEntities();
		destroyScene();
		destroyCameras();
		destroyPhysics();
		destroySingletons();
	}


	void AbstractLevel::destroyEntities(){

		std::list<cg::Entity*>::iterator entitiesIterator = _entities.begin();
		for(;entitiesIterator != _entities.end(); entitiesIterator++){

			std::string entityName = (*entitiesIterator)->getId();
			cg::Registry::instance()->destroy(entityName);
		}
		_entities.clear();
	}

	void AbstractLevel::destroyScene(){
		std::string sceneName = _activeScene->getId();
		cg::Registry::instance()->destroy(sceneName);
	}

	void AbstractLevel::destroyCameras(){
		std::list<Camera*>::iterator camerasIterator = _cameras.begin();
		for(;camerasIterator != _cameras.end(); camerasIterator++){

			std::string cameraName = (*camerasIterator)->getId();
			cg::Registry::instance()->destroy(cameraName);
		}
		_cameras.clear();
	}

	void AbstractLevel::destroyPhysics(){
		if(PhysicsRegistry::isInstantiated())
			PhysicsRegistry::deleteInstance();
	}

	void AbstractLevel::destroySingletons(){
		if(DebugRegistry::isInstantiated())
			DebugRegistry::deleteInstance();
		EventNull::deleteInstance();
	}

	void AbstractLevel::add(Camera* camera){
		_application->addLevelEntity(camera);
		_cameras.push_back(camera);
	}

	void AbstractLevel::add(Scene* scene){
		_application->addLevelEntity(scene);
		_activeScene = scene;
	}

	void AbstractLevel::add(cg::Entity* entity){
		_application->addLevelEntity(entity);
		_entities.push_back(entity);
	}

	void AbstractLevel::switchCamera(){
		_activeCamera++;
		if(_activeCamera == _cameras.end())
			_activeCamera = _cameras.begin();
	}

	void AbstractLevel::switchCamera(const std::string id){
		std::string cameraId;
		do{
			_activeCamera++;
			if(_activeCamera == _cameras.end())
				_activeCamera = _cameras.begin();
			cameraId = (*_activeCamera)->getId();
		}
		while(cameraId.compare(id) != 0);
	}
}