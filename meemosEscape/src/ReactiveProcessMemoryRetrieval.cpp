#include "ReactiveProcessMemoryRetrieval.h"

using namespace appraisal;

namespace meemo{

	ReactiveProcessMemoryRetrieval::ReactiveProcessMemoryRetrieval(const std::string& nameSelf,EmotionalState* emotionalState,MemoryStorage* memoryStorage):ReactiveProcess(nameSelf,emotionalState){
		_memoryStorage = memoryStorage;
	}

	void ReactiveProcessMemoryRetrieval::generateActionBasedEmotions(appraisal::Event* event, int praiseworthiness) {
		BaseEmotion* em;
		float potential;

		potential = praiseworthiness;
		if(event->type() == EventType::MEMORY_RETRIEVAL)
			potential = potential * meemo::MemoryParameters::MEMORY_RETRIEVAL_INTENSITY_BIAS;

		if(praiseworthiness >= 0) {
			if(event->type() != EventType::WITNESS) {
				em = new BaseEmotion(EmotionType::PRIDE, potential, event->copy(), "SELF");
			}
			else {
				em = new BaseEmotion(EmotionType::ADMIRATION, potential, event->copy(), "OTHER");//event.getSubject()
			}
		}
		else {
			if(event->type() != EventType::WITNESS) {
				em = new BaseEmotion(EmotionType::SHAME, -potential, event->copy(), "SELF");
			}
			else {
				em = new BaseEmotion(EmotionType::REPROACH, -potential, event->copy(), "OTHER");//event.getSubject()
			}
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		if(activeEmotion != NULL)
			_memoryStorage->addMemoryTrace(event,activeEmotion);
		delete em;
	}

	void ReactiveProcessMemoryRetrieval::generateFortuneForAll(appraisal::Event* event, int desirability, int desirabilityForOther, std::string other)
	{
		generateFortuneOfOtherEmotions(event,desirability,desirabilityForOther,other);
	}

	void ReactiveProcessMemoryRetrieval::generateFortuneOfOtherEmotions(appraisal::Event* event, int desirability, int desirabilityForOther,std::string target) {
		BaseEmotion* em;
		float potential;

		potential = (abs(desirabilityForOther) + abs(desirability)) / 2.0f;
		if(event->type() == EventType::MEMORY_RETRIEVAL)
			potential = potential * meemo::MemoryParameters::MEMORY_RETRIEVAL_INTENSITY_BIAS;

		if(desirability >= 0) {
			if(desirabilityForOther >= 0) {
				em = new BaseEmotion(EmotionType::HAPPYFOR, potential, event->copy(), target);	
			}
			else {
				em = new BaseEmotion(EmotionType::GLOATING, potential, event->copy(), target);
			}
		}
		else {
			if(desirabilityForOther >= 0) {
				em = new BaseEmotion(EmotionType::RESENTMENT, potential, event->copy(), target);
			}
			else {
				em = new BaseEmotion(EmotionType::PITY, potential, event->copy(), target);
			}
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		if(activeEmotion != NULL)
			_memoryStorage->addMemoryTrace(event,activeEmotion);
		delete em;
	}

	void ReactiveProcessMemoryRetrieval::generateWellBeingEmotions(appraisal::Event* event, int desirability) {
		BaseEmotion* em;
		float potential;

		potential = desirability;
		if(event->type() == EventType::MEMORY_RETRIEVAL)
			potential = potential * meemo::MemoryParameters::MEMORY_RETRIEVAL_INTENSITY_BIAS;

		if(desirability >= 0) {
			em = new BaseEmotion(EmotionType::JOY, potential, event->copy(), "");
		}
		else {
			em = new BaseEmotion(EmotionType::DISTRESS, -potential, event->copy(), "");
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		if(activeEmotion != NULL)
			_memoryStorage->addMemoryTrace(event,activeEmotion);
		delete em;
	}
}