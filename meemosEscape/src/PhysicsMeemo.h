#ifndef _MeemoPhysics_h
#define _MeemoPhysics_h

#include <stdio.h>
#include <math.h>
#include "Physics.h"
#include "EventCollisionDefault.h"
#include "EventWitnessNotifier.h"

namespace avt {

	class PhysicsMeemo : public Physics,
		public EventWitnessNotifier
	{
	public:
		PhysicsMeemo(void);
	protected:
		cg::Vector3d _velocity;
		double _hopLinearVelocity;
		double _dropDepth;
		double _riseLinearVelocity;
		double _hopDuration;
		cg::Quaterniond _turnOriginalOrientation;
		double _turnTotalRotationAngle;
		double _turnElapsedSeconds;
		cg::Vector3d _turnOriginalFront;
		cg::Vector3d _turnOriginalRight;
		double _pullLeverLinearVelocity;
	
	public:
		virtual void step(double elapsedSeconds) = 0;
		virtual void generatePhysicalEvents();
		virtual bool isColliding(Physics* physics);
		virtual EventCollision* getCollisionEvent();

	protected:
		virtual void rotate(const cg::Vector3d& direction);
		double rotationAngle(const cg::Vector3d& direction);
		double hopDuration();
		void stepTurnHopLastTurn();
		void stepTurnHopTurn(double elapsedSeconds);
	};
}

#endif