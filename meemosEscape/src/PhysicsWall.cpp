#include "PhysicsWall.h"

namespace avt {

	EventCollision* PhysicsWall::getCollisionEvent(){
		meemo::Location location(_position[0],_position[1]);
		return new EventCollisionWall(location);
	}

	bool PhysicsWall::isColliding(Physics* collider){
		// TODO: Repeated in Physics Lever
		float halfSideSize = _boundingSphereRadius;
		float colliderBoundingSphereRadius = collider->_boundingSphereRadius;

		cg::Vector3d colliderPosition = collider->getPosition();
		float colliderPositionX = colliderPosition[0];
		float colliderPositionY = colliderPosition[1];
		float colliderPositionZ = colliderPosition[2];

		float positionX = _position[0];
		float positionY = _position[1];
		float positionZ = _position[2];
			
		return
			colliderPositionX > positionX - halfSideSize - colliderBoundingSphereRadius &&
			colliderPositionX < positionX + halfSideSize + colliderBoundingSphereRadius &&
			colliderPositionY > positionY - halfSideSize - colliderBoundingSphereRadius &&
			colliderPositionY < positionY + halfSideSize + colliderBoundingSphereRadius;
	}
}