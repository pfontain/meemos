#include "PhysicsLever.h"

namespace avt{
	PhysicsLever::PhysicsLever(void){
		_state = PhysicsLeverState::UP;
	}

	void PhysicsLever::setUpAngle(float angle){
		_upAngle = angle;
	}

	void PhysicsLever::descend(void){
		_state = PhysicsLeverState::DESCENDING;
	}

	EventCollision* PhysicsLever::getCollisionEvent(void){
		return new EventCollisionDefault();
	}

	bool PhysicsLever::isColliding(avt::Physics *collider){
		float halfSideSize = _boundingSphereRadius;
		float colliderBoundingSphereRadius = collider->_boundingSphereRadius;

		cg::Vector3d colliderPosition = collider->getPosition();
		float colliderPositionX = colliderPosition[0];
		float colliderPositionY = colliderPosition[1];
		float colliderPositionZ = colliderPosition[2];

		float positionX = _position[0];
		float positionY = _position[1];
		float positionZ = _position[2];
			
		return
			colliderPositionX > positionX - halfSideSize - colliderBoundingSphereRadius &&
			colliderPositionX < positionX + halfSideSize + colliderBoundingSphereRadius &&
			colliderPositionY > positionY - halfSideSize - colliderBoundingSphereRadius &&
			colliderPositionY < positionY + halfSideSize + colliderBoundingSphereRadius;
	}

	void PhysicsLever::step(double elapsedSeconds){
		if(_state == PhysicsLeverState::DESCENDING)
			stepDescending(elapsedSeconds);
		Physics::step(elapsedSeconds);
	}

	void PhysicsLever::applyTransforms(void) {
		//glTranslated(_position[0]-_boundingSphereRadius,_position[1]-_boundingSphereRadius,_position[2]);
		glTranslated(_position[0],_position[1],_position[2]);
		glMultMatrixd(_rotationMatrix);
		//glTranslated(+_boundingSphereRadius,+_boundingSphereRadius,0);
	}

	void PhysicsLever::applyTranslation(void) {
		glTranslated(_position[0],_position[1],_position[2]);
	}

	void PhysicsLever::stepDescending(double elapsedSeconds){
		double rotationAngleModule = fabs(_orientation.w);
		if(1 - rotationAngleModule > _upAngle / 180)
			_state = PhysicsLeverState::DOWN;
		else
			pitchDown();
	}
}