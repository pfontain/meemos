#ifndef _MyObject_h_
#define _MyObject_h_

#include "cg/cg.h"
#include <vector>

namespace avt{

	class MeemoMesh
	{

	public:
		~MeemoMesh();
		void init();

	public:
		int number_vertices;
		int number_faces;
		cg::Vector3d _position, _size;
		cg::Vector3d _color;
		cg::Vector3d* _points;
		cg::Vector3d* _normals;
		GLuint* _indices;
	};
}


#endif