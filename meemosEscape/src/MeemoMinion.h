#ifndef _MeemoMinion_h_
#define _MeemoMinion_h_

#include "Meemo.h"
#include "MeemoCaptain.h"
#include "PhysicsMeemoMinion.h"
#include "MaterialStatic.h"
#include "BehaviourMeemoMinion.h"
#include "MovePathAgent.h"
#include "EventCollisionWall.h"
#include "UpdatePathAgent.h"
#include "NumberBillboard.h"

#include <typeinfo>
#include <string.h>
#include <queue>

namespace avt{
	namespace MeemoMinionState{
		enum State{
			PARADO = 1,
			MORTO = 2,
			TERMINADO = 3
		};
	}

	class MeemoMinion : public Meemo,
		public UpdatePathAgent,
		public cg::IKeyboardEventListener
	{
	public:
		MeemoMinion(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic);
		~MeemoMinion();
		virtual void init();

	protected:
		MeemoCaptain* _meemoCaptain;
		meemo::BehaviourMeemoMinion* _behaviourMeemoMinion;
		PhysicsMeemoMinion* _physicsMeemoMinion;
		int _orderId;
		unsigned int _idNumber;
		int _lifeState;
		NumberBillboard* _numberBillboard;
		std::queue<cg::Vector3d> _plan;


	public:
		void setPlan(std::queue<cg::Vector3d> plan);
		void setWaitDurations(std::queue<double> waitDurations);
		void onKeyPressed(unsigned char key);
        void onKeyReleased(unsigned char key);
        void onSpecialKeyPressed(int key);
        void onSpecialKeyReleased(int key);

		void draw(void);
		void update(unsigned long elapsed_millis);
		virtual void update(Event* event);
		void trigger(EventWitness* eventWitness);
		void trigger(EventCollisionWall* eventCollisionWall);
		void trigger(EventCollisionAnvil* eventCollisionAnvil);
		void trigger(EventCollisionHole* eventCollisionHole);
		void trigger(EventCollisionTrapDoor* eventCollisionTrapDoor);
		void trigger(EventCollisionDefault* eventCollisionDefault);
		void trigger(EventCollisionFinishLine* eventCollisionFinishLine);
		void trigger(EventSeeOpenDoor* eventSeeOpenDoor);
		void trigger(EventSeeDoor* eventSeeDoor);
		void trigger(EventSeeLever* eventSeeLever);
		void updateOrder(double elapsedSeconds);
		virtual bool movePath(std::list<meemo::Location*>* locations);
		void pullLever(void);

	protected:
		void initMaterial(void);
	};
}

#endif