#include "Lever.h"

namespace avt{
	Lever::Lever(int x, int z,float size,const std::string& id): cg::Entity(id){
		_size = size;
		_physics = new PhysicsLever();
		_physics->setPosition(x,z,_size/2);
		_materialBox = new MaterialStatic();
		_materialHandle = new MaterialStatic();
		_length = cg::Properties::instance()->getInt("LEVER_LENGTH");
		_upAngle = cg::Properties::instance()->getInt("LEVER_UP_ANGLE");
		_width = cg::Properties::instance()->getInt("LEVER_WIDTH");
		_anchorPosition = cg::Properties::instance()->getVector3d("LEVER_ANCHOR_POSITION");
	}

	Lever::~Lever(void){
		delete _physics;
		delete _materialBox;
		delete _materialHandle;
	}

	void Lever::init(void) {
		cg::Vector3d colourBox = cg::Properties::instance()->getVector3d("LEVER_BOX_COLOUR");
		_materialBox->setAmbient(colourBox[0],colourBox[1],colourBox[2],1.0);
		_materialBox->setDiffuse(colourBox[0],colourBox[1],colourBox[2],1.0);
		_materialBox->init();

		cg::Vector3d colourHandle = cg::Properties::instance()->getVector3d("LEVER_HANDLE_COLOUR");
		_materialHandle->setAmbient(colourHandle[0],colourHandle[1],colourHandle[2],1.0);
		_materialHandle->setDiffuse(colourHandle[0],colourHandle[1],colourHandle[2],1.0);
		_materialHandle->init();

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);
		_physics->setUpAngle(_upAngle);
		_physics->setAngularVelocity(cg::Properties::instance()->getInt("LEVER_ANGULAR_VELOCITY"));

		makeModel();
		DebugRegistry::getInstance()->add(this);
	}

	void Lever::draw(void){
		glPushMatrix();
		{
			_physics->applyTranslation();
			_materialBox->draw();
			glCallList(_modelBox);
		}
		glPopMatrix();
		glPushMatrix();
		{
			_physics->applyTransforms();
			_materialHandle->draw();
			glCallList(_modelHandle);
		}
		glPopMatrix();
		drawDebug();
	}

	void Lever::update(unsigned long elapsed_millis){
		double elapsed_seconds = elapsed_millis / (double)1000;
		_physics->step(elapsed_seconds);
	}

	void Lever::descend(void){
		_physics->descend();
	}

	void Lever::makeModel(void)
	{
		_modelBox = glGenLists(1);
		assert(_modelBox != 0);
		glNewList(_modelBox,GL_COMPILE);
		{
			glutSolidCube(_size);
		}
		glEndList();

		_modelHandle = glGenLists(1);
		assert(_modelHandle != 0);
		glNewList(_modelHandle,GL_COMPILE);
		{
			drawHandle();
		}
		glEndList();
	}

	void Lever::drawHandle(void){
		cg::Quaterniond orientation;
		double rotationMatrix[16];

		orientation.setRotationDeg(-_upAngle,cg::Vector3d(0,1,0));
		orientation.getGLMatrix(rotationMatrix);
		glMultMatrixd(rotationMatrix);
		drawExtrudedSquare(cg::Vector3d(0,0,0),_width,_length);

		glTranslated(_length,-_length/2,0);
		orientation.setRotationDeg(90,cg::Vector3d(0,0,1));
		orientation.getGLMatrix(rotationMatrix);
		glMultMatrixd(rotationMatrix);
		drawExtrudedSquare(cg::Vector3d(0,0,0),_width,_length);		
	}

	void Lever::drawExtrudedSquare(cg::Vector3d position,double squareSideSize,double extrusionLength){
		double halfSquareSideSize = squareSideSize/2;
		glBegin(GL_QUADS);
		{
			//face cima
			glNormal3f(0,0,1);
			glVertex3f(position[0], position[1] - halfSquareSideSize, position[2] + halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength, position[1] - halfSquareSideSize, position[2] + halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength, position[1] + halfSquareSideSize, position[2] + halfSquareSideSize);
			glVertex3f(position[0], position[1] + halfSquareSideSize, position[2] + halfSquareSideSize);

			//face tras
			glNormal3f(-1,0,0);
			glVertex3f(position[0], position[1] + halfSquareSideSize,position[2] - halfSquareSideSize);
			glVertex3f(position[0], position[1] - halfSquareSideSize,position[2] - halfSquareSideSize);
			glVertex3f(position[0], position[1] - halfSquareSideSize,position[2] + halfSquareSideSize);
			glVertex3f(position[0], position[1] + halfSquareSideSize,position[2] + halfSquareSideSize);

			//face frente
			glNormal3f(1,0,0);
			glVertex3f(position[0] + extrusionLength , position[1] - halfSquareSideSize,position[2] - halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength , position[1] + halfSquareSideSize,position[2] - halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength , position[1] + halfSquareSideSize,position[2] + halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength , position[1] - halfSquareSideSize,position[2] + halfSquareSideSize);	

			//face direita
			glNormal3f(0,-1,0);
			glVertex3f(position[0], position[1] - halfSquareSideSize, position[2] - halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength , position[1] - halfSquareSideSize, position[2] - halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength, position[1] - halfSquareSideSize, position[2] + halfSquareSideSize);
			glVertex3f(position[0], position[1] - halfSquareSideSize, position[2] + halfSquareSideSize);

			//face esquerda
			glNormal3f(0,1,0);
			glVertex3f(position[0] + extrusionLength , position[1] + halfSquareSideSize, position[2] - halfSquareSideSize);
			glVertex3f(position[0], position[1] + halfSquareSideSize, position[2] - halfSquareSideSize);
			glVertex3f(position[0], position[1] + halfSquareSideSize, position[2] + halfSquareSideSize);
			glVertex3f(position[0] + extrusionLength, position[1] + halfSquareSideSize, position[2] + halfSquareSideSize);
		}
		glEnd();
	}

	Physics* Lever::getPhysics(void){
		return _physics;
	}
}