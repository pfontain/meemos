#include "Timer.h"

namespace meemo{
	Timer::Timer(void){
		_on = false;
	}

	void Timer::setTimer(double value){
		_on = true;
		_value = value;
	}

	void Timer::resetTimer(double value){
		_value = value;
	}

	void Timer::update(double elapsedSeconds){
		if(_on){
			_value = _value - elapsedSeconds;
			if(_value < 0.0)
				_on = false;
		}
	}
	
	bool Timer::isOn(void){
		return _on;
	}
}