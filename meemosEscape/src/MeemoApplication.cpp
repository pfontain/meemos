#include "MeemoApplication.h"

namespace avt {

	MeemoApplication::MeemoApplication(void): GameApplication("Meemo"){
		_menuWindowWidth = 800;
		_menuWindowHeight = 600;
		_window.width = _menuWindowWidth;
		_window.height = _menuWindowHeight;
	}

	void MeemoApplication::createEntities(void){
		addEntity(new MenuController(this,true));
		//loadDebugLevel();
	}

	void MeemoApplication::loadDebugLevel(void){
		loadLevel(new Level(this));
	}

	void MeemoApplication::loadLevel(Level* level){
		_currentLevel = level;
		GameApplication::loadLevel(level);
	}

	void MeemoApplication::unloadLevel(){
		delete _currentLevel;
		_isLevelLoaded = false;
	}
}