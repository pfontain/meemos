// This file is an example for CGLib.
//
// CGLib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// CGLib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CGLib; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Copyright 2007 Carlos Martinho

#include "Light.h"


namespace avt {

	Light::Light(std::string id) : cg::Entity(id) {
	}
	Light::~Light() {
	}
	void Light::init() {
		int unitaryDistance = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		_physics.setLinearVelocity(unitaryDistance);
		_physics.setAngularVelocity(100.0);
		_physics.setAxesScale(unitaryDistance);

		cg::Vector3d lightPosition(cg::Properties::instance()->getVector3d("LIGHT_POSITION"));
		_physics.setPosition(lightPosition[0],lightPosition[1],lightPosition[2]);
		_physics.yawLeft(90);
		_physics.pitchDown(30);

		GLfloat ambient_light[] = {0.2,0.2,0.2,1.0};
		GLfloat diffuse_light[] = {0.9,0.9,0.9,1.0};
		GLfloat specular_light[] = {0.0,0.0,0.0,1.0};
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
		glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 85.0);
		glEnable(GL_LIGHT0);

		makeLight();
		DebugRegistry::getInstance()->add(this);
	}

	void Light::makeLight() {
		float position[] = {0.0,0.0,0.0,1.0};
		float direction[] = {1.0,0.0,0.0};
		_lightDL = glGenLists(1);
		assert(_lightDL != 0);
		glNewList(_lightDL,GL_COMPILE);
		glLightfv(GL_LIGHT0, GL_POSITION, position);
		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, direction);
		glEndList();
	}

	void Light::draw() {
		glPushMatrix();{
			_physics.applyTransforms();
			glCallList(_lightDL);
		}glPopMatrix();
		drawDebug();
	}

	void Light::update(unsigned long elapsed_millis) {
		if(cg::KeyBuffer::instance()->isKeyDown('t')) {
			_physics.goAhead();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('g')) {
			_physics.goBack();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('f')) {
			_physics.yawLeft();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('h')) {
			_physics.yawRight();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('r')) {
			_physics.pitchUp();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('v')) {
			_physics.pitchDown();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('b')) {
			_physics.rollLeft();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('n')) {
			_physics.rollRight();
		}
		double elapsed_seconds = elapsed_millis / (double)1000;
		_physics.step(elapsed_seconds);
	}

	Physics* Light::getPhysics(){
		return &_physics;
	}
}