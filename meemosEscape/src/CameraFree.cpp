#include "CameraFree.h"

namespace avt{

	CameraFree::CameraFree(const std::string& id,
		const cg::Vector3d& position,
		const cg::Vector3d& frontDirection,
		const cg::Vector3d& leftDirection,
		const cg::Vector3d& upDirection): Camera(id){

		int unitLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		_physics.setLinearVelocity(unitLength);
		_physics.setAngularVelocity(unitLength);
		_physics.setPosition(position[0],position[1],position[2]);
		_physics.setFront(frontDirection);
		_physics.setLeft(leftDirection);
		_physics.setUp(upDirection);
	}

	void CameraFree::init(){
		Camera::initWindowViewport();
	}

	void CameraFree::setModelViewMatrix() {
		Camera::setProjection();

		cg::Vector3d cameraPosition = _physics.getPosition();
		cg::Vector3d cameraFrontDirection = _physics.getFrontDirection();
		cg::Vector3d cameraUpDirection = _physics.getUpDirection();
		cg::Vector3d lookingPosition = cameraPosition + cameraFrontDirection;

		glLoadIdentity();
		gluLookAt(cameraPosition[0],cameraPosition[1],cameraPosition[2],
			lookingPosition[0],lookingPosition[1],lookingPosition[2],
			cameraUpDirection[0],cameraUpDirection[1],cameraUpDirection[2]);
	}

	void CameraFree::update(unsigned long elapsed_millis){
		//teclas de movimento
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_UP)) {
			_physics.goAhead();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_DOWN)) {
			_physics.goBack();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_LEFT)) {
			_physics.yawLeft();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_RIGHT)) {
			_physics.yawRight();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_PAGE_UP)) {
			_physics.goUp();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_PAGE_DOWN)) {
			_physics.goDown();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_HOME)) {
			_physics.pitchUp();
		}
		if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_END)) {
			_physics.pitchDown();
		}

		double elapsedSeconds = elapsed_millis / (double)1000;
		_physics.step(elapsedSeconds);
	}
}