#ifndef _StatePhysicsMeemoCaptainVisitor_h_avt_
#define _StatePhysicsMeemoCaptainVisitor_h_avt_

#include "StatePhysicsMeemoCaptainWalking.h"
#include "StatePhysicsMeemoCaptainFall.h"
#include "StatePhysicsMeemoCaptainTurnHop.h"
#include "StatePhysicsMeemoCaptainHit.h"
#include "StatePhysicsMeemoCaptainPullLever.h"

namespace avt{
	// Visitor Pattern
	class StatePhysicsMeemoCaptainWalking;
	class StatePhysicsMeemoCaptainFall;
	class StatePhysicsMeemoCaptainTurnHop;
	class StatePhysicsMeemoCaptainHit;
	class StatePhysicsMeemoCaptainPullLever;

	class StatePhysicsMeemoCaptainVisitor{
	public:
		virtual void step(StatePhysicsMeemoCaptainWalking* statePhysicsMeemoCaptainWalking,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoCaptainFall* statePhysicsMeemoCaptainFall,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoCaptainTurnHop* statePhysicsMeemoCaptainTurnHop,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoCaptainHit* statePhysicsMeemoCaptainHit,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoCaptainPullLever* statePhysicsMeemoCaptainPullLever,double elapsedSeconds) = 0;
	};
}

#endif //_StatePhysicsMeemoCaptainVisitor_h_avt_