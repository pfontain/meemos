#ifndef _BehaviourMeemoMinionWithMemory_h_
#define _BehaviourMeemoMinionWithMemory_h_

#include "BehaviourMeemoMinion.h"
#include "MovePathAgentWithMemory.h"
#include "Path.h"

namespace meemo{
	class BehaviourMeemoMinionWithMemory: public BehaviourMeemoMinion{
	public:
		BehaviourMeemoMinionWithMemory(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgentWithMemory* meemoMinionMovePathAgent,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic);

	private:
		MovePathAgentWithMemory* _meemoMinionWithMemory;

	public:
		void updateIntegerMap(int* integerMap,std::pair<int,int>& moveToPosition);
		avt::Path* updatePath(avt::Path* path);

	private:
		void updateIntegerMapAccordingToMemory(int* integerMap);
	};
}

#endif // _BehaviourMeemoMinionWithMemory_h_