#include "MindNoMemory.h"

namespace meemo{
	MindNoMemory::MindNoMemory(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent): Mind(physicalAgent,emotionalAgent){
		_reactiveProcessDefault = NULL;
	}

	MindNoMemory::~MindNoMemory(){
		if(_reactiveProcessDefault != NULL)
			delete _reactiveProcessDefault;
	}

	void MindNoMemory::initModules(){
		Mind::initModules();
		std::string emotionalAgentName = _emotionalAgent->getId();
		_reactiveProcessDefault = new appraisal::ReactiveProcessDefault(emotionalAgentName,_emotionalState);
		_reactiveProcess = _reactiveProcessDefault;
	}

	void MindNoMemory::initEmotionalReactionRules(){
		Mind::initEmotionalReactionRules();
	}

	void MindNoMemory::updateAppraisal(){
		_reactiveProcess->appraisal();
	}
}