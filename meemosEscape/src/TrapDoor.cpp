#include "TrapDoor.h"

namespace avt{

	TrapDoor::TrapDoor(int x, int y, float size, const std::string &id): Entity(id){
		_size = size;
		_physics = new PhysicsTrapDoor();
		_physics->setPosition(x,y,0);
		_material = new MaterialStatic();
	}

	TrapDoor::~TrapDoor(){
		delete _physics;
		delete _material;
	}

	void TrapDoor::init() {
		cg::Properties* properties = cg::Properties::instance();
		cg::Vector3d trapDoorColour = properties->getVector3d("FLOOR_COLOUR");
		_material->setAmbient(trapDoorColour[0],trapDoorColour[1],trapDoorColour[2],1.0);
		_material->setDiffuse(trapDoorColour[0],trapDoorColour[1],trapDoorColour[2],1.0);
		_material->init();

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/5);
		_physics->setEdgeSize(_size);
		_physics->setOpeningAngularVelocity(properties->getDouble("TRAP_DOOR_OPENING_ANGULAR_VELOCITY"));
		_physics->setClosingAngularVelocity(properties->getDouble("TRAP_DOOR_CLOSING_ANGULAR_VELOCITY"));
		_physics->setOpenDuration(properties->getDouble("TRAP_DOOR_OPEN_DURATION"));

		makeModel();
		DebugRegistry::getInstance()->add(this);
	}

	void TrapDoor::draw(void){
		glPushMatrix();
		{
			_physics->applyTransforms();
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
		drawDebug();
	}

	void TrapDoor::makeModel() {
		float halfSize = _size/2.0 + Utilities::Math::smallPositive();

		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			GLboolean glCullFace;
			glGetBooleanv(GL_CULL_FACE,&glCullFace);
			glDisable(GL_CULL_FACE);
			glBegin(GL_QUADS);
			glNormal3f(0,0,1);
			glVertex3f(-halfSize,-halfSize, 0);
			glVertex3f(+halfSize,-halfSize, 0);
			glVertex3f(+halfSize,+halfSize, 0);
			glVertex3f(-halfSize,+halfSize, 0);
			glEnd();
			if(glCullFace)
				glEnable(GL_CULL_FACE);
		}
		glEndList();
	}
	
	void TrapDoor::update(unsigned long elapsed_millis){
		double elapsed_seconds = elapsed_millis / (double)1000;
		_physics->step(elapsed_seconds);
	}

	Physics* TrapDoor::getPhysics(void){
		return _physics;
	}
}