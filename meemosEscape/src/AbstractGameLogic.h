#ifndef _AbstractGameLogic_avt_
#define _AbstractGameLogic_avt_

#include <set>
#include "AbstractLevel.h"
#include "AbstractMeemo.h"
#include "Physical.h"

namespace avt{
	class AbstractGameLogic{
	public:
		AbstractGameLogic(void);

	private:
		std::set<AbstractMeemo*> _meemoMinionsAlive;
		AbstractMeemo* _meemoCaptain;
		bool _meemoCaptainAlive;
		unsigned int _numberMeemoMinionsAtFinishLine;
		Physical* _door;
		Physical* _lever;
		bool _doorOpened;

	public:
		std::set<AbstractMeemo*>* getMeemoMinionsAlive();
		void addMeemoMinion(AbstractMeemo* meemoMinion);
		void setMeemoCaptain(AbstractMeemo* meemoCaptain);
		unsigned int getNumberMeemosAlive(void) const;
		unsigned int getNumberMeemoMinionsAlive(void) const;
		void notifyDeathMeemoMinion(AbstractMeemo* meemoMinion);
		void notifyDeathMeemoCaptain(AbstractMeemo* meemoCaptain);
		bool isMeemoCaptainAlive(void) const;
		bool isGameLost(void) const;
		void incrementNumberMeemoMinionsAtFinishLine(void);
		void setDoor(Physical* door);
		Physical* getDoor(void) const;
		void setLever(Physical* lever);
		Physical* getLever(void) const;
		void setDoorOpened(bool doorOpened);
		bool isDoorOpened(void) const;
	};
}

#endif