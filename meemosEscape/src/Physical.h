#ifndef _PhysicalEntity_h
#define _PhysicalEntity_h

#include "cg/cg.h"
#include <list>
#include "EventCollision.h"
#include "Physics.h"

namespace avt {

	class Physical{

	public:
		virtual const cg::Vector3d getPosition(void){ return getPhysics()->getPosition(); }

		float getX(void){ return getPhysics()->getPosition()[0]; }

		float getY(void){ return getPhysics()->getPosition()[1]; }

		float getZ(void){ return getPhysics()->getPosition()[2]; }

		virtual const cg::Quaterniond getOrientation(void){ return getPhysics()->getOrientation();	}

	protected:
		virtual Physics* getPhysics(void) = 0;
	};

}

#endif