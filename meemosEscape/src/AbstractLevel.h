#ifndef _Abstract_avt_
#define _Abstract_avt_

#include <list>
#include "cg/cg.h"
#include "GameApplication.h"
#include "Camera.h"
#include "Scene.h"
#include "Light.h"
#include "PhysicsRegistry.h"
#include "DebugRegistry.h"
#include "EventNull.h"
#include "Map.h"

namespace avt{
	class GameApplication;

	class AbstractLevel{
	public:
		AbstractLevel(std::string id,GameApplication* application);
		~AbstractLevel();
		virtual void init();

	protected:
		std::string _id;
		GameApplication* _application;
		Scene* _activeScene;
		std::list<cg::Entity*> _entities;
		std::list<Camera*> _cameras;
		std::list<Camera*>::iterator _activeCamera;
		Map* _map;

	public:
		const std::string getId();
		Camera* getActiveCamera();
		Scene* getActiveScene();
		void setMap(Map* map);
		Map* getMap() const;

	protected:
		virtual void createCameras() = 0;
		virtual void createScene() = 0;
		virtual void createEntities() = 0;

		void destroyScene();
		void destroyEntities();
		void destroyCameras();
		void destroyPhysics();
		void destroySingletons();
		virtual void destroyAll();
		virtual void registerEntities() = 0;

	public:
		virtual void add(Camera* camera);
		virtual void add(Scene* scene);
		virtual void add(cg::Entity* entity);

		void switchCamera();
		void switchCamera(const std::string id);
	};
}

#endif