#ifndef __StatePhysicsMeemoCaptainFall_avt__
#define __StatePhysicsMeemoCaptainFall_avt__

#include "StatePhysicsMeemoCaptain.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt{
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptainFall: public StatePhysicsMeemoCaptain{	
	public:
		void step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds);
		void destroy();
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoCaptainFall_avt__
