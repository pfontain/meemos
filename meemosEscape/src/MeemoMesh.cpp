#include "MeemoMesh.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace avt{


	MeemoMesh::~MeemoMesh(){
		delete[] _points;
		delete[] _indices;
		delete[] _normals;
	}

	void MeemoMesh::init() {

		
		FILE * ase;
		char char_buffer[100];
		int int_buffer, vertex_position=0, face_positon=0, normal_position=0, aux=1;
		float nothing;


		errno_t erro = fopen_s(&ase,"meemo.ASE", "r");
		if(erro)
		  printf( "Modelo de Meemo Inv�lido\n" );

		while (aux!=EOF)
		{
			aux = fscanf_s(ase, "%s", char_buffer, 100);


			//read the number of vertices of the object
			if(!strcmp("*MESH_NUMVERTEX", char_buffer))
			{
				fscanf_s(ase, "%d", &int_buffer);
				number_vertices = int_buffer;

				_points = new cg::Vector3d[number_vertices];
			}

		//	//read the number of faces of the object
			if(!strcmp("*MESH_NUMFACES", char_buffer))
			{
				fscanf_s(ase, "%d", &int_buffer);
				number_faces = int_buffer;
				_indices = new GLuint[number_faces*3];
				_normals = new cg::Vector3d[number_faces*3];
			}

			//read a vertice of the object
			if(!strcmp("*MESH_VERTEX", char_buffer))
			{
				fscanf_s(ase, "%f %lf %lf %lf", &nothing, &_points[vertex_position][0], &_points[vertex_position][2], &_points[vertex_position][1]);
				_points[vertex_position][2]= -_points[vertex_position][2];
				++vertex_position;
			}

			//read a face of the object
			if(!strcmp("*MESH_FACE", char_buffer))
			{
				fscanf_s(ase, "%d:    A:    %d B:    %d C:    %d AB:    %d BC:    %d CA:    %d", &nothing, &_indices[face_positon], &_indices[face_positon+1], &_indices[face_positon+2], &nothing, &nothing, &nothing, &nothing);
				face_positon = face_positon+3;
			}
		

			//read the normals of a face
			if(!strcmp("*MESH_VERTEXNORMAL", char_buffer))
			{
				fscanf_s(ase, "%f %lf %lf %lf", &nothing, &_normals[normal_position][0], &_normals[normal_position][2], &_normals[normal_position][1]);
				_normals[normal_position][2] = -_normals[normal_position][2];
				++normal_position;
			}
		}
		
		fclose(ase);

		_position = cg::Vector3d(0.0,0.0,0.0);
		_color = cg::Vector3d(0.2,0.3,0.9);
	}
}