/** 
* EventCollisionWall.h - Concrete Event that represents a collision with a wall
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/06/2009 - File created.
*/
#ifndef _EventCollisionWall_h_
#define _EventCollisionWall_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionWall: public EventCollision
	{	
	public:
		EventCollisionWall(){}
		EventCollisionWall(meemo::Location location): EventCollision(location){}

	public:
		EventType::Type type(){
			return EventType::COLLISION_WALL;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionWall(_location);
			else
				return new EventCollisionWall();
		}

		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionWall";
			if(isLocationSet())
			{
				stringStream << "[" << _location << "]";
			}
			return stringStream.str();
		}
	};
}

#endif