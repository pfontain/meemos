#include "EmotionalReactionRuleMemoryRetrieval.h"

namespace meemo{
	EmotionalReactionRuleMemoryRetrieval::EmotionalReactionRuleMemoryRetrieval(appraisal::EmotionalReactionRule* emotionalReactionRule){

		appraisal::Event* retrievedEvent = emotionalReactionRule->getEvent()->copy();
		avt::EventMemoryRetrieval* eventMemoryRetrieval = new avt::EventMemoryRetrieval(retrievedEvent);
		EmotionalReactionRule::setEvent(eventMemoryRetrieval);

		appraisal::Reaction reaction = emotionalReactionRule->getReaction();
		EmotionalReactionRule::setReaction(reaction);
	}

	bool EmotionalReactionRuleMemoryRetrieval::match(appraisal::Event* event){
		return event->match(_event) > Event::NO_MATCH;
	}
}