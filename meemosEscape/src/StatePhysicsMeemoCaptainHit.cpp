#include "StatePhysicsMeemoCaptainHit.h"

namespace avt{
	void StatePhysicsMeemoCaptainHit::step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds){
		statePhysicsMeemoCaptainVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoCaptainHit::destroy(){}

	bool StatePhysicsMeemoCaptainHit::canInterrupt(){
		return false;
	}
}