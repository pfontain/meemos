#include "Eyes.h"

namespace avt{
	Eyes::Eyes(Physical* physical){
		_physical = physical;
		_relativePosition = cg::Properties::instance()->getVector3d("EYES_RELATIVE_POSITION");
		_radius = cg::Properties::instance()->getInt("EYES_RADIUS");
		_numberEdges = cg::Properties::instance()->getInt("EYES_NUMBER_EDGES");
		_material = new MaterialStatic();
	}

	Eyes::~Eyes(){
		delete _material;
	}

	void Eyes::init() {
		_material->setAmbient(0.9,0.9,0.9,1.0);
		_material->setDiffuse(0.9,0.9,0.9,1.0);
		_material->init();

		_state = EyesState::NEUTRAL;

		initTextures();
		makeModels();
	}

	void Eyes::initTextures(){
		std::string eyesFileName;

		eyesFileName = cg::Properties::instance()->getString("EYE_NEUTRAL_TEXTURE");
		_textureNeutral.open(eyesFileName);

		eyesFileName = cg::Properties::instance()->getString("EYE_JOY_TEXTURE");
		_textureJoy.open(eyesFileName);

		eyesFileName = cg::Properties::instance()->getString("EYE_SAD_LEFT_TEXTURE");
		_textureSadLeft.open(eyesFileName);

		eyesFileName = cg::Properties::instance()->getString("EYE_SAD_RIGHT_TEXTURE");
		_textureSadRight.open(eyesFileName);

		eyesFileName = cg::Properties::instance()->getString("EYE_ANGRY_LEFT_TEXTURE");
		_textureAngryLeft.open(eyesFileName);

		eyesFileName = cg::Properties::instance()->getString("EYE_ANGRY_RIGHT_TEXTURE");
		_textureAngryRight.open(eyesFileName);
	}

	void Eyes::makeModels(){
		makeModelNeutral();
		makeModelJoy();
		makeModelSad();
		makeModelAngry();
	}

	void Eyes::makeModelNeutral()
	{
		_modelDLNeutral = glGenLists(1);
		assert(_modelDLNeutral != 0);
		glNewList(_modelDLNeutral,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureNeutral.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,+_radius,0.0));
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,-_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),+_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),-_radius,0.0));
		}
		glEndList();
	}

	void Eyes::makeModelJoy()
	{
		_modelDLJoy = glGenLists(1);
		assert(_modelDLJoy != 0);
		glNewList(_modelDLJoy,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureJoy.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,+_radius,0.0));
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,-_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),+_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),-_radius,0.0));
		}
		glEndList();
	}

	void Eyes::makeModelSad()
	{
		_modelDLSad = glGenLists(1);
		assert(_modelDLSad != 0);
		glNewList(_modelDLSad,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureSadLeft.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,+_radius,0.0));
			glBindTexture(GL_TEXTURE_2D, _textureSadRight.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,-_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),+_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),-_radius,0.0));
		}
		glEndList();
	}

	void Eyes::makeModelAngry()
	{
		_modelDLAngry = glGenLists(1);
		assert(_modelDLAngry != 0);
		glNewList(_modelDLAngry,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureAngryLeft.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,+_radius,0.0));
			glBindTexture(GL_TEXTURE_2D, _textureAngryRight.id);
			makeModelCircle(_relativePosition + cg::Vector3d(0.0,-_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),+_radius,0.0));
			makeModelCircleNonTextured(_relativePosition + cg::Vector3d(-Utilities::Math::smallPositive(),-_radius,0.0));
		}
		glEndList();
	}

	void Eyes::makeModelCircle(const cg::Vector3d& position){
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);
		{
			double deltaRadians = 2*Utilities::Math::pi()/_numberEdges;
			cg::Vector2d textureCenter(0.5,0.5);
			double textureRadius = 0.5;

			glNormal3f(1,0,0);
			for(int iTriangle = 0; iTriangle < _numberEdges; iTriangle++){
				glTexCoord2d(textureCenter[0],textureCenter[1]);
				glVertex3f(position[0], position[1], position[2]);

				int iTriangleNext = iTriangle + 1;
				cg::Vector2d iPoint(_radius*cos(deltaRadians*iTriangle),_radius*sin(deltaRadians*iTriangle));
				cg::Vector2d iPointNext(_radius*cos(deltaRadians*iTriangleNext),_radius*sin(deltaRadians*iTriangleNext));

				cg::Vector2d iPointTextureCoord = textureCenter +
					cg::Vector2d(textureRadius * cos(deltaRadians*iTriangle), textureRadius * sin(deltaRadians*iTriangle));
				cg::Vector2d iPointNextTextureCoord = textureCenter +
					cg::Vector2d(textureRadius * cos(deltaRadians*iTriangleNext), textureRadius * sin(deltaRadians*iTriangleNext));

				glTexCoord2f(iPointTextureCoord[0],iPointTextureCoord[1]);
				glVertex3f(position[0] ,position[1] + iPoint[0],position[2]+ iPoint[1]);
				glTexCoord2f(iPointNextTextureCoord[0],iPointNextTextureCoord[1]);
				glVertex3f(position[0],position[1] + iPointNext[0],position[2] + iPointNext[1]);
			}
		}
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	void Eyes::makeModelCircleNonTextured(const cg::Vector3d& position){
		glBegin(GL_TRIANGLES);
		{
			double deltaRadians = 2*Utilities::Math::pi()/_numberEdges;
			
			glNormal3f(-1,0,0);
			for(int iTriangle = 0; iTriangle < _numberEdges; iTriangle++){
				glVertex3f(position[0], position[1], position[2]);

				int iTriangleNext = iTriangle + 1;
				cg::Vector2d iPoint(_radius*cos(deltaRadians*iTriangle),_radius*sin(deltaRadians*iTriangle));
				cg::Vector2d iPointNext(_radius*cos(deltaRadians*iTriangleNext),_radius*sin(deltaRadians*iTriangleNext));

				glVertex3f(position[0] ,position[1] + iPointNext[0],position[2] + iPointNext[1]);
				glVertex3f(position[0] ,position[1] + iPoint[0],position[2]+ iPoint[1]);
			}
		}
		glEnd();
	}

	void Eyes::draw() {
		glPushMatrix();
		{
			_material->draw();
			switch(_state){
				case EyesState::JOY : glCallList(_modelDLJoy); break;
				case EyesState::SAD : glCallList(_modelDLSad); break;
				case EyesState::ANGRY : glCallList(_modelDLAngry); break;
				default: glCallList(_modelDLNeutral);
			}
		}
		glPopMatrix();
	}

	void Eyes::neutralOn(){
		_state = EyesState::NEUTRAL;
	}

	void Eyes::joyOn(){
		_state = EyesState::JOY;
	}

	void Eyes::sadOn(){
		_state = EyesState::SAD;
	}

	void Eyes::angryOn(){
		_state = EyesState::ANGRY;
	}

}