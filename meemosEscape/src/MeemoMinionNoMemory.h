#ifndef _MeemoMinionNoMemory_h_
#define _MeemoMinionNoMemory_h_

#include "MeemoMinion.h"
#include "MovePathAgent.h"
#include "BehaviourMeemoMinionNoMemory.h"
#include "BehaviourMeemoMinionNoMemoryRandomized.h"
#include "MindNoMemory.h"

namespace avt{
	class MeemoMinionNoMemory: public MeemoMinion, public meemo::MovePathAgent{
	public:
		MeemoMinionNoMemory(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic);
		meemo::BehaviourMeemoMinionNoMemory* createBehaviour(AbstractLevel* level,AbstractGameLogic* gameLogic);
		~MeemoMinionNoMemory();
		void init();

	private:
		meemo::BehaviourMeemoMinionNoMemory* _behaviourMeemoMinionNoMemory;
		meemo::MindNoMemory* _mindNoMemory;
	
	public:
		bool movePath(std::list<meemo::Location*>* locations);
		avt::Path* updatePath(avt::Path* path);
	};
}

#endif // _MeemoMinionNoMemory_h_