#ifndef _BehaviourMeemoMinion_h_
#define _BehaviourMeemoMinion_h_

#include "Behaviour.h"
#include "MovePathAgent.h"
#include "Map.h"
#include "PathPlan.h"
#include "Location.h"
#include "Utilities.h"
#include "MemoryStorage.h"
#include "AbstractLevel.h"
#include "AbstractMeemo.h"
#include "AbstractGameLogic.h"
#include <map>

namespace meemo{
	class BehaviourMeemoMinion: public Behaviour{
	public:
		BehaviourMeemoMinion(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgent* meemoMinionMovePathAgent,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic);

	private:
		MovePathAgent* _meemoMinion;
		std::map<char,int> _mappingToInteger;
		avt::Map* _map;
		std::set<avt::AbstractMeemo*>* _meemoMinionsAlive;
		int _unitaryLength;

	public:
		bool movePathPlanned(const cg::Vector3d& moveToPosition);

	protected:
		bool planPath(std::pair<int,int>& startPosition,std::pair<int,int>& moveToPosition,std::list<meemo::Location*>* outputPathLocations);
		virtual void updateIntegerMap(int* integerMap,std::pair<int,int>& moveToPosition) = 0;
		void updateIntegerMapAccordingToEvent(int* integerMap,appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion);
		void updateIntegerMapAccordingToPositiveEvent(int* integerMap,int locationInIntegerMap,float causedEmotionIntensity);
		void updateIntegerMapAccordingToNegativeEvent(int* integerMap,int locationInIntegerMap,float causedEmotionIntensity);
		void updateIntegerMapAccordingToOtherMeemos(int* integerMap);
		void clearPositionIntegerMap(int* integerMap,std::pair<int,int>& position);

		bool planPathDiscretized(int* integerMap,std::pair<int,int>& startPosition,std::pair<int,int>& moveToPosition,std::vector<aStar::Node*>* pathNodes);
		std::vector<aStar::Node*>* createSmoothPath(int* integerMap,std::vector<aStar::Node*>* inputPath);
		void pathReverseDiscretization(std::vector<aStar::Node*>* pathNodes,std::list<meemo::Location*>* outputPathLocations);		
		bool rayClear(int* integerMap,aStar::Node* nodeA,aStar::Node* nodeB);
		void recoverUndescrizedStartAndMoveToPositions(std::list<meemo::Location*>* pathLocations,const cg::Vector3d& startPosition,const cg::Vector3d& moveToPosition);

		std::pair<int,int> BehaviourMeemoMinion::discretize(const cg::Vector3d& position);
		std::pair<int,int> BehaviourMeemoMinion::discretize(const meemo::Location& location);
	};
}

#endif // _BehaviourMeemoMinion_h_