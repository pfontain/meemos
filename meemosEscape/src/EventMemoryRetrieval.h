#ifndef _EventMemoryRetrieval_h_
#define _EventMemoryRetrieval_h_

#include "Event.h"

namespace avt{

	class EventMemoryRetrieval: public Event
	{	
	public:
		EventMemoryRetrieval(appraisal::Event* retrievedEvent);
		EventMemoryRetrieval(appraisal::Event* retrievedEvent, meemo::Location location);
		~EventMemoryRetrieval();

	protected:
		appraisal::Event* _retrievedEvent;

	public:
		EventType::Type getRetrievedEventType(void) const;
		appraisal::Event* getRetrievedEvent(void);
		const std::string toString();
		EventType::Type type();
		avt::Event* copy();

		virtual bool equal(appraisal::Event* event);
		virtual int match(appraisal::Event* event);

	protected:
		void destroy();
	};
}

#endif //_EventMemoryRetrieval_h_