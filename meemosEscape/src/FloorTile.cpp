#include "FloorTile.h"

namespace avt{
	FloorTile::FloorTile(int x, int y,int size,const std::string& id): cg::Entity(id){
		_size = size;
		_physics = new PhysicsDefault();
		_physics->setPosition(x,y,0.0);
		_material = new MaterialStatic();
	}

	FloorTile::~FloorTile(){
		delete _physics;
		delete _material;
	}

	void FloorTile::init() {
		cg::Vector3d floorColour = cg::Properties::instance()->getVector3d("FLOOR_COLOUR");
		_material->setAmbient(floorColour[0],floorColour[1],floorColour[2],1.0);
		_material->setDiffuse(floorColour[0],floorColour[1],floorColour[2],1.0);
		_material->init();

		makeModel();
	}

	void FloorTile::draw() {
		glPushMatrix();
		{
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
	}

	void FloorTile::makeModel() {
		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			makeTile();
		}
		glEndList();
	}

	void FloorTile::makeTile(){
		glBegin(GL_QUADS);
		{
			int halfSize = _size / 2;
			glNormal3f(0,0,1);
			glVertex3f(getX()-halfSize,getY()-halfSize,0);
			glVertex3f(getX()+halfSize,getY()-halfSize,0);
			glVertex3f(getX()+halfSize,getY()+halfSize,0);
			glVertex3f(getX()-halfSize,getY()+halfSize,0);
		}
		glEnd();
	}

	Physics* FloorTile::getPhysics(void){
		return _physics;
	}
}