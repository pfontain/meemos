#include "EventCollision.h"

namespace avt{
	EventCollision::EventCollision(){
		_detectable = true;
	}

	EventCollision::EventCollision(meemo::Location location): Event(location){
		_detectable = true;
	}

	bool EventCollision::equal(appraisal::Event* event){
		if(this->type() != event->type()){
			return false;
		}
		else if(this->_locationSet && !event->isLocationSet()){
			return false;
		}
		else if(!this->_locationSet && event->isLocationSet()){
			return false;
		}
		else if(this->_locationSet){
			meemo::Location eventLocation = event->getLocation();
			float squaredDistance = _location.squaredDistance(eventLocation);
			if(Utilities::Math::isZero(squaredDistance))
				return true;
			else
				return false;
		}
		else{ // this->_locationSet == false
			return true;
		}
	}

	int EventCollision::match(appraisal::Event* event){
		if(this->type() != event->type()){
			return NO_MATCH;
		}
		else if(this->_locationSet && !event->isLocationSet()){
			return HALF_MATCH;
		}
		else if(!this->_locationSet && event->isLocationSet()){
			return HALF_MATCH;
		}
		else if(this->_locationSet){
			meemo::Location eventLocation = event->getLocation();
			float squaredDistance = _location.squaredDistance(eventLocation);
			if(Utilities::Math::isZero(squaredDistance))
				return TOTAL_MATCH;
			else
				return OVER_HALF_MATCH;
		}
		else{ // this->_locationSet == false
			return TOTAL_MATCH;
		}
	}

	bool EventCollision::isDetectable(){
		return _detectable;
	}
}