#include "MeemoMinionNoMemory.h"

namespace avt{
	MeemoMinionNoMemory::MeemoMinionNoMemory(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic): MeemoMinion(x,y,size,idNumber,id,level,gameLogic){
		_behaviourMeemoMinionNoMemory = createBehaviour(level,gameLogic);
		_behaviourMeemoMinion = _behaviourMeemoMinionNoMemory;
		_behaviour = _behaviourMeemoMinionNoMemory;

		_mindNoMemory = new meemo::MindNoMemory(this,this);
		_mind = _mindNoMemory;
	}

	meemo::BehaviourMeemoMinionNoMemory* MeemoMinionNoMemory::createBehaviour(AbstractLevel* level,AbstractGameLogic* gameLogic){
		std::string meemoNoMemoryType = cg::Properties::instance()->getString("MEEMO_NO_MEMORY_TYPE");
		
		if(meemoNoMemoryType.compare("RANDOMIZED")==0)
			return new meemo::BehaviourMeemoMinionNoMemoryRandomized(this,this,level,gameLogic);
		else
			return new meemo::BehaviourMeemoMinionNoMemory(this,this,level,gameLogic);
	}

	MeemoMinionNoMemory::~MeemoMinionNoMemory(){
		delete _behaviourMeemoMinionNoMemory;
		delete _mindNoMemory;
	}

	void MeemoMinionNoMemory::init() {
		MeemoMinion::init();
	}

	bool MeemoMinionNoMemory::movePath(std::list<meemo::Location*>* locations){
		return MeemoMinion::movePath(locations);
	}

	avt::Path* MeemoMinionNoMemory::updatePath(avt::Path* path){
		return path;
	}
}