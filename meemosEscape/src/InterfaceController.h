#ifndef _InterfaceController_h_
#define _InterfaceController_h_

#include "cg/cg.h"
#include "time.h"
#include "Meemo.h"
#include "Camera.h"
#include "Console.h"
#include "MeemoMinion.h"
#include "AbstractGameLogic.h"
#include "Lever.h"
#include "Door.h"
#include <sstream>

namespace avt{

	class InterfaceController: 
		public cg::Entity,
		public cg::IKeyboardEventListener,
		public cg::IDrawOverlayListener
	{
	public:
		InterfaceController(AbstractGameLogic* gameLogic);
		void init();

		MeemoCaptain* _meemoCaptain;
		AbstractGameLogic* _gameLogic;

		time_t _initialTime;
		time_t _elapsedTime;

	private:
		void onKeyPressed(unsigned char key);
		void onKeyReleased(unsigned char key);
		void onSpecialKeyPressed(int key);
		void onSpecialKeyReleased(int key);
		void drawOverlay();
	};
}

#endif