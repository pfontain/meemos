#include "Camera.h"

namespace avt{
	Camera::Camera(const std::string& id): Entity(id) {
		_fieldOfViewAngle = 50;
		_frustrumNear = 1;
		_frustrumFar = 3000;
	}

	Camera::~Camera() {}

	const cg::Vector3d Camera::getFrontDirection() const{
		return _physics.getFrontDirection();
	}

	void Camera::onReshape(int width, int height){
		_winWidth = width;
		_winHeight = Utilities::Math::isZero(height) ? 1.0 : height;
		_winRatio = _winWidth / _winHeight;
		glViewport(0.0,0.0,_winWidth,_winHeight);
	}

	const void Camera::printDescription() const{
		cg::Vector3d position = _physics.getPosition();
		cg::Vector3d frontDirection = _physics.getFrontDirection();
		cg::Vector3d leftDirection = _physics.getLeftDirection();
		cg::Vector3d upDirection = _physics.getUpDirection();

		printf("CAMERA1_POSITION = %f %f %f\n",position[0],position[1],position[2]);
		printf("CAMERA1_FRONT_DIRECTION = %f %f %f\n",frontDirection[0],frontDirection[1],frontDirection[2]);
		printf("CAMERA1_LEFT_DIRECTION = %f %f %f\n",leftDirection[0],leftDirection[1],leftDirection[2]);
		printf("CAMERA1_UP_DIRECTION = %f %f %f\n",upDirection[0],upDirection[1],upDirection[2]);
	}

	void Camera::initWindowViewport(){
		cg::tWindow win = cg::Manager::instance()->getApp()->getWindow();
		_winWidth = win.width;
		_winHeight = Utilities::Math::isZero(win.height) ? 1.0 : win.height;
		_winRatio = _winWidth / _winHeight;

		glViewport(0.0,0.0,_winWidth,_winHeight);
	}

	void Camera::setProjection(){
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(_fieldOfViewAngle,_winRatio,_frustrumNear,_frustrumFar);
		glMatrixMode(GL_MODELVIEW);
	}

	Physics* Camera::getPhysics(void){
		return &_physics;
	}
}