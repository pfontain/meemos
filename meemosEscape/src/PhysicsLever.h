#ifndef _PhysicsLever_avt_
#define _PhysicsLever_avt_

#include "Physics.h"
#include "EventCollisionDefault.h"

namespace avt{
	namespace PhysicsLeverState{
		enum State{UP, DESCENDING, DOWN};
	}

	class PhysicsLever: public Physics{
	public:
		PhysicsLever(void);

	private:
		PhysicsLeverState::State _state;
		float _upAngle;

	public:
		void setUpAngle(float angle);
		void descend(void);

		EventCollision* getCollisionEvent(void);
		bool isColliding(Physics* collider);
		void step(double elapsedSeconds);
		void applyTransforms(void);
		void applyTranslation(void);

	private:		
		void stepDescending(double elapsedSeconds);
	};
}

#endif