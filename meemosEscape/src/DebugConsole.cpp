#include "DebugConsole.h"

namespace avt
{
	DebugConsole::DebugConsole( const std::string& id ): cg::Console(id){
		state.disable();
	}

	void DebugConsole::update(unsigned long elapsed_millis){
		std::stringstream stringStream;

		avt::Camera* camera = dynamic_cast<avt::Camera*>(cg::Registry::instance()->get("CameraLevers"));
		cg::Vector3d cameraPosition = camera->getPosition();
		cg::Vector3d cameraFrontDirection = camera->getFrontDirection();
		stringStream << "CameraLevers(pos): "
			<< '(' << cameraPosition[0] << ')' 
			<< '(' << cameraPosition[1] << ')'
			<< '(' << cameraPosition[2] << ')'  << std::endl;

		//avt::Light* light = dynamic_cast<avt::Light*>(cg::Registry::instance()->get("Light"));
		//cg::Vector3d lightPosition = light->getPosition();
		//cg::Quaterniond lightQuaterniond = light->getOrientation();
		//stringStream << "Light(pos): "
		//	<< '(' << lightPosition[0] << ')' 
		//	<< '(' << lightPosition[1] << ')'
		//	<< '(' << lightPosition[2] << ')' << std::endl;

		clearConsoleBuffer();
		write(stringStream.str());
	}

	void DebugConsole::drawOverlay(){
		Console::drawOverlay();
	}

	void DebugConsole::displayText( void )
	{
		std::list<std::string>::reverse_iterator itr;
		cg::Vector2d cursorPoint;
		double drawingLimitY;
		int dy = +15;
		int i = 0;

		glColor4d(mColor[0], mColor[1], mColor[2], mAlpha);
		cursorPoint = cg::Vector2d(mBodyP1[0] + LEFT_MARGIN, mBodyP1[1] - UPPER_MARGIN);
		drawingLimitY = mBodyP2[1] + LOWER_MARGIN;
		
		for (itr = mBuffer.rbegin(); itr != mBuffer.rend() && cursorPoint[1] > drawingLimitY; itr++, i++)
		{
			if (i < mStartLine)
				continue;

			glRasterPos2d(cursorPoint[0], cursorPoint[1]);

			for (unsigned int j = 0; j < (*itr).length() && cursorPoint[1] > drawingLimitY; j++){
				if(!continueWriting()){
					cursorPoint[1] -= dy;
					glRasterPos2d(cursorPoint[0], cursorPoint[1]);
				}
				glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, (*itr)[j]);
			}

			cursorPoint[1] -= dy;
		}

		(itr == mBuffer.rend() && cursorPoint[1] > drawingLimitY) ? mScrollDown = false : mScrollDown = true;
		mStartLine == 0 ? mScrollUp = false : mScrollUp = true;
	}
}