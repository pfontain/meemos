#include "StatePhysicsMeemoMinionPullLever.h"

namespace avt{
	void StatePhysicsMeemoMinionPullLever::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		statePhysicsMeemoMinionVisitor->start(this);
	}

	void StatePhysicsMeemoMinionPullLever::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionPullLever::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){}

	bool StatePhysicsMeemoMinionPullLever::canInterrupt(){
		return false;
	}
}