/** 
* EventWitness.h - Concrete Event that represents witnissing an event
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/06/2009 - File created.
*/
#ifndef _EventWitness_h_
#define _EventWitness_h_

#include "Event.h"

namespace avt{
	class EventWitness: public Event
	{	
	public:
		EventWitness(Event* witnessedEvent);
		~EventWitness();

	private:
		Event* _witnessedEvent;

	public:
		Event* getWitnessedEvent();
		EventType::Type type();
		Event* copy();
		const std::string toString();
		EventType::Type getWitnessedEventType();
		bool equal(appraisal::Event* event);
		int match(appraisal::Event* event);

	protected:
		void destroy();
	};
}

#endif