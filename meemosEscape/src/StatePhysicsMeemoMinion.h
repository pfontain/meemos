#ifndef _StatePhysicsMeemoMinion_avt_
#define _StatePhysicsMeemoMinion_avt_

namespace avt{
	// Visitor Pattern
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinion{	
	public:
		StatePhysicsMeemoMinion(void){ _restartable = false; }

	protected:
		bool _restartable;

	public:
		virtual void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion) = 0;
		virtual void step(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion,double elapsedSeconds) = 0;
		virtual void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion) = 0;
		virtual bool canInterrupt(void) = 0;
		bool isRestartable(void) { return _restartable; }
	};
}

#endif // _StatePhysicsMeemoMinion_avt_