#ifndef _FloorTile_avt_
#define _FloorTile_avt_

#include <utility>
#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Texture.h"
#include "Physical.h"
#include "PhysicsDefault.h"

namespace avt{

	class FloorTile : public cg::Entity,
		public cg::IDrawListener,
		Physical
	{

	public:
		FloorTile(int x, int y,int unitaryDistance,const std::string& id);
		~FloorTile();
		void init();
		void draw();

	private:
		int _modelDL;
		Material* _material;
		Physics* _physics;
		int _size;

	private:
		void makeModel();
		void makeTile();
		Physics* getPhysics(void);
	};
}


#endif