#ifndef _PhysicsTrapTrigger_avt_
#define _PhysicsTrapTrigger_avt_

#include "Physics.h"
#include "EventCollisionTrapTrigger.h"

namespace avt{
	class PhysicsTrapTrigger: public Physics{
	public:
		EventCollision* getCollisionEvent(void);
		bool isColliding(Physics* collider);
		std::list<EventCollision*>* getCollisions(void);
	};
}

#endif