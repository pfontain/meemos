#ifndef _Physics_h
#define _Physics_h

#include "cg/cg.h"
#include <list>
#include "EventCollision.h"
#include "EventCollisionDefault.h"
#include "PhysicsRegistry.h"
#include "PhysicsParameters.h"
#include "Utilities.h"

namespace avt {

	class Physics {

	public:
		Physics();
		~Physics();

	public:
		cg::Vector3d _position;
		cg::Vector3d _front, _up, _right;

		double _linearVelocity;
		double _angularVelocity;

		cg::Quaterniond _orientation;
		cg::Quaterniond _q;
		double _rotationMatrix[16];

		bool _isGoAhead, _isGoBack;
		bool _isGoLeft, _isGoRight;
		bool _isYawLeft, _isYawRight;
		bool _isPitchUp, _isPitchDown;
		bool _isRollLeft, _isRollRight;
		virtual void rotate(double elapsedSeconds,int direction, cg::Vector3d axis, cg::Vector3d& v1, cg::Vector3d& v2);
		void pitchDown(float angle);
		void yawLeft(float angle);

		double _axesScale;
		double _boundingSphereRadius;

	public:
		const cg::Vector3d getPosition(void) const;
		const cg::Vector3d getFrontDirection(void) const;
		const cg::Vector3d getLeftDirection(void) const;
		const cg::Vector3d getUpDirection(void) const;
		const cg::Quaterniond getOrientation(void) const;
		double getBoundingSphereRadius(void) const;

		void goAhead();
		void goBack();
		void goLeft();
		void goRight();
		void yawLeft();
		void yawRight();
		void pitchUp();
		void pitchDown();
		void rollLeft();
		void rollRight();

		void setPosition(double x, double y, double z);
		void setLinearVelocity(double value);
		void setAngularVelocity(double value);
		void setBoundingSphereRadius(double radius);

		virtual void step(double elapsedSeconds);
		virtual void applyTransforms(void);

		void setAxesScale(double scale);
		void drawAxes();

		virtual EventCollision* getCollisionEvent(void) = 0;
		virtual bool isColliding(Physics* physics) = 0;

	protected:
		virtual std::list<EventCollision*>* getCollisions(void);

	public:
		static bool defaultIsColliding(Physics* physicsA,Physics* physicsB);
		static float squaredLength(cg::Vector3d& vector);
		static float isFloatZero(float f);
	};

}

#endif