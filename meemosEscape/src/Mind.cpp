#include "Mind.h"

namespace meemo{
	Mind::Mind(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent){
		_emotionalState = NULL;
		_reactiveProcess = NULL;
		_physicalAgent = physicalAgent;
		_emotionalAgent = emotionalAgent;
		_dead = false;
	}

	Mind::~Mind(){
		if(_emotionalState != NULL)
			delete _emotionalState;
	}

	void Mind::init(){
		initModules();
		initEmotionDispositions();
		initEmotionalReactionRules();
	}

	appraisal::EmotionalState* Mind::getEmotionalState(){
		return _emotionalState;
	}

	meemo::EmotionalAgent* Mind::getAgent(){
		return _emotionalAgent;
	}

	void Mind::setDead(bool deadValue){
		_dead = deadValue;
	}

	bool Mind::isDead(void) const{
		return _dead;
	}

	void Mind::update(){
		updateEmotionalState();
		updateAppraisal();
	}

	void Mind::onEvent(avt::Event* event){
		_reactiveProcess->addEvent(event);
	}

	void Mind::initModules(){
		_emotionalState = new appraisal::EmotionalState();
	}

	void Mind::initEmotionDispositions(){
		appraisal::EmotionType::Type emotionType;
		int threshold;
		int decay;
		appraisal::EmotionDisposition* emotionDisposition;

		emotionType = appraisal::EmotionType::JOY;
		threshold = 2;
		decay = 5;
		emotionDisposition = new appraisal::EmotionDisposition(emotionType,threshold,decay);
		_emotionalState->addEmotionDisposition(emotionDisposition);

		emotionType = appraisal::EmotionType::DISTRESS;
		threshold = 2;
		decay = 4;
		emotionDisposition = new appraisal::EmotionDisposition(emotionType,threshold,decay);
		_emotionalState->addEmotionDisposition(emotionDisposition);

		emotionType = appraisal::EmotionType::PITY;
		threshold = 2;
		decay = 6;
		emotionDisposition = new appraisal::EmotionDisposition(emotionType,threshold,decay);
		_emotionalState->addEmotionDisposition(emotionDisposition);
	}

	void Mind::initEmotionalReactionRules(){
		appraisal::Reaction* reaction;
		appraisal::EmotionalReactionRule* emotionalReactionRule;
		avt::Event* eventMain;
		avt::Event* eventWitnessed;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-7);
		eventMain = new avt::EventCollisionHole();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-7);
		eventMain = new avt::EventCollisionTrapDoor();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-4);
		eventMain = new avt::EventCollisionAnvil();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(6);
		eventMain = new avt::EventCollisionFinishLine();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(3);
		eventMain = new avt::EventSeeDoor();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(3);
		eventMain = new avt::EventSeeLever();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(6);
		eventMain = new avt::EventSeeOpenDoor();
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-2);
		reaction->setDesirabilityForOther(-4);
		eventWitnessed = new avt::EventCollisionAnvil();
		eventMain = new avt::EventWitness(eventWitnessed);
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-4);
		reaction->setDesirabilityForOther(-6);
		eventWitnessed = new avt::EventCollisionHole();
		eventMain = new avt::EventWitness(eventWitnessed);
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;

		reaction = new appraisal::Reaction();
		reaction->setDesirability(-4);
		reaction->setDesirabilityForOther(-6);
		eventWitnessed = new avt::EventCollisionTrapDoor();
		eventMain = new avt::EventWitness(eventWitnessed);
		emotionalReactionRule = new appraisal::EmotionalReactionRuleDefault(eventMain,*reaction);
		_reactiveProcess->addEmotionalReactionRule(emotionalReactionRule);
		delete reaction;
	}

	void Mind::updateEmotionalState(){
		_emotionalState->decay();
	}
}