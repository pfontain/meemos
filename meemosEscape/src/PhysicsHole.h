#ifndef _PhysicsHole_h
#define _PhysicsHole_h

#include "Physics.h"
#include "EventCollisionHole.h"

namespace avt {

	class PhysicsHole : public Physics {

	public:
		EventCollision* getCollisionEvent();
		bool isColliding(Physics* collider);
	};
}

#endif