#ifndef _EventNotifier_h_
#define _EventNotifier_h_

#include <list>
#include <utility>
#include "cg/cg.h"
#include "EventSubscriber.h"

namespace avt{
	class EventNotifier
	{	
	public:
		EventNotifier::~EventNotifier();

		protected:
			std::list<EventSubscriber*> _eventSubscribers;
			std::list<std::pair<Event*,short>*> _activeEvents;

		public:
			virtual void registerSubscriber(EventSubscriber* eventSubscriber);
			virtual void unregisterSubscriber(EventSubscriber* eventSubscriber);
			virtual std::pair<Event*,short>* searchActivePairEvent(Event* event);
			virtual bool notify(Event* event);

		protected:
			virtual void step();
	};
}

#endif