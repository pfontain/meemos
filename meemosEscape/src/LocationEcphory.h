#ifndef _LocationEcphory_h_
#define _LocationEcphory_h_

#include "MemoryStorage.h"
#include "EventNotifier.h"
#include "EventMemoryRetrieval.h"
#include "Physical.h"
#include "MemoryParameters.h"
#include "EmotionalState.h"
#include <cmath>
#include <list>

namespace meemo{
	class LocationEcphory: public avt::EventNotifier {
	public:
		LocationEcphory(MemoryStorage* memoryStorage,avt::Physical* physicalAgent);

	private:
		MemoryStorage* _memoryStorage;
		avt::Physical* _physicalAgent;	
		int _shortTermMemoryDuration;

	public:
		void generateMemoryRetrievalEvents();

	private:
		bool ecphoricMatch(MemoryTrace* memoryTrace);
		bool isEventInShortTermMemory(MemoryTrace* memoryTrace);
	};
}

#endif //_LocationEcphory_h_