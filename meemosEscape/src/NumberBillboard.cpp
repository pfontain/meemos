#include "NumberBillboard.h"

namespace avt{
	NumberBillboard::NumberBillboard(Physical* physical,unsigned int number){
		_physical = physical;
		_material = new MaterialStatic();
		_physics = new PhysicsDefault();
		_number = number;
		_sideSize = cg::Properties::instance()->getDouble("NUMBER_BILLBOARD_SIDE_SIZE");
		_relativePosition = cg::Properties::instance()->getVector2d("NUMBER_BILLBOARD_RELATIVE_POSITION");
	}

	NumberBillboard::~NumberBillboard(){
		delete _material;
		delete _physics;
	}

	void NumberBillboard::init() {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("NUMBER_BILLBOARD_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->init();

		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		std::stringstream textureFileNameStream;
		textureFileNameStream << cg::Properties::instance()->getString("NUMBER_BILLBOARD_TEXTURE_NAME");
		textureFileNameStream << _number << ".";
		textureFileNameStream << cg::Properties::instance()->getString("NUMBER_BILLBOARD_TEXTURE_EXTENSION");
		std::string textureFileName = textureFileNameStream.str();
		_texture.open(textureFileName);

		makeModel();
	}

	void NumberBillboard::update(const cg::Vector3d& observerPosition){
		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		cg::Vector3d observerDirection;
		observerDirection[0]= observerPosition[0] - physicalPosition[0];
		observerDirection[1]= observerPosition[1] - physicalPosition[1];
		observerDirection[2]= 0.0;
		cg::Vector3d observerDirectionNormalized(normalize(observerDirection));

		_physics->rotate(observerDirectionNormalized);
		_physics->step();
	}

	void NumberBillboard::draw(){
		glPushMatrix();
		{
			_physics->applyTransforms();
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
	}

	void NumberBillboard::makeModel(){
		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{	
			GLfloat lightingAmbientArray[4];
			GLfloat* lightingAmbient = &(lightingAmbientArray[0]);
			glGetFloatv(GL_LIGHT_MODEL_AMBIENT,lightingAmbient);
			GLfloat lightingAmbientNumberBillboard[] = {1,1,1,1.0};
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbientNumberBillboard);

			drawBillboard();

			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbient);
		}
		glEndList();		
	}

	void NumberBillboard::drawBillboard(void){
		float halfSideSize = _sideSize/2;
		double z = _relativePosition[1];
		double y = _relativePosition[0];

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, _texture.id);
		glBegin(GL_QUADS);
		{
			glNormal3f(1,0,0);
			glTexCoord2f(0,0);
			glVertex3f(-Utilities::Math::smallPositive(),-halfSideSize+y,z-halfSideSize);
			glTexCoord2f(1,0);
			glVertex3f(-Utilities::Math::smallPositive(),+halfSideSize+y,z-halfSideSize);
			glTexCoord2f(1,1);
			glVertex3f(-Utilities::Math::smallPositive(),+halfSideSize+y,z+halfSideSize);
			glTexCoord2f(0,1);
			glVertex3f(-Utilities::Math::smallPositive(),-halfSideSize+y,z+halfSideSize);	
		}
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}
}