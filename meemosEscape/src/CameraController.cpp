#include "CameraController.h"

namespace avt{

	CameraController::CameraController(AbstractLevel* level): cg::Entity("CameraController"){
		_level = level;
	}

	CameraController::~CameraController(){}

	void CameraController::init(){
	}

	void CameraController::onKeyPressed(unsigned char key){
		if(key == 'c'){
			_level->switchCamera();
		}
	}
	void CameraController::onKeyReleased(unsigned char key){}
	void CameraController::onSpecialKeyPressed(int key){}
	void CameraController::onSpecialKeyReleased(int key){}
}