#include "Anvil.h"

namespace avt{
	Anvil::Anvil(int x, int y,float size,const std::string& id): cg::Entity(id){
		_size = size;

		cg::Properties* properties = cg::Properties::instance();
		_physics = new PhysicsAnvil();
		double dropHeight = properties->getDouble("ANVIL_DROP_HEIGHT");
		_physics->setPosition(x,y,dropHeight);
		_physics->setTriggerPosition(cg::Vector3d(x,y,0));
		_physics->setDropHeight(dropHeight);
		_physics->setTriggerBoundingSphereRadius(properties->getDouble("TRIGGER_BOUNDING_SPHERE_RADIUS"));
		_physics->setDropLinearVelocity(properties->getDouble("ANVIL_DROP_LINEAR_VELOCITY"));
		_physics->setRiseLinearVelocity(properties->getDouble("ANVIL_RISE_LINEAR_VELOCITY"));
		_physics->setWaitTime(properties->getDouble("ANVIL_WAIT_TIME"));

		_material = new MaterialStatic();
	}

	Anvil::~Anvil(){
		delete _physics;
		delete _material;
	}

	void Anvil::init() {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("ANVIL_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->setDiffuse(colour[0],colour[1],colour[2],1.0);
		_material->init();

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);

		makeModel();
		DebugRegistry::getInstance()->add(this);
	}

	void Anvil::draw() {
		PhysicsAnvilState::State state = _physics->getState();
		if(state == PhysicsAnvilState::FALL || state == PhysicsAnvilState::WAIT){
			glPushMatrix();
			{
				_physics->applyTransforms();
				_material->draw();
				glCallList(_modelDL);
			}
		}
		glPopMatrix();
		drawDebug();
	}

	void Anvil::update(unsigned long elapsed_millis){
		double elapsed_seconds = elapsed_millis / (double)1000;
		_physics->step(elapsed_seconds);
	}

	void Anvil::makeModel()
	{
		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			glutSolidSphere(_size/2.0,5,5);
		}
		glEndList();
	}

	Physics* Anvil::getPhysics(void){
		return _physics;
	}
}