/** 
* EventSeeLever.h
*  
* Copyright (C) 2010 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 06/02/2010
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
*/
#ifndef _EventSeeLever_h_
#define _EventSeeLever_h_

#include "Event.h"

namespace avt{

	class EventSeeLever: public Event
	{	
	public:
		EventSeeLever(){}
		EventSeeLever(meemo::Location location): Event(location){}

		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventSeeLever";
			if(isLocationSet())
				stringStream << "[" << _location << "]";
			return stringStream.str();
		}

		Event* copy(){
			if(isLocationSet())
				return new EventSeeLever(_location);
			else
				return new EventSeeLever();
		}

		EventType::Type type(){
			return EventType::SEE_LEVER;
		}

		virtual bool equal(appraisal::Event* event){ return Event::defaultEqual(event); }

		virtual int match(appraisal::Event* event){ return Event::defaultMatch(event); }

	protected:
		void destroy(){};
	};
}

#endif