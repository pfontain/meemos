#ifndef CG_CONSOLE_H
#define CG_CONSOLE_H

#include <list>
#include "cg/cg.h"

#define TITLEBAR_HEIGHT 20
#define TITLE_LEFT_MARGIN 5
#define TITLE_BOTTOM_MARGIN 5

#define OUTSIDE_BORDER_SPACE 5
#define LEFT_MARGIN 5
#define RIGHT_MARGIN 20
#define UPPER_MARGIN 15
#define LOWER_MARGIN 5

#define RESIZE_LINE_SIZE 10
#define WIDGET_UPPER_BORDER 5
#define WIDGET_LOWER_BORDER 10
#define WIDGET_SIDE_BORDER 1

#define MIN_XSIZE 30
#define MIN_YSIZE TITLEBAR_HEIGHT + 40
#define WIDGET_ARROW_XSIZE 10
#define WIDGET_ARROW_YSIZE 10

namespace cg
{

    /** cg::Console is a grafical console that can display strings as an overlay.
        It also let the user move and/or resize it.
     */
  class Console : public cg::Entity, public cg::IDrawOverlayListener,
                  public cg::IMouseEventListener, public cg::IReshapeEventListener
    {
    private:
        std::string mTitle;
        cg::Vector2d mPosition, mSize, mMousePos;
        bool mMoving, mResizing, mMinimized;
        bool mShowTitle, mCanMove, mCanResize, mCanMinimize, mCanScroll; 
        int mWinWidth, mWinHeight;
        static GLuint msScrollWidgetDL, msResizeWidgetDL;

        void moveToRelativePos( int dx, int dy );
        void resizeToRelativeSize( int dx, int dy );
        void drawTitleBar( void );
        void drawBody( void );
        void drawResizeWidget( void );
        void drawUpDownWidgets( void );
        void refreshDrawingData( void );
        
        /** Check if (x,y) is in between 'p1' (upper left corner) and 'p2' (lower right corner). */
        bool isInArea( int x, int y, const cg::Vector2d& p1, const cg::Vector2d& p2 );

	protected:
		std::list<std::string> mBuffer;
		cg::Vector2d mTitleP1, mTitleP2, mBodyP1, mBodyP2;
		double mAlpha, mBgAlpha, mTAlpha, mTBgAlpha;
		cg::Vector3d mColor, mBgColor, mTColor, mTBgColor;
		bool mScrollUp, mScrollDown;
		int mStartLine;

		virtual void displayText( void );
		bool continueWriting( void );
        
    public:
        Console( const std::string& id );
        ~Console( void );
        
        void init( void );
        void drawOverlay( void );
        void onReshape( int width, int height );
        void onMouse( int button, int state, int x, int y );
        void onMouseMotion( int x, int y );
        void onMousePassiveMotion( int x, int y );

        /** Writes lines into the console.
              Lines are separated by '\n'.
        */
        void write( const std::string& str );

        /** Writes a char into the current line.
            'Enter' forces a line change.
        */
        void write( const char& c);

        /** Clears the console buffer (which reflects on screen). */
        void clearConsoleBuffer( void );
        
        void showTitle   ( bool b ) { mShowTitle = b;   }
        void canMove     ( bool b ) { mCanMove = b;     }
        void canResize   ( bool b ) { mCanResize = b;   }
        void canMinimize ( bool b ) { mCanMinimize = b; }
        void canScroll   ( bool b ) { mCanScroll = b;   }
        
        void setTitle    ( const std::string& t )  { mTitle = t;      }
        void setSize     ( const cg::Vector2d& s ) { mSize = s;       }
        void setPosition ( const cg::Vector2d& p ) { mPosition = p;   }
        void setStartLine( int sl )                { mStartLine = sl; }
        void setColor    ( const cg::Vector3d& c, double a = 1.0 ) { mColor = c;    mAlpha = a;    }
        void setBgColor  ( const cg::Vector3d& c, double a = 1.0 ) { mBgColor = c;  mBgAlpha = a;  }
        void setTColor   ( const cg::Vector3d& c, double a = 1.0 ) { mTColor = c;   mTAlpha = a;   }
        void setTBgColor ( const cg::Vector3d& c, double a = 1.0 ) { mTBgColor = c; mTBgAlpha = a; }
    };

}

#endif