#ifndef _NumberBillboard_avt_
#define _NumberBillboard_avt_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Physical.h"
#include "PhysicsDefault.h"
#include "Texture.h"

namespace avt{
	class NumberBillboard{
	public:
		NumberBillboard(Physical* physical,unsigned int number);
		~NumberBillboard();
		void init();

	private:
		int _modelDL;
		Material* _material;
		Texture _texture;
		Physical* _physical;
		PhysicsDefault* _physics;
		unsigned int _number;
		double _sideSize;
		cg::Vector2d _relativePosition;

	public:
		void update(const cg::Vector3d& observerPosition);
		void draw(void);

	private:
		void makeModel(void);
		void drawBillboard(void);
	};
}

#endif // _NumberBillboard_avt_