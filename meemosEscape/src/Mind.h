#ifndef _Mind_h_
#define _Mind_h_

#include "EmotionalState.h"
#include "ReactiveProcess.h"
#include "EmotionalReactionRuleDefault.h"
#include "EventWitnessSubscriber.h"
#include "Event.h"
#include "EventCollisionAnvil.h"
#include "EventCollisionFinishLine.h"
#include "EventCollisionHole.h"
#include "EventCollisionTrapDoor.h"
#include "EventWitness.h"
#include "EventSeeDoor.h"
#include "EventSeeOpenDoor.h"
#include "EventSeeLever.h"
#include "EmotionalAgent.h"
#include "Physical.h"

namespace meemo{
	class Mind: public avt::EventWitnessSubscriber{
	public:
		Mind(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent);
		~Mind();
		void init();

	protected:
		appraisal::EmotionalState* _emotionalState;
		appraisal::ReactiveProcess* _reactiveProcess;
		avt::Physical* _physicalAgent;
		meemo::EmotionalAgent* _emotionalAgent;
		bool _dead;

	public:
		appraisal::EmotionalState* getEmotionalState();
		meemo::EmotionalAgent* getAgent();
		void setDead(bool deadValue);
		bool isDead(void) const;

		void update();
		void onEvent(avt::Event* event);

	protected:
		virtual void initModules();
		void initEmotionDispositions();
		virtual void initEmotionalReactionRules();

		void updateEmotionalState();
		virtual void updateAppraisal() = 0;
	};
}

#endif