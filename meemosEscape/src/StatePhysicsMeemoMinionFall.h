#ifndef __StatePhysicsMeemoMinionFall_avt__
#define __StatePhysicsMeemoMinionFall_avt__

#include "StatePhysicsMeemoMinion.h"
#include "StatePhysicsMeemoMinionVisitor.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionFall: public StatePhysicsMeemoMinion{	
	public:
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoMinionFall_avt__
