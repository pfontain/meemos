#ifndef _PhysicsDoor_avt_
#define _PhysicsDoor_avt_

#include "Physics.h"
#include "EventCollisionDefault.h"

namespace avt{
	namespace PhysicsDoorState{
		enum State{OPEN, OPENING, CLOSED};
	}

	class PhysicsDoor: public Physics{
	public:
		PhysicsDoor(void);

	private:
		PhysicsDoorState::State _state;
		float _openLinearVelocity;

	public:
		void open(void);

		EventCollision* getCollisionEvent(void);
		bool isColliding(Physics* collider);
		void step(double elapsedSeconds);

	private:		
		//void stepOpening(double elapsedSeconds);
	};
}

#endif