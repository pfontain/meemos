#include "PhysicsAnvil.h"

namespace avt {

	PhysicsAnvil::PhysicsAnvil(){
		start();
	}

	PhysicsAnvilState::State PhysicsAnvil::getState(void) const{
		return _state;
	}

	void PhysicsAnvil::setDropHeight(float dropHeight){
		_dropHeight = dropHeight;
	}

	void PhysicsAnvil::setRiseLinearVelocity(float velocity){
		_riseLinearVelocity = velocity;
	}

	void PhysicsAnvil::setDropLinearVelocity(float velocity){
		_dropLinearVelocity = velocity;
	}

	void PhysicsAnvil::setTriggerPosition(const cg::Vector3d& triggerPosition){
		_trigger.setPosition(triggerPosition[0],triggerPosition[1],triggerPosition[2]);
	}

	void PhysicsAnvil::setTriggerBoundingSphereRadius(double triggerBoundingSphere){
		_trigger.setBoundingSphereRadius(triggerBoundingSphere);
	}

	void PhysicsAnvil::setWaitTime(float waitTime){
		_waitTime = waitTime;
	}

	void PhysicsAnvil::start(void) {
		_state = PhysicsAnvilState::START;
		_linearVelocity = 0.0;
	}

	void PhysicsAnvil::fall(void) {
		_state = PhysicsAnvilState::FALL;
		_linearVelocity = -_dropLinearVelocity;
	}

	void PhysicsAnvil::wait(void) {
		_state = PhysicsAnvilState::WAIT;
		_linearVelocity = 0.0; 
		_timeCounter = _waitTime;
	}

	void PhysicsAnvil::rise(void) {
		_state = PhysicsAnvilState::RISE;
		_linearVelocity = _riseLinearVelocity;
	}

	EventCollision* PhysicsAnvil::getCollisionEvent(void){
		meemo::Location location(_position[0],_position[1]);
		return new EventCollisionAnvil(location);
	}

	bool PhysicsAnvil::isColliding(Physics* physics){
		if(_state == PhysicsAnvilState::FALL || _state == PhysicsAnvilState::WAIT)
			return defaultIsColliding(this,physics);
		else
			return false;
	}

	void PhysicsAnvil::step(double elapsedSeconds) {
		switch(_state){
			case PhysicsAnvilState::START: stepStart(elapsedSeconds); break;
			case PhysicsAnvilState::FALL: stepFall(elapsedSeconds); break;
			case PhysicsAnvilState::WAIT: stepWait(elapsedSeconds); break;
			case PhysicsAnvilState::RISE: stepRise(elapsedSeconds); break;
		}
		_orientation.getGLMatrix(_rotationMatrix);
	}

	void PhysicsAnvil::stepStart(double elapsedSeconds){
		bool triggerActivated = false;
		std::list<EventCollision*>* collisions = _trigger.getCollisions();
		if(!collisions->empty()){
			triggerActivated = true;
		}
		Utilities::destroy(collisions);
		if(triggerActivated)
			fall();
	}

	void PhysicsAnvil::stepFall(double elapsedSeconds){
		std::list<EventCollision*>* collisions = getCollisions();
		if(!collisions->empty()){
			wait();
		}
		else{
			_linearVelocity = _linearVelocity + (-PhysicsParameters::GRAVITY) * elapsedSeconds;
			_position = _position + cg::Vector3d(0,0,_linearVelocity * elapsedSeconds);

			if(_position[2] - _boundingSphereRadius <= 0){
				_position[2] = _boundingSphereRadius;
				wait();
			} 
		}
		Utilities::destroy(collisions);
	}

	void PhysicsAnvil::stepWait(double elapsedSeconds){
		_timeCounter = _timeCounter - elapsedSeconds;
		if(_timeCounter < 0)
			rise();
	}

	void PhysicsAnvil::stepRise(double elapsedSeconds){
		_position = _position + cg::Vector3d(0,0,_linearVelocity * elapsedSeconds);

		if(_position[2] > _dropHeight){
			_position[2] = _dropHeight;
			start();
		} 
	}
}