#ifndef _PhysicsAnvil_h_
#define _PhysicsAnvil_h_

#include <stdlib.h>
#include "Physics.h"
#include "EventCollisionAnvil.h"
#include "PhysicsTrapTrigger.h"

namespace avt {

	namespace PhysicsAnvilState{
		enum State{
			START,
			FALL,
			WAIT,
			RISE,
		};
	}

	class PhysicsAnvil : public Physics {
	public:
		PhysicsAnvil();

	private:
		PhysicsAnvilState::State _state;
		PhysicsTrapTrigger _trigger;
		float _dropHeight;
		float _riseLinearVelocity;
		float _dropLinearVelocity;
		float _waitTime;
		float _timeCounter;

	public:
		PhysicsAnvilState::State getState(void) const;
		void setDropHeight(float height);
		void setRiseLinearVelocity(float velocity);
		void setDropLinearVelocity(float velocity);
		void setTriggerPosition(const cg::Vector3d& triggerPosition);
		void setTriggerBoundingSphereRadius(double triggerBoundingSphere);
		void setWaitTime(float waitTime);

		void fall(void);
		void start(void);
		void rise(void);
		void wait(void);
		EventCollision* getCollisionEvent(void);
		bool isColliding(Physics* collider);
		void step(double elapsedSeconds);

	private:
		void stepStart(double elapsedSeconds);
		void stepFall(double elapsedSeconds);
		void stepWait(double elapsedSeconds);
		void stepRise(double elapsedSeconds);
	};
}

#endif
