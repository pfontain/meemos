#include "Location.h"

namespace meemo{
	Location::Location(){
		_firstCoordinate = 0.0f;
		_secondCoordinate = 0.0f;
	}

	Location::Location(float firstCoordinate,float secondCoordinate){
		_firstCoordinate = firstCoordinate;
		_secondCoordinate = secondCoordinate;
	}

	float Location::getFirstCoordinate() const{
		return _firstCoordinate;
	}

	float Location::getSecondCoordinate() const{
		return _secondCoordinate;
	}

	cg::Vector3d Location::getVector3d(){
		cg::Vector3d locationVector(_firstCoordinate,_secondCoordinate,0.0);
		return locationVector;
	}

	float Location::squaredDistance(const Location& location){
		float distanceVectorFirstCoordinate = location._firstCoordinate - this->_firstCoordinate;
		float distanceVectorSecondCoordinate = location._secondCoordinate - this->_secondCoordinate;

		return distanceVectorFirstCoordinate * distanceVectorFirstCoordinate + 
			   distanceVectorSecondCoordinate * distanceVectorSecondCoordinate;
	}

	Location::Location(const cg::Vector3d vector){
		_firstCoordinate = vector[0];
		_secondCoordinate = vector[1];
	}

	const std::string Location::toString(){
		std::stringstream stringStream;
		stringStream << "l(";
		stringStream << "C1: " << _firstCoordinate << ",";
		stringStream << "C2: " << _secondCoordinate;
		stringStream << ")";
		return stringStream.str();
	}
}