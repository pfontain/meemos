#include "Hole.h"

namespace avt{
	Hole::Hole(int x, int y,float size,const std::string& id): cg::Entity(id){
		_size = size;
		_physics = new PhysicsHole();
		_physics->setPosition(x,y,0);
	}

	Hole::~Hole(){
		delete _physics;
	}

	void Hole::init() {
		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);

		DebugRegistry::getInstance()->add(this);
	}

	void Hole::draw() {
		drawDebug();
	}

	Physics* Hole::getPhysics(void){
		return _physics;
	}
}