#ifndef _MovePathAgentWithMemory_h_
#define _MovePathAgentWithMemory_h_

#include "MovePathAgent.h"
#include "MemoryStorage.h"

namespace meemo{
	class MovePathAgentWithMemory : public MovePathAgent{
	public:
		virtual MemoryStorage* getMemoryStorage(void) = 0;
	};
}

#endif // _MovePathAgentWithMemory_h_
