#ifndef _EventWitnessSubscriber_h_
#define _EventWitnessSubscriber_h_

#include "Location.h"
#include "EventSubscriber.h"
#include "EmotionalAgent.h"

namespace avt{
	class EventWitnessSubscriber: public EventSubscriber
	{	
	public:
		virtual meemo::EmotionalAgent* getAgent() = 0;
	};
}

#endif //_EventWitnessSubscriber_h_