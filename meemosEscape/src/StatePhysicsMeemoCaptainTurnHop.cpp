#include "StatePhysicsMeemoCaptainTurnHop.h"

namespace avt{
	void StatePhysicsMeemoCaptainTurnHop::step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds){
		statePhysicsMeemoCaptainVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoCaptainTurnHop::destroy(){}

	bool StatePhysicsMeemoCaptainTurnHop::canInterrupt(){
		return true;
	}
}