#ifndef _Camera_h_
#define _Camera_h_

#include <string>
#include <math.h>
#include "cg/cg.h"
#include "Physical.h"
#include "PhysicsDefault.h"
#include "Utilities.h"

namespace avt{

	class Camera :
		public cg::Entity,
		public cg::IReshapeEventListener,
		public Physical
	{
	public:
		Camera(const std::string& id);
		virtual ~Camera();

	protected:
		double _winWidth, _winHeight, _winRatio;
		int _fieldOfViewAngle;
		int _frustrumNear;
		int _frustrumFar;

		PhysicsDefault _physics;

	public:
		const cg::Vector3d getFrontDirection() const;
		virtual void setModelViewMatrix() = 0;
		void onReshape(int width, int height);
		const void printDescription() const;

	protected:
		void initWindowViewport();
		void setProjection();
		Physics* getPhysics(void);
	};
}

#endif