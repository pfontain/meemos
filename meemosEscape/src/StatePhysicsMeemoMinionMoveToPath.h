#ifndef _StatePhysicsMeemoMinionMoveToPath_h_avt_
#define _StatePhysicsMeemoMinionMoveToPath_h_avt_

#include "StatePhysicsMeemoMinionVisitor.h"
#include "StatePhysicsMeemoMinion.h"
#include "UpdatePathAgent.h"
#include "Path.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionMoveToPath: public StatePhysicsMeemoMinion{
	public:
		StatePhysicsMeemoMinionMoveToPath(UpdatePathAgent* meemoMinion, std::list<meemo::Location*>* locations);

	private:
		Path* _path;
		bool _moving;
		bool _started;
		//bool _collidedNearToTarget;
		UpdatePathAgent* _meemoMinion;

	public:
		Path* getPath(void) const;
		bool isMoving(void) const;
		void setMoving(bool movingValue);
		//bool hasCollidedNearToTarget(void) const;
		//void setCollidedNearToTarget(void);
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt(void);
	};
}

#endif //_StatePhysicsMeemoMinionMoveToPath_h_avt_