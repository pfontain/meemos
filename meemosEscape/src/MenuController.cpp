#include "MenuController.h"

namespace avt{

	MenuController::MenuController(MeemoApplication* application,bool menuOn): Entity("MenuController"){
		_application = application;
		_menuOn = menuOn;
	}

	MenuController::~MenuController(){

	}

	void MenuController::init() {
		_texture.open("menu.bmp");
	}

	void MenuController::drawOverlay(){
		if(_menuOn)
		{
			unsigned int width= _application->getWindow().width;
			unsigned int height= _application->getWindow().height;

			glPushMatrix();
			{
				glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
				glClear( GL_COLOR_BUFFER_BIT );
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, _texture.id);

				glColor3f(1,1,1);
				glBegin(GL_QUADS);
				{
					glNormal3f(0,0,-1);
					glTexCoord2f(0,0);
					glVertex2d(0,0);
					glTexCoord2f(1,0);
					glVertex2d(width,0);
					glTexCoord2f(1,1);
					glVertex2d(width,height);
					glTexCoord2f(0,1);
					glVertex2d(0,height);
				}
				glEnd();
				glDisable(GL_TEXTURE_2D);
			}
			glPopMatrix();
		}
	}


	void MenuController::onKeyPressed(unsigned char key){
		if (key == 13){// ENTER
			if(!_application->islevelLoaded()){
				_menuOn=false;
				_application->loadDebugLevel();
			}
		}
		else if (key == 27){// ESC
			_menuOn=true;
			if(!_application->islevelLoaded()){
				cg::Manager::instance()->shutdownApp();
			}
			else{
				_application->markLevelForDeletion();
			}
		}
	}
	void MenuController::onKeyReleased(unsigned char key){}
	void MenuController::onSpecialKeyPressed(int key){}
	void MenuController::onSpecialKeyReleased(int key){}
}