#ifndef _PhysicsMeemoCaptain_h
#define _PhysicsMeemoCaptain_h

#include "PhysicsMeemo.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt {

	class PhysicsMeemoCaptain :
		public PhysicsMeemo,
		public StatePhysicsMeemoCaptainVisitor {
	public:
		PhysicsMeemoCaptain();
		~PhysicsMeemoCaptain();

	private:
		StatePhysicsMeemoCaptain* _state;

	public:
		bool stop();
		bool fall();
		bool turnHop(const cg::Vector3d& position);
		bool hit();
		bool pullLever();
		void step(double elapsedSeconds);
		void step(StatePhysicsMeemoCaptainWalking* statePhysicsMeemoCaptainWalking,double elapsedSeconds);
		void step(StatePhysicsMeemoCaptainFall* statePhysicsMeemoCaptainFall,double elapsedSeconds);
		void step(StatePhysicsMeemoCaptainTurnHop* statePhysicsMeemoCaptainTurnHop,double elapsedSeconds);
		void step(StatePhysicsMeemoCaptainHit* statePhysicsMeemoCaptainHit,double elapsedSeconds);
		void step(StatePhysicsMeemoCaptainPullLever* statePhysicsMeemoCaptainPullLever,double elapsedSeconds);

	private:
		void rotate(double elapsedSeconds,int direction, cg::Vector3d axis, cg::Vector3d& v1, cg::Vector3d& v2);
		void stepRotation(double elapsedSeconds);
	};
}

#endif