#ifndef _EventCollisionFinishLine_h_
#define _EventCollisionFinishLine_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionFinishLine: public EventCollision
	{	
	public:
		EventCollisionFinishLine(){}
		EventCollisionFinishLine(meemo::Location location): EventCollision(location){}

		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionFinishLine";
			return stringStream.str();
		}

		EventType::Type type(){
			return EventType::COLLISION_FINISH_LINE;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionFinishLine(_location);
			else
				return new EventCollisionFinishLine();
		}
	};
}

#endif