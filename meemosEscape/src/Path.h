#ifndef _Path_h_
#define _Path_h_

#include "Location.h"
#include "Utilities.h"
#include <list>

namespace avt{
	class Path{
	public:
		Path(std::list<meemo::Location*>* locations);
		~Path(void);

	private:
		std::list<meemo::Location*>* _locations;
		std::list<meemo::Location*>::iterator _currentLocation;

	public:
		meemo::Location* getCurrentLocation(void);
		meemo::Location* getTargetLocation(void);
		void nextLocation(void);
		bool isEnd(void);
	};
}

#endif // _Path_h_