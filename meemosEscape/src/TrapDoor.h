#ifndef _TrapDoor_avt_
#define _TrapDoor_avt_

#include "cg/cg.h"
#include "Debugable.h"
#include "MaterialStatic.h"
#include "Texture.h"
#include "DebugRegistry.h"
#include "PhysicsTrapDoor.h"

namespace avt{
	class TrapDoor: public cg::Entity,
		public cg::IDrawListener,
		public cg::IUpdateListener,
		public Debugable
	{
	public:
		TrapDoor(int x, int y,float size,const std::string& id);
		TrapDoor::~TrapDoor();

	private:
		float _size;
		PhysicsTrapDoor* _physics;
		int _modelDL;
		Material* _material;

	public:
		void init(void);
		
		void draw(void);
		void update(unsigned long elapsed_millis);
		Physics* getPhysics(void);

	private:
		void makeModel(void);
	};
}

#endif