#ifndef _MovePathAgent_h_
#define _MovePathAgent_h_

#include "Location.h"

namespace meemo{
	class MovePathAgent{
	public:
		virtual bool movePath(std::list<meemo::Location*>* locations) = 0;
	};
}

#endif // _MovePathAgent_h_
