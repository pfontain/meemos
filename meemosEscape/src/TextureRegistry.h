#ifndef _TextureRegistry_h_
#define _TextureRegistry_h_

#include "cg/cg.h"
#include "Texture.h"
#include <map>
#include <string>

namespace avt{
	class Texture;
	/**
	 * Singleton pattern 
	 */
	class TextureRegistry
	{	
	private:
		TextureRegistry(){};

		static TextureRegistry* _instance;
		static std::map<std::string,GLuint>* _textures;

	public:
		static TextureRegistry* getInstance();
		static void deleteInstance();
		static bool isInstantiated();

		static void add(const std::string& fileName,GLuint id);
		static bool isRegistered(const std::string& fileName);
		static GLuint getId(const std::string& fileName);
		static void clearList();
	};
}

#endif