#ifndef _MaterialStatic_h_
#define _MaterialStatic_h_

#include "Material.h"

namespace avt{
	class MaterialStatic: public Material{
	private:
		int _materialDL;

	public:
		void init();
		void draw();

	private:
		void makeMaterial();
	};
}

#endif // _MaterialStatic_h_