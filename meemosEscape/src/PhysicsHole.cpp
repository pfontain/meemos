#include "PhysicsHole.h"

namespace avt {

	EventCollision* PhysicsHole::getCollisionEvent(){
		meemo::Location location(_position[0],_position[1]);
		return new EventCollisionHole(location);
	}

	bool PhysicsHole::isColliding(Physics* collider){
		float halfSideSize = _boundingSphereRadius;

		cg::Vector3d colliderPosition = collider->getPosition();
		float colliderPositionX = colliderPosition[0];
		float colliderPositionY = colliderPosition[1];
		float colliderPositionZ = colliderPosition[2];

		float positionX = _position[0];
		float positionY = _position[1];
		float positionZ = _position[2];
			
		return
			colliderPositionX > positionX - halfSideSize &&
			colliderPositionX < positionX + halfSideSize &&
			colliderPositionY > positionY - halfSideSize &&
			colliderPositionY < positionY + halfSideSize &&
			Physics::isFloatZero(colliderPositionZ);
	}
}