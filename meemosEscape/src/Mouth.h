#ifndef _Mouth_h_
#define _Mouth_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Physical.h"
#include "Utilities.h"

namespace avt{

	namespace MouthState{
		enum State{ NEUTRAL = 0, SMILE = 1, DISTRESS = 2 };
	}

	class Mouth{
	public:
		Mouth(Physical* physical);
		~Mouth();
		void init();

	private:
		int _modelDLNeutral;
		int _modelDLSmile;
		int _modelDLDistress;
		MouthState::State _state;

		float _radius;
		float _width;
		unsigned short _numberEdges;
		Material* _material;
		cg::Vector3d _relativePosition;
		Physical* _physical;

	private:
		void makeModels();
		void makeModelNeutral();
		void makeModelSmile();
		void makeModelDistress();
		void makeModelBand(const cg::Vector3d& centerPosition,float deltaRadians,float startAngle);
		void makeModelRectangle();

	public:
		void draw();
		void neutralOn();
		void smileOn();
		void distressOn();
	};
}

#endif // _Mouth_h_