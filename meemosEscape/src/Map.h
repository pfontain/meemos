#ifndef _Map_avt_h_
#define _Map_avt_h_

#include "Printable.h"
#include <utility>
#include <vector>
#include <map>

namespace avt{
	class Map: public Printable{
	public:
		Map(const std::pair<int,int>& dimensions);
		Map::Map(const Map& map);
		Map();
		~Map();

	private:
		std::pair<unsigned int,unsigned int> _dimensions;
		std::vector<char>* _map;

	public:
		void setDimensions(const std::pair<int,int>& dimensions);
		const std::pair<unsigned int,unsigned int>& getDimensions();
		int getSizeX();
		int getSizeY();
		char& operator[](const std::pair<int,int>& location);
		char& operator[](int position);

		int* createIntegerMap();
		int* createIntegerMap(std::map<char,int>& mappingToInteger);
		static int* copyIntegerMap(int* integerMap,const std::pair<unsigned int,unsigned int>& dimensions);
		const std::string toString();
		static const std::string toStringIntegerMap(int* integerMap,unsigned int width,int height);
	};
}

#endif