#include "StatePhysicsMeemoCaptainPullLever.h"

namespace avt{
	void StatePhysicsMeemoCaptainPullLever::step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds){
		statePhysicsMeemoCaptainVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoCaptainPullLever::destroy(){}

	bool StatePhysicsMeemoCaptainPullLever::canInterrupt(){
		return false;
	}
}