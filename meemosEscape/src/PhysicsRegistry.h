#ifndef _PhysicsRegistry_h_
#define _PhysicsRegistry_h_

#include "Physics.h"
#include <map>
#include <utility>

namespace avt{
	class Physics;
	/**
	 * Singleton pattern 
	 */
	class PhysicsRegistry
	{	
	private:
		PhysicsRegistry(){};

		static PhysicsRegistry* _instance;
		static std::list<Physics*>* _physics;

	public:
		static PhysicsRegistry* getInstance();
		static void deleteInstance();
		static bool isInstantiated();

		static std::list<Physics*>* getPhysicsList(void);
		static void add(Physics* physics);
		static void remove(Physics* physical);
		static void clearList();
	};
}

#endif