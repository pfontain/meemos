#include "PhysicsMeemoCaptain.h"

namespace avt {
	PhysicsMeemoCaptain::PhysicsMeemoCaptain(){
		_state = new StatePhysicsMeemoCaptainWalking();
	}

	PhysicsMeemoCaptain::~PhysicsMeemoCaptain(){
		_state->destroy();
		delete _state;
	}

	bool PhysicsMeemoCaptain::stop(){
		if(_state->canInterrupt()){
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainWalking();
			return true;
		}
		else return false;
	}

	bool PhysicsMeemoCaptain::fall() {
		if(_state->canInterrupt()){
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainFall();
			_velocity = cg::Vector3d(0,0,0);
			return true;
		}
		else return false;
	}

	bool PhysicsMeemoCaptain::turnHop(const cg::Vector3d& position){
		if(_state->canInterrupt()){
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainTurnHop();
			cg::Vector3d hopVelocity(0.0,0.0,_hopLinearVelocity);
			_velocity = hopVelocity;
			_turnOriginalOrientation = _orientation;
			_turnOriginalFront = _front;
			_turnOriginalRight = _right;
			cg::Vector3d turnToPosition = position;
			cg::Vector3d turnDirection;
			turnDirection = turnToPosition - _position;
			cg::Vector3d normalizedTurnDirection(normalize(turnDirection));
			_turnTotalRotationAngle = rotationAngle(normalizedTurnDirection);
			_hopDuration = hopDuration();
			_turnElapsedSeconds = 0.0;
			return true;
		}
		else return false;
	}

	bool PhysicsMeemoCaptain::hit() {
		_state->destroy();
		delete _state;
		_state = new StatePhysicsMeemoCaptainHit();
		_position = _position - cg::Vector3d(0.0,0.0,_dropDepth);
		_velocity = cg::Vector3d(0,0,_riseLinearVelocity);
		return true;
	}

	bool PhysicsMeemoCaptain::pullLever(){
		if(_state->canInterrupt()){
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainPullLever();
			_velocity = cg::Vector3d(0.0,0.0,_pullLeverLinearVelocity);
			return true;
		}
		else return false;
	}

	void PhysicsMeemoCaptain::step(double elapsedSeconds) {
		_state->step(this,elapsedSeconds);
		stepRotation(elapsedSeconds);
		_orientation.getGLMatrix(_rotationMatrix);
	}

	// factorize
	void PhysicsMeemoCaptain::step(StatePhysicsMeemoCaptainWalking* statePhysicsMeemoCaptainWalking,double elapsedSeconds){
		cg::Vector3d positionChange;

		if(_isGoAhead) {
			positionChange = _front * _linearVelocity * elapsedSeconds;
			_isGoAhead = false;
		}
		if(_isGoBack) {
			positionChange = - _front * _linearVelocity * elapsedSeconds;
			_isGoBack = false;
		}
		if(_isGoRight) {
			positionChange = _right * _linearVelocity * elapsedSeconds;
			_isGoRight = false;
		}
		if(_isGoLeft) {
			positionChange = - _right * _linearVelocity * elapsedSeconds;
			_isGoLeft = false;
		}

		cg::Vector3d positionOriginal;
		positionOriginal = _position;
		_position += positionChange;

		std::list<EventCollision*>* collisions = getCollisions();
		std::list<EventCollision*>::iterator iteratorCollisions = collisions->begin();
		for(; iteratorCollisions != collisions->end(); iteratorCollisions++){
			EventCollision* event = *iteratorCollisions;
			if(event->type() == EventType::COLLISION_WALL){
				_position = positionOriginal;
				break;
			}
		}
		Utilities::destroy(collisions);
	}

	void PhysicsMeemoCaptain::step(StatePhysicsMeemoCaptainFall* statePhysicsMeemoCaptainFall,double elapsedSeconds){
		_velocity = _velocity + cg::Vector3d(0,0,-PhysicsParameters::GRAVITY) * elapsedSeconds;
		_position = _position + _velocity * elapsedSeconds;
	}

	void PhysicsMeemoCaptain::step(StatePhysicsMeemoCaptainTurnHop* statePhysicsMeemoCaptainTurnHop,double elapsedSeconds){
		_turnElapsedSeconds = _turnElapsedSeconds + elapsedSeconds;

		if(_turnElapsedSeconds > _hopDuration){
			stepTurnHopLastTurn();
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainWalking();
		}
		else{
			stepTurnHopTurn(elapsedSeconds);
		}
	}

	void PhysicsMeemoCaptain::step(StatePhysicsMeemoCaptainHit* statePhysicsMeemoMinionHit,double elapsedSeconds){
		_position = _position + _velocity * elapsedSeconds;
		if(_position[2] > 0){
			_position[2] = 0;
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainWalking();
		}
	}

	void PhysicsMeemoCaptain::step(StatePhysicsMeemoCaptainPullLever* statePhysicsMeemoCaptainPullLever,double elapsedSeconds){
		_velocity = _velocity + cg::Vector3d(0,0,-PhysicsParameters::GRAVITY) * elapsedSeconds;
		_position = _position + _velocity * elapsedSeconds;
		if(_position[2] < 0){
			_position[2] = 0;
			_state->destroy();
			delete _state;
			_state = new StatePhysicsMeemoCaptainWalking();
		}
	}


	void PhysicsMeemoCaptain::stepRotation(double elapsedSeconds){
		if(_isYawLeft) {
			rotate(elapsedSeconds,1,_up,_front,_right);
			_isYawLeft = false;
		}
		if(_isYawRight) {
			rotate(elapsedSeconds,-1,_up,_front,_right);
			_isYawRight = false;
		}
		if(_isPitchUp) {
			rotate(elapsedSeconds,1,_right,_up,_front);
			_isPitchUp = false;
		}
		if(_isPitchDown) {
			rotate(elapsedSeconds,-1,_right,_up,_front);
			_isPitchDown = false;
		}
		if(_isRollLeft) {
			rotate(elapsedSeconds,1,_front,_right,_up);
			_isRollLeft = false;
		}
		if(_isRollRight) {
			rotate(elapsedSeconds,-1,_front,_right,_up);
			_isRollRight = false;
		}
	}

	void PhysicsMeemoCaptain::rotate(double elapsedSeconds,int direction, cg::Vector3d axis, cg::Vector3d& v1, cg::Vector3d& v2){
		Physics::rotate(elapsedSeconds,direction,axis,v1,v2);
	}
}