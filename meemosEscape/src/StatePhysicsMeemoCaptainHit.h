#ifndef __StatePhysicsMeemoCaptainHit_avt__
#define __StatePhysicsMeemoCaptainHit_avt__

#include "StatePhysicsMeemoCaptain.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt{
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptainHit: public StatePhysicsMeemoCaptain{	
	public:
		void step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds);
		void destroy();
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoCaptainHit_avt__
