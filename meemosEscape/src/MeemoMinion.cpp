#include "MeemoMinion.h"

namespace avt{
	MeemoMinion::MeemoMinion(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic): Meemo(id,size,level,gameLogic){
		_physicsMeemoMinion = new PhysicsMeemoMinion(this);
		_physics = _physicsMeemoMinion;
		_physics->setPosition(x, y, 0);
		std::stringstream idStringStream(id);
		_orderId = idNumber;
		_idNumber = idNumber;
		_lifeState = 0;
		_numberBillboard = new NumberBillboard(this,idNumber);
	}

	MeemoMinion::~MeemoMinion(){
		delete _physicsMeemoMinion;
		delete _numberBillboard;
	}

	void MeemoMinion::init() {
		Meemo::init();

		initMaterial();
		_numberBillboard->init();
		if(cg::Registry::instance()->exists("MeemoCaptain"))
			_meemoCaptain = dynamic_cast <MeemoCaptain*>(cg::Registry::instance()->get("MeemoCaptain"));
		else
			_meemoCaptain = NULL;
	}

	void MeemoMinion::setPlan(std::queue<cg::Vector3d> plan){
		_plan = plan;
	}

	void MeemoMinion::setWaitDurations(std::queue<double> waitDurations){
		_physicsMeemoMinion->setWaitDurations(waitDurations);
	}

	void MeemoMinion::onKeyPressed(unsigned char key){
		if(key == '.' && _lifeState != MeemoMinionState::MORTO && !_plan.empty()){
			cg::Vector3d planPosition = _plan.front();
			bool canMovePathPlanned = _behaviourMeemoMinion->movePathPlanned(planPosition);
			if(!canMovePathPlanned)
				printf("meemo %s: I can't go to plan position\n", _id.c_str());
			_plan.pop();
		}
	}

	void MeemoMinion::onKeyReleased(unsigned char key){

	}

	void MeemoMinion::onSpecialKeyPressed(int key){

	}

	void MeemoMinion::onSpecialKeyReleased(int key){

	}

	void MeemoMinion::draw(void) {
		glPushMatrix();{
			_physics->applyTransforms();
			_eyes->draw();
			_mouth->draw();
			_material->draw();
			glCallList(_modelDL);
		}glPopMatrix();

		if(cg::Properties::instance()->getInt("THOUGHT_BALLOON_ACTIVE"))
			_thoughtBalloon->draw();
		if(cg::Properties::instance()->getInt("NUMBER_BILLBOARD_ACTIVE"))
			_numberBillboard->draw();
		drawDebug();
	}

	void MeemoMinion::update(unsigned long elapsed_millis){
		Meemo::update(elapsed_millis);
		double elapsedSeconds = elapsed_millis / (double)1000;
		Camera* activeCamera = _level->getActiveCamera();
		cg::Vector3d activeCameraPosition = activeCamera->getPosition();
		_numberBillboard->update(activeCameraPosition);

		Event* event;
		if(!_eventBuffer->empty()){
			event = _eventBuffer->pop();
			update(event);
		}

		if(_lifeState != MeemoMinionState::TERMINADO && _lifeState != MeemoMinionState::MORTO && _meemoCaptain != NULL)
			updateOrder(elapsedSeconds);

		_physics->step(elapsedSeconds);
	}

	void MeemoMinion::update(Event* event){
		if(event->type() == EventType::WITNESS){
			EventWitness* eventWitness = static_cast<EventWitness*>(event);
			trigger(eventWitness);
		}
		else if(event->type() == EventType::COLLISION_WALL){
			EventCollisionWall* eventCollisionWall = static_cast<EventCollisionWall*>(event);
			trigger(eventCollisionWall);
		}
		else if(event->type() == EventType::COLLISION_ANVIL){
			EventCollisionAnvil* eventCollisionAnvil = static_cast<EventCollisionAnvil*>(event);
			trigger(eventCollisionAnvil);
		}
		else if(event->type() == EventType::COLLISION_HOLE){
			EventCollisionHole* eventCollisionHole = static_cast<EventCollisionHole*>(event);
			trigger(eventCollisionHole);
		}
		else if(event->type() == EventType::COLLISION_TRAP_DOOR){
			EventCollisionTrapDoor* eventCollisionTrapDoor = static_cast<EventCollisionTrapDoor*>(event);
			trigger(eventCollisionTrapDoor);
		}
		else if(event->type() == EventType::COLLISION_DEFAULT){
			EventCollisionDefault* eventCollisionDefault = static_cast<EventCollisionDefault*>(event);
			trigger(eventCollisionDefault);
		}
		else if(event->type() == EventType::COLLISION_FINISH_LINE){
			EventCollisionFinishLine* eventCollisionFinishLine = static_cast<EventCollisionFinishLine*>(event);
			trigger(eventCollisionFinishLine);
		}
		else if(event->type() == EventType::SEE_OPEN_DOOR){
			EventSeeOpenDoor* eventSeeOpenDoor = static_cast<EventSeeOpenDoor*>(event);
			trigger(eventSeeOpenDoor);
		}
		else if(event->type() == EventType::SEE_DOOR){
			EventSeeDoor* eventSeeDoor = static_cast<EventSeeDoor*>(event);
			trigger(eventSeeDoor);
		}
		else if(event->type() == EventType::SEE_LEVER){
			EventSeeLever* eventSeeLever = static_cast<EventSeeLever*>(event);
			trigger(eventSeeLever);
		}
	}

	void MeemoMinion::trigger(EventWitness* eventWitness){
		if(eventWitness->getWitnessedEventType() != EventType::COLLISION_DEFAULT){
			Event* witnessedEvent = eventWitness->getWitnessedEvent();
			if(witnessedEvent->isLocationSet())
			{
				meemo::Location witnessedEventLocation = witnessedEvent->getLocation();
				cg::Vector3d witnessedEventLocationVector = witnessedEventLocation.getVector3d();
				_physicsMeemoMinion->turnHop(witnessedEventLocationVector);
			}
		}
	}

	void MeemoMinion::trigger(EventCollisionWall* eventCollisionWall){
		this->_lifeState = MeemoMinionState::PARADO;
		_physicsMeemoMinion->stop();
	}

	void MeemoMinion::trigger(EventCollisionAnvil* eventCollisionAnvil){
		this->_lifeState = MeemoMinionState::PARADO;
		_physicsMeemoMinion->hit();
	}

	void MeemoMinion::trigger(EventCollisionHole* eventCollisionHole){
		_gameLogic->notifyDeathMeemoMinion(this);
		_mind->setDead(true);
		this->_lifeState = MeemoMinionState::MORTO;
		_physicsMeemoMinion->fall();
	}

	void MeemoMinion::trigger(EventCollisionTrapDoor* eventCollisionTrapDoor){
		_gameLogic->notifyDeathMeemoMinion(this);
		_mind->setDead(true);
		this->_lifeState = MeemoMinionState::MORTO;
		_physicsMeemoMinion->fall();
	}

	void MeemoMinion::trigger(EventCollisionDefault* eventCollisionDefault){
		this->_lifeState = MeemoMinionState::PARADO;
		_physicsMeemoMinion->stop();
	}

	void MeemoMinion::trigger(EventCollisionFinishLine* eventCollisionFinishLine){
		if(	this->_lifeState != MeemoMinionState::TERMINADO){
			this->_lifeState = MeemoMinionState::TERMINADO;
			_gameLogic->incrementNumberMeemoMinionsAtFinishLine();
			printf("meemo %s: I'm at the finish line\n", this->_id.c_str());  //DEBUG
			_physicsMeemoMinion->stop();
		}
	}

	void MeemoMinion::trigger(EventSeeOpenDoor* eventSeeOpenDoor){}
	void MeemoMinion::trigger(EventSeeDoor* eventSeeDoor){}
	void MeemoMinion::trigger(EventSeeLever* eventSeeLever){}

	//fun��o de update das ordens que o captain da ao meemo
	void MeemoMinion::updateOrder(double seconds){

		//verifica se a ordem actual � para si ou para todos
		if(_orderId == _meemoCaptain->getActionId())
		{
			//verifica se a ac�ao � ir at� ao captain
			if(_meemoCaptain->getAction() == MeemoAction::COME_HERE)
			{
				printf("meemo %s: Meemo Captain told me to go to him\n", this->_id.c_str());
				cg::Vector3d captainPosition = _meemoCaptain->getPosition();
				bool canMovePathPlanned = _behaviourMeemoMinion->movePathPlanned(captainPosition);
				if(!canMovePathPlanned)
					printf("meemo %s: I can't go to him\n", this->_id.c_str());
			}

			//verifica se a ac�ao � parar
			if(_meemoCaptain->getAction() == MeemoAction::STOP){
				printf("meemo %s: Meemo Captain told me to stop\n", this->_id.c_str());
				bool canStop = _physicsMeemoMinion->stop();
				if(!canStop)
					printf("meemo %s: I can't stop\n", this->_id.c_str());
			}

			//verifica se a ac�ao � olhar
			if(_meemoCaptain->getAction() == MeemoAction::LOOK_AT_ME){
				printf("meemo %s: Meemo Captain told me to look at him\n", this->_id.c_str());
				cg::Vector3d captainPosition = _meemoCaptain->getPosition();
				bool canTurnHop = _physicsMeemoMinion->turnHop(captainPosition);
				if(!canTurnHop)
					printf("meemo %s: I can't look at him\n", this->_id.c_str());
			}
		}

	}

	bool MeemoMinion::movePath(std::list<meemo::Location*>* locations){
		return _physicsMeemoMinion->movePath(locations);
	}

	void MeemoMinion::pullLever(void){
		_physicsMeemoMinion->pullLever();
	}

	void MeemoMinion::initMaterial(void){
		_materialColourFirst = cg::Properties::instance()->getVector3d("MEEMO_MINION_COLOUR_NEUTRAL");
		_materialColourSecond = cg::Properties::instance()->getVector3d("MEEMO_MINION_COLOUR_LOW_MOOD");
		Meemo::initMaterial();
	}
}