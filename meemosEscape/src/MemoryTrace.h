#ifndef _MemoryTrace_h_
#define _MemoryTrace_h_

#include "Event.h"
#include "ActiveEmotion.h"
#include "Printable.h"

namespace meemo{
	class MemoryTrace: public Printable{
	public:
		MemoryTrace(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion,long time);
		~MemoryTrace();
	private:
		appraisal::Event* _event;
		appraisal::ActiveEmotion* _causedEmotion;
		long _time;
	public:
		appraisal::Event* getEvent(void) const;
		appraisal::ActiveEmotion* getCausedEmotion(void) const;
		long getTime(void) const;
		const std::string toString();
	};
}

#endif //_MemoryTrace_h_