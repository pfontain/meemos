/** 
* Event.h - Event that can return a string describing it
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/05/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/05/2009 - File created.
*/
#ifndef _Event_h_
#define _Event_h_

#include <string>
#include "Printable.h"
#include "Location.h"
#include <crtdbg.h>

#ifdef _DEBUG
   #ifndef DBG_NEW
      #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
      #define new DBG_NEW
   #endif
#endif  // _DEBUG

namespace EventType{
	enum Type {
		DEFAULT,
		NULL_EVENT,
		WITNESS,
		COLLISION_WALL,
		COLLISION_HOLE,
		COLLISION_FINISH_LINE,
		COLLISION_ANVIL,
		COLLISION_DEFAULT,
		COLLISION_TRAP_DOOR,
		COLLISION_TRAP_TRIGGER,
		SEE_LEVER,
		PULL_LEVER,
		SEE_DOOR,
		SEE_OPEN_DOOR,
		MEMORY_RETRIEVAL
	};
}

namespace appraisal{
	class Event: public Printable 
	{	
	public:
		Event();
		Event(meemo::Location location);

	protected:
		bool _locationSet;
		meemo::Location _location;

	public:
		virtual EventType::Type type() = 0;
		virtual Event* copy() = 0;
		virtual void destroy() = 0;
		
		virtual bool equal(Event* event) = 0;
		bool defaultEqual(Event*);
		virtual int match(Event*) = 0;
		int defaultMatch(Event*);

		virtual const std::string toString() = 0;

		bool isLocationSet();
		const meemo::Location getLocation();

		static const int NO_MATCH = 0;
		static const int HALF_MATCH = 5;
		static const int OVER_HALF_MATCH = 7;
		static const int TOTAL_MATCH = 10;
	};
}

namespace meemo{
	class Event: public appraisal::Event
	{
	public:
		Event(){}
		Event(meemo::Location location): appraisal::Event(location){}
	};
}

namespace avt{
	class Event: public meemo::Event
	{	
	public:
		Event(){}
		Event(meemo::Location location): meemo::Event(location){}
	public:
		virtual Event* copy() = 0;
		virtual bool isNull() { return false; }
	};
}

#endif