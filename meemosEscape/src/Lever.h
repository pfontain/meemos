#ifndef _Lever_h_
#define _Lever_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "PhysicsLever.h"
#include "DebugRegistry.h"

namespace avt{

	class Lever : public cg::Entity,
		public cg::IDrawListener,
		public cg::IUpdateListener,
		public Debugable
	{
	public:
		Lever(int x, int z,float size,const std::string& id);
		~Lever(void);

	private:
		GLuint _modelBox;
		GLuint _modelHandle;
		float _size;
		Material* _materialBox;
		Material* _materialHandle;
		PhysicsLever* _physics;
		float _length;
		float _upAngle;
		float _width;
		cg::Vector3d _anchorPosition;

	public:
		void init(void);
		void draw(void);
		void update(unsigned long elapsed_millis);
		void descend(void);

	private:
		void makeModel(void);
		void drawHandle(void);
		// TODO: Utilities
		static void drawExtrudedSquare(cg::Vector3d position,double squareSideSize,double extrusionLength);
		Physics* getPhysics(void);
	};
}


#endif