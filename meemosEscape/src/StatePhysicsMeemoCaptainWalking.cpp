#include "StatePhysicsMeemoCaptainWalking.h"

namespace avt{
	void StatePhysicsMeemoCaptainWalking::step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds){
		statePhysicsMeemoCaptainVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoCaptainWalking::destroy(){}

	bool StatePhysicsMeemoCaptainWalking::canInterrupt(){
		return true;
	}
}