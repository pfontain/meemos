#ifndef _PhysicsWall_h
#define _PhysicsWall_h

#include "Physics.h"
#include "EventCollisionWall.h"

namespace avt {

	class PhysicsWall : public Physics {

	public:
		EventCollision* getCollisionEvent();
		bool isColliding(Physics* collider);
	};
}

#endif