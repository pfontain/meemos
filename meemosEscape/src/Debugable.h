#ifndef _Debugable_h_
#define _Debugable_h_

#include "cg/cg.h"
#include "Physical.h"
#include "Physics.h"

namespace avt{
	class Debugable: public Physical{
	protected:
		Debugable(){ _isDebug = false; }
	protected:
		bool _isDebug;
	public:
		inline void toggleDebugMode() { _isDebug = !_isDebug; }
		inline void drawDebug(){
			if(_isDebug) {
				Physics* physics = getPhysics();
				glDisable(GL_LIGHTING);
				glPushMatrix();
				physics->drawAxes();
				glPopMatrix();
				glEnable(GL_LIGHTING);
			}
		}
	};
}

#endif