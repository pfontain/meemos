#ifndef __StatePhysicsMeemoMinionIdle_avt__
#define __StatePhysicsMeemoMinionIdle_avt__

#include "StatePhysicsMeemoMinion.h"
#include "StatePhysicsMeemoMinionVisitor.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionIdle: public StatePhysicsMeemoMinion{
	public:
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoMinionIdle_avt__
