/** 
* EventSeeDoor.h
*  
* Copyright (C) 2010 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 06/02/2010
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
*/
#ifndef _EventSeeDoor_h_
#define _EventSeeDoor_h_

#include "Event.h"

namespace avt{

	class EventSeeDoor: public Event
	{	
	public:
		EventSeeDoor(){}
		EventSeeDoor(meemo::Location location): Event(location){}

		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventSeeDoor";
			if(isLocationSet())
				stringStream << "[" << _location << "]";
			return stringStream.str();
		}

		Event* copy(){
			if(isLocationSet())
				return new EventSeeDoor(_location);
			else
				return new EventSeeDoor();
		}

		EventType::Type type(){
			return EventType::SEE_DOOR;
		}

		virtual bool equal(appraisal::Event* event){ return Event::defaultEqual(event); }

		virtual int match(appraisal::Event* event){ return Event::defaultMatch(event); }

	protected:
		void destroy(){};
	};
}

#endif