#ifndef _EventBuffer_h_
#define _EventBuffer_h_

#include <list>
#include "Event.h"
#include "EventWitnessSubscriber.h"
#include "EmotionalAgent.h"

namespace avt{

	class EventBuffer: public EventWitnessSubscriber 
	{

	public:
		EventBuffer(meemo::EmotionalAgent* agent);
		~EventBuffer();

	private:
		std::list<Event*> _events;
		std::list<Event*> _trash;
		meemo::EmotionalAgent* _agent;

	public:
		meemo::EmotionalAgent* getAgent();

		Event* pop();
		void push(Event* event);
		bool empty();
		void onEvent(Event* event);
		void clean();
	};
}

#endif