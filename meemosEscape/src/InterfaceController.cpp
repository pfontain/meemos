#include "InterfaceController.h"

namespace avt{

	InterfaceController::InterfaceController(AbstractGameLogic* gameLogic): Entity("InterfaceController"){
		_gameLogic = gameLogic;
	}

	void InterfaceController::init() {
		_initialTime = time (NULL);
	}

	void InterfaceController::onKeyPressed(unsigned char key){
		if (key == 32){ // SPACE
			Lever* lever1 = dynamic_cast<Lever*>(cg::Registry::instance()->get("l1"));
			lever1->descend();
			Lever* lever2 = dynamic_cast<Lever*>(cg::Registry::instance()->get("l2"));
			lever2->descend();
			Door* door = dynamic_cast<Door*>(cg::Registry::instance()->get("door"));
			door->open();
			MeemoMinion* m1 = dynamic_cast<MeemoMinion*>(cg::Registry::instance()->get("m1"));
			m1->pullLever();
			MeemoMinion* m2 = dynamic_cast<MeemoMinion*>(cg::Registry::instance()->get("m2"));
			m2->pullLever();
		}
	}

	void InterfaceController::onKeyReleased(unsigned char key){}

	void InterfaceController::onSpecialKeyPressed(int key){
		if (key == GLUT_KEY_F1) {
			cg::Manager::instance()->getApp()->dump();
		}
		if (key == GLUT_KEY_F2)
		{
			DebugRegistry::getInstance()->toggleDebugMode();
			cg::Console* console = dynamic_cast<cg::Console*>(cg::Registry::instance()->get("Console"));
			if (console)
				console->state.toggle();
		}
		if(key == GLUT_KEY_F3){
			avt::Camera* camera = dynamic_cast<avt::Camera*>(cg::Registry::instance()->get("CameraLevers"));
			camera->printDescription();
		}
 	}

	void InterfaceController::onSpecialKeyReleased(int key){}

	void InterfaceController::drawOverlay() {
		//glColor3f(0.7,0.7,0.7);
		//cg::Util::instance()->drawBitmapString("Press [C] to switch cameras.",10,52);
		//cg::Util::instance()->drawBitmapString("Press [ESC] to exit.",10,38);
		//cg::Util::instance()->drawBitmapString("Press [F1] to dump registry to logfile.",10,24);
		//cg::Util::instance()->drawBitmapString("Press [F2] to toggle debug mode.",10,10);
	}
}