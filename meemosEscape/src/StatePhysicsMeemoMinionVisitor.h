#ifndef _StatePhysicsMeemoMinionVisitor_avt_
#define _StatePhysicsMeemoMinionVisitor_avt_

#include "StatePhysicsMeemoMinionMoveToPath.h"
#include "StatePhysicsMeemoMinionTurnHop.h"
#include "StatePhysicsMeemoMinionFall.h"
#include "StatePhysicsMeemoMinionIdle.h"
#include "StatePhysicsMeemoMinionHit.h"
#include "StatePhysicsMeemoMinionWait.h"
#include "StatePhysicsMeemoMinionPullLever.h"

namespace avt{
	// Visitor Pattern
	class StatePhysicsMeemoMinionMoveToPath;
	class StatePhysicsMeemoMinionTurnHop;
	class StatePhysicsMeemoMinionFall;
	class StatePhysicsMeemoMinionIdle;
	class StatePhysicsMeemoMinionHit;
	class StatePhysicsMeemoMinionWait;
	class StatePhysicsMeemoMinionPullLever;

	class StatePhysicsMeemoMinionVisitor{
	public:
		virtual void start(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath) = 0;
		virtual void start(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop) = 0;
		virtual void start(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall) = 0;
		virtual void start(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle) = 0;
		virtual void start(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit) = 0;
		virtual void start(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait) = 0;
		virtual void start(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever) = 0;
		virtual void step(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait,double elapsedSeconds) = 0;
		virtual void step(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever,double elapsedSeconds) = 0;
	};
}

#endif //_StatePhysicsMeemoMinionVisitor_avt_