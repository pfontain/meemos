#include "MaterialDynamic.h"

namespace avt{
	void MaterialDynamic::init(){}

	void MaterialDynamic::draw(){
		glMaterialfv(GL_FRONT,GL_EMISSION,mat_emission);
		glMaterialfv(GL_FRONT,GL_AMBIENT,mat_ambient);
		glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_diffuse);
		glMaterialfv(GL_FRONT,GL_SPECULAR,mat_specular);
		glMaterialfv(GL_FRONT,GL_SHININESS,mat_shininess);
	}
}