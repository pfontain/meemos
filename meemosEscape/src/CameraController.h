#ifndef _CameraController_h_
#define _CameraController_h_

#include "cg/cg.h"
#include "AbstractLevel.h"

namespace avt{
	class CameraController: public cg::Entity,
		public cg::IKeyboardEventListener
	{
	public:
		CameraController(AbstractLevel* level);
		~CameraController();
		void init();

	private:
		AbstractLevel* _level;

	public:
		void onKeyPressed(unsigned char key);
		void onKeyReleased(unsigned char key);
		void onSpecialKeyPressed(int key);
		void onSpecialKeyReleased(int key);
	};
}

#endif // _CameraController_h_