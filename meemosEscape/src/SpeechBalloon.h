#ifndef _SpeechBalloo_avt_
#define _SpeechBalloo_avt_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Physical.h"
#include "PhysicsDefault.h"
#include "Texture.h"
#include "Utilities.h"

namespace avt{
	class SpeechBalloon{
	public:
		SpeechBalloon(Physical* physical);
		~SpeechBalloon();
		void init();

	private:
		int _modelDLFirst;
		Material* _material;
		Physical* _physical;
		PhysicsDefault* _physics;
		float _ellipseA;
		float _ellipseB;
		unsigned short _numberEdges;
		std::vector<Texture> _textures;
		unsigned int _numberTextures;
		cg::Vector2d _ellipsePosition;
		cg::Vector2d _tailTipPosition;
		double _tailSize;
		unsigned int _activeTexture;
		bool _on;
		double _timer;
		double _onDuration;
		double _speechSize;

	public:
		void activate(unsigned int speechNumber);
		void turnOff(void);
		void update(double elapsedSeconds,const cg::Vector3d& observerPosition);
		void draw();

	private:
		void initTextures(void);
		void makeModels(void);
		void lightsAndModel(void);
		void makeModelBalloon(void);
		void makeModelTail(void);
		void makeModelSpeech(void);
	};
}

#endif //_SpeechBalloon_avt_