#ifndef _EventCollisionHole_h_
#define _EventCollisionHole_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionHole: public EventCollision
	{	
	public:
		EventCollisionHole(){}
		EventCollisionHole(meemo::Location location): EventCollision(location){}

	public:
		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionHole";
			if(isLocationSet())
			{
				stringStream << "[" << _location << "]";
			}
			return stringStream.str();
		}

		EventType::Type type(){
			return EventType::COLLISION_HOLE;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionHole(_location);
			else
				return new EventCollisionHole();
		}
	};
}

#endif