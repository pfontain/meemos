#include "StatePhysicsMeemoCaptainFall.h"

namespace avt{
	void StatePhysicsMeemoCaptainFall::step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds){
		statePhysicsMeemoCaptainVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoCaptainFall::destroy(){}

	bool StatePhysicsMeemoCaptainFall::canInterrupt(){
		return false;
	}
}