#include "StatePhysicsMeemoMinionIdle.h"

namespace avt{
	void StatePhysicsMeemoMinionIdle::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		statePhysicsMeemoMinionVisitor->start(this);
	}

	void StatePhysicsMeemoMinionIdle::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionIdle::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){}

	bool StatePhysicsMeemoMinionIdle::canInterrupt(){
		return true;
	}
}