#include "MemoryStorage.h"

namespace meemo{
	MemoryStorage::MemoryStorage(){
		_retrievedMemoryTracesInShortTermMemory = new std::list<MemoryTrace*>();
		_shortTermMemoryDuration = cg::Properties::instance()->getInt("SHORT_TERM_MEMORY_DURATION");
	}

	MemoryStorage::~MemoryStorage(){
		std::list<MemoryTrace*>::iterator iteratorMemoryTraces;
		iteratorMemoryTraces = _memoryTraces.begin();
		for(; iteratorMemoryTraces != _memoryTraces.end(); iteratorMemoryTraces++){
			MemoryTrace* memoryTrace = *iteratorMemoryTraces;
			delete memoryTrace;
		}
		Utilities::destroy(_retrievedMemoryTracesInShortTermMemory);
	}

	std::list<MemoryTrace*>::iterator MemoryStorage::getMemoryTracesIterator(){
		return _memoryTraces.begin();
	}

	std::list<MemoryTrace*>::iterator MemoryStorage::getMemoryTracesEnd(){
		return _memoryTraces.end();
	}

	void MemoryStorage::addMemoryTrace(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion){
		if(event->type() != EventType::MEMORY_RETRIEVAL){
			appraisal::Event* copyEvent;
			appraisal::ActiveEmotion* copyCausedEmotion;
			long currentTime;

			copyEvent = event->copy();
			copyCausedEmotion = new appraisal::ActiveEmotion(*causedEmotion);
			currentTime = Utilities::Time::getTime();
			MemoryTrace* memoryTrace = new MemoryTrace(copyEvent,copyCausedEmotion,currentTime);
			_memoryTraces.push_back(memoryTrace);
		}
	}

	void MemoryStorage::addRetrievedMemoryTraceToShortTermMemory(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion){
		long currentTime = Utilities::Time::getTime();
		MemoryTrace* memoryTrace = new MemoryTrace(event,causedEmotion,currentTime);
		_retrievedMemoryTracesInShortTermMemory->push_back(memoryTrace);
	}

	std::list<MemoryTrace*>* MemoryStorage::getRetrievedMemoryTracesInShortTermMemory(void){
		lazyUpdateShortTermMemory();
		return _retrievedMemoryTracesInShortTermMemory;
	}

	const std::string MemoryStorage::toString(){
		std::stringstream stringStream;
		stringStream << "(MemoryStorage: ";
		stringStream << _memoryTraces;
		stringStream << ")";
		return stringStream.str();
	}

	void MemoryStorage::lazyUpdateShortTermMemory(void){
		std::list<MemoryTrace*>::iterator iteratorMemoryTraces;
		iteratorMemoryTraces = _retrievedMemoryTracesInShortTermMemory->begin();
		while(iteratorMemoryTraces != _retrievedMemoryTracesInShortTermMemory->end()){
			MemoryTrace* memoryTrace = *iteratorMemoryTraces;
			long memoryTraceTime = memoryTrace->getTime();
			long currentTime = Utilities::Time::getTime();
			long elapsedTime = currentTime - memoryTraceTime;
			if(elapsedTime > _shortTermMemoryDuration){
				std::list<MemoryTrace*>::iterator erasedMemoryTracePosition = iteratorMemoryTraces;
				iteratorMemoryTraces++;
				delete memoryTrace;
				_retrievedMemoryTracesInShortTermMemory->erase(erasedMemoryTracePosition);
			}
			else
				iteratorMemoryTraces++;
		}
	}
}