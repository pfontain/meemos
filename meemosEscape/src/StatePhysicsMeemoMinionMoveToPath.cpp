#include "StatePhysicsMeemoMinionMoveToPath.h"

namespace avt{
	StatePhysicsMeemoMinionMoveToPath::StatePhysicsMeemoMinionMoveToPath(UpdatePathAgent* meemoMinion,std::list<meemo::Location*>* locations){
		_path = new Path(locations);
		_moving = false;
		_restartable = true;
		_started = false;
		//_collidedNearToTarget = false;
		_meemoMinion = meemoMinion;
	}

	Path* StatePhysicsMeemoMinionMoveToPath::getPath(void) const{
		return _path;
	}

	bool StatePhysicsMeemoMinionMoveToPath::isMoving(void) const{
		return _moving;
	}
	
	void StatePhysicsMeemoMinionMoveToPath::setMoving(bool movingValue){
		_moving = movingValue;
	}

	////bool StatePhysicsMeemoMinionMoveToPath::hasCollidedNearToTarget(void) const{
	////	return _collidedNearToTarget;
	////}

	//void StatePhysicsMeemoMinionMoveToPath::setCollidedNearToTarget(void){
	//	_collidedNearToTarget = true;
	//}

	void StatePhysicsMeemoMinionMoveToPath::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		if(_started){
			_path = _meemoMinion->updatePath(_path);
			_moving = false;
			statePhysicsMeemoMinionVisitor->start(this);
		}
		else{
			statePhysicsMeemoMinionVisitor->start(this);
			_started = true;
		}
	}

	void StatePhysicsMeemoMinionMoveToPath::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionMoveToPath::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){
		delete _path;
	}

	bool StatePhysicsMeemoMinionMoveToPath::canInterrupt(void){
		return true;
	}
}