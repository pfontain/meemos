#ifndef _MenuController_h_
#define _MenuController_h_

#include "cg/cg.h"
#include "MeemoApplication.h"
#include "Texture.h"

namespace avt{
	class MeemoApplication;

	class MenuController: 
		public cg::Entity,
		public cg::IKeyboardEventListener,
		public cg::IDrawOverlayListener
	{
	public:
		MenuController(MeemoApplication* application,bool menuOn = false);
		~MenuController();
		void init(); 
		void drawOverlay();

	private:
		MeemoApplication* _application;

	private:
		void onKeyPressed(unsigned char key);
		void onKeyReleased(unsigned char key);
		void onSpecialKeyPressed(int key);
		void onSpecialKeyReleased(int key);
		Texture _texture;
		bool _menuOn;
	};
}

#endif