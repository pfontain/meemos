#include "BehaviourMeemoMinionNoMemoryRandomized.h"

namespace meemo{
	BehaviourMeemoMinionNoMemoryRandomized::BehaviourMeemoMinionNoMemoryRandomized(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgent* meemoMinionWithMemory,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic): BehaviourMeemoMinionNoMemory(meemoMinionEmotionalAgent,meemoMinionWithMemory,level,gameLogic){
		_thoughtBalloonDisplayInterval = cg::Properties::instance()->getInt("THOUGHT_BALLOON_DISPLAY_INTERVAL");
		_thoughtBalloonDisplayDuration = cg::Properties::instance()->getInt("THOUGHT_BALLOON_DISPLAY_DURATION");
		_thoughtBalloonDisplayVariation = cg::Properties::instance()->getInt("THOUGHT_BALLOON_DISPLAY_VARIATION");

		_randomThoughtBalloonDisplayInterval = Utilities::Math::randomFloat(0,_thoughtBalloonDisplayInterval);
		_randomThoughtBalloonDisplayDuration = Utilities::Math::randomFloat(0,_thoughtBalloonDisplayDuration);

		//if(rand()%2 == 0)
			_thoughtBalloonState = avt::ThoughtBalloonState::ANVIL_HIT;
		//else
		//	_thoughtBalloonState = avt::ThoughtBalloonState::TRAPDOOR_FALL;

		_thoughtBalloonTimer = 0.0;
	}

	void BehaviourMeemoMinionNoMemoryRandomized::update(double elapsedSeconds){
		updateMoodColour();
		updateFace();
		updateThoughtBalloon(elapsedSeconds);
	}

	void BehaviourMeemoMinionNoMemoryRandomized::updateThoughtBalloon(double elapsedSeconds){
		_thoughtBalloonTimer = _thoughtBalloonTimer + elapsedSeconds;

		if(_thoughtBalloonTimer > _randomThoughtBalloonDisplayInterval + _randomThoughtBalloonDisplayDuration){
			_thoughtBalloonTimer = _thoughtBalloonTimer - (_randomThoughtBalloonDisplayInterval + _randomThoughtBalloonDisplayDuration);

			setRandomThoughtBalloonDisplayInterval();
			setRandomThoughtBalloonDisplayDuration();
			
//			if(rand()%2 == 0)
				_thoughtBalloonState = avt::ThoughtBalloonState::ANVIL_HIT;
			//else
			//	_thoughtBalloonState = avt::ThoughtBalloonState::TRAPDOOR_FALL;

			_emotionalAgent->setStateThoughtBalloon(avt::ThoughtBalloonState::OFF);
		}
		else if(_thoughtBalloonTimer > _randomThoughtBalloonDisplayInterval){
			_emotionalAgent->setStateThoughtBalloon(_thoughtBalloonState);
		}
		else{ // _thoughtBalloonTimer < _randomThoughtBalloonDisplayInterval
			_emotionalAgent->setStateThoughtBalloon(avt::ThoughtBalloonState::OFF);
		}
	}

	void BehaviourMeemoMinionNoMemoryRandomized::setRandomThoughtBalloonDisplayInterval(void){
		float intervalVariation = Utilities::Math::randomFloat(-_thoughtBalloonDisplayVariation,+_thoughtBalloonDisplayVariation);
		_randomThoughtBalloonDisplayInterval = _thoughtBalloonDisplayInterval + intervalVariation;
	}

	void BehaviourMeemoMinionNoMemoryRandomized::setRandomThoughtBalloonDisplayDuration(void){
		float durationVariation = Utilities::Math::randomFloat(-_thoughtBalloonDisplayVariation,+_thoughtBalloonDisplayVariation);
		_randomThoughtBalloonDisplayDuration = _thoughtBalloonDisplayDuration + durationVariation;
	}
}