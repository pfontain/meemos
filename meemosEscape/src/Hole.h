#ifndef _Hole_avt_
#define _Hole_avt_

#include "cg/cg.h"
#include "Debugable.h"
#include "PhysicsHole.h"
#include "Utilities.h"
#include "DebugRegistry.h"

namespace avt{

	class Hole : public cg::Entity,
		public cg::IDrawListener,
		public Debugable
	{

	public:
		Hole(int x, int y,float size,const std::string& id);
		~Hole();
		virtual void init();
		virtual void draw();

	private:
		float _size;
		Physics* _physics;

	private:
		Physics* getPhysics();
	};
}


#endif