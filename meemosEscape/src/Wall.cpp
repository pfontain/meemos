#include "Wall.h"

namespace avt{
	Wall::Wall(int x, int y,float size,float height,const std::string& id): cg::Entity(id){
		_size = size;
		_height = height;
		_physics = new PhysicsWall();
		_physics->setPosition(x,y,0);
		_material = new MaterialStatic();
	}

	Wall::~Wall(){
		delete _physics;
		delete _material;
	}

	void Wall::init() {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("WALL_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->setDiffuse(colour[0],colour[1],colour[2],1.0);
		_material->init();

		std::string debugLevelWallFileName = cg::Properties::instance()->getString("WALL_TEXTURE");
		_texture.open(debugLevelWallFileName);

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);

		makeModel();
		DebugRegistry::getInstance()->add(this);
	}

	void Wall::draw() {
		glPushMatrix();
		{
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
		drawDebug();
	}

	void Wall::makeModel()
	{
		float halfSideSize = _size/2.0;

		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			//glEnable(GL_TEXTURE_2D);
			//glBindTexture(GL_TEXTURE_2D, _texture.id);
			glBegin(GL_QUADS);
			{
				//face cima
				glNormal3f(0,0,1);
				//glTexCoord2f(0,0);
				glVertex3f(getX() - halfSideSize, getY() - halfSideSize, _height);
				//glTexCoord2f(1,0);
				glVertex3f(getX() + halfSideSize , getY() - halfSideSize, _height);
				//glTexCoord2f(1,1);
				glVertex3f(getX() + halfSideSize , getY() + halfSideSize, _height);
				//glTexCoord2f(0,1);
				glVertex3f(getX() - halfSideSize , getY() + halfSideSize, _height);

				//face tras
				glNormal3f(-1,0,0);
				//glTexCoord2f(0,0);
				glVertex3f(getX() - halfSideSize , getY() + halfSideSize,-_height);
				//glTexCoord2f(1,0);
				glVertex3f(getX() - halfSideSize , getY() - halfSideSize,-_height);
				//glTexCoord2f(1,1);
				glVertex3f(getX() - halfSideSize , getY() - halfSideSize,_height);
				//glTexCoord2f(0,1);
				glVertex3f(getX() - halfSideSize , getY() + halfSideSize,_height);

				//face frente
				glNormal3f(1,0,0);
				//glTexCoord2f(0,0);
				glVertex3f(getX() + halfSideSize , getY() - halfSideSize,-_height);
				//glTexCoord2f(1,0);
				glVertex3f(getX() + halfSideSize , getY() + halfSideSize,-_height);
				//glTexCoord2f(1,1);
				glVertex3f(getX() + halfSideSize , getY() + halfSideSize,_height);
				//glTexCoord2f(0,1);
				glVertex3f(getX() + halfSideSize , getY() - halfSideSize,_height);	

				//face direita
				glNormal3f(0,-1,0);
				//glTexCoord2f(0,0);
				glVertex3f(getX() - halfSideSize , getY() - halfSideSize,-_height);
				//glTexCoord2f(1,0);
				glVertex3f(getX() + halfSideSize , getY() - halfSideSize,-_height);
				//glTexCoord2f(1,1);
				glVertex3f(getX() + halfSideSize, getY() - halfSideSize,_height);
				//glTexCoord2f(0,1);
				glVertex3f(getX() - halfSideSize, getY() - halfSideSize,_height);

				//face esquerda
				glNormal3f(0,1,0);
				//glTexCoord2f(0,0);
				glVertex3f(getX() + halfSideSize , getY() + halfSideSize,-_height);
				//glTexCoord2f(1,0);
				glVertex3f(getX() - halfSideSize , getY() + halfSideSize,-_height);
				//glTexCoord2f(1,1);
				glVertex3f(getX() - halfSideSize, getY() + halfSideSize,_height);
				//glTexCoord2f(0,1);
				glVertex3f(getX() + halfSideSize, getY() + halfSideSize,_height);
			}
			glEnd();
			//glDisable(GL_TEXTURE_2D);
		}
		glEndList();
	}

	Physics* Wall::getPhysics(void){
		return _physics;
	}
}