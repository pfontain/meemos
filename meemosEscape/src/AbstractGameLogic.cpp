#include "AbstractGameLogic.h"

namespace avt{
	AbstractGameLogic::AbstractGameLogic(void){
		_meemoCaptainAlive = true;
		_numberMeemoMinionsAtFinishLine = 0;
	}

	std::set<AbstractMeemo*>* AbstractGameLogic::getMeemoMinionsAlive(){
		return &_meemoMinionsAlive;
	}

	void AbstractGameLogic::addMeemoMinion(AbstractMeemo* meemoMinion){
		_meemoMinionsAlive.insert(meemoMinion);
	}

	void AbstractGameLogic::setMeemoCaptain(AbstractMeemo* meemoCaptain){
		_meemoCaptain = meemoCaptain;
	}

	unsigned int AbstractGameLogic::getNumberMeemosAlive(void) const{
		unsigned int numberMeemosAlive;
		numberMeemosAlive = (unsigned int)(_meemoMinionsAlive.size());
		if(_meemoCaptainAlive)
			numberMeemosAlive++;
		return numberMeemosAlive;
	}

	unsigned int AbstractGameLogic::getNumberMeemoMinionsAlive(void) const{
		unsigned int numberMeemoMinionsAlive;
		numberMeemoMinionsAlive = (unsigned int)(_meemoMinionsAlive.size());
		return numberMeemoMinionsAlive;
	}

	void AbstractGameLogic::notifyDeathMeemoMinion(AbstractMeemo* meemoMinion){
		_meemoMinionsAlive.erase(meemoMinion);
	}

	void AbstractGameLogic::notifyDeathMeemoCaptain(AbstractMeemo* meemoCaptain){
		_meemoCaptainAlive = false;
	}

	bool AbstractGameLogic::isMeemoCaptainAlive(void) const{
		return _meemoCaptainAlive;
	}

	bool AbstractGameLogic::isGameLost(void) const{
		bool gameLost;
		gameLost = !_meemoCaptainAlive || _meemoMinionsAlive.size() == 0;
		return gameLost;
	}

	void AbstractGameLogic::incrementNumberMeemoMinionsAtFinishLine(void){
		_numberMeemoMinionsAtFinishLine++;
	}

	void AbstractGameLogic::setDoor(Physical* door){
		_door = door;
	}
	
	Physical* AbstractGameLogic::getDoor(void) const{
		return _door;
	}

	void AbstractGameLogic::setLever(Physical* lever){
		_lever = lever;
	}

	Physical* AbstractGameLogic::getLever(void) const{
		return _lever;
	}

	void AbstractGameLogic::setDoorOpened(bool doorOpened){
		_doorOpened = doorOpened;
	}

	bool AbstractGameLogic::isDoorOpened(void) const{
		return _doorOpened;
	}
}