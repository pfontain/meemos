#include "EventNotifier.h"

namespace avt{

	EventNotifier::~EventNotifier(){

		std::list<std::pair<Event*,short>*>::iterator iteratorEvent = _activeEvents.begin();
		for(;iteratorEvent != _activeEvents.end(); iteratorEvent++){
			Event* event = (*iteratorEvent)->first;
			event->destroy();
			delete event;
			delete (*iteratorEvent);
		}
	}


	void EventNotifier::registerSubscriber(EventSubscriber* eventSubscriber){
		_eventSubscribers.push_back(eventSubscriber);
	}

	void EventNotifier::unregisterSubscriber(EventSubscriber* eventSubscriber){
		_eventSubscribers.remove(eventSubscriber);
	}

	std::pair<Event*,short>* EventNotifier::searchActivePairEvent(Event* event){

		std::list<std::pair<Event*,short>*>::iterator iteratorEvent = _activeEvents.begin();
		for(;iteratorEvent != _activeEvents.end(); iteratorEvent++){
			Event* activeEvent = (*iteratorEvent)->first;
			if(activeEvent->equal(event))
				return (*iteratorEvent);
		}

		return NULL;
	}

	bool EventNotifier::notify(Event* event){
		bool isEventNew;
		std::pair<Event*,short>* pairEvent;

		pairEvent = searchActivePairEvent(event);
		if(pairEvent == NULL){
  			isEventNew = true;
			_activeEvents.push_back(new std::pair<Event*,short>(event,1));

			std::list<EventSubscriber*>::iterator iteratorEventSubscriber = _eventSubscribers.begin();
			for(;iteratorEventSubscriber != _eventSubscribers.end(); iteratorEventSubscriber++){
				Event* newEvent = event->copy();
				(*iteratorEventSubscriber)->onEvent(newEvent);
			}
		}
		else{
			isEventNew = false;
			pairEvent->second = 1;
			event->destroy();
			delete event;
		}

		return isEventNew;
	}

	void EventNotifier::step(){
		//Decreases marker and erases event if the marker is too old
		std::list<std::pair<Event*,short>*>::iterator iteratorEvent = _activeEvents.begin();
		while(iteratorEvent != _activeEvents.end()){
			((*iteratorEvent)->second)--;
			if((*iteratorEvent)->second < 0){
				Event* event = (*iteratorEvent)->first;
				event->destroy();
				delete event;
				delete (*iteratorEvent);
				iteratorEvent = _activeEvents.erase(iteratorEvent);
			}
			else
				iteratorEvent++;
		}
	}
}