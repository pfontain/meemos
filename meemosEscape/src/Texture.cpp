#include "Texture.h"

namespace avt{
	#define GL_BGR_EXT 0x80E0

	void Texture::open(const std::string fileName){
		if(TextureRegistry::getInstance()->isRegistered(fileName)){
			id = TextureRegistry::getInstance()->getId(fileName);
		}
		else{
			load(fileName.c_str());
			TextureRegistry::getInstance()->add(fileName,id);
		}
	}

	void Texture::load(const char* fileName){
		unsigned int width;
		unsigned int height;
		unsigned int offset;
		FILE *file;
		fopen_s(&file,fileName,"rb");
		fseek(file, 10, SEEK_SET);
		fread(&offset, 4, 1,  file);
		fseek(file, 18, SEEK_SET);
		fread(&width, 4, 1, file);
		fread(&height, 4, 1, file);
		unsigned char* image = new unsigned char[width*height*3];
		fseek(file, offset, SEEK_SET);
		fread(image, (width*height*3), 1, file);		
		fclose(file);

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexImage2D(GL_TEXTURE_2D,0, GL_RGB8, width, height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, image);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		delete image;
	}
}