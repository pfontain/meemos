#ifndef _DebugConsole_h_
#define _DebugConsole_h_

#include "Console.h"
#include "Camera.h"
#include "Meemo.h"
#include "MeemoCaptain.h"

namespace avt{

	class DebugConsole :
		public cg::Console,
		public cg::IUpdateListener
	{
	public:
		DebugConsole( const std::string& id );
		void update(unsigned long elapsed_millis);
		void drawOverlay();
		void displayText();
	};

}

#endif