#include "Level.h"

namespace avt{

	Level::Level(GameApplication* application): AbstractLevel("Level",application){
		_gameLogic = new GameLogic();
	}

	Level::~Level(void){
		delete _gameLogic;
	}

	GameLogic* Level::getGameLogic() const{
		return _gameLogic;
	}

	void Level::add(Camera* camera){
		AbstractLevel::add(camera);
	}
	
	void Level::add(Scene* scene){
		AbstractLevel::add(scene);
	}

	void Level::add(cg::Entity* entity){
		AbstractLevel::add(entity);
	}

	void Level::add(MeemoCaptain* meemoCaptain){
		_application->addLevelEntity(meemoCaptain);
		_meemoCaptain = meemoCaptain;
		_meemos.push_back(meemoCaptain);
		_entities.push_back(meemoCaptain);
		_gameLogic->setMeemoCaptain(meemoCaptain);
	}

	void Level::add(MeemoMinion* meemoMinion){
		_application->addLevelEntity(meemoMinion);
		_meemoMinions.push_back(meemoMinion);
		_meemos.push_back(meemoMinion);
		_entities.push_back(meemoMinion);
		_gameLogic->addMeemoMinion(meemoMinion);
	}

	void Level::createCameras(void){
		std::stringstream cameraName;
		unsigned int i = 1;
		cameraName << "CAMERA" << i << "_" << "NAME";
		do{
			std::stringstream cameraPositionName;
			std::stringstream cameraFrontDirectionName;
			std::stringstream cameraLeftDirectionName;
			std::stringstream cameraUpDirectionName;
			
			
			cameraPositionName << "CAMERA" << i << "_" << "POSITION";
			cameraFrontDirectionName << "CAMERA" << i << "_" << "FRONT_DIRECTION";
			cameraLeftDirectionName << "CAMERA" << i << "_" << "LEFT_DIRECTION";
			cameraUpDirectionName << "CAMERA" << i << "_" << "UP_DIRECTION";

			cg::Vector3d cameraPosition(cg::Properties::instance()->getVector3d(cameraPositionName.str()));
			cg::Vector3d cameraFrontDirection(cg::Properties::instance()->getVector3d(cameraFrontDirectionName.str()));
			cg::Vector3d cameraLeftDirection(cg::Properties::instance()->getVector3d(cameraLeftDirectionName.str()));
			cg::Vector3d cameraUpDirection(cg::Properties::instance()->getVector3d(cameraUpDirectionName.str()));
			CameraFree* camera = new CameraFree(cg::Properties::instance()->getString(cameraName.str()),cameraPosition,
				cameraFrontDirection,cameraLeftDirection,cameraUpDirection);
			add(camera);

			i++;
			cameraName.str("");
			cameraName << "CAMERA" << i << "_" << "NAME";
		}while(cg::Properties::instance()->exists(cameraName.str()));
	}

	void Level::createScene(void){
		add(new Scene());
	}

	void Level::createEntities(void){
		add(new InterfaceController(_gameLogic));
		add(new CameraController(this));
		add(new DebugConsole("Console"));
		add(new Light ("Light"));

		LevelLoader levelLoader(this);
		std::string debugLevelMapFileName = cg::Properties::instance()->getString("DEBUG_LEVEL_MAP");
		levelLoader.load(debugLevelMapFileName);

		printf("Mundo Carregado\n");
		printf("Mapa carregado com sucesso\n");
	}

	void Level::registerEntities(void){
		std::list<Meemo*>::iterator iteratorMeemosA = _meemos.begin();
		for(; iteratorMeemosA != _meemos.end(); iteratorMeemosA++)
		{
			std::list<Meemo*>::iterator iteratorMeemosB = _meemos.begin();
			for(; iteratorMeemosB != _meemos.end();	iteratorMeemosB++)
				if(iteratorMeemosB != iteratorMeemosA)
				{
					PhysicsMeemo* physics = static_cast <PhysicsMeemo*>((*iteratorMeemosA)->getPhysics());
					(*iteratorMeemosB)->subscribe(physics);
				}
		}
	}
}
