#ifndef _EventCollisionAnvil_h_
#define _EventCollisionAnvil_h_

#include "EventCollision.h"

namespace avt{
	class EventCollisionAnvil: public EventCollision{
	public:
		EventCollisionAnvil();
		EventCollisionAnvil(meemo::Location location);

	public:
		const std::string toString();
		EventType::Type type();
		Event* copy();
	};
}

#endif