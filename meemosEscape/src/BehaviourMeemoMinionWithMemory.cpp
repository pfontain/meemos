#include "BehaviourMeemoMinionWithMemory.h"

namespace meemo{
	BehaviourMeemoMinionWithMemory::BehaviourMeemoMinionWithMemory(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgentWithMemory* meemoMinionWithMemory,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic): BehaviourMeemoMinion(meemoMinionEmotionalAgent,meemoMinionWithMemory,level,gameLogic){
		_meemoMinionWithMemory = meemoMinionWithMemory;
	}

	void BehaviourMeemoMinionWithMemory::updateIntegerMap(int* integerMap,std::pair<int,int>& moveToPosition){
		updateIntegerMapAccordingToMemory(integerMap);
		//updateIntegerMapAccordingToOtherMeemos(integerMap);
	}

	avt::Path* BehaviourMeemoMinionWithMemory::updatePath(avt::Path* oldPath){
		std::pair<int,int> startPositionDiscretized;
		std::pair<int,int> moveToPositionDiscretized;
		cg::Vector3d startPosition;
		cg::Vector3d moveToPosition;

		startPosition = _physics->getPosition();
		Location* moveToLocation = oldPath->getTargetLocation();
		moveToPosition = moveToLocation->getVector3d();
		startPositionDiscretized = discretize(startPosition);
		moveToPositionDiscretized = discretize(moveToPosition);

		std::list<meemo::Location*>* pathLocations = new std::list<meemo::Location*>();
		bool pathExists;
		pathExists = planPath(startPositionDiscretized,moveToPositionDiscretized,pathLocations);
		if(pathExists){
			delete oldPath;
			return new avt::Path(pathLocations);
		}
		else{
			delete pathLocations;
			return oldPath;
		}
	}

	void BehaviourMeemoMinionWithMemory::updateIntegerMapAccordingToMemory(int* integerMap){
		MemoryStorage* memoryStorage = _meemoMinionWithMemory->getMemoryStorage();
		std::list<MemoryTrace*>::iterator memoryTracesIterator;
		memoryTracesIterator = memoryStorage->getMemoryTracesIterator();
		std::list<MemoryTrace*>::const_iterator memoryTracesEnd;
		memoryTracesEnd = memoryStorage->getMemoryTracesEnd();

		for(;memoryTracesIterator != memoryTracesEnd; memoryTracesIterator++){
			MemoryTrace* memoryTrace = *memoryTracesIterator;
			appraisal::Event* memoryTraceEvent = memoryTrace->getEvent();
			if(memoryTraceEvent->isLocationSet()){
				appraisal::ActiveEmotion* causedEmotion = memoryTrace->getCausedEmotion();
				updateIntegerMapAccordingToEvent(integerMap,memoryTraceEvent,causedEmotion);
			}
		}
	}
}