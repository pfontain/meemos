/** 
* Node.h
* 
* a-star - Raster based a* search
* Copyright (C) 2007  gscrivano & sentrycoder
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Created: 14/06/2007 
* @author: felix.fuellgraf
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* Original Code: http://code.google.com/p/a-star/
* 
* History: 
* felix.fuellgraf: 14/06/2007  - File created
* Paulo Gomes: 19/09/2009 - minor changes
*/
#include "Node.h"

namespace aStar{

	using namespace std;

	Node::Node()   // Constructor
	{
		x = 0;
		y = 0;
		F = 0;
		G = 0;
		H = 0;
		px = 0;
		py = 0;
		used=false;
	}

	Node::Node(const Node &copyin)   // Copy constructor to handle pass by value.
	{
		x = copyin.x;
		y = copyin.y;
		F = copyin.F;
		G = copyin.G;
		H = copyin.H;
		px = copyin.px;
		py = copyin.py;
		used = copyin.used;
	}

	ostream &operator<<(ostream &output, const Node &aaa)
	{
		output << "x:" << aaa.x << " y:" << aaa.y << " F:" << aaa.F << " G:" << aaa.G << " H:" << aaa.H << " parentx:" << aaa.px << " parenty:" << aaa.py << endl;
		return output;
	}

	Node& Node::operator=(const Node &rhs)
	{
		this->x = rhs.x;
		this->y = rhs.y;
		this->F = rhs.F;
		this->G = rhs.G;
		this->H = rhs.H;
		this->px = rhs.px;
		this->py = rhs.py;
		this->used = rhs.used;
		return *this;
	}

	int Node::operator==(const Node &rhs) const
	{
		if( this->x != rhs.x) return 0;
		if( this->y != rhs.y) return 0;
		if( this->F != rhs.F) return 0;
		if( this->G != rhs.G) return 0;
		if( this->H != rhs.H) return 0;
		if( this->px != rhs.px) return 0;
		if( this->py != rhs.py) return 0;
		if( this->used != rhs.used) return 0;

		return 1;
	}


	// This function is required for built-in STL list functions like sort
	/*
	Modifiziert: Sortierung nach F Wert. Aufsteigend.
	*/
	int Node::operator<(const Node &rhs) const
	{
		if( this->F < rhs.F ) return 1;
		return 0;
	}

}