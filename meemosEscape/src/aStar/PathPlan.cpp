/** 
* PathPlan.cpp
* 
* a-star - Raster based a* search
* Copyright (C) 2007  gscrivano & sentrycoder
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Created: 14/06/2007 
* @author: felix.fuellgraf
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* Original Code: http://code.google.com/p/a-star/
* 
* History: 
* felix.fuellgraf: 14/06/2007  - File created
* Paulo Gomes: 19/09/2009 - G changed (cost to reach a node)
*/
#include "PathPlan.h"

namespace aStar{

	using namespace std;

	bool PathPlan::getPath(int* integerMap,const std::pair<int,int>& dimensions,const std::pair<int,int>& startPosition,const std::pair<int,int>& endPosition,std::vector<Node*>* outputPath)
	{
		counter = 0;
		w = dimensions.first;
		h = dimensions.second;
		int tx=targetx=endPosition.first;
		int ty=targety=endPosition.second;
		int sx = startPosition.first;
		int sy = startPosition.second;

		list<Node>act;
		list<Node>pass;

		tmap = integerMap;

		if(tmap[tx+ty*w]==0) { cout << "target is invalid." << endl; return false; }
		if(tmap[sx+sy*w]==0) { cout << "starting position is invalid." << endl; return false; }

		list<Node>::iterator ci;

		Node A;
		A.x = sx;
		A.y = sy;
		A.F = prune(sx,sy,tx,tx)+tmap[sx+sy*w];
		A.G = 0;
		A.H = prune(sx,sy,tx,tx);
		A.px = sx;
		A.py = sy;
		act.push_back(A);

		bool found = false;

		while(!found && !act.empty())
		{
			if(counter!=0)
			{
				act.sort();
				A = act.front();
				sx= A.x;
				sy= A.y;
			}

			Node B;
			B = expand(sx,sy,sx+1,sy, A.G);
			if(B.used) act.push_back(B);

			Node C;
			C = expand(sx,sy,sx-1,sy, A.G);
			if(C.used) act.push_back(C);

			Node D;
			D = expand(sx,sy,sx,sy+1, A.G);
			if(D.used) act.push_back(D);

			Node F;
			F = expand(sx,sy,sx,sy-1, A.G);
			if(F.used) act.push_back(F);


			pass.push_back(A);
			act.remove(A);
			tmap[sx+sy*w] = 0;

			//if(counter>30)break;

			if(A.x==tx && A.y==ty) found=true;

			counter++;
		}

		if(!found){
			cout << "No Path..\n";
			return false;
		}
		else{
			// Path Reconstruction
			list<Node*> path;
			bool wayok = false;
			while(!wayok)
			{
				Node A;
				for(ci=pass.begin(); ci!=pass.end(); ci++)
				{
					A = *ci;
					//cout << A << sx <<endl;
					if(A.x==sx && A.y==sy)
					{
						path.push_front(new Node(A));
						sx = A.px;
						sy = A.py;
						break;
					}
				}
				//Anfang gefunden. Knoten verweist auf sich selbst.
				if(sx==A.x && sy==A.y) { wayok = true;  }
			}
			list<Node*>::iterator iteratorPath;
			for(iteratorPath = path.begin(); iteratorPath != path.end(); iteratorPath++)
				outputPath->push_back(*iteratorPath);
			return true;
		}
	}


	double PathPlan::prune(int sx, int sy, int tx, int ty)
	{
		if(sx>=0 && sx<w && tx>=0 && tx<w && sy>=0 && sy<h && ty>=0 && ty<h )
		{
			double a;
			double b;
			a = (double)sx - (double)tx;
			b = (double)sy - (double)ty;
			return (int(sqrt(a*a + b*b)));
		}
		else return w*h;
	}



	bool PathPlan::enter(int sx, int sy, int tx, int ty)
	{

		if(sx>=0 && sx<w && tx>=0 && tx<w && sy>=0 && sy<h && ty>=0 && ty<h)
		{
			if(tmap[tx+ty*w]>0 && tmap[sx+sy*w]>0)
			{
				if( dif(sx,tx)<=1 && dif(sy, ty)<=1 )
				{
					return true;
				}
			}
		}
		return false;
	}

	int PathPlan::dif(int w1, int w2)
	{
		int temp = w1 - w2;
		if(temp<0) temp *= -1;
		return temp;
	}

	Node PathPlan::expand(int sx, int sy, int x, int y, double cost)
	{
		Node B;
		if(enter(sx,sy,x,y))
		{
			B.x = x;
			B.y = y;
			B.G = tmap[x+(y)*w] + cost;
			B.H = prune(x,y,targetx,targety);
			B.F = B.H+B.G;
			B.px = sx;
			B.py = sy;
			B.used = true;

		}
		return B;
	}
}