/** 
* PathPlan.h
* 
* a-star - Raster based a* search
* Copyright (C) 2007  gscrivano & sentrycoder
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Created: 14/06/2007 
* @author: felix.fuellgraf
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* Original Code: http://code.google.com/p/a-star/
* 
* History: 
* felix.fuellgraf: 14/06/2007  - File created
* Paulo Gomes: 19/09/2009 - G changed (cost to reach a node)
*/
#include <vector>
#include <list>
#include <iostream>
#include <math.h>
#include <utility>
#include "Node.h"
#include "Map.h"

namespace aStar{
	class PathPlan
	{
	private:
		int dif(int w1, int w2);
		bool enter(int sx, int sy, int tx, int ty);
		double prune(int sx, int sy, int tx, int ty);
		int w;
		int h;
		int *tmap;
		Node expand(int sx, int sy, int x, int y, double cost);
		int targetx;
		int targety;

	public:
		const static int MOVE_COST_OBSTACLE = 0;

		bool getPath(int* integerMap,const std::pair<int,int>& dimensions,const std::pair<int,int>& startPosition,const std::pair<int,int>& endPosition,std::vector<Node*>* outputPath);
		int counter;
	};
}