/** 
* Node.h
* 
* a-star - Raster based a* search
* Copyright (C) 2007  gscrivano & sentrycoder
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Created: 14/06/2007 
* @author: felix.fuellgraf
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* Original Code: http://code.google.com/p/a-star/
* 
* History: 
* felix.fuellgraf: 14/06/2007  - File created
* Paulo Gomes: 19/09/2009 - minor changes
*/
#include <iostream>

namespace aStar{

	class Node
	{
		friend std::ostream &operator<<(std::ostream &, const Node &);

	public:
		int x;
		int y;
		double F; // heuristik F = G+H (bewegungskosten+luftlinie)
		double G;
		double H;
		int px; //parent
		int py;
		bool used;

		Node();
		Node(const Node &);
		~Node(){};
		Node &operator=(const Node &rhs);
		int operator==(const Node &rhs) const;
		int operator<(const Node &rhs) const;
	};
}