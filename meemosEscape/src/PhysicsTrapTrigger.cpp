#include "PhysicsTrapTrigger.h"

namespace avt{
	EventCollision* PhysicsTrapTrigger::getCollisionEvent(void){
		meemo::Location location(_position[0],_position[1]);
		return new EventCollisionTrapTrigger(location);
	}

	bool PhysicsTrapTrigger::isColliding(avt::Physics *collider){
		using namespace Utilities::Math;
		cg::Vector3d distanceToCenter = collider->getPosition() - this->getPosition();
		float distanceToCenterLength = length(distanceToCenter);
		return distanceToCenterLength + collider->getBoundingSphereRadius()/2.0 < this->_boundingSphereRadius;
	}

	std::list<EventCollision*>* PhysicsTrapTrigger::getCollisions(void){
		return Physics::getCollisions();
	}
}