#include "MeemoCaptain.h"

namespace avt{
	MeemoCaptain::MeemoCaptain(float x, float y,int size,AbstractLevel* level,AbstractGameLogic* gameLogic): Meemo("MeemoCaptain",size,level,gameLogic){
		_physicsMeemoCaptain = new PhysicsMeemoCaptain();
		_physics = _physicsMeemoCaptain;
		_physics->setPosition(x, y, 0);

		_behaviour = new meemo::Behaviour(this);
		_mindWithMemory = new meemo::MindWithMemory(this,this);
		_mind = _mindWithMemory;
	}

	MeemoCaptain::~MeemoCaptain(void){
		delete _physicsMeemoCaptain;
		delete _behaviour;
		delete _mindWithMemory;
	}

	void MeemoCaptain::init(void){
		Meemo::init();

		initMaterial();
		_alive = true;
		_actionIdLock = false;
		_actionLock = false;
		_action = MeemoAction::NONE;
		_potentialAction = MeemoAction::NONE;
		_actionMeemoId = 0;
		_potentialActionMeemoId = 0;
	}

	int MeemoCaptain::getAction(void){
		return _action;
	}

	int MeemoCaptain::getActionId(void){
		return _actionMeemoId;
	}

	void MeemoCaptain::update(unsigned long elapsed_millis) {
		Meemo::update(elapsed_millis);

		_action = MeemoAction::NONE;
		_actionMeemoId = 0;

		Event* event;
		if(!_eventBuffer->empty()){
			event = _eventBuffer->pop();
			update(event);
		}
		if(_alive){
			updateKeyboardActions();
			updateMinionNumberCommand();
			updateActionCommand();
			updateCompleteCommandCheck();
		}

		double elapsedSeconds = elapsed_millis / (double)1000;
		_physics->step(elapsedSeconds);
	}

	void MeemoCaptain::draw(void){
		glPushMatrix();{
			_physics->applyTransforms();
			_eyes->draw();
			_mouth->draw();
			// TODO: remove code
			//_material->setAmbient(0.99,0.39,0.0,1.0);
			//_material->setDiffuse(0.99,0.39,0.0,1.0);
			_material->draw();
			glCallList(_modelDL);
		}glPopMatrix();

		_thoughtBalloon->draw();
		_speechBalloon->draw();
		drawDebug();
	}

	void MeemoCaptain::initMaterial(void){
		_materialColourFirst = cg::Properties::instance()->getVector3d("MEEMO_CAPTAIN_COLOUR_NEUTRAL");
		_materialColourSecond = cg::Properties::instance()->getVector3d("MEEMO_CAPTAIN_COLOUR_LOW_MOOD");
		Meemo::initMaterial();
	}

	void MeemoCaptain::update(Event* event){
		if(event->type() == EventType::COLLISION_ANVIL){
			EventCollisionAnvil* eventCollisionAnvil = static_cast<EventCollisionAnvil*>(event);
			trigger(eventCollisionAnvil);
		}
		else if(event->type() == EventType::WITNESS){
			EventWitness* eventWitness = static_cast<EventWitness*>(event);
			trigger(eventWitness);
		}
		else if(event->type() == EventType::COLLISION_TRAP_DOOR){
			EventCollisionTrapDoor* eventCollisionTrapDoor = static_cast<EventCollisionTrapDoor*>(event);
			trigger(eventCollisionTrapDoor);
		}
		else if(event->type() == EventType::COLLISION_HOLE){
			EventCollisionHole* eventCollisionHole = static_cast<EventCollisionHole*>(event);
			trigger(eventCollisionHole);
		}
		else if(event->type() == EventType::SEE_OPEN_DOOR){
			EventSeeOpenDoor* eventSeeOpenDoor = static_cast<EventSeeOpenDoor*>(event);
			trigger(eventSeeOpenDoor);
		}
		else if(event->type() == EventType::SEE_DOOR){
			EventSeeDoor* eventSeeDoor = static_cast<EventSeeDoor*>(event);
			trigger(eventSeeDoor);
		}
		else if(event->type() == EventType::SEE_LEVER){
			EventSeeLever* eventSeeLever = static_cast<EventSeeLever*>(event);
			trigger(eventSeeLever);
		}
	}

	void MeemoCaptain::trigger(EventWitness* eventWitness){
		if(eventWitness->getWitnessedEventType() != EventType::COLLISION_DEFAULT){
			Event* witnessedEvent = eventWitness->getWitnessedEvent();
			if(witnessedEvent->isLocationSet())
			{
				meemo::Location witnessedEventLocation = witnessedEvent->getLocation();
				cg::Vector3d witnessedEventLocationVector = witnessedEventLocation.getVector3d();
				_physicsMeemoCaptain->turnHop(witnessedEventLocationVector);
			}
		}
	}

	void MeemoCaptain::trigger(EventCollisionAnvil* eventCollisionAnvil){
		_physicsMeemoCaptain->hit();
	}

	void MeemoCaptain::trigger(EventCollisionTrapDoor* eventCollisionTrapDoor){
		_physicsMeemoCaptain->fall();
		_alive = false;
		_mind->setDead(true);
		_gameLogic->notifyDeathMeemoCaptain(this);
	}

	void MeemoCaptain::trigger(EventCollisionHole* eventCollisionHole){
		_physicsMeemoCaptain->fall();
		_alive = false;
		_mind->setDead(true);
		_gameLogic->notifyDeathMeemoCaptain(this);
	}

	void MeemoCaptain::trigger(EventSeeOpenDoor* eventSeeOpenDoor){}
	void MeemoCaptain::trigger(EventSeeDoor* eventSeeDoor){}
	void MeemoCaptain::trigger(EventSeeLever* eventSeeLever){}

	void MeemoCaptain::updateKeyboardActions(void){
		if(cg::KeyBuffer::instance()->isKeyDown('w')) {
			_physics->goAhead();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('s')) {
			_physics->goBack();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('a')) {
			_physics->goLeft();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('d')) {
			_physics->goRight();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('q')) {
			_physics->yawLeft();
		}
		if(cg::KeyBuffer::instance()->isKeyDown('e')) {
			_physics->yawRight();
		}
		if(cg::KeyBuffer::instance()->isKeyDown(' ')){
			_physicsMeemoCaptain->pullLever();
		}
	}

	void MeemoCaptain::updateMinionNumberCommand(void){
		for(char minionNumber = '1';minionNumber <= '9'; minionNumber++){
			if(cg::KeyBuffer::instance()->isKeyDown(minionNumber) && (!_actionIdLock)){
				_potentialActionMeemoId = (int)(minionNumber - '0');
				_actionIdLock = true;
				printf("capitao: defini um minion %d\n", _potentialActionMeemoId);
			}
		}
	}

	void MeemoCaptain::updateActionCommand(void){
		if(cg::KeyBuffer::instance()->isKeyDown('j') && !_actionLock) {
			_potentialAction = MeemoAction::JUMP;
			_actionLock = true;
			printf("capitao: defini uma accao %d saltar\n", _potentialAction);
		}
		if(cg::KeyBuffer::instance()->isKeyDown('k') && !_actionLock) {
			_potentialAction = MeemoAction::COME_HERE;
			_actionLock = true;
			printf("capitao: defini uma accao %d vir\n", _potentialAction);
		}
		if(cg::KeyBuffer::instance()->isKeyDown('i') && !_actionLock) {
			_potentialAction = MeemoAction::STOP;
			_actionLock = true;
			printf("capitao: defini uma accao %d parar\n", _potentialAction);
		}
		if(cg::KeyBuffer::instance()->isKeyDown('l') && !_actionLock) {
			_potentialAction = MeemoAction::LOOK_AT_ME;
			_actionLock = true;
			printf("capitao: defini uma accao %d look at me\n", _potentialAction);
		}
	}

	void MeemoCaptain::updateCompleteCommandCheck(void){
		//miniom complete command check
		if(cg::KeyBuffer::instance()->isKeyDown('m') && _actionIdLock && _actionLock) {
			printf("capitao: defini um comando completo Meemo:%d Accao:%d\n", _potentialActionMeemoId, _potentialAction);
			_action = _potentialAction;
			_actionMeemoId = _potentialActionMeemoId;
			_actionIdLock = false;
			_actionLock = false;
			_potentialAction = MeemoAction::NONE;
			_potentialActionMeemoId=0;
			_speechBalloon->activate(_actionMeemoId);
		}
	}
}