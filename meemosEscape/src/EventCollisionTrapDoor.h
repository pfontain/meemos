#ifndef _EventCollisionTrapDoor_h_
#define _EventCollisionTrapDoor_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionTrapDoor: public EventCollision
	{	
	public:
		EventCollisionTrapDoor(){ _detectable = false;}
		EventCollisionTrapDoor(meemo::Location location): EventCollision(location){ _detectable = false;}

	public:
		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionTrapDoor";
			if(isLocationSet())
			{
				stringStream << "[" << _location << "]";
			}
			return stringStream.str();
		}

		EventType::Type type(){
			return EventType::COLLISION_TRAP_DOOR;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionTrapDoor(_location);
			else
				return new EventCollisionTrapDoor();
		}
	};
}

#endif