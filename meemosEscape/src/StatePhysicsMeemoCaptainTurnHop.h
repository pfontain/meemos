#ifndef __StatePhysicsMeemoCaptainTurnHop_avt__
#define __StatePhysicsMeemoCaptainTurnHop_avt__

#include "StatePhysicsMeemoCaptain.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt{
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptainTurnHop: public StatePhysicsMeemoCaptain{	
	public:
		void step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds);
		void destroy();
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoCaptainTurnHop_avt__
