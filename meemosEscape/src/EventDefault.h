/** 
* EventDefault.h - Concrete Default Event. Description can be any string.
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 19/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 19/06/2009 - File created.
*/
#ifndef _EventDefault_h_
#define _EventDefault_h_

#include "Event.h"

namespace appraisal{

	class EventDefault: public Event
	{	
	private:
		std::string _description;
	public:
		EventDefault(std::string description){ _description = description; }

		const std::string toString(){
			return _description;
		}

		Event* copy(){
			return new EventDefault(_description);
		}

		EventType::Type type(){
			return EventType::DEFAULT;
		}

		virtual bool equal(appraisal::Event* event){ return Event::defaultEqual(event); }

		virtual int match(appraisal::Event* event){ return Event::defaultMatch(event); }

	protected:
		void destroy(){};
	};
}

#endif