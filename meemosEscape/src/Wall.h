#ifndef _Wall_h_
#define _Wall_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Texture.h"
#include "PhysicsWall.h"
#include "DebugRegistry.h"

namespace avt{

	class Wall : public cg::Entity,
		public cg::IDrawListener,
		public Debugable
	{

	public:
		Wall(int x, int y,float size,float height,const std::string& id);
		~Wall();
		void init();
		void draw();

	private:
		int _modelDL;
		float _size;
		float _height;
		Material* _material;
		Texture _texture;
		Physics* _physics;

	private:
		void makeModel();
		Physics* getPhysics();
	};
}


#endif