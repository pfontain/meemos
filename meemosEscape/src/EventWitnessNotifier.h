#ifndef _EventWitnessNotifier_h_
#define _EventWitnessNotifier_h_

#include <list>
#include "Utilities.h"
#include "Location.h"
#include "EventNotifier.h"
#include "EventWitness.h"
#include "EventWitnessSubscriber.h"

namespace avt{
	class EventWitnessNotifier: public EventNotifier
	{	
		private:
			std::list<EventWitnessSubscriber*> _eventWitnessSubscribers;

		public:
			virtual void registerWitnessSubscriber(EventWitnessSubscriber* eventSubscriber);
			virtual void unregisterWitnessSubscriber(EventWitnessSubscriber* eventSubscriber);
			virtual bool notify(Event* event);

		private:
			bool isEventClose(meemo::Location& eventLocation,EventWitnessSubscriber* eventWitness);
			bool isEventWithinVisionAngle(meemo::Location& eventLocation,EventWitnessSubscriber* eventWitness);
			bool isEventInFront(cg::Vector3d& crossProductVisionEventDirection,float dotProductVisionEventDirection);
			bool isEventBehind(cg::Vector3d& crossProductVisionEventDirection);
			bool isAngleVisionEventDirectionSmallEnough(float dotProductVisionEventDirection,float eventWitnessVisionAngle);
			bool isEventWitnessFishEyed(float eventWitnessVisionAngle);
	};
}

#endif