#include "EventCollisionAnvil.h"

namespace avt{
	EventCollisionAnvil::EventCollisionAnvil(){
	}

	EventCollisionAnvil::EventCollisionAnvil(meemo::Location location): EventCollision(location){
	}

	const std::string EventCollisionAnvil::toString(){
		std::stringstream stringStream;
		stringStream << "EventCollisionAnvil";
		return stringStream.str();
	}

	EventType::Type EventCollisionAnvil::type(){
		return EventType::COLLISION_ANVIL;
	}

	Event* EventCollisionAnvil::copy(){
		if(isLocationSet())
			return new EventCollisionAnvil(_location);
		else
			return new EventCollisionAnvil();
	}
}