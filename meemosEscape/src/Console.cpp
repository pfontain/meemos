#include "Console.h"

GLuint cg::Console::msScrollWidgetDL = 0;
GLuint cg::Console::msResizeWidgetDL = 0;

namespace cg
{

  // ---------------------------------------------------------------
  // Constructor & Destructor
  // ---------------------------------------------------------------
  Console::Console( const std::string& id ) : cg::Entity(id)
  {
    cg::tWindow win = cg::Manager::instance()->getApp()->getWindow();
    
    mWinWidth = win.width;
    mWinHeight = win.height;

    mTitle = id;
    mPosition = cg::Vector2d(10.0, mWinHeight - 10.0);
    mSize = cg::Vector2d(mWinWidth * 0.95, mWinHeight / 2.0);
    mBgColor = cg::Vector3d(0.3, 0.3, 0.3);
    mColor = cg::Vector3d(1.0, 1.0, 1.0);
    mTBgColor = cg::Vector3d(0.0, 0.0, 0.0);
    mTColor = cg::Vector3d(1.0, 1.0, 1.0);
    mBgAlpha = 0.3;
    mAlpha = 1.0;
    mTBgAlpha = 1.0;
    mTAlpha = 1.0;
    mStartLine = 0;

    mMoving = false;
    mResizing = false;
    mMinimized = false;

    mShowTitle = true;
    mCanMove = true;
    mCanResize = true;
    mCanMinimize = true;
    mCanScroll = true;
  }
  // ---------------------------------------------------------------
  Console::~Console( void ) {}
  // ---------------------------------------------------------------
  // Entity & IDrawOverlay abstract methods override
  // ---------------------------------------------------------------
  void Console::init( void )
  {
    // create both scroll & resize widgets' display lists
    if (!msScrollWidgetDL)
    {
      msScrollWidgetDL = glGenLists(1);
      glNewList(msScrollWidgetDL, GL_COMPILE);
        glBegin(GL_TRIANGLES);
          glVertex2d(- WIDGET_ARROW_XSIZE, - WIDGET_ARROW_YSIZE);
          glVertex2d(0, - WIDGET_ARROW_YSIZE);
          glVertex2d(- (WIDGET_ARROW_XSIZE / 2.0), 0);
        glEnd();
      glEndList();
    }

    if (!msResizeWidgetDL)
    {
      msResizeWidgetDL = glGenLists(1);
      glNewList(msResizeWidgetDL, GL_COMPILE);
        glBegin(GL_LINE_STRIP);
          glVertex2d(- RESIZE_LINE_SIZE, 0);
          glVertex2d(0, 0);
          glVertex2d(0, RESIZE_LINE_SIZE);
        glEnd();
      glEndList();
    }

    refreshDrawingData();
    clearConsoleBuffer();
  }
  // ---------------------------------------------------------------
  void Console::drawOverlay( void )
  {
    if (mShowTitle) drawTitleBar();
    if (mMinimized) return;
    drawBody();
    displayText();
    if (mCanResize) drawResizeWidget();
    if (mCanScroll) drawUpDownWidgets();
  }
  // ---------------------------------------------------------------
  void Console::onReshape( int width, int height )
  {
    int dx, dy;

    // lets try to keep the console in the same screen area
    dx = (int) (( mPosition[0] / mWinWidth ) * width - mPosition[0]);
    dy = (int) (( mPosition[1] / mWinHeight ) * height - mPosition[1]);

    mWinWidth = width;
    mWinHeight = height;

    moveToRelativePos(dx, dy);
  }
  // ---------------------------------------------------------------
  // Mouse related events: move, resize, minimize
  // ---------------------------------------------------------------
  void Console::onMouse( int button, int state, int x, int y )
  {
    y = mWinHeight - y;

    // click on the title bar
    if (mShowTitle && isInArea(x, y, mTitleP1, mTitleP2))
    {
      if (mCanMove && button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
      {
        mMousePos[0] = x;
        mMousePos[1] = y;
        mMoving = true;
      }
      else if (mCanMinimize && button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
      {
        mMinimized ? mMinimized = false : mMinimized = true;
      }
    }

    // click on the resize area
    else if (mCanResize && button == GLUT_LEFT_BUTTON && state == GLUT_DOWN &&
             isInArea(x, y, mBodyP2 + cg::Vector2d(-RESIZE_LINE_SIZE, RESIZE_LINE_SIZE),
                      mBodyP2 + cg::Vector2d(OUTSIDE_BORDER_SPACE, -OUTSIDE_BORDER_SPACE)))
    {
        mMousePos[0] = x;
        mMousePos[1] = y;
        mResizing = true;
    }

    // click on the up arrow
    else if (mCanScroll && mScrollUp && button == GLUT_LEFT_BUTTON && state == GLUT_DOWN &&
             isInArea(x, y, cg::Vector2d(mBodyP2[0] - WIDGET_ARROW_XSIZE - WIDGET_SIDE_BORDER, mBodyP1[1] - WIDGET_UPPER_BORDER),
                      cg::Vector2d(mBodyP2[0] - WIDGET_SIDE_BORDER, mBodyP1[1] - WIDGET_ARROW_YSIZE - WIDGET_UPPER_BORDER)))
    {
      mStartLine++;
    }

    // click on the down arrow
    else if (mCanScroll && mScrollDown && button == GLUT_LEFT_BUTTON && state == GLUT_DOWN &&
             isInArea(x, y, cg::Vector2d(mBodyP2[0] - WIDGET_ARROW_XSIZE - WIDGET_SIDE_BORDER, mBodyP2[1] + WIDGET_ARROW_YSIZE + WIDGET_LOWER_BORDER),
                      cg::Vector2d(mBodyP2[0] - WIDGET_SIDE_BORDER, mBodyP2[1] + WIDGET_LOWER_BORDER)))
    {
      mStartLine--;
    }

    // mouse click is over, reset the console state
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
      if (mMoving)
      {
        mMoving = false;
        moveToRelativePos(x - mMousePos[0], y - mMousePos[1]);
      }
      else if (mResizing)
      {
        mResizing = false;
        resizeToRelativeSize(x - mMousePos[0], y - mMousePos[1]);
      }
    }
  }
  // ---------------------------------------------------------------
  void Console::onMouseMotion( int x, int y )
  {
    y = mWinHeight - y;

    if (mMoving)
    {
      moveToRelativePos(x - mMousePos[0], y - mMousePos[1]);
      mMousePos[0] = x;
      mMousePos[1] = y;
    }
    else if (mResizing)
    {
      resizeToRelativeSize(x - mMousePos[0], y - mMousePos[1]);
      mMousePos[0] = x;
      mMousePos[1] = y;
    }
  }
  // ---------------------------------------------------------------
  void Console::onMousePassiveMotion( int x, int y ) {}
  // ---------------------------------------------------------------
  // Helper methods
  // ---------------------------------------------------------------
  bool Console::isInArea( int x, int y, const cg::Vector2d& p1, const cg::Vector2d& p2 )
  {
    if (x > p1[0] && x < p2[0] && y < p1[1] && y > p2[1]) return true;
    else return false;
  }
  // ---------------------------------------------------------------
  void Console::moveToRelativePos( int dx, int dy )
  {
    mPosition[0] += dx;
    mPosition[1] += dy;

    if (mPosition[0] < OUTSIDE_BORDER_SPACE)
      mPosition[0] = OUTSIDE_BORDER_SPACE;

    if (mPosition[0] + mSize[0] + OUTSIDE_BORDER_SPACE > mWinWidth)
      mPosition[0] = mWinWidth - (mSize[0] + OUTSIDE_BORDER_SPACE);

    if (mPosition[1] + OUTSIDE_BORDER_SPACE > mWinHeight)
      mPosition[1] = mWinHeight - OUTSIDE_BORDER_SPACE;

    if (mPosition[1] - TITLEBAR_HEIGHT < 0)
      mPosition[1] = TITLEBAR_HEIGHT;

    refreshDrawingData();
  }
  // ---------------------------------------------------------------
  void Console::resizeToRelativeSize( int dx, int dy )
  {
    mSize[0] += dx;
    mSize[1] -= dy;

    if (mSize[0] < MIN_XSIZE)
      mSize[0] = MIN_XSIZE;

    if (mSize[1] < MIN_YSIZE)
      mSize[1] = MIN_YSIZE;

    refreshDrawingData();
  }
  // ---------------------------------------------------------------
  void Console::refreshDrawingData( void )
  {
    mTitleP1 = mPosition;
    mTitleP2 = mPosition + cg::Vector2d(mSize[0], - TITLEBAR_HEIGHT);
    mBodyP1 = mPosition + cg::Vector2d(0, - TITLEBAR_HEIGHT);
    mBodyP2 = mPosition + cg::Vector2d(mSize[0], - mSize[1]);
  }
  // ---------------------------------------------------------------
  // 'Client' methods
  // ---------------------------------------------------------------
  void Console::clearConsoleBuffer( void )
  {
    mBuffer.clear();
    mStartLine = 0;
  }
  // ---------------------------------------------------------------
  void Console::write( const std::string& str )
  {
    std::string tmp = str;
    std::string::size_type pos;

    while ((pos = tmp.find('\n')) != std::string::npos)
    {
      mBuffer.push_front(tmp.substr(0, pos));
      tmp = tmp.substr(pos + 1);
    }

    if (tmp.length() > 0)
      mBuffer.push_front(tmp);
  }
  // ---------------------------------------------------------------
  void Console::write( const char& c )
  {
    if (c == '\n' || c == 13) // 13 == Enter
    {
      mBuffer.push_front(std::string(""));
    }
    else if (c == 8) // 8 == Backspace
    {
      if (!mBuffer.empty() && !mBuffer.front().empty())
      {
        std::string tmp = mBuffer.front();
        mBuffer.pop_front();
        mBuffer.push_front(tmp.substr(0, tmp.size() - 1));
      }
    }
    else if (mBuffer.empty())
    {
      mBuffer.push_front(std::string(1, c));
    }
    else
    {
      mBuffer.front().append(1, c);
    }
  }
  // ---------------------------------------------------------------
  // Text display
  // ---------------------------------------------------------------
  void Console::displayText( void )
  {
    std::list<std::string>::iterator itr;
    cg::Vector2d cursorPoint, limit;
    int dy = +15;
    int i = 0;

    glColor4d(mColor[0], mColor[1], mColor[2], mAlpha);
    cursorPoint = cg::Vector2d(mBodyP1[0] + LEFT_MARGIN, mBodyP2[1] + LOWER_MARGIN);
    limit = mBodyP1 - UPPER_MARGIN;

    for (itr = mBuffer.begin(); itr != mBuffer.end() && cursorPoint[1] < limit[1]; itr++, i++)
    {
      if (i < mStartLine)
        continue;

      glRasterPos2d(cursorPoint[0], cursorPoint[1]);
       
      for (unsigned int j = 0; j < (*itr).length() && continueWriting(); j++)
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, (*itr)[j]);

      cursorPoint[1] += dy;
    }

    (itr == mBuffer.end() && cursorPoint[1] < limit[1]) ? mScrollUp = false : mScrollUp = true;
    mStartLine == 0 ? mScrollDown = false : mScrollDown = true;
  }
  // ---------------------------------------------------------------
  bool Console::continueWriting( void )
  {
    float rasterPos[4];

    glGetFloatv(GL_CURRENT_RASTER_POSITION, rasterPos);

    if (rasterPos[0] >= mBodyP2[0] - RIGHT_MARGIN)
    {
      // can't continue writing, display instead '...'
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, '.');
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, '.');
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, '.');

      return false;
    }

    return true;
  }
  // ---------------------------------------------------------------
  // Console Drawing
  // ---------------------------------------------------------------
  void Console::drawTitleBar( void )
  {      
    glColor4d(mTBgColor[0], mTBgColor[1], mTBgColor[2], mTBgAlpha);
    glRectdv(mTitleP1.get(), mTitleP2.get());
    glColor4d(mTColor[0], mTColor[1], mTColor[2], mTAlpha);
    glRasterPos2d(mTitleP1[0] + TITLE_LEFT_MARGIN, mTitleP2[1] + TITLE_BOTTOM_MARGIN);

    for (int i = 0; mTitle[i] != '\0' && continueWriting(); i++)
    {
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, mTitle[i]);
    }
  }
  // ---------------------------------------------------------------
  void Console::drawBody( void )
  {
    glColor4d(mBgColor[0], mBgColor[1], mBgColor[2], mBgAlpha);
    glRectdv(mBodyP1.get(), mBodyP2.get());
  }
  // ---------------------------------------------------------------
  void Console::drawResizeWidget( void )
  {
    glColor3d(0, 0, 0);
    glPushMatrix();
      glTranslated(mBodyP2[0], mBodyP2[1], 0);
      glCallList(msResizeWidgetDL);
    glPopMatrix();
  }
  // ---------------------------------------------------------------
  void Console::drawUpDownWidgets( void )
  {
    glColor3d(0, 0, 0);

    if (mScrollUp)
    {
      glPushMatrix();
        glTranslated(mBodyP2[0] - WIDGET_SIDE_BORDER, mBodyP1[1] - WIDGET_UPPER_BORDER, 0);
        glCallList(msScrollWidgetDL);
      glPopMatrix();
    }

    if (mScrollDown)
    {
      glPushMatrix();
        glTranslated(mBodyP2[0] - WIDGET_ARROW_XSIZE - WIDGET_SIDE_BORDER, mBodyP2[1] + WIDGET_LOWER_BORDER, 0);
        glRotated(180, 0, 0, 1);
        glCallList(msScrollWidgetDL);
      glPopMatrix();
    }
  }
  // ---------------------------------------------------------------  

}