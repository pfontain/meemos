#include "PhysicsRegistry.h"

namespace avt{

	PhysicsRegistry* PhysicsRegistry::_instance = 0;
	std::list<Physics*>* PhysicsRegistry::_physics = 0;

	PhysicsRegistry* PhysicsRegistry::getInstance(){ 
		if(!_instance){
			_instance = new PhysicsRegistry();
			_physics = new std::list<Physics*>();
		}
		return _instance; 
	}

	void PhysicsRegistry::deleteInstance(){
		assert(_instance);
		delete _instance;
		_instance = 0;
		delete _physics;
		_physics = 0;
	}

	bool PhysicsRegistry::isInstantiated(){
		return (_instance != 0);
	}

	std::list<Physics*>* PhysicsRegistry::getPhysicsList(void){
		assert(_instance);
		return _physics;
	}
		
    void PhysicsRegistry::add(Physics* physics){ 
		assert(_instance);
		_physics->push_back(physics);
	}

	void PhysicsRegistry::remove(Physics* physics){
		assert(_instance);
		_physics->remove(physics);
	}

	void PhysicsRegistry::clearList(){
		assert(_instance);
		_physics->clear();
	}
}