#include "DebugRegistry.h"

namespace avt{

	DebugRegistry* DebugRegistry::_instance = 0;
	std::list<Debugable*>* DebugRegistry::_debugables = 0;

	DebugRegistry* DebugRegistry::getInstance(){ 
		if(!_instance){
			_instance = new DebugRegistry();
			_debugables = new std::list<Debugable*>();
		}
		return _instance; 
	}

	void DebugRegistry::deleteInstance(){
		assert(_instance);
		delete _instance;
		_instance = 0;
		delete _debugables;
		_debugables = 0;
	}

	bool DebugRegistry::isInstantiated(){
		return (_instance != 0);
	}
		
    void DebugRegistry::add(Debugable* debugable){ 
		assert(_instance);
		_debugables->push_back(debugable);
	}

	void DebugRegistry::remove(Debugable* debugable){
		assert(_instance);
		_debugables->remove(debugable);
	}

	void DebugRegistry::clearList(){
		assert(_instance);
		_debugables->clear();
	}

	void DebugRegistry::toggleDebugMode(){
		std::list<Debugable*>::iterator iteratorDebugables;
		iteratorDebugables = _debugables->begin();
		for(;iteratorDebugables != _debugables->end();iteratorDebugables++){
			Debugable* debugable = *iteratorDebugables;
			debugable->toggleDebugMode();
		}
	}
}