#include "PhysicsTrapDoor.h"

namespace avt{
	PhysicsTrapDoor::PhysicsTrapDoor(void){
		_state = PhysicsTrapDoorState::CLOSED;
	}

	void PhysicsTrapDoor::setOpeningAngularVelocity(float openingAngularVelocity){
		_openingAngularVelocity = openingAngularVelocity;
	}

	void PhysicsTrapDoor::setClosingAngularVelocity(float closingAngularVelocity){
		_closingAngularVelocity = closingAngularVelocity;
	}

	void PhysicsTrapDoor::setOpenDuration(float openDuration){
		_openDuration = openDuration;
	}

	void PhysicsTrapDoor::setEdgeSize(float edgeSize){
		_halfEdgeSize = edgeSize / 2;
	}

	EventCollision* PhysicsTrapDoor::getCollisionEvent(){
		meemo::Location location(_position[0],_position[1]);
		return new EventCollisionTrapDoor(location);
	}

	bool PhysicsTrapDoor::isColliding(avt::Physics *collider){
		return Physics::defaultIsColliding(this,collider);
	}

	void PhysicsTrapDoor::step(double elapsedSeconds){
		switch(_state){
			case PhysicsTrapDoorState::CLOSED: stepClosed(elapsedSeconds); break;
			case PhysicsTrapDoorState::OPENING: stepOpening(elapsedSeconds); break;
			case PhysicsTrapDoorState::OPEN: stepOpen(elapsedSeconds); break;
			case PhysicsTrapDoorState::CLOSING: stepClosing(elapsedSeconds); break;
		}
		Physics::step(elapsedSeconds);
	}

	void PhysicsTrapDoor::applyTransforms(void) {
		glTranslated(_position[0]-_halfEdgeSize,_position[1]-_halfEdgeSize,_position[2]);
		glMultMatrixd(_rotationMatrix);
		glTranslated(+_halfEdgeSize,+_halfEdgeSize,0);
	}

	void PhysicsTrapDoor::transitionOpening(void){
		_state = PhysicsTrapDoorState::OPENING;
		_angularVelocity = _openingAngularVelocity;
	}

	void PhysicsTrapDoor::transitionOpen(void){
		_state = PhysicsTrapDoorState::OPEN;
		_timeCounter = _openDuration;
	}

	void PhysicsTrapDoor::transitionClosing(void){
		_state = PhysicsTrapDoorState::CLOSING;
		_angularVelocity = _closingAngularVelocity;
		_previousRotationAngleModule = 0.0;
	}

	void PhysicsTrapDoor::transitionClosed(void){
		_state = PhysicsTrapDoorState::CLOSED;
	}

	void PhysicsTrapDoor::stepOpening(double elapsedSeconds){
		double rotationAngleModule = fabs(_orientation.w);
		if(rotationAngleModule < 0.5)
			transitionOpen();
		else
			pitchDown();
	}

	void PhysicsTrapDoor::stepOpen(double elapsedSeconds){
		_timeCounter = _timeCounter - elapsedSeconds;
		if(_timeCounter < 0)
			transitionClosing();
	}

	void PhysicsTrapDoor::stepClosing(double elapsedSeconds){
		std::list<EventCollision*>* collisions = getCollisions();
		double rotationAngleModule = fabs(_orientation.w);
		if(!collisions->empty()){
			transitionOpening();
		}
		else if(rotationAngleModule < _previousRotationAngleModule){
			transitionClosed();
			_orientation.w = 1.0;
			_orientation.v[1] = 0.0;
		}
		else{
			pitchUp();
			_previousRotationAngleModule = rotationAngleModule;
		}
		Utilities::destroy(collisions);
	}

	void PhysicsTrapDoor::stepClosed(double elapsedSeconds){
		std::list<EventCollision*>* collisions = getCollisions();
		if(!collisions->empty()){
			transitionOpening();
		}
		Utilities::destroy(collisions);
	}
}