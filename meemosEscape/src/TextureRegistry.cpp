#include "TextureRegistry.h"

namespace avt{

	TextureRegistry* TextureRegistry::_instance = 0;
	std::map<std::string,GLuint>* TextureRegistry::_textures = 0;

	TextureRegistry* TextureRegistry::getInstance(){ 
		if(!_instance){
			_instance = new TextureRegistry();
			_textures = new std::map<std::string,GLuint>();
		}
		return _instance; 
	}

	void TextureRegistry::deleteInstance(){
		assert(_instance);
		delete _instance;
		_instance = 0;
		delete _textures;
		_textures = 0;
	}

	bool TextureRegistry::isInstantiated(){
		return (_instance != 0);
	}

	void TextureRegistry::add(const std::string& fileName,GLuint id){
		(*_textures)[fileName]=id;
	}

	bool TextureRegistry::isRegistered(const std::string& fileName){
		return _textures->count(fileName) == 1;
	}

	GLuint TextureRegistry::getId(const std::string& fileName){
		return (*_textures)[fileName];
	}
}