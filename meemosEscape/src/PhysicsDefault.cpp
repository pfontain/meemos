#include "PhysicsDefault.h"

namespace avt {

	PhysicsDefault::PhysicsDefault(){
		_isGoUp = false;
		_isGoDown = false;
	}

	void PhysicsDefault::goUp(){
		_isGoUp = true;
	}

	void PhysicsDefault::goDown(){
		_isGoDown = true;
	}

	EventCollision* PhysicsDefault::getCollisionEvent(){
		return new EventCollisionDefault();
	}

	bool PhysicsDefault::isColliding(Physics* physics){
		return Physics::defaultIsColliding(this,physics);
	}

	void PhysicsDefault::step(double elapsedSeconds) {
		if(_isGoUp) {
			_position += _up * _linearVelocity * elapsedSeconds;
			_isGoUp = false;
		}
		if(_isGoDown) {
			_position -= _up * _linearVelocity * elapsedSeconds;
			_isGoDown = false;
		}
		Physics::step(elapsedSeconds);
	}

	void PhysicsDefault::step() {
		_orientation.getGLMatrix(_rotationMatrix);
	}

	void PhysicsDefault::rotate(const cg::Vector3d& direction){
		cg::Vector3d orientation(1,0,0);
		orientation = apply(_orientation, orientation);
		cg::Vector3d directionCrossProduct  = cross(orientation, direction);
		double directionDotProduct = dot(orientation, direction);
		double angleRad = acos(directionDotProduct);
		
		cg::Quaterniond quaternion;
		if(fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive() &&
			directionDotProduct > 0)
			quaternion.setRotationRad(0.0, _up);
		else if (fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive())
			quaternion.setRotationRad(Utilities::Math::pi(), _up);
		else if(directionCrossProduct[2] < 0)
			quaternion.setRotationRad(-angleRad, _up);
		else //if(directionCrossProduct[2] > 0)
			quaternion.setRotationRad(angleRad, _up);

		_orientation= _orientation * quaternion;
		_front = apply(quaternion, _front);
		_right = apply(quaternion, _right);
	}

	void PhysicsDefault::setFront(const cg::Vector3d& frontDirection){
		_front = frontDirection;
	}

	void PhysicsDefault::setLeft(const cg::Vector3d& leftDirection){
		_right = - leftDirection;
	}

	void PhysicsDefault::setUp(const cg::Vector3d& upDirection){
		_up = upDirection;
	}
}