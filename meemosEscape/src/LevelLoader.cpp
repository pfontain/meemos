#include "LevelLoader.h"

namespace avt{

	LevelLoader::LevelLoader(Level* level){
		_level = level;
		_numberWalls = 0;
		_numberHoles = 0;
		_numberFinishLines = 0;
		_numberAnvils = 0;
		_numberMeemoMinions = 0;
		_numberFloorTiles = 0;
		_numberTrapDoors = 0;
		_numberLevers = 0;
	}

	void LevelLoader::load(const std::string& levelNameFile){
		openLevelFile(levelNameFile);
		_map = new Map();
		_level->setMap(_map);
		loadMapSize();
		loadMapFromFile();
		loadMeemoCaptainFromMap();
		loadEntitiesFromMap();
		closeLevelFile();
	}

	void LevelLoader::openLevelFile(const std::string& levelNameFile){
		_levelFile = new std::ifstream(levelNameFile.c_str());
		if(_levelFile->fail()){
			std::cerr << "ERROR: Unable to open " << levelNameFile << std::endl;
			std::cin.ignore();
			exit(1);
		}
	}

	void LevelLoader::loadMapSize(){
		int sizeX = 0;
		int sizeY = 0;
		bool sizeXSet = false;

		int levelMapLineMaxSize;
		levelMapLineMaxSize = cg::Properties::instance()->getInt("LEVEL_MAP_LINE_MAX_SIZE");
		_levelFile->seekg(std::ios_base::beg);
		while (_levelFile->good())
		{
			char* fileLine = new char[levelMapLineMaxSize];
			_levelFile->getline(fileLine,levelMapLineMaxSize);
			std::string fileLineString(fileLine);
			if(!sizeXSet)
				sizeX = (int)(fileLineString.size());
			if(fileLineString.size() < 3 || sizeX != fileLineString.size()){
				std::cerr << "ERROR: Incorrect Map format" << std::endl;
				std::cin.ignore();
				exit(1);
			}
			sizeY++;
			delete[] fileLine;
		}
		_map->setDimensions(std::pair<int,int>(sizeX,sizeY));
	}

	void LevelLoader::loadMapFromFile(){
		int x = 0, y = _map->getSizeY() - 1;
		_levelFile->clear();
		_levelFile->seekg(std::ios_base::beg);
		while (_levelFile->good())
		{
			int levelMapLineMaxSize;
			levelMapLineMaxSize = cg::Properties::instance()->getInt("LEVEL_MAP_LINE_MAX_SIZE");
			char* fileLine = new char[levelMapLineMaxSize];
			_levelFile->getline(fileLine,levelMapLineMaxSize);
			std::string fileLineString(fileLine);
			std::string::iterator lineIterator = fileLineString.begin();

			for(;lineIterator != fileLineString.end(); lineIterator++){
				char lineChar = *lineIterator;
				if(lineChar != ' '){
					std::pair<int,int> location(x,y);
					(*_map)[location] = lineChar;
				}
				x++;
			}
			y--;
			x = 0;
			delete[] fileLine;
		}
	}

	void LevelLoader::loadMeemoCaptainFromMap(){
		int mapSizeX = _map->getSizeX();
		int mapSizeY = _map->getSizeY();

		for(int y=0; y < mapSizeY; y++)
			for(int x=0; x < mapSizeX; x++){
				std::pair<int,int> location(x,y);
				char entityChar = (*_map)[location];
				if(entityChar == 'c')
					addMeemoCaptain(x,y);
			}
	}

	void LevelLoader::loadEntitiesFromMap(){
		int mapSizeX = _map->getSizeX();
		int mapSizeY = _map->getSizeY();

		for(int y=0; y < mapSizeY; y++)
			for(int x=0; x < mapSizeX; x++){
				std::pair<int,int> location(x,y);
				char entityChar = (*_map)[location];
				if(entityChar != ' ')
					addEntityChar(entityChar,x,y);
				if(entityChar != 'h' && entityChar != 't')
					addFloorTile(x,y);
			}
	}

	void LevelLoader::addEntityChar(char lineChar,int x,int y){
		switch(lineChar){
					case 'w': addWall(x,y); break;
					case 'h': addHole(x,y); break;
					case 'f': addFinishLine(x,y); break;
					case 'a': addAnvil(x,y); break;
					case 't': addTrapDoor(x,y); break;
					case 'l': addLever(x,y); break;
					case 'd': addDoor(x,y); break;
					default:
						if( lineChar >= '1' && lineChar <= '9'){
							addMeemoMinion(x,y,lineChar);
						}
		}
	}

	void LevelLoader::addFloorTile(int x,int y){
		_numberFloorTiles++;
		std::stringstream idStringStream;
		idStringStream << "floorTile" << _numberFloorTiles;
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		FloorTile* floorTile = new FloorTile(x*unitaryLength, y*unitaryLength,unitaryLength,idStringStream.str());
		_level->add(floorTile);
	}

	void LevelLoader::addWall(int x, int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int size = unitaryLength;
		int wallHeight = cg::Properties::instance()->getInt("WALL_HEIGHT");
		_numberWalls++;
		std::stringstream idStringStream;
		idStringStream << "w" << _numberWalls;
		Wall* wall = new Wall(x*unitaryLength, y*unitaryLength,size,wallHeight,idStringStream.str());
		_level->add(wall);
	}

	void LevelLoader::addHole(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int size = unitaryLength;
		_numberHoles++;
		std::stringstream idStringStream;
		idStringStream << "h" << _numberHoles;
		Hole* hole = new Hole(x*unitaryLength, y*unitaryLength, size, idStringStream.str());
		_level->add(hole);
	}

	void LevelLoader::addFinishLine(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int finishLineSize = cg::Properties::instance()->getInt("FINISH_LINE_SIZE");
		_numberFinishLines++;
		std::stringstream idStringStream;
		idStringStream << "f" << _numberFinishLines;
		FinishLine* finishLine = new FinishLine(x*unitaryLength, y*unitaryLength,finishLineSize,idStringStream.str());
		_level->add(finishLine);
	}

	void LevelLoader::addAnvil(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int anvilSize = cg::Properties::instance()->getInt("ANVIL_SIZE");
		_numberAnvils++;
		std::stringstream idStringStream;
		idStringStream << "a" << _numberAnvils;
		Anvil* anvil = new Anvil(x*unitaryLength, y*unitaryLength,anvilSize,idStringStream.str());
		_level->add(anvil);
	}

	void LevelLoader::addTrapDoor(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int trapDoorSize = unitaryLength;
		_numberTrapDoors++;
		std::stringstream idStringStream;
		idStringStream << "t" << _numberTrapDoors;
		TrapDoor* trapDoor = new TrapDoor(x*unitaryLength, y*unitaryLength,trapDoorSize,idStringStream.str());
		_level->add(trapDoor);
	}

	void LevelLoader::addLever(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int leverSize = unitaryLength;
		_numberLevers++;
		std::stringstream idStringStream;
		idStringStream << "l" << _numberLevers;
		Lever* lever = new Lever(x*unitaryLength, y*unitaryLength,leverSize,idStringStream.str());
		_level->add(lever);
		GameLogic* gameLogic = _level->getGameLogic();
		gameLogic->setLever(lever);
	}

	void LevelLoader::addDoor(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int doorSize = unitaryLength;
		GameLogic* gameLogic = _level->getGameLogic();
		Door* door = new Door(x*unitaryLength, y*unitaryLength,doorSize,"door",gameLogic);
		_level->add(door);
	}

	void LevelLoader::addMeemoMinion(int x,int y,char number){
		_numberMeemoMinions++;
		unsigned int idNumber = number - '0';
		MeemoMinion* meemoMinion = createMeemoMinion(x,y,idNumber);
		_level->add(meemoMinion);
		setMeemoMinionScript(meemoMinion);
	}

	MeemoMinion* LevelLoader::createMeemoMinion(int x,int y,unsigned int idNumber){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int meemoSize = cg::Properties::instance()->getInt("MEEMO_SIZE");
		std::string meemoType = cg::Properties::instance()->getString("MEEMO_TYPE");
		std::stringstream idStringStream;
		idStringStream << "m" << idNumber;
		GameLogic* gameLogic = _level->getGameLogic();
		if(meemoType.compare("NO_MEMORY")==0)
			return new MeemoMinionNoMemory(x*unitaryLength, y*unitaryLength,meemoSize,idNumber,idStringStream.str(),_level,gameLogic);
		else
			return new MeemoMinionWithMemory(x*unitaryLength, y*unitaryLength,meemoSize,idNumber,idStringStream.str(),_level,gameLogic);
	}

	void LevelLoader::setMeemoMinionScript(MeemoMinion* meemoMinion){
		setMeemoMinionScriptPositions(meemoMinion);
		setMeemoMinionScriptWaitDurations(meemoMinion);
	}

	void LevelLoader::setMeemoMinionScriptPositions(MeemoMinion* meemoMinion){
		std::queue<cg::Vector3d> plan;
		unsigned int iPosition = 0;
		std::stringstream positionName;
		positionName << meemoMinion->getId() << "_" << "POSITION" << "_" << iPosition;
		while(cg::Properties::instance()->exists(positionName.str())){
			cg::Vector3d position(cg::Properties::instance()->getVector3d(positionName.str()));
			plan.push(position);

			iPosition++;
			positionName.str("");
			positionName << meemoMinion->getId() << "_" << "POSITION" << "_" << iPosition;
		}
		meemoMinion->setPlan(plan);
	}

	void LevelLoader::setMeemoMinionScriptWaitDurations(MeemoMinion* meemoMinion){
		std::queue<double> waitDurations;
		unsigned int iWaitDuration = 0;
		std::stringstream waitDurationName;
		waitDurationName << meemoMinion->getId() << "_" << "WAIT_DURATION" << "_" << iWaitDuration;
		while(cg::Properties::instance()->exists(waitDurationName.str())){
			double waitDuration = cg::Properties::instance()->getDouble(waitDurationName.str());
			waitDurations.push(waitDuration);

			iWaitDuration++;
			waitDurationName.str("");
			waitDurationName << meemoMinion->getId() << "_" << "WAIT_DURATION" << "_" << iWaitDuration;
		};
		meemoMinion->setWaitDurations(waitDurations);
	}

	void LevelLoader::addMeemoCaptain(int x,int y){
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		int meemoSize = cg::Properties::instance()->getInt("MEEMO_SIZE");
		GameLogic* gameLogic = _level->getGameLogic();
		MeemoCaptain* meemoCaptain = new MeemoCaptain(x*unitaryLength, y*unitaryLength,meemoSize,_level,gameLogic);
		_level->add(meemoCaptain);
	}

	void LevelLoader::closeLevelFile(){
		_levelFile->close();
		delete _levelFile;
	}
}
