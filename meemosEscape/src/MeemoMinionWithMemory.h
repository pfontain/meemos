#ifndef _MeemoMinionWithMemory_h_avt_
#define _MeemoMinionWithMemory_h_avt_

#include "MeemoMinion.h"
#include "BehaviourMeemoMinionWithMemory.h"
#include "MovePathAgentWithMemory.h"
#include "Path.h"

namespace avt{
	class MeemoMinionWithMemory: public MeemoMinion, public meemo::MovePathAgentWithMemory{
	public:
		MeemoMinionWithMemory(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic);
		~MeemoMinionWithMemory();
		void init();

	private:
		meemo::BehaviourMeemoMinionWithMemory* _behaviourMeemoMinionWithMemory;
		meemo::MindWithMemory* _mindWithMemory;

	public:
		meemo::MemoryStorage* getMemoryStorage();
		bool movePath(std::list<meemo::Location*>* locations);
		avt::Path* updatePath(avt::Path* path);
	};
}

#endif // _MeemoMinionWithMemory_h_avt_