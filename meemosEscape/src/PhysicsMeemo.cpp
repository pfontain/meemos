#include "PhysicsMeemo.h"

namespace avt {
	PhysicsMeemo::PhysicsMeemo(void){
		_hopLinearVelocity = cg::Properties::instance()->getDouble("MEEMO_HOP_VELOCITY");
		_dropDepth = cg::Properties::instance()->getDouble("MEEMO_DROP_DEPTH");
		_riseLinearVelocity = cg::Properties::instance()->getDouble("MEEMO_RISE_VELOCITY");
		_pullLeverLinearVelocity = cg::Properties::instance()->getDouble("MEEMO_PULL_LEVER_LINEAR_VELOCITY");
	}

	void PhysicsMeemo::generatePhysicalEvents(){
		std::list<EventCollision*>* collisions = getCollisions();

		std::list<EventCollision*>::iterator iteratorCollisions = collisions->begin();
		for(; iteratorCollisions != collisions->end(); iteratorCollisions++){
			notify(*iteratorCollisions);
		}
		delete collisions;

		EventNotifier::step();
	}

	bool PhysicsMeemo::isColliding(Physics* physics){
		return Physics::defaultIsColliding(this,physics);
	}

	EventCollision* PhysicsMeemo::getCollisionEvent(){
		return new EventCollisionDefault();
	}

	void PhysicsMeemo::rotate(const cg::Vector3d& direction){
		cg::Vector3d orientation(1,0,0);
		orientation = apply(_orientation, orientation);
		cg::Vector3d directionCrossProduct  = cross(orientation, direction);
		double directionDotProduct = dot(orientation, direction);
		double angleRad = acos(directionDotProduct);

		cg::Quaterniond quaternion;
		if(fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive() &&
			directionDotProduct > 0)
			quaternion.setRotationRad(0.0, _up);
		else if (fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive())
			quaternion.setRotationRad(Utilities::Math::pi(), _up);
		else if(directionCrossProduct[2] < 0)
			quaternion.setRotationRad(-angleRad, _up);
		else //if(directionCrossProduct[2] > 0)
			quaternion.setRotationRad(angleRad, _up);

		_orientation= _orientation * quaternion;
		_front = apply(quaternion, _front);
		_right = apply(quaternion, _right);
	}

	double PhysicsMeemo::rotationAngle(const cg::Vector3d& direction){
		cg::Vector3d orientation(1,0,0);
		orientation = apply(_orientation, orientation);
		cg::Vector3d directionCrossProduct  = cross(orientation, direction);
		double directionDotProduct = dot(orientation, direction);
		double angleRad = acos(directionDotProduct);

		cg::Quaterniond quaternion;
		if(fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive() &&
			directionDotProduct > 0)
			angleRad = 0.0;
		else if (fabs(directionCrossProduct[2]) < Utilities::Math::smallPositive())
			angleRad = Utilities::Math::pi();
		else if(directionCrossProduct[2] < 0)
			angleRad = -angleRad;
		else //if(directionCrossProduct[2] > 0)
			angleRad = angleRad;

		return angleRad;
	}

	double PhysicsMeemo::hopDuration(){
		// See Implementation Notes
		double hopDuration = 2 * (_hopLinearVelocity / PhysicsParameters::GRAVITY);
		return hopDuration;
	}

	void PhysicsMeemo::stepTurnHopLastTurn(){
		cg::Quaterniond quaternion;
		_position[2] = 0;
		quaternion.setRotationRad(_turnTotalRotationAngle,_up);
		_orientation = _turnOriginalOrientation * quaternion;
		_front = apply(quaternion, _turnOriginalFront);
		_right = apply(quaternion, _turnOriginalRight);
	}

	void PhysicsMeemo::stepTurnHopTurn(double elapsedSeconds){
		cg::Quaterniond quaternion;
		double currentRotationAngle;
		currentRotationAngle = (_turnElapsedSeconds * _turnTotalRotationAngle) / _hopDuration;
		quaternion.setRotationRad(currentRotationAngle, _up);
		_orientation= _turnOriginalOrientation * quaternion;
		_front = apply(quaternion, _turnOriginalFront);
		_right = apply(quaternion, _turnOriginalRight);

		_velocity = _velocity + cg::Vector3d(0,0,-PhysicsParameters::GRAVITY) * elapsedSeconds;
		_position = _position + _velocity * elapsedSeconds;
	}
}