#ifndef _Texture_h_
#define _Texture_h_

#include <stdio.h>
#include "cg/cg.h"
#include "TextureRegistry.h"

namespace avt{

	class Texture{
	public:
		void open(const std::string fileName);
		GLuint id;

	private:
		void load(const char* fileName);
	};
}


#endif