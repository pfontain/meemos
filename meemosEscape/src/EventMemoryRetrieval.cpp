#include "EventMemoryRetrieval.h"

namespace avt{
	EventMemoryRetrieval::EventMemoryRetrieval(appraisal::Event* retrievedEvent){
		_retrievedEvent = retrievedEvent;
	}

	EventMemoryRetrieval::EventMemoryRetrieval(appraisal::Event* retrievedEvent, meemo::Location location): Event(location){
		_retrievedEvent = retrievedEvent;
	}

	EventMemoryRetrieval::~EventMemoryRetrieval(){
		destroy();
	}

	EventType::Type EventMemoryRetrieval::getRetrievedEventType(void) const{
		return _retrievedEvent->type();
	}

	appraisal::Event* EventMemoryRetrieval::getRetrievedEvent(void){
		return _retrievedEvent;
	}

	const std::string EventMemoryRetrieval::toString(){
		std::stringstream descriptionStringstream;
		descriptionStringstream << "EventMemoryRetrieval - ";
		descriptionStringstream << *_retrievedEvent;
		return descriptionStringstream.str();
	}

	EventType::Type EventMemoryRetrieval::type(){
		return EventType::MEMORY_RETRIEVAL;
	}

	Event* EventMemoryRetrieval::copy(){
		if(isLocationSet())
			return new EventMemoryRetrieval(_retrievedEvent->copy(),_location);
		else
			return new EventMemoryRetrieval(_retrievedEvent->copy());
	}

	bool EventMemoryRetrieval::equal(appraisal::Event* event){ 
		if(event->type() == EventType::MEMORY_RETRIEVAL){
			EventMemoryRetrieval* eventMemoryRetrieval = static_cast<EventMemoryRetrieval*>(event);
			appraisal::Event* retrievedEvent = eventMemoryRetrieval->_retrievedEvent;
			return _retrievedEvent->equal(retrievedEvent);
		}	
		else
			return false;
	}

	int EventMemoryRetrieval::match(appraisal::Event* event){ 
		if(event->type() == EventType::MEMORY_RETRIEVAL){
			EventMemoryRetrieval* eventMemoryRetrieval = static_cast<EventMemoryRetrieval*>(event);
			appraisal::Event* retrievedEvent = eventMemoryRetrieval->_retrievedEvent;
			return _retrievedEvent->match(retrievedEvent);
		}	
		else
			return NO_MATCH;
	}

	void EventMemoryRetrieval::destroy(){
		_retrievedEvent->destroy();
		delete _retrievedEvent;
	}
}