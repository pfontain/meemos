#include "Physics.h"

namespace avt {

	Physics::Physics() {
		_axesScale = 1.0;
		_position.set(0,0,0);
		_orientation.setRotationDeg(0,cg::Vector3d(0,0,1));
		_front.set(1,0,0);
		_up.set(0,0,1);
		_right.set(0,-1,0);
		_linearVelocity = 0;
		_angularVelocity = 0;
		_boundingSphereRadius = 0.0;
		_isGoAhead = false;
		_isGoBack = false;
		_isGoLeft = false;
		_isGoRight = false;
		_isYawLeft = false;
		_isYawRight = false;
		_isPitchUp = false;
		_isPitchDown = false;
		_isRollLeft = false;
		_isRollRight = false;
	}

	Physics::~Physics() {
	}

	const cg::Vector3d Physics::getPosition(void) const{
		return _position;
	}

	const cg::Vector3d Physics::getFrontDirection(void) const{
		return _front;
	}

	const cg::Vector3d Physics::getLeftDirection(void) const{
		return - _right;
	}

	const cg::Vector3d Physics::getUpDirection(void) const{
		return _up;
	}

	const cg::Quaterniond Physics::getOrientation(void) const{
		return _orientation;
	}

	double Physics::getBoundingSphereRadius(void) const{
		return _boundingSphereRadius;
	}

	void Physics::setPosition(double x, double y, double z) {
		_position.set(x,y,z);
	}
	void Physics::setLinearVelocity(double value) {
		_linearVelocity = value;
	}
	void Physics::setAngularVelocity(double value) {
		_angularVelocity = value;
	}
	void Physics::setBoundingSphereRadius(double radius) {
		_boundingSphereRadius = radius;
	}
	void Physics::goAhead() { _isGoAhead = true; }
	void Physics::goBack() { _isGoBack = true; }
	void Physics::goLeft() { _isGoLeft = true; }
	void Physics::goRight() { _isGoRight = true; }
	void Physics::yawLeft() { _isYawLeft = true; }
	void Physics::yawRight() { _isYawRight = true; }
	void Physics::pitchUp() { _isPitchUp = true; }
	void Physics::pitchDown() { _isPitchDown = true; }
	void Physics::rollLeft() { _isRollLeft = true; }
	void Physics::rollRight() { _isRollRight = true; }

	inline
		void Physics::rotate(double elapsedSeconds, int direction, 
		cg::Vector3d axis, cg::Vector3d& v1, cg::Vector3d& v2) 
	{
		_q.setRotationDeg(direction * _angularVelocity * elapsedSeconds,axis);
		v1 = apply(_q,v1);
		v2 = apply(_q,v2);
		_orientation = _q * _orientation;
	}

	void Physics::pitchDown(float angle){
			int direction = -1;
			_q.setRotationDeg(direction * angle,_right);

			_up = apply(_q,_up);
			_front = apply(_q,_front);
			_orientation = _q * _orientation;
	}

	void Physics::yawLeft(float angle){
			_q.setRotationDeg(angle,_up);
			_right = apply(_q,_right);
			_front = apply(_q,_front);
			_orientation = _q * _orientation;
	}

	void Physics::step(double elapsedSeconds) {
		if(_isGoAhead) {
			_position += _front * _linearVelocity * elapsedSeconds;
			_isGoAhead = false;
		}
		if(_isGoBack) {
			_position -= _front * _linearVelocity * elapsedSeconds;
			_isGoBack = false;
		}
		if(_isYawLeft) {
			rotate(elapsedSeconds,1,_up,_front,_right);
			_isYawLeft = false;
		}
		if(_isYawRight) {
			rotate(elapsedSeconds,-1,_up,_front,_right);
			_isYawRight = false;
		}
		if(_isPitchUp) {
			rotate(elapsedSeconds,1,_right,_up,_front);
			_isPitchUp = false;
		}
		if(_isPitchDown) {
			rotate(elapsedSeconds,-1,_right,_up,_front);
			_isPitchDown = false;
		}
		if(_isRollLeft) {
			rotate(elapsedSeconds,1,_front,_right,_up);
			_isRollLeft = false;
		}
		if(_isRollRight) {
			rotate(elapsedSeconds,-1,_front,_right,_up);
			_isRollRight = false;
		}
		_orientation.getGLMatrix(_rotationMatrix);
	}

	void Physics::applyTransforms() {
		glTranslated(_position[0],_position[1],_position[2]);
		glMultMatrixd(_rotationMatrix);
	}
	void Physics::setAxesScale(double scale) {
		_axesScale = scale;
	}
	void Physics::drawAxes() {
		glPushMatrix();
		glTranslatef(_position[0],_position[1],_position[2]);
		glScalef(_axesScale,_axesScale,_axesScale);
		glColor3f(1,0,0);
		glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(_front[0],_front[1],_front[2]);
		glEnd();
		glColor3f(0,1,0);
		glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(_up[0],_up[1],_up[2]);
		glEnd();
		glColor3f(0,0,1);
		glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(-_right[0],-_right[1],-_right[2]);
		glEnd();
		glPopMatrix();
	}

	std::list<EventCollision*>* Physics::getCollisions(void){
		std::list<EventCollision*>* eventCollisions = new std::list<EventCollision*>();

		std::list<Physics*>* physicsList = PhysicsRegistry::getInstance()->getPhysicsList();
		std::list<Physics*>::iterator iteratorPhysicsList = physicsList->begin();

		for(;iteratorPhysicsList != physicsList->end(); iteratorPhysicsList++){
			Physics* physics = *iteratorPhysicsList;
			if(physics!= this && physics->isColliding(this))
					eventCollisions->push_back(physics->getCollisionEvent());
		}

		return eventCollisions;
	}

	bool Physics::defaultIsColliding(Physics* physicsA,Physics* physicsB){
		using namespace Utilities::Math;
		cg::Vector3d distance = physicsB->getPosition() - physicsA->getPosition();
		float maximumCollissionDistanceLength = physicsA->_boundingSphereRadius + physicsB->_boundingSphereRadius;

		return Physics::squaredLength(distance) < square(maximumCollissionDistanceLength);
	}

	float Physics::squaredLength(cg::Vector3d& vector){
		return vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2];
	}

	float Physics::isFloatZero(float f){
		return (f - 0.0 < 0.00001f);
	}
}