#ifndef _MeemoMinionPhysics_h
#define _MeemoMinionPhysics_h

#include "PhysicsMeemo.h"
#include "MeemoCaptain.h"
#include "Path.h"
#include "Location.h"
#include "StatePhysicsMeemoMinionVisitor.h"
#include "Utilities.h"
#include "AbstractMeemo.h"
#include "UpdatePathAgent.h"

namespace avt {

	class PhysicsMeemoMinion :
		public PhysicsMeemo,
		public StatePhysicsMeemoMinionVisitor {
	public:
		PhysicsMeemoMinion(UpdatePathAgent* meemoMinion);
		~PhysicsMeemoMinion();

	private:
		std::deque<StatePhysicsMeemoMinion*> _states;
		cg::Vector3d _moveToPosition;
		cg::Vector3d _targetPosition;
		double _stopDistance;
		double _stopDistanceRelaxed;
		double _waitDuration;
		UpdatePathAgent* _meemoMinion;
		std::queue<double> _waitDurations;

	public:
		void setWaitDurations(std::queue<double> waitDurations);
		bool stop();
		bool movePath(std::list<meemo::Location*>* locations);
		bool turnHop(const cg::Vector3d& position);
		bool fall();
		bool hit();
		bool pullLever();
		void start(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath);
		void start(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop);
		void start(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall);
		void start(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle);
		void start(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit);
		void start(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait);
		void start(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever);
		void step(double elapsedSeconds);
		void step(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait,double elapsedSeconds);
		void step(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever,double elapsedSeconds);

	private:
		void moveTo(StatePhysicsMeemoMinionMoveToPath* state,const cg::Vector3d& meemoPosition);
		bool isCloseToMoveToPosition();
		void stepMoveTo(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath,double elapsedSeconds);
		bool antecipatesCollision(const cg::Vector3d& position);
	};
}

#endif