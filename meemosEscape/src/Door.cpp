#include "Door.h"

namespace avt{
	Door::Door(int x, int z,float size,const std::string& id,AbstractGameLogic* gameLogic): cg::Entity(id){
		_size = size;
		_physics = new PhysicsDoor();
		_physics->setPosition(x,z,_size/2);
		_material = new MaterialStatic();
		_gameLogic = gameLogic;
		_gameLogic->setDoor(this);
		_gameLogic->setDoorOpened(false);
	}

	Door::~Door(void){
		delete _physics;
		delete _material;
	}

	void Door::init(void) {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("LEVER_BOX_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->setDiffuse(colour[0],colour[1],colour[2],1.0);
		_material->init();

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);

		makeModel();
		DebugRegistry::getInstance()->add(this);
	}

	void Door::draw(void){
		glPushMatrix();
		{
			_physics->applyTransforms();
			_material->draw();
			glCallList(_modelId);
		}
		glPopMatrix();
		drawDebug();
	}

	void Door::update(unsigned long elapsed_millis){
		double elapsed_seconds = elapsed_millis / (double)1000;
		_physics->step(elapsed_seconds);
	}

	void Door::open(void){
		_physics->open();
		_gameLogic->setDoorOpened(true);
	}

	void Door::makeModel(void)
	{
		_modelId = glGenLists(1);
		assert(_modelId != 0);
		glNewList(_modelId,GL_COMPILE);
		{
			glutSolidCube(_size);
		}
		glEndList();
	}

	Physics* Door::getPhysics(void){
		return _physics;
	}
}