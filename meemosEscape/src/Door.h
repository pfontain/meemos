#ifndef _Door_h_
#define _Door_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "PhysicsDoor.h"
#include "DebugRegistry.h"
#include "AbstractGameLogic.h"

namespace avt{

	class Door : public cg::Entity,
		public cg::IDrawListener,
		public cg::IUpdateListener,
		public Debugable
	{
	public:
		Door(int x, int z,float size,const std::string& id,AbstractGameLogic* gameLogic);
		~Door(void);

	private:
		GLuint _modelId;
		float _size;
		Material* _material;
		PhysicsDoor* _physics;
		AbstractGameLogic* _gameLogic;

	public:
		void init(void);
		void draw(void);
		void update(unsigned long elapsed_millis);
		void open(void);

	private:
		void makeModel(void);
		Physics* getPhysics(void);
	};
}


#endif