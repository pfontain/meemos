#include "BehaviourMeemoMinion.h"

namespace meemo{
	BehaviourMeemoMinion::BehaviourMeemoMinion(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgent* meemoMinionMovePathAgent,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic): Behaviour(meemoMinionEmotionalAgent){
		_meemoMinion = meemoMinionMovePathAgent;
		_map = level->getMap();
		_meemoMinionsAlive = gameLogic->getMeemoMinionsAlive();

		_mappingToInteger['w']=aStar::PathPlan::MOVE_COST_OBSTACLE;
		_mappingToInteger['h']=aStar::PathPlan::MOVE_COST_OBSTACLE;
		_mappingToInteger['l']=aStar::PathPlan::MOVE_COST_OBSTACLE;
		_mappingToInteger['t']=cg::Properties::instance()->getInt("MOVE_COST_NEUTRAL");
		_mappingToInteger[' ']=cg::Properties::instance()->getInt("MOVE_COST_NEUTRAL");
		_unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
	}

	bool BehaviourMeemoMinion::movePathPlanned(const cg::Vector3d& moveToPosition){
		cg::Vector3d startPosition;
		startPosition = _physics->getPosition();

		std::pair<int,int> startPositionDiscretized;
		startPositionDiscretized = discretize(startPosition);
		std::pair<int,int> moveToPositionDiscretized;
		moveToPositionDiscretized = discretize(moveToPosition);

		std::list<meemo::Location*>* pathLocations = new std::list<meemo::Location*>();
		bool pathExists = false;
		bool canMovePath = false;
		pathExists = planPath(startPositionDiscretized,moveToPositionDiscretized,pathLocations);
		if(pathExists){
			recoverUndescrizedStartAndMoveToPositions(pathLocations,startPosition,moveToPosition);
			canMovePath = _meemoMinion->movePath(pathLocations);
		}
		else
			delete pathLocations;

		return pathExists && canMovePath;
	}

	bool BehaviourMeemoMinion::planPath(std::pair<int,int>& startPosition,std::pair<int,int>& moveToPosition,std::list<meemo::Location*>* outputPathLocations){
		std::vector<aStar::Node*>* pathNodes = new std::vector<aStar::Node*>();

		int* integerMap = _map->createIntegerMap(_mappingToInteger);
		updateIntegerMap(integerMap,moveToPosition);
		clearPositionIntegerMap(integerMap,moveToPosition);
		clearPositionIntegerMap(integerMap,startPosition);
		std::cout << avt::Map::toStringIntegerMap(integerMap,_map->getSizeX(),_map->getSizeY());

		bool pathExists;
		pathExists = planPathDiscretized(integerMap,startPosition,moveToPosition,pathNodes);
		if(pathExists){
			pathNodes = createSmoothPath(integerMap,pathNodes);
			pathReverseDiscretization(pathNodes,outputPathLocations);
		}
		std::vector<aStar::Node*>::iterator iteratorPathNodes;
		iteratorPathNodes = pathNodes->begin();
		for(; iteratorPathNodes != pathNodes->end(); iteratorPathNodes++)
			delete *iteratorPathNodes;
		delete pathNodes;
		delete integerMap;
		return pathExists;
	}

	void BehaviourMeemoMinion::updateIntegerMapAccordingToEvent(int* integerMap,appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion){
		meemo::Location eventLocation = event->getLocation();
		unsigned int mapSizeX = _map->getSizeX();
		std::pair<int,int> locationDiscretized = discretize(eventLocation);
		int locationDiscretizedX = locationDiscretized.first;
		int locationDiscretizedY = locationDiscretized.second;
		int locationInIntegerMap = locationDiscretizedY * mapSizeX + locationDiscretizedX;

		appraisal::EmotionValence::Valence causedEmotionValence = causedEmotion->getValence();
		float causedEmotionIntensity = causedEmotion->getIntensityATt0();

		if(causedEmotionValence==appraisal::EmotionValence::POSITIVE){
			updateIntegerMapAccordingToPositiveEvent(integerMap,locationInIntegerMap,causedEmotionIntensity);
		}
		if(causedEmotionValence==appraisal::EmotionValence::NEGATIVE){
			updateIntegerMapAccordingToNegativeEvent(integerMap,locationInIntegerMap,causedEmotionIntensity);
		}
	}

	void BehaviourMeemoMinion::updateIntegerMapAccordingToPositiveEvent(int* integerMap,int locationInIntegerMap,float causedEmotionIntensity){
		int moveCostNeutral = cg::Properties::instance()->getInt("MOVE_COST_NEUTRAL");
		int moveCostMinimum = cg::Properties::instance()->getInt("MOVE_COST_MINIMUM");
		int moveCostChange = (moveCostNeutral - moveCostMinimum) * causedEmotionIntensity/10.0;
		int newMoveCost = integerMap[locationInIntegerMap] - moveCostChange;
		if(newMoveCost < moveCostMinimum) newMoveCost = moveCostMinimum;
		integerMap[locationInIntegerMap] = newMoveCost;
	}

	void BehaviourMeemoMinion::updateIntegerMapAccordingToNegativeEvent(int* integerMap,int locationInIntegerMap,float causedEmotionIntensity){
		int moveCostNeutral = cg::Properties::instance()->getInt("MOVE_COST_NEUTRAL");
		int moveCostMaximum = cg::Properties::instance()->getInt("MOVE_COST_MAXIMUM");
		int moveCostChange = (moveCostMaximum - moveCostNeutral) * causedEmotionIntensity/10.0;
		int newMoveCost = integerMap[locationInIntegerMap] + moveCostChange;
		if(newMoveCost > moveCostMaximum) newMoveCost = moveCostMaximum;
		integerMap[locationInIntegerMap] = newMoveCost;
	}

	void BehaviourMeemoMinion::updateIntegerMapAccordingToOtherMeemos(int* integerMap){
		std::set<avt::AbstractMeemo*>::iterator meemosAliveIterator = _meemoMinionsAlive->begin();
		for(; meemosAliveIterator != _meemoMinionsAlive->end(); meemosAliveIterator++){
			avt::AbstractMeemo* otherMeemo = *meemosAliveIterator;
			avt::Physics* otherMeemoPhysics = otherMeemo->getPhysics();
			cg::Vector3d otherMeemoPosition(otherMeemoPhysics->getPosition());
			std::pair<int,int> locationDiscretized;
			locationDiscretized = discretize(otherMeemoPosition);
			unsigned int mapSizeX = _map->getSizeX();
			int locationDiscretizedX = locationDiscretized.first;
			int locationDiscretizedY = locationDiscretized.second;
			int locationInIntegerMap = locationDiscretizedY * mapSizeX + locationDiscretizedX;
			int moveCostMaximum = aStar::PathPlan::MOVE_COST_OBSTACLE;
			integerMap[locationInIntegerMap] = moveCostMaximum;
		}
	}

	void BehaviourMeemoMinion::clearPositionIntegerMap(int* integerMap,std::pair<int,int>& position){
		int mapSizeX = _map->getSizeX();
		int positionX = position.first;
		int positionY = position.second;
		int positionIntegerMap = mapSizeX * positionY + positionX;
		integerMap[positionIntegerMap] = _mappingToInteger[' '];
	}

	bool BehaviourMeemoMinion::planPathDiscretized(int* integerMap,std::pair<int,int>& startPosition,std::pair<int,int>& moveToPosition,std::vector<aStar::Node*>* outputPathNodes){
		const std::pair<unsigned int,unsigned int> mapDimensions = _map->getDimensions();
		int* copyIntegerMap = avt::Map::copyIntegerMap(integerMap,mapDimensions);

		aStar::PathPlan plan;
		bool pathExists;
		pathExists = plan.getPath(copyIntegerMap,mapDimensions,startPosition,moveToPosition,outputPathNodes);
		delete copyIntegerMap;

		return pathExists;
	}

	std::vector<aStar::Node*>* BehaviourMeemoMinion::createSmoothPath(int* integerMap,std::vector<aStar::Node*>* inputPath){
		if(inputPath->size() == 2)
			return inputPath;

		std::vector<aStar::Node*>* outputPath = new std::vector<aStar::Node*>();
		aStar::Node* firstNode = (*inputPath)[0];
		outputPath->push_back(new aStar::Node(*firstNode));

		for(unsigned int inputIndex=2; inputIndex < inputPath->size(); inputIndex++){
			aStar::Node* previousOutputPathNode = (*outputPath)[outputPath->size()-1];
			aStar::Node* inputPathNode = (*inputPath)[inputIndex];
			if(!rayClear(integerMap,previousOutputPathNode,inputPathNode)){
				aStar::Node* previousInputPathNode = (*inputPath)[inputIndex-1];
				outputPath->push_back(new aStar::Node(*previousInputPathNode));
			}
		}
		aStar::Node* lastInputPathNode = (*inputPath)[inputPath->size()-1];
		outputPath->push_back(new aStar::Node(*lastInputPathNode));
		std::vector<aStar::Node*>::iterator iteratorInputPath;
		iteratorInputPath = inputPath->begin();
		for(; iteratorInputPath != inputPath->end(); iteratorInputPath++)
			delete *iteratorInputPath;
		delete inputPath;
		return outputPath;
	}

	bool BehaviourMeemoMinion::rayClear(int* integerMap,aStar::Node* nodeA,aStar::Node* nodeB){
		if(nodeA->x == nodeB->x && nodeA->x == nodeB->x)
			return true;

		int minimumX = Utilities::Math::minInt(nodeA->x,nodeB->x);
		int maximumX = Utilities::Math::maxInt(nodeA->x,nodeB->x);
		int minimumY = Utilities::Math::minInt(nodeA->y,nodeB->y);
		int maximumY = Utilities::Math::maxInt(nodeA->y,nodeB->y);

		int mapSizeX = _map->getSizeX();
		for(int y=minimumY; y <= maximumY; y++){
			for(int x=minimumX; x <= maximumX; x++){
				int locationInIntegerMap = y * mapSizeX + x;
				if(integerMap[locationInIntegerMap]!=_mappingToInteger[' '])
					return false;
			}
		}

		return true;
	}

	void BehaviourMeemoMinion::pathReverseDiscretization(std::vector<aStar::Node*>* pathNodes,std::list<meemo::Location*>* outputPathLocations){
		for(unsigned int i=0; i < pathNodes->size(); i++){
			float LocationX = (*pathNodes)[i]->x * _unitaryLength;
			float LocationY = (*pathNodes)[i]->y * _unitaryLength;
			meemo::Location* location = new meemo::Location(LocationX,LocationY);
			outputPathLocations->push_back(location);
		}
	}

	void BehaviourMeemoMinion::recoverUndescrizedStartAndMoveToPositions(std::list<meemo::Location*>* pathLocations,const cg::Vector3d& startPosition,const cg::Vector3d& moveToPosition){
		Location* firstLocation = pathLocations->front();
		delete firstLocation;
		pathLocations->pop_front();
		firstLocation = new Location(startPosition[0],startPosition[1]);
		pathLocations->push_front(firstLocation);

		Location* lastLocation = pathLocations->back();
		delete lastLocation;
		pathLocations->pop_back();
		lastLocation = new Location(moveToPosition[0],moveToPosition[1]);
		pathLocations->push_back(lastLocation);
	}

	std::pair<int,int> BehaviourMeemoMinion::discretize(const cg::Vector3d& position){
		std::pair<int,int> positionDiscretized;

		double positionXDouble = position[0] / _unitaryLength;
		double positionYDouble = position[1] / _unitaryLength;
		positionDiscretized.first = Utilities::Math::round(positionXDouble);
		positionDiscretized.second = Utilities::Math::round(positionYDouble);

		return positionDiscretized;
	}

	std::pair<int,int> BehaviourMeemoMinion::discretize(const meemo::Location& location){
		std::pair<int,int> locationDiscretized;

		double locationFirstCoordinate = location.getFirstCoordinate() / _unitaryLength;
		double locationSecondCoordinate = location.getSecondCoordinate() / _unitaryLength;
		locationDiscretized.first = Utilities::Math::round(locationFirstCoordinate);
		locationDiscretized.second = Utilities::Math::round(locationSecondCoordinate);

		return locationDiscretized;
	}
}