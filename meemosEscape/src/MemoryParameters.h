#ifndef _MemoryParameters_h_
#define _MemoryParameters_h_

/**
* Defines the values for memory parameters used in the dynamics of the 
* memory retrieval process.
**/
namespace meemo{
	class MemoryParameters{
	public:
		static float MEMORY_RETRIEVAL_INTENSITY_BIAS;
	};
}

#endif