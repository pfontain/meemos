#include "PhysicsDoor.h"

namespace avt{
	PhysicsDoor::PhysicsDoor(void){
		_openLinearVelocity = cg::Properties::instance()->getInt("DOOR_OPEN_LINEAR_VELOCITY");
		_state = PhysicsDoorState::CLOSED;
	}

	void PhysicsDoor::open(void){
		_state = PhysicsDoorState::OPENING;
		_linearVelocity = -_openLinearVelocity;
	}

	EventCollision* PhysicsDoor::getCollisionEvent(void){
		return new EventCollisionDefault();
	}

	bool PhysicsDoor::isColliding(avt::Physics *collider){
		if(_state == PhysicsDoorState::OPEN)
			return false;
		else
			return Physics::defaultIsColliding(this,collider);
	}

	void PhysicsDoor::step(double elapsedSeconds){
		Physics::step(elapsedSeconds);
		if(_state == PhysicsDoorState::OPENING){
			float openPositionZ = -2*_boundingSphereRadius + Utilities::Math::smallPositive();
			if(_position[2] < openPositionZ){
				_position[2] = openPositionZ;
				_state = PhysicsDoorState::OPEN;
			}
			else{
				_position = _position + cg::Vector3d(0,0,_linearVelocity * elapsedSeconds);
			}
		}
	}
}