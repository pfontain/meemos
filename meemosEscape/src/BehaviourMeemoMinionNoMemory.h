#ifndef _BehaviourMeemoMinionNoMemory_h_
#define _BehaviourMeemoMinionNoMemory_h_

#include "BehaviourMeemoMinion.h"

namespace meemo{
	class BehaviourMeemoMinionNoMemory: public BehaviourMeemoMinion{
	public:
		BehaviourMeemoMinionNoMemory(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgent* meemoMinionMovePathAgent,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic);
		void updateIntegerMap(int* integerMap,std::pair<int,int>& moveToPosition);
	};
}

#endif // _BehaviourMeemoMinionNoMemory_h_