#ifndef _MemoryStorage_h_
#define _MemoryStorage_h_

#include <vector>
#include <list>
#include <utility>
#include "MemoryTrace.h"
#include "Printable.h"
#include "MemoryParameters.h"

namespace meemo{
	class MemoryStorage: public Printable{
	public:
		MemoryStorage();
		~MemoryStorage();

	private:
		std::list<MemoryTrace*> _memoryTraces;
		std::list<MemoryTrace*>* _retrievedMemoryTracesInShortTermMemory;
		int _shortTermMemoryDuration;

	public:
		std::list<MemoryTrace*>::iterator getMemoryTracesIterator();
		std::list<MemoryTrace*>::iterator getMemoryTracesEnd();
		void addMemoryTrace(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion);
		void addRetrievedMemoryTraceToShortTermMemory(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion);
		std::list<MemoryTrace*>* getRetrievedMemoryTracesInShortTermMemory(void);

		const std::string toString();
	private:
		void lazyUpdateShortTermMemory(void);
	};
}

#endif //_MemoryStorage_h_