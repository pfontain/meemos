#ifndef __StatePhysicsMeemoCaptain_avt__
#define __StatePhysicsMeemoCaptain_avt__

namespace avt{
	// Visitor Pattern
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptain{	
	public:
		virtual void step(StatePhysicsMeemoCaptainVisitor* physicsMeemoMinion,double elapsedSeconds) = 0;
		virtual void destroy() = 0;
		virtual bool canInterrupt() = 0;
	};
}

#endif // __StatePhysicsMeemoCaptain_avt__