#include "MindWithMemory.h"

namespace meemo{
	MindWithMemory::MindWithMemory(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent): Mind(physicalAgent,emotionalAgent){
		_locationEcphory = NULL;
		_reactiveProcessMemoryRetrieval = NULL;
	}

	MindWithMemory::~MindWithMemory(){
		if(_locationEcphory != NULL)
			delete _locationEcphory;
		if(_reactiveProcessMemoryRetrieval != NULL)
			delete _reactiveProcessMemoryRetrieval;
	}

	LocationEcphory* MindWithMemory::getLocationEcphory(void) const{
		return _locationEcphory;
	}

	MemoryStorage* MindWithMemory::getMemoryStorage(void){
		return &_memoryStorage;
	}

	void MindWithMemory::initModules(){
		Mind::initModules();
		std::string emotionalAgentName = _emotionalAgent->getId();
		_reactiveProcessMemoryRetrieval = new meemo::ReactiveProcessMemoryRetrieval(emotionalAgentName,_emotionalState,&_memoryStorage);
		_reactiveProcess = _reactiveProcessMemoryRetrieval;
		_locationEcphory = new LocationEcphory(&_memoryStorage,_physicalAgent);
		_locationEcphory->registerSubscriber(this);
	}

	void MindWithMemory::initEmotionalReactionRules(){
		Mind::initEmotionalReactionRules();
		
		std::vector<appraisal::EmotionalReactionRule*> emotionalReactionRules;
		emotionalReactionRules = _reactiveProcess->getEmotionalReactionRules();
		std::vector<appraisal::EmotionalReactionRule*>::iterator iteratorEmotionalReactionRules;
		iteratorEmotionalReactionRules = emotionalReactionRules.begin();

		for(; iteratorEmotionalReactionRules != emotionalReactionRules.end(); iteratorEmotionalReactionRules++){
			appraisal::EmotionalReactionRule* emotionalReactionRuleMemoryRetrieval;
			appraisal::EmotionalReactionRule* emotionalReactionRule;
			emotionalReactionRule = *iteratorEmotionalReactionRules;

			emotionalReactionRuleMemoryRetrieval = new meemo::EmotionalReactionRuleMemoryRetrieval(emotionalReactionRule);
			_reactiveProcess->addEmotionalReactionRule(emotionalReactionRuleMemoryRetrieval);
		}
	}

	void MindWithMemory::updateAppraisal(){
		if(!_dead)
			_locationEcphory->generateMemoryRetrievalEvents();
		_reactiveProcess->appraisal();
	}
}