#ifndef _MindNoMemory_meemo_
#define _MindNoMemory_meemo_

#include "Mind.h"
#include "ReactiveProcessDefault.h"

namespace meemo{
	class MindNoMemory: public Mind{
	public:
		MindNoMemory(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent);
		~MindNoMemory();

	private:
		appraisal::ReactiveProcessDefault* _reactiveProcessDefault;

	public:
		void update();

	private:
		void initModules();
		void initEmotionalReactionRules();
		void updateAppraisal();
	};
}

#endif // _MindNoMemory_meemo_