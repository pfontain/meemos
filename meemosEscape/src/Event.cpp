/** 
* Event.cpp - Event that can return a string describing it
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/05/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/05/2009 - File created.
*/
#include "Event.h"

namespace appraisal{

	Event::Event(){
		_locationSet = false;
	}
	
	Event::Event(meemo::Location location) {
		_locationSet = true;
		_location = location;
	}

	bool Event::defaultEqual(Event* event){ 
		return this->type() == event->type();	
	}

	int Event::defaultMatch(Event* event){
		if(this->equal(event))
			return TOTAL_MATCH;
		else
			return NO_MATCH;
	}

	bool Event::isLocationSet(){
		return _locationSet;
	}
	
	const meemo::Location Event::getLocation(){
		return _location;
	}
}