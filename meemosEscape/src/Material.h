// This file is an example for CGLib.
//
// CGLib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// CGLib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CGLib; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Copyright 2007 Carlos Martinho

#ifndef _Material_h
#define _Material_h

#include <string>
#include "cg/cg.h"


namespace avt {

	class Material {
	public:
		Material();

	protected:
		GLfloat mat_emission[4];
		GLfloat mat_ambient[4];
		GLfloat mat_diffuse[4];
		GLfloat mat_specular[4];
		GLfloat mat_shininess[1];

	public:
		void setEmission(GLfloat, GLfloat, GLfloat, GLfloat);
		void setAmbient(GLfloat, GLfloat, GLfloat, GLfloat);
		void setDiffuse(GLfloat, GLfloat, GLfloat, GLfloat);
		void setSpecular(GLfloat, GLfloat, GLfloat, GLfloat);
		void setShininess(GLfloat);

		virtual void init() = 0;
		virtual void draw() = 0;
	};
}

#endif