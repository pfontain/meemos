#include "StatePhysicsMeemoMinionTurnHop.h"

namespace avt{
	StatePhysicsMeemoMinionTurnHop::StatePhysicsMeemoMinionTurnHop(const cg::Vector3d& position){
		_turnToPosition = position;
	}

	const cg::Vector3d& StatePhysicsMeemoMinionTurnHop::getTurnToPosition(void){
		return _turnToPosition;
	}

	void StatePhysicsMeemoMinionTurnHop::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		statePhysicsMeemoMinionVisitor->start(this);
	}

	void StatePhysicsMeemoMinionTurnHop::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionTurnHop::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){}

	bool StatePhysicsMeemoMinionTurnHop::canInterrupt(){
		return false;
	}
}