#ifndef _Location_h_
#define _Location_h_

#include "Printable.h"
// REMOVE IF NECESSARY
#include "cg/cg.h"

namespace meemo{
	class Location: public Printable 
	{	
	public:
		Location();
		Location(float _firstCoordinate,float _secondCoordinate);
		// REMOVE IF NECESSARY
		Location(const cg::Vector3d vector);

	private:
		float _firstCoordinate;
		float _secondCoordinate;

	public:
		float getFirstCoordinate() const;
		float getSecondCoordinate() const;
		cg::Vector3d getVector3d();

	public:
		float squaredDistance(const Location& location);
		const std::string toString();
	};
}

#endif // _Location_h_