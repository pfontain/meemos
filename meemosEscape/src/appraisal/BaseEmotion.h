/** 
* BaseEmotion.h - Represents an emotion, which is an
* instance of a particular Emotion Type
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 24/05/2006 - Added comments to each public method's header
* Jo�o Dias: 10/07/2006 - the class is now serializable 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Serialization removed.
*/
#ifndef _BaseEmotion_h_
#define _BaseEmotion_h_

#include <string>
#include "EmotionEnumerables.h"
#include "Event.h"
#include "Printable.h"

namespace appraisal{

	/**
	* Represents an emotion, which is an
	* instance of a particular Emotion Type
	* @author Jo�o Dias
	*/
	class BaseEmotion : public Printable {
	public:
		BaseEmotion(EmotionType::Type, float potential, Event* cause,std::string direction);
		~BaseEmotion();
	protected:
		BaseEmotion(const BaseEmotion& em);

	protected:
		Event* _cause;
		std::string _direction;
		float _potential;
		EmotionType::Type _type;
		EmotionValence::Valence _valence; 

	public:
		Event* getCause();
		void setCause(Event* cause);
		std::string getDirection();
		float getPotential();
		EmotionType::Type getType();
		EmotionValence::Valence getValence();
		int getHashCode();
		virtual const std::string toString();
	};

}

#endif