/** 
* EmotionalReactionRule.h - Rule that maps a type of event to a change
*                            in appraisal variables. This change is stored
*							 in a auxiliary Reaction.
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 26/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 26/06/2009 - File created.
*/
#ifndef _EmotionalReactionRule_h_
#define _EmotionalReactionRule_h_

#include <string>
#include <sstream>
#include "Event.h"
#include "Reaction.h"
#include "Printable.h"

namespace appraisal{
	class EmotionalReactionRule: public Printable{
	public:
		EmotionalReactionRule(Event* event,Reaction reaction);
		~EmotionalReactionRule();

	protected:
		EmotionalReactionRule();

	protected:
		Event* _event;
		Reaction _reaction;

	public:
		Event* getEvent();
		Reaction getReaction();

	protected:
		void setEvent(Event* event);
		void setReaction(Reaction& reaction);

	public:
		virtual bool match(Event* event) = 0;
		const std::string toString();
	};
}

#endif