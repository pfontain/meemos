#include "ReactiveProcessDefault.h"

namespace appraisal{

	ReactiveProcessDefault::ReactiveProcessDefault(const std::string& nameSelf,EmotionalState* emotionalState):ReactiveProcess(nameSelf,emotionalState)
	{}

	void ReactiveProcessDefault::generateActionBasedEmotions(Event* event, int praiseworthiness) {
		BaseEmotion* em;

		if(praiseworthiness >= 0) {
			if(event->type() != EventType::WITNESS) {
				em = new BaseEmotion(EmotionType::PRIDE, (float)praiseworthiness, event->copy(), "SELF");
			}
			else {
				em = new BaseEmotion(EmotionType::ADMIRATION, (float)praiseworthiness, event->copy(), "OTHER");//event.getSubject()
			}
		}
		else {
			if(event->type() != EventType::WITNESS) {
				em = new BaseEmotion(EmotionType::SHAME, (float)-praiseworthiness, event->copy(), "SELF");
			}
			else {
				em = new BaseEmotion(EmotionType::REPROACH, (float)-praiseworthiness, event->copy(), "OTHER");//event.getSubject()
			}
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		delete em;
	}

	void ReactiveProcessDefault::generateFortuneForAll(Event* event, int desirability, int desirabilityForOther, std::string other)
	{
		generateFortuneOfOtherEmotions(event,desirability,desirabilityForOther,other);
	}

	void ReactiveProcessDefault::generateFortuneOfOtherEmotions(Event* event, int desirability, int desirabilityForOther,std::string target) {
		BaseEmotion* em;
		float potential;

		potential = (abs(desirabilityForOther) + abs(desirability)) / 2.0f;

		if(desirability >= 0) {
			if(desirabilityForOther >= 0) {
				em = new BaseEmotion(EmotionType::HAPPYFOR, potential, event->copy(), target);	
			}
			else {
				em = new BaseEmotion(EmotionType::GLOATING, potential, event->copy(), target);
			}
		}
		else {
			if(desirabilityForOther >= 0) {
				em = new BaseEmotion(EmotionType::RESENTMENT, potential, event->copy(), target);
			}
			else {
				em = new BaseEmotion(EmotionType::PITY, potential, event->copy(), target);
			}
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		delete em;
	}

	void ReactiveProcessDefault::generateWellBeingEmotions(Event* event, int desirability) {
		BaseEmotion* em;

		if(desirability >= 0) {
			em = new BaseEmotion(EmotionType::JOY, (float)desirability, event->copy(), "");
		}
		else {
			em = new BaseEmotion(EmotionType::DISTRESS, (float)-desirability, event->copy(), "");
		}

		ActiveEmotion* activeEmotion = _emotionalState->addEmotion(*em);
		delete em;
	}
}