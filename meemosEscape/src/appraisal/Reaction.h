/** 
* Reaction.h - Emotional Reaction based in Construal Frames that specify values
* for some of OCC's appraisal variables: Desirability, DesirabilityForOther and
* Praiseworthiness.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 23/05/2006 - Added comments to each public method's header
* Jo�o Dias: 10/07/2006 - the class is now serializable
* Jo�o DiaS: 18/09/2006 - Removed the Like attribute because it's no longer used
* 						 - added the attribute other;
* 						 - the class is now groundable and clonable
* Paulo Gomes: 26/06/2009 - Converted and adapted to C++. Removed groundability.
*/

#ifndef _Reaction_h_
#define _Reaction_h_

#include <string>
#include <sstream>
#include "Event.h"
#include "Printable.h"

namespace appraisal{
	class Reaction: public Printable {
	public:
		Reaction();
		Reaction(Event* event);
		Reaction(int desirability, int desirabilityForOther, int praiseworthiness);
		Reaction(int desirability, int desirabilityForOther, int praiseworthiness, std::string other);
		Reaction(const Reaction& reaction);
		Reaction& operator=(const Reaction & reaction);
		~Reaction();

	protected:
		int _desirability;
		int _desirabilityForOther;
		int _praiseworthiness;
		std::string _other;
		Event* _event;

	public:
		static const int NULL_APPRAISAL_VALUE = -20;
		static const int MAX_APPRAISAL_VALUE = 10;
		static const int MIN_APPRAISAL_VALUE = -10;

	public:
		int getDesirability();
		int getDesirabilityForOther();
		Event* getEvent();
		int getPraiseworthiness();
		std::string getOther();
		void setDesirability(int integer);
		void setDesirabilityForOther(int integer);
		void setEvent(Event* event);
		void setPraiseworthiness(int integer);

	public:
		const std::string toString();
	};
}

#endif