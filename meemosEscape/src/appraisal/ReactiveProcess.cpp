#include "ReactiveProcess.h"

namespace appraisal{
	/**
	* Creates a new ReactiveProcess
	* @param name - the name of the agent
	*/
	ReactiveProcess::ReactiveProcess(const std::string& nameSelf,EmotionalState* emotionalState) {
		_self = nameSelf;
		_emotionalState = emotionalState;
	}

	ReactiveProcess::~ReactiveProcess() {
		unsigned int iReactionRule;
		for(iReactionRule = 0; iReactionRule < _emotionalReactionRules.size(); iReactionRule++){
			EmotionalReactionRule* emotionalReactionRule = _emotionalReactionRules[iReactionRule];
			delete emotionalReactionRule;
		}
		_emotionalReactionRules.clear();
	}

	/**
	* Adds an event to the layer so that it can be appraised
	* @param event - the event to be appraised
	*/
	void ReactiveProcess::addEvent(Event* event) {
		_eventPool.push_back(event);
	}

	const std::vector<EmotionalReactionRule*>& ReactiveProcess::getEmotionalReactionRules() const{
		return _emotionalReactionRules;
	}

	/**
	* Adds a emotional Reaction rule to the agent's emotional reactions rules
	* @param emotionalReactionRule - the Reaction rule to add
	*/
	void ReactiveProcess::addEmotionalReactionRule(EmotionalReactionRule* emotionalReactionRule) {
		_emotionalReactionRules.push_back(emotionalReactionRule);
	}

	/**
	* Reactive appraisal. Appraises received events according to the emotional
	* reaction rules
	*/
	// We are assuming that an Event only matches one rule
	void ReactiveProcess::appraisal() {
		std::list<Event*>::iterator iteratorEventPool;
		iteratorEventPool = _eventPool.begin();
		for(; iteratorEventPool != _eventPool.end(); iteratorEventPool++){

			Event* event = *iteratorEventPool;
			std::cout << _self << " mind: " << *event << std::endl;
			std::vector<EmotionalReactionRule*>::iterator iteratorEmotionalReactionRules;
			iteratorEmotionalReactionRules = _emotionalReactionRules.begin();
			bool matchEmotionalReactionRuleFound = false;
			Reaction emotionalReaction;
			for(; iteratorEmotionalReactionRules != _emotionalReactionRules.end(); iteratorEmotionalReactionRules++){
				EmotionalReactionRule* emotionalReactionRule = *iteratorEmotionalReactionRules;
				if(emotionalReactionRule->match(event))
				{
					emotionalReaction = emotionalReactionRule->getReaction();
					matchEmotionalReactionRuleFound = true;
					break;
				}
			}
			if(matchEmotionalReactionRuleFound)
				generateEmotions(event, &emotionalReaction);
		}
		clearEventPool();
	}

	/**
	* Resets the reactive layer, clearing all received events that
	* were not appraised yet
	*/
	void ReactiveProcess::reset(){
		clearEventPool();
	}

	void ReactiveProcess::clearEventPool() {
		std::list<Event*>::iterator iteratorEventPool;
		iteratorEventPool = _eventPool.begin();
		for(; iteratorEventPool != _eventPool.end(); iteratorEventPool++){
			Event* event = *iteratorEventPool;
			event->destroy();
			delete event;
		}
		_eventPool.clear();
	}

	const std::string ReactiveProcess::toString(){
		std::stringstream stringStream;

		stringStream << "(ReactiveProcess ";
		stringStream << "_self: " << _self << ", ";
		stringStream << "_emotionalState: " << *_emotionalState << ", ";
		stringStream << "_eventPool: " << _eventPool << ", ";
		stringStream << "_emotionalReactionRules: " << _emotionalReactionRules;
		stringStream << ")";

		return stringStream.str();
	}

	void ReactiveProcess::generateEmotions(Event* event,Reaction* emReaction) {
		int desirability = emReaction->getDesirability();
		int desirabilityForOther = emReaction->getDesirabilityForOther();
		int praiseworthiness = emReaction->getPraiseworthiness();
		std::string other = emReaction->getOther();

		if (desirability != Reaction::NULL_APPRAISAL_VALUE) {
			generateWellBeingEmotions(event, desirability);

			if(desirabilityForOther != Reaction::NULL_APPRAISAL_VALUE)
				generateFortuneForAll(event, desirability, desirabilityForOther, other);
		}
		if(praiseworthiness != Reaction::NULL_APPRAISAL_VALUE)
			generateActionBasedEmotions(event, praiseworthiness);
	}
}