/** 
* Utilities.h - Small Math and Time functions needed for the reactive
*               appraisal process
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 02/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 02/06/2009 - File created.
*/
#include "Utilities.h"

namespace Utilities{
	namespace Time{
		long getTime(){
			return (long)(clock()/(float)CLOCKS_PER_SEC);
		}

		void wait(int seconds )
		{
			clock_t endwait;
			endwait = clock () + seconds * CLOCKS_PER_SEC ;
			while (clock() < endwait) {}
		}
	}

	namespace Draw{
		void drawEllipse(float ellipseA,float ellipseB,unsigned int numberEdges){
			glBegin(GL_TRIANGLES);
			{
				double deltaRadians = 2*Math::pi()/numberEdges;
				glNormal3f(1,0,0);
				for(unsigned int iTriangle = 0; iTriangle < numberEdges; iTriangle++){
					glVertex3f(0,0,0);

					int iTriangleNext = iTriangle + 1;
					cg::Vector2d iPoint(ellipseA*cos(deltaRadians*iTriangle),
										ellipseB*sin(deltaRadians*iTriangle));
					cg::Vector2d iPointNext(ellipseA*cos(deltaRadians*iTriangleNext),
											ellipseB*sin(deltaRadians*iTriangleNext));

					glVertex3f(0,iPoint[0],iPoint[1]);
					glVertex3f(0,iPointNext[0],iPointNext[1]);
				}
			}
			glEnd();
		}
	}
}