/** 
* Emotion.cpp - Implementation of a enumerable for OCC's 22 emotion types
*             and an enumerable for emotion valence
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/05/2009 
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/05/2009 - File created
*/
#include "EmotionEnumerables.h"

namespace appraisal{
	namespace EmotionType{
		static const std::string emotionTypes[] = {
			"Joy",
			"Distress",
			"Love",
			"Hate",
			"Happy-For",
			"Resentment",
			"Gloating",
			"Pity",
			"Pride",
			"Shame",
			"Admiration",
			"Reproach",
			"Hope",
			"Fear",
			"Satisfaction",
			"Disappointment",
			"Relief",
			"Fears-Confirmed",
			"Gratification",
			"Remorse",
			"Gratitude",
			"Anger",
			"Neutral"
		};

		std::string emotionTypeToString(EmotionType::Type emType) 
		{
			unsigned short emotionType = (unsigned short) emType;
			return emotionTypes[emotionType];

			throw new InvalidEmotionTypeException(emotionType);
		}
	}

	namespace EmotionValence{
		const std::string emotionValenceToString(EmotionValence::Valence valence) 
		{
			unsigned short valenceNumber = (unsigned short) valence;
			if(valenceNumber == 0)
				return "POSITIVE";
			else
				return "NEGATIVE";
		}
	}
}