/** 
* Mood.h - Represents a character's mood
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 20/01/2004 - File created
* Jo�o Dias: 02/07/2006 - Replaced System's timer by an internal agent simulation timer
* Jo�o Dias: 10/07/2006 - the class is now serializable 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Serialization removed.
*/
#ifndef _Mood_h_
#define _Mood_h_

#include <math.h>
#include "EmotionEnumerables.h"
#include "ActiveEmotion.h"
#include "EmotionalParameters.h"
#include "Utilities.h"

namespace appraisal{

	class Mood{
	public:
		Mood();

	private:
		float _intensityATt0; 
		long _t0;
		float _intensity;

	public:
		float getMoodValue();
		float decayMood();

		// mood
		void updateMood(ActiveEmotion* em);
		void setMood(float newMood);

	};
}

#endif