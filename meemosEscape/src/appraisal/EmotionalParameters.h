/** 
* EmotionalParameters.h - Defines the values for emotional parameters
* 							  used in the dynamics of the emotional state. ex:
* 							  the decay of an emotion, decay of mood, influence of
* 							  mood in emotion.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 20/01/2004 - File created
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. MINIMUM_EMOTION_VALUE and
*                           LIKE_INFLUENCE_ON_ATTRACTION_REACTIONS added.
*/
#ifndef _EmotionalParameters_h_
#define _EmotionalParameters_h_

/**
* Defines the values for emotional parameters used in the dynamics of the 
* emotional state. ex: the decay of an emotion, decay of mood, influence of
* mood in emotion.
*/
namespace appraisal{
	class EmotionalParameters{
	public:
		static float EMOTION_DECAY_FACTOR;
		static float MOOD_DECAY_FACTOR;
		static float EMOTION_INFLUENCE_ON_MOOD;
		static float MOOD_INFLUENCE_ON_EMOTION;
		static float MINIMUM_MOOD_VALUE;

		static float MINIMUM_EMOTION_VALUE;
		static float LIKE_INFLUENCE_ON_ATTRACTION_REACTIONS;
		static float NEUTRAL_MOOD;
		static float MAXIMUM_MOOD_DEVIATION;
	};
}

#endif