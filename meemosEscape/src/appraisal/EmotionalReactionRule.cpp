/** 
* EmotionalReactionRule.cpp - Rule that maps a type of event to a change
*                            in appraisal variables. This change is stored
*							 in a auxiliary Reaction.
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 26/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 26/06/2009 - File created.
*/
#include "EmotionalReactionRule.h"

namespace appraisal{
	EmotionalReactionRule::EmotionalReactionRule(Event* event,Reaction reaction){
		_event = event;
		_reaction = reaction;
	}

	EmotionalReactionRule::EmotionalReactionRule(){}

	EmotionalReactionRule::~EmotionalReactionRule(){
		_event->destroy();
		delete _event;
	}

	Event* EmotionalReactionRule::getEvent(){
		return _event;
	}

	Reaction EmotionalReactionRule::getReaction(){
		return _reaction;
	}

	void EmotionalReactionRule::setEvent(Event* event){
		_event = event;
	}

	void EmotionalReactionRule::setReaction(Reaction& reaction){
		_reaction = reaction;
	}

	const std::string EmotionalReactionRule::toString(){
		std::stringstream stringStream;

		stringStream << "[EmotionalReactionRule] ";
	
		stringStream << "_event: " << *_event << ", ";
		stringStream << "_reaction: " << _reaction;

		return stringStream.str();
	}
}