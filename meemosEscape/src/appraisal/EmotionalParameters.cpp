/** 
* EmotionalParameters.h - Defines the values for emotional parameters
* 							  used in the dynamics of the emotional state. ex:
* 							  the decay of an emotion, decay of mood, influence of
* 							  mood in emotion.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 20/01/2004 - File created
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. MINIMUM_EMOTION_VALUE and
*                           LIKE_INFLUENCE_ON_ATTRACTION_REACTIONS added.
*/
#include "EmotionalParameters.h"

namespace appraisal{

		/**
		* Constant value that defines how fast should a emotion decay over time.
		* This value is adjusted so that the slowest decaying emotions takes
		* aproximately 15 seconds to decay to half of the original intensity, 
		* the fastest decaying emotions take 4 seconds, and the normal ones takes
		* 7 seconds.
		*/
		float EmotionalParameters::EMOTION_DECAY_FACTOR = 0.02f;

		/**
		* Constant value that defines how fast should mood decay over time.
		* This value is adjusted so that mood decays 3 times slower
		* than the slowest decaying emotion in order to represent 
		* a longer persistence and duration of mood over emotions.
		* So, it takes aproximately 60 seconds for the mood to decay to half
		* of the initial value.
		*/
		float EmotionalParameters::MOOD_DECAY_FACTOR = 0.01f;

		/**
		* Defines how strong is the influence of the emotion's intensity
		* on the character's mood. Since we don't want the mood to be very
		* volatile, we only take into account 30% of the emotion's intensity
		*/
		float EmotionalParameters::EMOTION_INFLUENCE_ON_MOOD = 0.3f;

		/**
		* Defines how strong is the influence of the current mood 
		* in the intensity of the emotion. We don't want the influence
		* of mood to be that great, so we only take into account 30% of 
		* the mood's value
		*/
		float EmotionalParameters::MOOD_INFLUENCE_ON_EMOTION = 0.3f;

		/**
		* Defines the minimum absolute value that mood must have,
		* in order to be considered for influencing emotions. At the 
		* moment, values of mood ranged in ]-0.5;0.5[ are considered
		* to be neutral moods that do not infuence emotions  
		*/
		float EmotionalParameters::MINIMUM_MOOD_VALUE = 0.5f;

		/**
		* Defines the minimum absolute value for an emotion intensity,
		* in order for the corresponding emotion to be considered an active.  
		*/
		float EmotionalParameters::MINIMUM_EMOTION_VALUE = 0.1f;

		/**
		* Defines the ponderator for the like appraisal feature effect on
		* Love & Hate emotional reactions (attraction reactions)
		*/
		float EmotionalParameters::LIKE_INFLUENCE_ON_ATTRACTION_REACTIONS = 0.7f;

		/**
		* Defines the mood value corresponding to a non-influence in the emotional
		* creation.
		*/
		float EmotionalParameters::NEUTRAL_MOOD = 0.0f;

		/**
		* Defines the maximum deviation from NEUTRAL_MOOD a mood value can have. Mood is ranged 
		* between [-MAXIMUM_MOOD_DEVIATION;MAXIMUM_MOOD_DEVIATION], a negative value
		* represents a bad mood, a positive value represents good mood and values near 0
		* represent a neutral mood
		*/
		float EmotionalParameters::MAXIMUM_MOOD_DEVIATION = 10.0f;
}
