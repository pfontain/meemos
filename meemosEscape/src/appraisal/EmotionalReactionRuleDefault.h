#ifndef _EmotionalReactionRuleDefault_h_
#define _EmotionalReactionRuleDefault_h_

#include "EmotionalReactionRule.h"

namespace appraisal{
	class EmotionalReactionRuleDefault: public EmotionalReactionRule{
	public:
		EmotionalReactionRuleDefault(Event* event,Reaction reaction);

	public:
		bool EmotionalReactionRule::match(Event* event);
	};
}

#endif // _EmotionalReactionRuleDefault_h_