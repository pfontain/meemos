/** 
* InvalidEmotionTypeException.h - Exception thrown when invalid emotion type given
*									for parsing.
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/05/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/05/2009 - File created.
*/
#ifndef _InvalidEmotionTypeException_h_
#define _InvalidEmotionTypeException_h_

#include <exception>
#include <string>
#include <sstream>

namespace appraisal
{
	class InvalidEmotionTypeException: public std::exception
	{
	public:
		InvalidEmotionTypeException(std::string emotion)
		{
			isArgumentString = true;
			isArgumentInt = false;
			_emotionString = emotion;
		}
    
		InvalidEmotionTypeException(int num)
		{
			isArgumentString = false;
			isArgumentInt = true;
		}

	private:
		bool isArgumentString;
		bool isArgumentInt;
		std::string _emotionString;
		int _emotionNumber;

	public:
		std::string what(){
			std::stringstream what;
			if(isArgumentString){
				what << "ERROR: Invalid emotion type " << _emotionString;
				return what.str().c_str();
			}
			else{
				what << "ERROR: invalid emotion type indentifier " << _emotionNumber;
				return what.str().c_str();
			}
		}
	};
}

#endif