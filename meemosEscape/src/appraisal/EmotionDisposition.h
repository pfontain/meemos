/** 
* EmotionDisposition.h - Represents the character's emotional disposition 
* (Emotional Threshold + Decay Rate) towards an Emotion Type.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 10/07/2006 - the class is now serializable
* Jo�o Dias: 23/05/2006 - Added comments to each public method's header 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Removed Serialization.
*/
#ifndef _EmotionDisposition_h_
#define _EmotionDisposition_h_

#include <string>
#include <sstream>
#include "EmotionEnumerables.h"
#include "Printable.h"

namespace appraisal{

	class EmotionDisposition: public Printable {

	public:
		EmotionDisposition(EmotionType::Type emotionType, int threshold, int decay);

	private:
		int _decay;
		EmotionType::Type _emotionType;
		int _threshold;

	public:
		int getDecay();
		EmotionType::Type getEmotionType();
		int getThreshold();
		const std::string toString();
	};

}

#endif