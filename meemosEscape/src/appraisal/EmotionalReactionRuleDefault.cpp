#include "EmotionalReactionRuleDefault.h"

namespace appraisal{
	EmotionalReactionRuleDefault::EmotionalReactionRuleDefault(Event* event,Reaction reaction):
EmotionalReactionRule(event,reaction){}

	bool EmotionalReactionRuleDefault::match(Event* event){
		return event->match(_event) > 0;
	}
}