/** 
* ActiveEmotion.cpp - Emotion with intensity that is active in the character's 
* emotional state, i.e, the character is feeling the emotion
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 24/05/2006 - Added comments to each public method's header
* Jo�o Dias: 02/07/2006 - Replaced System's timer by an internal agent simulation timer
* Jo�o Dias: 26/07/2006 - from now on, the intensity for an emotion cannot be greater than 10
* Jo�o Dias: 06/08/2007 - Added a getPotential method that overrides the BaseEmotion getPotential
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++
*/
#include "ActiveEmotion.h"

namespace appraisal{

	/**
	* Creates a new ActiveEmotion
	* @param potEm - the BaseEmotion that is the base for this ActiveEmotion
	* @param potential - the potential for the intensity of the emotion
	* @param threshold - the threshold for the specific emotion
	* @param decay - the decay rate for the specific emotion
	*/
	ActiveEmotion::ActiveEmotion(const BaseEmotion& potEm, float potential, int threshold, int decay): BaseEmotion(potEm)
	{
		_t0=0L;
		_potential = potential;
		_threshold = threshold;
		_decay = decay;
		setIntensity(potential);
	}

	/**
	 * Creates a new ActiveEmotion that consists in a copy of a given ActiveEmotion
	 * @param em - the emotion that will be copied into our new emotion
	 */
	ActiveEmotion::ActiveEmotion(const ActiveEmotion& em):
	BaseEmotion(em._type,em._potential,em._cause->copy(),em._direction)
	{
		_intensityATt0 = em._intensityATt0;
		_t0 = em._t0;
		_decay = em._decay;
		_intensity = em._intensity;
		_threshold = em._threshold;
	}

	/**
	* Gets the emotion's intensity
	* @return the intensity of the emotion
	*/
	float ActiveEmotion::getIntensity() {
		return _intensity;
	}

	float ActiveEmotion::getIntensityATt0(){
		return _intensityATt0;
	}

	/**
	* Sets the intensity of the emotion
	* @param potential - the potential for the emotion's intensity
	*/
	void ActiveEmotion::setIntensity(float potential) {
		_t0 = Utilities::Time::getTime();
		_intensity = potential - _threshold;
		if(_intensity > 10)
		{
			_intensity = 10;
		}
		else if(_intensity < 0)
		{
			_intensity = 0;
		}
		_intensityATt0 = _intensity;
	}

	/**
	* Sets the decay rate for the emotion
	* @param decay - the new decay rate (ranged between 1 and 10)
	*/
	void ActiveEmotion::setDecay(int decay) {
		_decay = decay;
	}

	/**
	* Gets the emotion's potential
	* @return - the potential for the intensity of the emotion
	*/
	float ActiveEmotion::getPotential() {
		return _intensity + _threshold;
	}

	/**
	* Sets the threshold for the specific emotion
	* @param threshold - the new threshold (ranged between 1 and 10)
	*/
	void ActiveEmotion::setThreshold(int threshold) {
		_threshold = threshold;
	}

	long ActiveEmotion::getActivationTime(){
		return _t0;
	}

	/**
	* Decays the emotion according to the system's timer
	* @return the intensity of the emotion after being decayed
	*/
	float ActiveEmotion::decayEmotion() {
		long deltaT;
		deltaT = (Utilities::Time::getTime() - _t0);
		_intensity = _intensityATt0 * (exp(- EmotionalParameters::EMOTION_DECAY_FACTOR * _decay * deltaT));
		return _intensity;
	}

	/**
	* Reforces the intensity of the emotion by a given potential
	* @param potential - the potential for the reinformcement of the emotion's intensity
	*/
	void ActiveEmotion::reforceEmotion(float potential) {
		setIntensity(log(exp(_intensity + _threshold) + exp(potential)));		
	}

	const std::string ActiveEmotion::toString() {
		std::stringstream stringStream;
		stringStream << "[ActiveEmotion] ";
		stringStream << "_intensityATt0: " << _intensityATt0 << ", ";
		stringStream << "_t0: " << _t0 << ", ";
		stringStream << "_decay: " << _decay << ", ";
		stringStream << "_intensity: " << _intensity << ", ";
		stringStream << "_threshold: " << _threshold << ", ";
		stringStream << BaseEmotion::toString();
		return stringStream.str();
	}

}