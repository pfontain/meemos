/** 
* Printable.h - Interface and functions for java-like toString behaviour
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 04/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 04/06/2009 - File created.
*/
#ifndef _Printable_h_
#define _Printable_h_

#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <map>

class Printable{
	public:
		virtual const std::string toString() =0;	
	};

std::ostream& operator<<(std::ostream& out,Printable& r);

template <class T>
std::ostream& operator<<(std::ostream& out,std::list<T*>& listT){
	std::stringstream stringStream;

	std::list<T*>::iterator iteratorT;
	iteratorT = listT.begin();
	if(iteratorT != listT.end()){
		T* t = *iteratorT;
		stringStream << *t;
		iteratorT++;

		for(;iteratorT != listT.end(); iteratorT++){
			t = *iteratorT;
			stringStream << ", " << *t;
		}
	}

	return out<< stringStream.str();
}

template <class T>
std::ostream& operator<<(std::ostream& out,std::vector<T*>& listT){
	std::stringstream stringStream;

	std::vector<T*>::iterator iteratorT;
	iteratorT = listT.begin();
	if(iteratorT != listT.end()){
		T* t = *iteratorT;
		stringStream << *t;
		iteratorT++;

		for(;iteratorT != listT.end(); iteratorT++){
			t = *iteratorT;
			stringStream << ", " << *t;
		}
	}

	return out<< stringStream.str();
}

template <class T,class H>
std::ostream& operator<<(std::ostream& out,std::map<H,T*>& listT){
	std::stringstream stringStream;

	std::map<H,T*>::iterator iteratorT;
	iteratorT = listT.begin();
	if(iteratorT != listT.end()){
		T* t = iteratorT->second;
		stringStream << *t;
		iteratorT++;

		for(;iteratorT != listT.end(); iteratorT++){
			t = iteratorT->second;
			stringStream << ", " << *t;
		}
	}

	return out<< stringStream.str();
}

#endif