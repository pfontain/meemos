/** 
* EmotionEnumerables.h - Implementation of a enumerable for OCC's 22 emotion types
*             and an enumerable for emotion valence
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 29/05/2009 
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 29/05/2009 - File created
*/
#ifndef _EmotionEnumerables_h_
#define _EmotionEnumerables_h_

#include <string>
#include "InvalidEmotionTypeException.h"

namespace appraisal{
	namespace EmotionValence{
		enum Valence{
			POSITIVE = 0,
			NEGATIVE = 1
		};

		const std::string emotionValenceToString(EmotionValence::Valence valence);
	}

	namespace EmotionType{
		enum Type{
			JOY = 0,
			DISTRESS = 1,
			LOVE = 2,
			HATE = 3,
			HAPPYFOR = 4,
			RESENTMENT = 5,
			GLOATING = 6,
			PITY = 7,
			PRIDE = 8,
			SHAME = 9,
			ADMIRATION = 10,
			REPROACH = 11,
			HOPE = 12,
			FEAR = 13,
			SATISFACTION = 14,
			DISAPPOINTMENT = 15,
			RELIEF = 16,
			FEARSCONFIRMED = 17,
			GRATIFICATION = 18,
			REGRET =19,
			GRATITUDE = 20,
			ANGER = 21,
			NEUTRAL = 22
		};

		const unsigned short EMOTION_TYPES_NUMBER = 22;
		std::string emotionTypeToString(EmotionType::Type emType);	
	}
}
#endif