/** 
* Utilities.h - Small Math and Time functions needed for the reactive
*               appraisal process
*  
* Copyright (C) 2009 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: Meemo
* Created: 02/06/2009
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Paulo Gomes: 02/06/2009 - File created.
*/
#ifndef _Utilities_h_
#define _Utilities_h_

#include <time.h>
#include <cmath>
#include <list>
#include "cg/cg.h"

namespace Utilities{
	namespace Time{
		long getTime();

		void wait(int seconds);
	}

	namespace Math{
		inline float min(float a,float b){
			return a < b ? a : b;
		}

		inline float max(float a,float b){
			return a > b ? a : b;
		}

		inline int minInt(int a,int b){
			return a < b ? a : b;
		}

		inline int maxInt(int a,int b){
			return a > b ? a : b;
		}

		inline bool isZero(float a){
			return fabs(a) < 0.0000001f;
		}

		inline double pi(){
			return 3.14159265;
		}

		inline double smallPositive(){
			return 0.1;
		}

		inline int round(double value){
			double valueRounded = floor(value + 0.5);
			return (int)valueRounded;
		}

		template <class T>
		T square(T a) {
			return a*a;
		}

		inline float randomFloat(float leftIntervalLimit,float rightIntervalLimit){
			float intervalLength = rightIntervalLimit - leftIntervalLimit;
			return (rand() / (float)RAND_MAX)*intervalLength + leftIntervalLimit;
		}
	}

	namespace Draw{
		void drawEllipse(float ellipseA,float ellipseB,unsigned int numberEdges);
	}

	template <class T>
	void destroy(std::list<T*>* listT){
		std::list<T*>::iterator iteratorListT = listT->begin();
		for(;iteratorListT != listT->end(); iteratorListT++)
			delete (*iteratorListT);
		delete listT;
	}
}

#endif