/** 
* ActiveEmotion.h - Emotion with intensity that is active in the character's 
* emotional state, i.e, the character is feeling the emotion
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 24/05/2006 - Added comments to each public method's header
* Jo�o Dias: 02/07/2006 - Replaced System's timer by an internal agent simulation timer
* Jo�o Dias: 26/07/2006 - from now on, the intensity for an emotion cannot be greater than 10
* Jo�o Dias: 06/08/2007 - Added a getPotential method that overrides the BaseEmotion getPotential
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++
*/
#ifndef _ActiveEmotion_h_
#define _ActiveEmotion_h_

#include <math.h>
#include "Utilities.h"
#include "EmotionEnumerables.h"
#include "BaseEmotion.h"
#include "EmotionalParameters.h"
#include "Printable.h"

namespace appraisal{
	/**
	* Represents an Emotion with intensity that is active in the character's 
	* emotional state, i.e, the character is feeling the emotion
	* @author Jo�o Dias
	*/
	class ActiveEmotion : public BaseEmotion{

	public:
		ActiveEmotion(const BaseEmotion& potEm, float potential, int threshold, int decay);
		ActiveEmotion(const ActiveEmotion& em);

	private:
		float _intensityATt0; 
		long _t0;
		int _decay;
		float _intensity;	
		int _threshold;

	public:
		float getIntensity();
		float getIntensityATt0();
		void setIntensity(float potential);
		void setDecay(int decay);
		float getPotential();
		void setThreshold(int threshold);

		long getActivationTime();

		float decayEmotion();
		void reforceEmotion(float potential);
		const std::string toString();
	};

}

#endif