/** 
* Reaction.cpp - Emotional Reaction based in Construal Frames that specify values
* for some of OCC's appraisal variables: Desirability, DesirabilityForOther and
* Praiseworthiness.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 23/05/2006 - Added comments to each public method's header
* Jo�o Dias: 10/07/2006 - the class is now serializable
* Jo�o DiaS: 18/09/2006 - Removed the Like attribute because it's no longer used
* 						 - added the attribute other;
* 						 - the class is now groundable and clonable
* Paulo Gomes: 26/06/2009 - Converted and adapted to C++. Removed groundability.
*/
#include "Reaction.h"

namespace appraisal{
	/**
	* Creates a new empty Emotional Reaction
	*/
	Reaction::Reaction() {
		_desirability = NULL_APPRAISAL_VALUE;
		_desirabilityForOther = NULL_APPRAISAL_VALUE;
		_praiseworthiness = NULL_APPRAISAL_VALUE;
		_event = NULL;
		_other = "";
	}

	/**
	* Creates a new empty Emotional Reaction
	* @param event - the event that this reaction references 
	*/
	Reaction::Reaction(Event* event){
		_event = event;
		_desirability = NULL_APPRAISAL_VALUE;
		_praiseworthiness = NULL_APPRAISAL_VALUE;
		_desirabilityForOther = NULL_APPRAISAL_VALUE;
		_other = "";
	}

	/**
	* Creates a new Emotional Reaction
	* @param desirability - the desirability of the event
	* @param desirabilityForOther - the desirability of the event for other agents
	* @param praiseworthiness - the paiseworthiness of the event
	*/
	Reaction::Reaction(int desirability, int desirabilityForOther, int praiseworthiness) {
		_desirability = desirability;
		_desirabilityForOther = desirabilityForOther;
		_praiseworthiness = praiseworthiness;
		_event = NULL;
		_other = "";
	}

	/**
	* Creates a new Emotional Reaction
	* @param desirability - the desirability of the event
	* @param desirabilityForOther - the desirability of the event for other agents
	* @param praiseworthiness - the paiseworthiness of the event
	* @param other - which character does the desirabilityForOther variable reference
	*/
	Reaction::Reaction(int desirability, int desirabilityForOther, int praiseworthiness, std::string other) {
		_desirability = desirability;
		_desirabilityForOther = desirabilityForOther;
		_praiseworthiness = praiseworthiness;
		_other = other;
		_event = NULL;
	}

	/**
	* Copy Constructor
	* @param reaction - reaction we are copying
	*/
	Reaction::Reaction (const Reaction& reaction) {
		_desirability = reaction._desirability;
		_desirabilityForOther = reaction._desirabilityForOther;
		_praiseworthiness = reaction._praiseworthiness;
		_other = reaction._other;
		if(reaction._event!=NULL)
				_event = reaction._event->copy();
			else
				_event = NULL;
	}

	/**
	* Assignment operator
	* @param reaction - reaction we are copying
	*/
	Reaction & Reaction::operator = (const Reaction & reaction)
	{
		if (this != &reaction)
		{
			if(_event!=NULL){
				_event->destroy();
				delete _event;
			}

			if(reaction._event!=NULL)
				_event = reaction._event->copy();
			else
				_event = NULL;

			_desirability = reaction._desirability;
			_desirabilityForOther = reaction._desirabilityForOther;
			_praiseworthiness = reaction._praiseworthiness;

			_other = reaction._other;
		}
		return *this;
	}

	/**
	* Destructor
	* Frees allocated space for event
	*/
	Reaction::~Reaction(){
		if(_event!= NULL){
			_event->destroy();
			delete _event;
		}
	}

	/**
	* Gets the appraisal variable: Desirability of the event
	* @return - the event's desirability
	*/
	int Reaction::getDesirability() {
		return _desirability;
	}

	/**
	* Gets the appraisal variable: DesirabilityForOther of the event
	* @return - the event's desirability for other agent
	*/
	int Reaction::getDesirabilityForOther() {
		return _desirabilityForOther;
	}

	/**
	* Gets the event referenced by the emotional reaction
	* @return the reaction's event
	*/
	Event* Reaction::getEvent() {
		return _event;
	}

	/**
	* Gets the appraisal variable: Praiseworthiness of the event
	* @return - the event's praiseworthiness for the agent
	*/
	int Reaction::getPraiseworthiness() {
		return _praiseworthiness;
	}

	/**
	* Gets the name of the character that the appraisal variable
	* desirabilityForOther refers
	* @return - the name of the desirabilityForOther's character
	*/
	std::string Reaction::getOther()
	{
		return _other;
	}

	/**
	* Sets the appraisal variable: Desirability
	* @param integer - the new value of Desirability for the reaction
	*/
	void Reaction::setDesirability(int integer) {
		_desirability = integer;
	}

	/**
	* Sets the appraisal variable: DesirabilityForOther
	* @param integer - the new value of DesirabilityForOther for the reaction
	*/
	void Reaction::setDesirabilityForOther(int integer) {
		_desirabilityForOther = integer;
	}

	/**
	* Sets the event that the emotional reaction references
	* @param event - the new event referenced by the reaction 
	*/
	void Reaction::setEvent(Event* event) {
		_event = event;
	}

	/**
	* Sets the appraisal variable: Praiseworhtiness
	* @param integer - the new value of Praiseworthiness for the reaction
	*/
	void Reaction::setPraiseworthiness(int integer) {
		_praiseworthiness = integer;
	}

	/**
	* Converts the emotional Reaction to a String
	* @return the converted String
	*/
	const std::string Reaction::toString() {
		std::stringstream stringStream;

		stringStream << "(Reaction ";
		stringStream << "_event: ";
		if(_event != NULL)
			stringStream << *_event << ", ";
		else
			stringStream << "NULL" << ", ";
		stringStream << "_desirability: ";
		if(_desirability != NULL_APPRAISAL_VALUE)
			stringStream << _desirability << ", ";
		else
			stringStream << "NULL" << ", ";
		stringStream << "_desirabilityForOther: ";
		if(_desirabilityForOther != NULL_APPRAISAL_VALUE)
			stringStream << _desirabilityForOther << ", ";
		else
			stringStream << "NULL" << ", ";
		stringStream << "_praiseworthiness: ";
		if(_praiseworthiness != NULL_APPRAISAL_VALUE)
			stringStream << _praiseworthiness << ", ";
		else
			stringStream << "NULL" << ", ";
		stringStream << "_other: " << _other;
		stringStream << ")";

		return stringStream.str();
	}

}

//
///**
// * Checks the integrity of the Reaction by testing if the reaction references an event
// * with an speechAct not defined. In this case it throws the exception.
// */
//public void CheckIntegrity(IntegrityValidator val) throws UnknownSpeechActException {
//    String aux;
//    aux = _event.GetAction() + "(" +  _event.GetTarget();
//    ListIterator li = _event.GetParameters().listIterator();
//    while(li.hasNext()) {
//        aux = aux + "," + ((Parameter) li.next()).GetValue();
//    }
//    aux = aux + ")";
//    val.CheckSpeechAction(Name.ParseName(aux));
//}

///**
// * Replaces all unbound variables in the object by applying a numeric 
//    * identifier to each one. For example, the variable [x] becomes [x4]
//    * if the received ID is 4. 
//    * Attention, this method modifies the original object.
//    * @param variableID - the identifier to be applied
// */
//   public void ReplaceUnboundVariables(int variableID)
//   {
//   	if(this._other != null)
//   	{
//   		this._other.ReplaceUnboundVariables(variableID);
//   	}
//   }

//
//
///**
// * Applies a set of substitutions to the object, grounding it.
// * Example: Applying the substitution "[X]/John" in the name "Weak([X])" returns
// * "Weak(John)". 
// * Attention, this method modifies the original object.
// * @param bindings - A list of substitutions of the type "[Variable]/value"
// * @see Substitution
// */
//   public void MakeGround(ArrayList bindings)
//   {
//   	if(this._other != null)
//   	{
//   		this._other.MakeGround(bindings);
//   	}
//   }
//
//
///**
// * Applies a set of substitutions to the object, grounding it.
// * Example: Applying the substitution "[X]/John" in the name "Weak([X])" returns
// * "Weak(John)". 
// * Attention, this method modifies the original object.
// * @param subst - a substitution of the type "[Variable]/value"
// * @see Substitution
// */
//   public void MakeGround(Substitution subst)
//   {
//   	if(this._other != null)
//   	{
//   		this._other.MakeGround(subst);
//   	}
//   }
//
///**
// * Indicates if the Predicate is grounded (no unbound variables in it's WFN)
// * Example: Stronger(Luke,John) is grounded while Stronger(John,[X]) is not.
// * @return true if the Predicate is grounded, false otherwise
// */
//public boolean isGrounded() {
//	if(this._other == null) return true;
//	return this._other.isGrounded();
//}