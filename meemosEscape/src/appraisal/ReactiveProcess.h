/** 
* ReactiveProcess.h - Implements FearNot's Agent Reactive appraisal process
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 21/12/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 21/12/2004 - File created
* Jo�o Dias: 23/05/2006 - Added comments to each public method's header
* Jo�o Dias: 10/07/2006 - the class is now serializable 
* Jo�o Dias: 15/07/2006 - Removed the EmotionalState from the Class fields since the 
* 						   EmotionalState is now a singleton that can be used anywhere 
* 					       without previous references.
* Jo�o Dias: 17/07/2006 - The following methods were moved from class Agent to this class
* 							  - AddEmotionalReaction
* 							  - GetEmotionalReactions
* 							  - GetActionTendencies
* Jo�o Dias: 21/07/2006 - The class constructor now only receives the agent's name
* Jo�o Dias: 05/09/2006 - Changed the way in which Attribution Emotions are generated. From
* 						   now on, is no longer necessary to specify attribution reactions for
* 						   a character. They are automatically generated when a "look-at" action
* 						   is perceived, and the like appraisal variable is retrieved from semantic
* 						   memory (the KnowledgeBase)
* 						 - Changed the way in which FortuneOfOthers Emotions are determined, now 
* 						   the interpersonal relationShip (like/dislike) between characters affects the desirability
* 						   of the event and thus the final emotions being generated.
* Jo�o Dias: 06/09/2006 - Another change in the way that FortuneOfOthers Emotions are determined,
* 						   they now are generated for the Actor that performs the actions, and the
* 						   object or character that is the target of the action. We take into account
* 						   the like relations between every character to determine the event's desirability.
* Jo�o Dias: 18/09/2006 - Small change in the generation of FortuneOfOther emotions. Now they consider the 
* 						   the new other variable. If this variable is defined the FortuneOfOther emotion
* 						   created will be directed to the character specified in the variable. If the variable
* 						   is not defined (null) the emotion is proccessed as before.
* Jo�o Dias: 20/09/2006 - Removed the method RemoveSelectedAction. The method
* 						   GetSelectedMethod now additionally has the functionality 
* 						   of the RemoveSelectedAction method
* Jo�o Dias: 03/02/2007 - the Reset command now removes the selectAction if any
* Jo�o Dias: 04/08/2007 - The intensity of attribution emotions (Love/Hate) were halved
* Paulo Gomes: 27/06/2009 - Converted and adapted to C++. Removed Action/Coping methods.
*/
// Removed Relation Bias
#ifndef _ReactiveProcess_h_
#define _ReactiveProcess_h_

#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <utility>
#include "EmotionalReactionRule.h"
#include "Event.h"
#include "EmotionalState.h"
#include "EmotionalParameters.h"
#include "Printable.h"

namespace appraisal{
	class ReactiveProcess: public Printable{

	public:
		ReactiveProcess(const std::string& nameSelf,EmotionalState* emotionalState);
		~ReactiveProcess();

	protected:
		std::list<Event*> _eventPool;
		std::string _self;
		std::vector<EmotionalReactionRule*> _emotionalReactionRules;
		EmotionalState* _emotionalState;

	public:
		void addEvent(Event* event);
		const std::vector<EmotionalReactionRule*>& getEmotionalReactionRules() const;
		void addEmotionalReactionRule(EmotionalReactionRule* emotionalReactionRule);
		void appraisal();
		void clearEventPool();
		void reset();
		const std::string toString();

	private:
		void generateEmotions(Event* event,Reaction* emReaction);

	protected:
		virtual void generateActionBasedEmotions(Event* event, int praiseworthiness) = 0;
		virtual void generateFortuneForAll(Event* event, int desirability, int desirabilityForOther, std::string other) = 0;
		virtual void generateFortuneOfOtherEmotions(Event* event, int desirability, int desirabilityForOther,std::string target) = 0;
		virtual void generateWellBeingEmotions(Event* event, int desirability) = 0;
	};
}

#endif