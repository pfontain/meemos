/** 
* Mood.cpp - Represents a character's mood
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 20/01/2004 - File created
* Jo�o Dias: 02/07/2006 - Replaced System's timer by an internal agent simulation timer
* Jo�o Dias: 10/07/2006 - the class is now serializable 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Serialization removed.
*/
#include "Mood.h"

namespace appraisal{
	/**
	* Creates a new neutral Mood
	*/
	Mood::Mood()
	{
		//the mood allways starts as neutral
		_intensityATt0 = 0.0f;
		_intensity = 0.0f;
		_t0 = 0L;
	}

	/**
	* Gets a float value that represents mood. Mood is ranged 
	* between [-10;10], a negative value represents a bad mood,
	* a positive value represents good mood and values near 0
	* represent a neutral mood
	* @return a float value representing mood ranged in [-10;10]
	*/
	float Mood::getMoodValue()
	{
		return _intensity;
	}

	/**
	* Decays the mood according to the system's timer.
	* @return the mood's intensity after being decayed
	*/
	float Mood::decayMood()
	{
		if(_intensityATt0 == 0)
		{
			_intensity = 0;
			return 0;
		}

		long deltaT;
		deltaT = (Utilities::Time::getTime() - _t0);
		_intensity = _intensityATt0 * (exp(- EmotionalParameters::MOOD_DECAY_FACTOR * deltaT));

		if(fabs(_intensity) < EmotionalParameters::MINIMUM_MOOD_VALUE)
		{
			_intensityATt0 = 0;
			_t0 = Utilities::Time::getTime();
			_intensity = 0;
		}
		return _intensity;
	}

	/**
	* Updates the character's mood when a given emotion is "felt" by
	* the character. 
	* @param em - the ActiveEmotion that will influence the character's
	* 		    current mood
	*/
	void Mood::updateMood(ActiveEmotion* em)
	{
		float newMood;

		if(em->getValence() == EmotionValence::POSITIVE) {
			newMood = _intensity + (em->getIntensity() * EmotionalParameters::EMOTION_INFLUENCE_ON_MOOD);
			//mood is limited between -10 and 10
			newMood = Utilities::Math::min(newMood, 10);
		}
		else 
		{
			newMood = _intensity - (em->getIntensity() * EmotionalParameters::EMOTION_INFLUENCE_ON_MOOD);
			//mood is limited between -10 and 10
			newMood = Utilities::Math::max(newMood, -10);
		}

		_intensityATt0 = _intensity = newMood;
		_t0 = Utilities::Time::getTime();
	}

	/**
	* Manually sets a new value for Mood. Use it with caution and only
	* when you explicitly want to change a character's mood without anything
	* happening
	* @param newMood - the new value of the mood. Must be ranged in [-10;10].
	*                  Remember that -10 represents a very bad mood, 10 a very
	* 			     good mood and values near 0 represent a neutral mood
	*/
	void Mood::setMood(float newMood)
	{
		float aux;

		if(newMood > 10)
		{
			aux = 10; 
		}
		else if(newMood < -10)
		{
			aux = -10;
		}
		else
		{
			aux = newMood;
		}

		_intensityATt0 = _intensity = aux;
		_t0 = Utilities::Time::getTime();
	}
}