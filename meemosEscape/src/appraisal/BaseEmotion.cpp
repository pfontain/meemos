/** 
* BaseEmotion.cpp - Represents an emotion, which is an
* instance of a particular Emotion Type
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 24/05/2006 - Added comments to each public method's header
* Jo�o Dias: 10/07/2006 - the class is now serializable 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Serialization removed.
*/
#include "BaseEmotion.h"

namespace appraisal{
	/**
	* Creates a new BasicEmotion
	* @param type - the type of the Emotion (ex: Hope, Distress). use the enumerable
	* 				 EmotionType for the possible set of emotion types
	* @param potential - the potential value for the intensity of the emotion
	* @param cause - the event that caused the emotion
	* @param direction - if the emotion is targeted to someone (ex: angry with Luke),
	* 				      this parameter specifies the target
	*/
	BaseEmotion::BaseEmotion(EmotionType::Type type, float potential, Event* cause,std::string direction){
		_type = type;
		_potential = potential;
		_cause = cause;
		_direction = direction;

		if(type == EmotionType::JOY ||
			type == EmotionType::LOVE ||
			type == EmotionType::HOPE ||
			type == EmotionType::SATISFACTION ||
			type == EmotionType::RELIEF ||
			type == EmotionType::GLOATING ||
			type == EmotionType::HAPPYFOR ||
			type == EmotionType::ADMIRATION ||
			type == EmotionType::PRIDE || 
			type == EmotionType::GRATIFICATION ||
			type == EmotionType::GRATITUDE) 
		{
			_valence = EmotionValence::POSITIVE;
		}
		else 
		{
			_valence = EmotionValence::NEGATIVE;
		}
	}

	/**
	 * Creates a new BaseEmotion that consists in a copy of a given emotion
	 * @param em - the emotion that will be copied into our new emotion
	 */
	BaseEmotion::BaseEmotion(const BaseEmotion& em) {
		_type = em._type;
		_potential = em._potential;
		_cause = em._cause->copy();
		_direction = em._direction;
		_valence = em._valence;
	}

	/*
	* Destroys a BaseEmotion
    * dallocates space reserved for the cause event
	*/
	BaseEmotion::~BaseEmotion(){
		_cause->destroy();
		delete _cause;
	}

	/**
	* Gets the cause of the emotion
	* @return - the event that caused the emotion
	*/
	Event* BaseEmotion::getCause() {
		return _cause;
	}

	/**
	* Sets the emotion's cause
	* @param cause - the event that caused the emotion
	*/
	void BaseEmotion::setCause(Event* cause) {
		_cause = cause;
	}

	/* Gets the direction of the emotion (if the emotion directed to someone)
	* @return - the name of the character to whom this emotion is directed
	*/
	std::string BaseEmotion::getDirection() {
		return _direction;
	}

	/**
	* Gets the emotion's potential
	* @return - the potential for the intensity of the emotion
	*/
	float BaseEmotion::getPotential() {
		return _potential;
	}

	/**
	* Gets the emotion's type 
	* @return a EmotionType enumerable
	*/
	EmotionType::Type BaseEmotion::getType() {
		return _type;
	}

	/**
	* Gets the emotion's valence (either positive or negative)
	* @return a EmotionValence enumerable
	*/
	EmotionValence::Valence BaseEmotion::getValence() {
		return _valence;
	}

	/**
	 * Gets an HashCode
	 * @return an int that can be used as hashcode
	 */
	int BaseEmotion::getHashCode(){
		short emotionTypeNumber = (short)_type;
		short causeNumber = (short)(_cause->toString()).length();
		short directionNumber = (short)_direction.length();
		int hashCode = directionNumber + causeNumber*100 + emotionTypeNumber*10000;
		return hashCode;
	}

	/**
	* Converts the BaseEmotion to a String
	* @return the converted String
	*/
	const std::string BaseEmotion::toString() {
		std::stringstream stringStream;
		stringStream << "[BaseEmotion] ";
		stringStream << "EmotionType: " << EmotionType::emotionTypeToString(_type) << ", ";
		stringStream << "Valence: " << EmotionValence::emotionValenceToString(_valence) << ", ";
		stringStream << "_potential: " << _potential << ", ";
		stringStream << "Cause: " << *_cause << ", ";
		stringStream << "Direction: " << _direction;
		return stringStream.str();
	}

}