/** 
* EmotionDisposition.cpp - Represents the character's emotional disposition 
* (Emotional Threshold + Decay Rate) towards an Emotion Type.
*  
* Copyright (C) 2006 GAIPS/INESC-ID 
*  
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Company: GAIPS/INESC-ID
* Project: FAtiMA
* Created: 17/01/2004 
* @author: Jo�o Dias
* Email to: joao.assis@tagus.ist.utl.pt
* @author: Paulo Gomes
* Email to: paulo.f.gomes@ist.utl.pt
* 
* History: 
* Jo�o Dias: 17/01/2004 - File created
* Jo�o Dias: 10/07/2006 - the class is now serializable
* Jo�o Dias: 23/05/2006 - Added comments to each public method's header 
* Paulo Gomes: 25/06/2009 - Converted and adapted to C++. Removed Serialization.
*/
#include "EmotionDisposition.h"

namespace appraisal{
	/**
	* Creates a new EmotionDisposition
	* 
	* @param emotionType - a enuberable value representing the type of the Emotion (ex: Fear, Hope, etc)
	* @param threshold - the threshold for the emotion
	* @param decay - the decay rate for the emotion
	* 
	* @see the enumerable EmotionType to see the possible types of Emotion
	*/
	EmotionDisposition::EmotionDisposition(EmotionType::Type emotionType, int threshold, int decay) {
		_emotionType = emotionType;
		_threshold = threshold;
		_decay = decay;
	}

	/**
	* Gets the decay rate for the emotion
	* @return the decay rate
	*/
	int EmotionDisposition::getDecay() {
		return _decay;
	}

	/**
	* Gets the emotion's type
	* @return an emotion type (enumerable)
	* @see the enumerable EmotionType::Type
	*/
	EmotionType::Type EmotionDisposition::getEmotionType() {
		return _emotionType;
	}

	int EmotionDisposition::getThreshold() {
		return _threshold;
	}

	/**
	* Converts the emotional disposition to a String
	* @return the converted String
	*/
	const std::string EmotionDisposition::toString() {
		std::stringstream stringStream;

		stringStream << "(EmotionDisposition ";
		stringStream << "_emotionType: " << EmotionType::emotionTypeToString(_emotionType) << ", ";
		stringStream << "_decay: " << _decay << ", ";
		stringStream << "_threshold: " << _threshold;
		stringStream << ")";

		return stringStream.str();
	}
}

