#include "EventWitness.h"

namespace avt{

	EventWitness::EventWitness(Event* witnessedEvent){
		_witnessedEvent = witnessedEvent;
		_locationSet = witnessedEvent->isLocationSet();
		_location = witnessedEvent->getLocation();
	}

	EventWitness::~EventWitness(){
		destroy();
	}

	Event* EventWitness::getWitnessedEvent(){
		return _witnessedEvent;
	}

	EventType::Type EventWitness::type(){
		return EventType::WITNESS;
	}

	Event* EventWitness::copy(){
		return new EventWitness(_witnessedEvent->copy());
	}

	const std::string EventWitness::toString(){
			std::string description;
			description = "EventWitness - ";
			description.append(_witnessedEvent->toString());
			return description;
		}

	EventType::Type EventWitness::getWitnessedEventType(){
			return _witnessedEvent->type();
		}

	bool EventWitness::equal(appraisal::Event* event){
		if(event->type() == EventType::WITNESS){
			EventWitness* otherEventWitness = dynamic_cast<EventWitness*>(event);
			Event* otherEventWitnessedEvent = otherEventWitness->getWitnessedEvent();

			return _witnessedEvent->equal(otherEventWitnessedEvent);
		}
		else
			return false;
	}

	int EventWitness::match(appraisal::Event* event){
		if(event->type() == EventType::WITNESS){
			EventWitness* otherEventWitness = dynamic_cast<EventWitness*>(event);
			Event* otherEventWitnessedEvent = otherEventWitness->getWitnessedEvent();

			return _witnessedEvent->match(otherEventWitnessedEvent);
		}
		else
			return NO_MATCH;
	}

	void EventWitness::destroy(){
			_witnessedEvent->destroy();
			delete _witnessedEvent;
	}
}