#include "MemoryTrace.h"

namespace meemo{
	MemoryTrace::MemoryTrace(appraisal::Event* event,appraisal::ActiveEmotion* causedEmotion,long time){
		_event = event;
		_causedEmotion = causedEmotion;
		_time = time;
	}

	MemoryTrace::~MemoryTrace(){
		_event->destroy();
		delete _event;
		delete _causedEmotion;
	}

	appraisal::Event* MemoryTrace::getEvent(void) const{
		return _event;
	}

	appraisal::ActiveEmotion* MemoryTrace::getCausedEmotion(void) const{
		return _causedEmotion;
	}

	long MemoryTrace::getTime(void) const{
		return _time;
	}

	const std::string MemoryTrace::toString(){
		std::stringstream stringStream;
		stringStream << "(MemoryTrace: ";
		stringStream << "_event: " << *_event << ", ";
		appraisal::EmotionType::Type emotionType = _causedEmotion->getType();
		stringStream << "_causedEmotion: " << appraisal::EmotionType::emotionTypeToString(emotionType);
		stringStream << ")";
		return stringStream.str();
	}
}