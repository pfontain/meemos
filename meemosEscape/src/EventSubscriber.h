#ifndef _EventSubscriber_h_
#define _EventSubscriber_h_

#include "Event.h"

namespace avt{
	class EventSubscriber
	{	
	public:
		virtual void onEvent(Event* event) = 0;
	};
}

#endif