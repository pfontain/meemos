#include "SpeechBalloon.h"

namespace avt{
	SpeechBalloon::SpeechBalloon(Physical* physical){
		_physical = physical;
		_ellipseA = cg::Properties::instance()->getInt("BALLOON_ELLIPSE_A");
		_ellipseB = cg::Properties::instance()->getInt("BALLOON_ELLIPSE_B");
		_numberEdges = cg::Properties::instance()->getInt("BALLOON_NUMBER_EDGES");
		_ellipsePosition = cg::Properties::instance()->getVector2d("BALLOON_ELLIPSE_1_POSITION");
		_tailTipPosition = cg::Properties::instance()->getVector2d("TAIL_TIP_POSITION");
		_tailSize = cg::Properties::instance()->getInt("TAIL_SIZE");
		_material = new MaterialStatic();
		_physics = new PhysicsDefault();
		_numberTextures = cg::Properties::instance()->getInt("MAX_NUMBER_MEEMOS");
		_on = false;
		_activeTexture = 1;
		_textures.resize(_numberTextures);
		_onDuration = cg::Properties::instance()->getDouble("SPEECH_BALLOON_DURATION");
		_speechSize = cg::Properties::instance()->getInt("BALLOON_SQUARE_SIDE_SIZE");
	}

	SpeechBalloon::~SpeechBalloon(){
		delete _material;
		delete _physics;
	}

	void SpeechBalloon::init() {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("BALLOON_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->init();

		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		initTextures();

		makeModels();
	}

	void SpeechBalloon::activate(unsigned int speechNumber){
		_on = true;
		_timer = 0.0;
		_activeTexture = speechNumber;
	}

	void SpeechBalloon::turnOff(void){
		_on = false;
	}

	void SpeechBalloon::update(double elapsedSeconds,const cg::Vector3d& observerPosition){
		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		cg::Vector3d observerDirection;
		observerDirection[0]= observerPosition[0] - physicalPosition[0];
		observerDirection[1]= observerPosition[1] - physicalPosition[1];
		observerDirection[2]= 0.0;
		cg::Vector3d observerDirectionNormalized(normalize(observerDirection));

		_physics->rotate(observerDirectionNormalized);
		_physics->step();

		if(_on){
			_timer += elapsedSeconds;
			if(_timer > _onDuration)
				_on = false;
		}
	}

	void SpeechBalloon::draw(){
		if(_on){
			glPushMatrix();
			{
				_physics->applyTransforms();
				_material->draw();
				glCallList(_modelDLFirst + _activeTexture - 1);
			}
			glPopMatrix();
		}
	}

	void SpeechBalloon::initTextures(void){
		for(unsigned int textureNumber = 0; textureNumber < _numberTextures; textureNumber++){
			std::stringstream textureFileNameStream;
			textureFileNameStream << cg::Properties::instance()->getString("NUMBER_BILLBOARD_TEXTURE_NAME");
			textureFileNameStream << textureNumber + 1 << ".";
			textureFileNameStream << cg::Properties::instance()->getString("NUMBER_BILLBOARD_TEXTURE_EXTENSION");
			std::string textureFileName = textureFileNameStream.str();
			_textures[textureNumber].open(textureFileName);
		}
	}

	void SpeechBalloon::makeModels(void){
		_modelDLFirst = glGenLists(_numberTextures);
		for(unsigned int textureNumber = 0; textureNumber < _numberTextures; textureNumber++){
			assert(_modelDLFirst + textureNumber != 0);
			glNewList(_modelDLFirst + textureNumber,GL_COMPILE);
			glBindTexture(GL_TEXTURE_2D, _textures[textureNumber].id);
			lightsAndModel();
			glEndList();				
		}
	}

	void SpeechBalloon::lightsAndModel(void){
		GLfloat lightingAmbientArray[4];
		GLfloat* lightingAmbient = &(lightingAmbientArray[0]);
		glGetFloatv(GL_LIGHT_MODEL_AMBIENT,lightingAmbient);
		GLfloat lightingAmbientSpeechBalloon[] = {1,1,1,1.0};
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbientSpeechBalloon);

		makeModelBalloon();
		makeModelTail();
		makeModelSpeech();

		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbient);
	}

	void SpeechBalloon::makeModelBalloon(void){
		using namespace Utilities::Draw;
		glPushMatrix();{
			glTranslated(0,_ellipsePosition[0],_ellipsePosition[1]);
			drawEllipse(_ellipseA,_ellipseB,_numberEdges);
		}glPopMatrix();
	}

	void SpeechBalloon::makeModelTail(void){
		glBegin(GL_TRIANGLES);
		{
			glNormal3f(1,0,0);
			glVertex3f(0.0, _tailTipPosition[0], _tailTipPosition[1]);
			glVertex3f(0.0, _ellipsePosition[0] + _tailSize/2.0,_ellipsePosition[1]);
			glVertex3f(0.0, _ellipsePosition[0] - _tailSize/2.0,_ellipsePosition[1]);
		}
		glEnd();
	}

	void SpeechBalloon::makeModelSpeech(void){
		cg::Vector3d position = cg::Vector3d(0,_ellipsePosition[0],_ellipsePosition[1]);
		double halfSideSize = _speechSize/2;
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);
		{
			glNormal3f(1,0,0);
			glTexCoord2f(0,0);
			glVertex3f(Utilities::Math::smallPositive(),-halfSideSize+position[1],-halfSideSize+position[2]);
			glTexCoord2f(1,0);
			glVertex3f(Utilities::Math::smallPositive(),+halfSideSize+position[1],-halfSideSize+position[2]);
			glTexCoord2f(1,1);
			glVertex3f(Utilities::Math::smallPositive(),+halfSideSize+position[1],+halfSideSize+position[2]);
			glTexCoord2f(0,1);
			glVertex3f(Utilities::Math::smallPositive(),-halfSideSize+position[1],+halfSideSize+position[2]);	
		}
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

}