#ifndef _BehaviourMeemoMinionNoMemoryRandomized_h_
#define _BehaviourMeemoMinionNoMemoryRandomized_h_

#include "BehaviourMeemoMinionNoMemory.h"

namespace meemo{
	class BehaviourMeemoMinionNoMemoryRandomized: public BehaviourMeemoMinionNoMemory{
	public:
		BehaviourMeemoMinionNoMemoryRandomized(EmotionalAgent* meemoMinionEmotionalAgent,MovePathAgent* meemoMinionMovePathAgent,avt::AbstractLevel* level,avt::AbstractGameLogic* gameLogic);

	private:
		int _thoughtBalloonDisplayInterval;
		int _thoughtBalloonDisplayDuration;
		int _thoughtBalloonDisplayVariation;
		double _thoughtBalloonTimer;
		float _randomThoughtBalloonDisplayInterval;
		float _randomThoughtBalloonDisplayDuration;
		avt::ThoughtBalloonState::State _thoughtBalloonState;

	public:
		virtual void update(double elapsedSeconds);

	protected:
		void updateThoughtBalloon(double elapsedSeconds);
		void setRandomThoughtBalloonDisplayInterval(void);
		void setRandomThoughtBalloonDisplayDuration(void);
	};
}

#endif // _BehaviourMeemoMinionNoMemoryRandomized_h_