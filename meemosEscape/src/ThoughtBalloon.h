#ifndef _ThoughtBalloonBaloon_avt_
#define _ThoughtBalloonBaloon_avt_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Physical.h"
#include "PhysicsDefault.h"
#include "Texture.h"

namespace avt{
	namespace ThoughtBalloonState{
		enum State{
			OFF = 0,
			ANVIL_HIT = 1,
			TRAPDOOR_FALL = 2
		};
	}

	class ThoughtBalloon{
	public:
		ThoughtBalloon(Physical* physical);
		~ThoughtBalloon();
		void init();

	private:
		int _modelDLAnvilHit;
		int _modelDLTrapDoorHit;
		Material* _material;
		Physical* _physical;
		PhysicsDefault* _physics;
		float _ellipseA;
		float _ellipseB;
		unsigned short _numberEdges;
		ThoughtBalloonState::State _state;
		Texture _textureAnvilHit;
		Texture _textureTrapdoorFall;
		cg::Vector2d _ellipse1Position;
		cg::Vector2d _ellipse2Position;
		cg::Vector2d _ellipse3Position;
		cg::Vector3d _ellipseSizes;
		double _thoughtSize;

	public:
		void setState(ThoughtBalloonState::State state);
		void update(const cg::Vector3d& observerPosition);
		void draw();

	private:
		void makeModels(void);
		void lightsAndModel(void);
		void makeModelThought(void);
		void makeModelBalloon(void);
	};
}

#endif //_ThoughtBalloonBaloon_avt_