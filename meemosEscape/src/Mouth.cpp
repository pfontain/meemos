#include "Mouth.h"

namespace avt{
	Mouth::Mouth(Physical* physical){
		_physical = physical;
		_relativePosition = cg::Properties::instance()->getVector3d("MOUTH_RELATIVE_POSITION");
		_radius = cg::Properties::instance()->getInt("MOUTH_RADIUS");
		_width = cg::Properties::instance()->getInt("MOUTH_WIDTH");
		_numberEdges = cg::Properties::instance()->getInt("MOUTH_NUMBER_EDGES");
		_material = new MaterialStatic();
		_state = MouthState::NEUTRAL;
	}

	Mouth::~Mouth(){
		delete _material;
	}

	void Mouth::init() {
		_material->setAmbient(0.1,0.1,0.1,1.0);
		_material->setDiffuse(0.1,0.1,0.1,1.0);
		_material->init();

		

		makeModels();
	}

	void Mouth::makeModels(){
		makeModelNeutral();
		makeModelSmile();
		makeModelDistress();
	}

	void Mouth::makeModelNeutral()
	{
		_modelDLNeutral = glGenLists(1);
		assert(_modelDLNeutral != 0);
		glNewList(_modelDLNeutral,GL_COMPILE);
		{
			GLboolean glCullFace;
			glGetBooleanv(GL_CULL_FACE,&glCullFace);
			glDisable(GL_CULL_FACE);
			makeModelRectangle();
			if(glCullFace)
				glEnable(GL_CULL_FACE);
		}
		glEndList();
	}

	void Mouth::makeModelRectangle(){
		glBegin(GL_QUADS);
		float mouthLenght = 0.25*Utilities::Math::pi()*_radius;
		float halfMouthLenght = mouthLenght/2;
		float halfWidth = _width/2;

		cg::Vector3d position;
		glNormal3f(1,0,0);
		position = _relativePosition + cg::Vector3d(0,halfMouthLenght,halfWidth-_radius);
		glVertex3f(position[0],position[1],position[2]);
		position = _relativePosition + cg::Vector3d(0,-halfMouthLenght,halfWidth-_radius);
		glVertex3f(position[0],position[1],position[2]);	
		position = _relativePosition + cg::Vector3d(0,-halfMouthLenght,-halfWidth-_radius);
		glVertex3f(position[0],position[1],position[2]);	
		position = _relativePosition + cg::Vector3d(0,halfMouthLenght,-halfWidth-_radius);
		glVertex3f(position[0],position[1],position[2]);	
		glEnd();
	}

	void Mouth::makeModelSmile()
	{
		_modelDLSmile = glGenLists(1);
		assert(_modelDLSmile != 0);
		glNewList(_modelDLSmile,GL_COMPILE);
		{	
			float deltaRadians = 0.5*Utilities::Math::pi()/_numberEdges;
			float startAngle = 0.5*Utilities::Math::pi()*2.5;
			GLboolean glCullFace;
			glGetBooleanv(GL_CULL_FACE,&glCullFace);
			glDisable(GL_CULL_FACE);
			makeModelBand(_relativePosition,deltaRadians,startAngle);
			if(glCullFace)
				glEnable(GL_CULL_FACE);
		}
		glEndList();
	}

	void Mouth::makeModelDistress(){
		_modelDLDistress = glGenLists(1);
		assert(_modelDLDistress != 0);
		glNewList(_modelDLDistress,GL_COMPILE);
		{	
			double deltaRadians = (Utilities::Math::pi()/3.0)/_numberEdges;
			double startAngle = Utilities::Math::pi()/3.0;
			cg::Vector3d mouthCenterPosition = _relativePosition - cg::Vector3d(0,0,2*_radius);
			GLboolean glCullFace;
			glGetBooleanv(GL_CULL_FACE,&glCullFace);
			glDisable(GL_CULL_FACE);
			makeModelBand(mouthCenterPosition,deltaRadians,startAngle);
			if(glCullFace)
				glEnable(GL_CULL_FACE);
		}
		glEndList();
	}

	void Mouth::makeModelBand(const cg::Vector3d& centerPosition,float deltaRadians,float startAngle){
		glBegin(GL_QUAD_STRIP);
		{
			for(int iQuad = 0; iQuad <= _numberEdges; iQuad++){
				cg::Vector3d iPointInnerCircle;
				cg::Vector3d iPointOuterCircle;

				float iPointInnerCircleY = _radius*cos(deltaRadians*iQuad+startAngle);
				float iPointInnerCircleZ = _radius*sin(deltaRadians*iQuad+startAngle);
				iPointInnerCircle.set(0,iPointInnerCircleY,iPointInnerCircleZ);

				float iPointOuterCircleY = (_radius+_width)*cos(deltaRadians*iQuad+startAngle);
				float iPointOuterCircleZ = (_radius+_width)*sin(deltaRadians*iQuad+startAngle); 
				iPointOuterCircle.set(0,iPointOuterCircleY,iPointOuterCircleZ);

				cg::Vector3d position;
				position = centerPosition + iPointInnerCircle;
				glVertex3f(position[0],position[1],position[2]);
				position = centerPosition + iPointOuterCircle;
				glVertex3f(position[0],position[1],position[2]);				
			}
		}
		glEnd();
	}

	void Mouth::draw() {
		glPushMatrix();
		{
			_material->draw();
			switch(_state){
				case MouthState::SMILE : glCallList(_modelDLSmile); break;
				case MouthState::DISTRESS : glCallList(_modelDLDistress); break;
				default: glCallList(_modelDLNeutral);
			}
		}
		glPopMatrix();
	}

	void Mouth::neutralOn(){
		_state = MouthState::NEUTRAL;
	}

	void Mouth::smileOn(){
		_state = MouthState::SMILE;
	}

	void Mouth::distressOn(){
		_state = MouthState::DISTRESS;
	}
}