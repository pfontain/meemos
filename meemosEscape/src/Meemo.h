#ifndef _Meemo_h_
#define _Meemo_h_

#include "cg/cg.h"
#include "MeemoMesh.h"
#include "MaterialDynamic.h"
#include "EventBuffer.h"
#include "AbstractLevel.h"
#include "EventWitnessNotifier.h"
#include "Physical.h"
#include "PhysicsRegistry.h"
#include "PhysicsMeemo.h"
#include "Eyes.h"
#include "Mouth.h"
#include "ThoughtBalloon.h"
#include "SpeechBalloon.h"
#include "DebugRegistry.h"
#include "AbstractMeemo.h"
#include "Behaviour.h"
#include "Mind.h"
#include "AbstractGameLogic.h"
#include "EventSeeOpenDoor.h"
#include "EventSeeDoor.h"
#include "EventSeeLever.h"

namespace avt{
	namespace MeemoAction{
		enum Action{
			NONE = 0,
			COME_HERE = 1,
			LOOK_AT_ME = 2,
			JUMP = 3,
			STOP = 4
		};
	}

	namespace MeemoRGB{
		enum RGB{
			RED = 0,
			GREEN = 1,
			BLUE = 2
		};
	}

	class Meemo : public cg::Entity,
		public cg::IDrawListener,
		public cg::IUpdateListener,
		public Debugable,
		public AbstractMeemo
	{
	public:
		Meemo(std::string id,int size,AbstractLevel* level,AbstractGameLogic* gameLogic);
		~Meemo();
		virtual void init();

	protected:
		int _modelDL;
		MeemoMesh meemoMesh;
		Material* _material;
		PhysicsMeemo* _physics;
		float _size;
		Eyes* _eyes;
		Mouth* _mouth;
		ThoughtBalloon* _thoughtBalloon;
		SpeechBalloon* _speechBalloon;
		EventBuffer* _eventBuffer;
		AbstractLevel* _level;
		float _visionRadius;
		float _visionAngle;
		float _hearingRadius;

		meemo::Mind* _mind;
		meemo::Behaviour* _behaviour;
		cg::Vector3d _materialColourFirst;
		cg::Vector3d _materialColourSecond;
		AbstractGameLogic* _gameLogic;

		bool _doorFound;
		bool _openDoorFound;
		bool _leverFound;

	public:
		virtual void draw(void);
		void toggleDebugMode(void);
		void subscribe(EventWitnessNotifier* eventNotifier);

		const cg::Vector3d getPosition(void);
		const cg::Quaterniond getOrientation(void);
		appraisal::EmotionalState* getEmotionalState(void);
		meemo::MemoryStorage* getMemoryStorage(void);
		meemo::Mind* getMind();
		float getVisionRadius();
		const cg::Vector3d getVisionDirection();
		float getVisionAngle();
		const std::string& getId() const;
		virtual Physics* getPhysics(void);

		bool canSenseEvent(meemo::Location& eventLocation);
		bool isLocationVisible(meemo::Location& location);
		void turnOnNeutralEyes();
		void turnOnJoyEyes();
		void turnOnDistressEyes();
		void turnOnAngerEyes();
		void turnOnNeutralMouth();
		void turnOnSmileMouth();
		void turnOnDistressMouth();
		void setStateThoughtBalloon(ThoughtBalloonState::State state);
		void changeColourIntensity(float intensity);
		void activateSpeechBalloon(unsigned int speechNumber);
		void turnOffSpeechBalloon(void);

	protected:
		void makeModel();
		virtual void update(unsigned long elapsed_millis);
		virtual void initMaterial();

	private:
		void initPhysics();

		void generateVisionEvents(void);
		bool isWithinVisionRadius(meemo::Location& eventLocation);
		bool isWithinVisionAngle(meemo::Location& eventLocation);
		bool isLocationInFront(cg::Vector3d& crossProductVisionLocationDirection,float dotProductVisionLocationDirection);
		bool isLocationBehind(cg::Vector3d& crossProductVisionLocationDirection);
		bool isAngleVisionLocationDirectionSmallEnough(float dotProductVisionLocationDirection);
		bool isFishEyed();
		bool isEventAudible(meemo::Location& eventLocation);
		bool isWithinHearingRadius(meemo::Location& eventLocation);
		bool isWithinRadius(meemo::Location& eventLocation,float radius);
	};
}

#endif