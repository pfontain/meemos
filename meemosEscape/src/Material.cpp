// This file is an example for CGLib.
//
// CGLib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// CGLib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CGLib; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Copyright 2007 Carlos Martinho

#include "Material.h"


namespace avt {

	Material::Material() {
		mat_emission[0] = 0.0;
		mat_emission[1] = 0.0;
		mat_emission[2] = 0.0;
		mat_emission[3] = 1.0;

		mat_ambient[0] = 0.0;
		mat_ambient[1] = 0.0;
		mat_ambient[2] = 0.0;
		mat_ambient[3] = 1.0;

		mat_diffuse[0] = 0.0;
		mat_diffuse[1] = 0.0;
		mat_diffuse[2] = 0.0;
		mat_diffuse[3] = 1.0;

		mat_specular[0] = 0.0;
		mat_specular[1] = 0.0;
		mat_specular[2] = 0.0;
		mat_specular[3] = 1.0;

		mat_shininess[0] = 0.0;
	}

	void Material::setEmission(GLfloat emission1, GLfloat emission2, GLfloat emission3, GLfloat emission4) {
		mat_emission[0] = emission1;
		mat_emission[1] = emission2;
		mat_emission[2] = emission3;
		mat_emission[3] = emission4;
	}

	void Material::setAmbient(GLfloat ambient1, GLfloat ambient2, GLfloat ambient3, GLfloat ambient4) {
		mat_ambient[0] = ambient1;
		mat_ambient[1] = ambient2;
		mat_ambient[2] = ambient3;
		mat_ambient[3] = ambient4;
	}

	void Material::setDiffuse(GLfloat diffuse1, GLfloat diffuse2, GLfloat diffuse3, GLfloat diffuse4) {
		mat_diffuse[0] = diffuse1;
		mat_diffuse[1] = diffuse2;
		mat_diffuse[2] = diffuse3;
		mat_diffuse[3] = diffuse4;
	}

	void Material::setSpecular(GLfloat specular1, GLfloat specular2, GLfloat specular3, GLfloat specular4) {
		mat_specular[0] = specular1;
		mat_specular[1] = specular2;
		mat_specular[2] = specular3;
		mat_specular[3] = specular4;
	}

	void Material::setShininess(GLfloat shininess) {
		mat_shininess[0] = shininess;
	}
}