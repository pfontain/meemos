#ifndef __StatePhysicsMeemoMinionTurnHop_avt__
#define __StatePhysicsMeemoMinionTurnHop_avt__

#include "StatePhysicsMeemoMinionVisitor.h"
#include "StatePhysicsMeemoMinion.h"
#include "cg/cg.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionTurnHop: public StatePhysicsMeemoMinion{	
	public:
		StatePhysicsMeemoMinionTurnHop(const cg::Vector3d& position);

	private:
		cg::Vector3d _turnToPosition;

	public:
		const cg::Vector3d& getTurnToPosition(void);
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt(void);
	};
}


#endif // __StatePhysicsMeemoMinionTurnHop_avt__
