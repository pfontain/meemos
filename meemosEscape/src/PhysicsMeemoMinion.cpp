#include "PhysicsMeemoMinion.h"

namespace avt {

	PhysicsMeemoMinion::PhysicsMeemoMinion(UpdatePathAgent* meemoMinion){
		_stopDistance = cg::Properties::instance()->getDouble("MEEMO_STOP_DISTANCE");
		_stopDistanceRelaxed = cg::Properties::instance()->getDouble("MEEMO_STOP_DISTANCE_RELAXED");
		_states.push_back(new StatePhysicsMeemoMinionIdle());
		_waitDuration = cg::Properties::instance()->getDouble("MEEMO_WAIT_DURATION");
		_meemoMinion = meemoMinion;
	}

	PhysicsMeemoMinion::~PhysicsMeemoMinion(){
		while(!_states.empty()){
			StatePhysicsMeemoMinion* state = _states.front();
			state->destroy(this);
			delete state;
			_states.pop_front();
		}
	}

	void PhysicsMeemoMinion::setWaitDurations(std::queue<double> waitDurations){
		_waitDurations = waitDurations;
	}

	bool PhysicsMeemoMinion::stop() {
		StatePhysicsMeemoMinion* currentState = _states.front();
		if(currentState->canInterrupt()){
			while(!_states.empty()){
				StatePhysicsMeemoMinion* state = _states.front();
				state->destroy(this);
				delete state;
				_states.pop_front();
			}
			StatePhysicsMeemoMinion* state = new StatePhysicsMeemoMinionIdle();
			_states.push_back(state);
			state->start(this);
			return true;
		}
		else return false;
	}

	bool PhysicsMeemoMinion::movePath(std::list<meemo::Location*>* locations){
		StatePhysicsMeemoMinion* currentState = _states.front();
		if(currentState->canInterrupt()){
			currentState->destroy(this);
			delete currentState;
			_states.pop_front();

			StatePhysicsMeemoMinion* state;
			state = new StatePhysicsMeemoMinionMoveToPath(_meemoMinion,locations);
			_states.push_front(state);
			if(!_waitDurations.empty()){
				double waitDuration = _waitDurations.front();
				_waitDurations.pop();
				state = new StatePhysicsMeemoMinionWait(waitDuration);
				_states.push_front(state);
			}
			state->start(this);
		}
		else{
			_states.push_back(new StatePhysicsMeemoMinionMoveToPath(_meemoMinion,locations));
		}
		return true;
	}

	bool PhysicsMeemoMinion::turnHop(const cg::Vector3d& position){
		StatePhysicsMeemoMinion* currentState = _states.front();
		if(currentState->canInterrupt()){
			if(currentState->isRestartable())
				_states.push_back(currentState);
			else{
				currentState->destroy(this);
				delete currentState;
			}
			_states.pop_front();
			StatePhysicsMeemoMinion* state = new StatePhysicsMeemoMinionTurnHop(position);
			_states.push_front(state);
			state->start(this);
			return true;
		}
		else return false;
	}

	bool PhysicsMeemoMinion::pullLever() {
		StatePhysicsMeemoMinion* currentState = _states.front();
		if(currentState->canInterrupt()){
			currentState->destroy(this);
			delete currentState;
			_states.pop_front();
			StatePhysicsMeemoMinion* state = new StatePhysicsMeemoMinionPullLever();
			_states.push_front(state);
			state->start(this);
			return true;
		}
		else
			return false;
	}

	bool PhysicsMeemoMinion::fall() {
		while(!_states.empty()){
			StatePhysicsMeemoMinion* state = _states.front();
			state->destroy(this);
			delete state;
			_states.pop_front();
		}
		StatePhysicsMeemoMinion* state = new StatePhysicsMeemoMinionFall();
		_states.push_front(state);
		state->start(this);
		return true;
	}

	bool PhysicsMeemoMinion::hit(){
		StatePhysicsMeemoMinion* currentState = _states.front();
		if(currentState->isRestartable())
			_states.push_back(currentState);
		else{
			currentState->destroy(this);
			delete currentState;
		}
		_states.pop_front();
		StatePhysicsMeemoMinion* state = new StatePhysicsMeemoMinionHit();
		_states.push_front(state);
		state->start(this);
		return true;
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath){
		Path* path = statePhysicsMeemoMinionMoveToPath->getPath();
		meemo::Location* firstLocation = path->getCurrentLocation();
		meemo::Location* targetLocation = path->getTargetLocation();
		cg::Vector3d firstLocationVector = firstLocation->getVector3d();
		moveTo(statePhysicsMeemoMinionMoveToPath,firstLocationVector);
		_targetPosition = targetLocation->getVector3d();
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop){
		cg::Vector3d hopVelocity(0.0,0.0,_hopLinearVelocity);
		_velocity = hopVelocity;
		_turnOriginalOrientation = _orientation;
		_turnOriginalFront = _front;
		_turnOriginalRight = _right;
		cg::Vector3d turnToPosition = statePhysicsMeemoMinionTurnHop->getTurnToPosition();
		cg::Vector3d turnDirection;
		turnDirection = turnToPosition - _position;
		cg::Vector3d normalizedTurnDirection(normalize(turnDirection));
		_turnTotalRotationAngle = rotationAngle(normalizedTurnDirection);
		_hopDuration = hopDuration();
		_turnElapsedSeconds = 0.0;
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall){
		_velocity = cg::Vector3d(0,0,0);
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle){}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit){
		_position = _position - cg::Vector3d(0.0,0.0,_dropDepth);
		_velocity = cg::Vector3d(0,0,_riseLinearVelocity);
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait){
	}

	void PhysicsMeemoMinion::start(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever){
		_velocity = cg::Vector3d(0.0,0.0,_pullLeverLinearVelocity);
	}

	void PhysicsMeemoMinion::step(double elapsedSeconds) {
		StatePhysicsMeemoMinion* currentState = _states.front();
		currentState->step(this,elapsedSeconds);
		_orientation.getGLMatrix(_rotationMatrix);
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath,double elapsedSeconds){
		Path* path = statePhysicsMeemoMinionMoveToPath->getPath();
		if(statePhysicsMeemoMinionMoveToPath->isMoving()){
			stepMoveTo(statePhysicsMeemoMinionMoveToPath,elapsedSeconds);
		}
		else{
			path->nextLocation();
			if(path->isEnd()){
				statePhysicsMeemoMinionMoveToPath->destroy(this);
				delete statePhysicsMeemoMinionMoveToPath;
				_states.pop_front();
				if(_states.empty())
					_states.push_back(new StatePhysicsMeemoMinionIdle());
				_states.front()->start(this);
			}
			else{
				meemo::Location* currentLocation = path->getCurrentLocation();
				cg::Vector3d currentLocationVector = currentLocation->getVector3d();
				moveTo(statePhysicsMeemoMinionMoveToPath,currentLocationVector);
			}
		}
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionTurnHop* statePhysicsMeemoMinionTurnHop,double elapsedSeconds){
		_turnElapsedSeconds = _turnElapsedSeconds + elapsedSeconds;

		if(_turnElapsedSeconds > _hopDuration){
			stepTurnHopLastTurn();
			StatePhysicsMeemoMinion* currentState = _states.front();
			currentState->destroy(this);
			delete currentState;
			_states.pop_front();
			if(_states.empty())
				_states.push_back(new StatePhysicsMeemoMinionIdle());
			else 
				_states.push_front(new StatePhysicsMeemoMinionWait(_waitDuration));
			_states.front()->start(this);
		}
		else{
			stepTurnHopTurn(elapsedSeconds);
		}
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionFall* statePhysicsMeemoMinionFall,double elapsedSeconds){
		_velocity = _velocity + cg::Vector3d(0,0,-PhysicsParameters::GRAVITY) * elapsedSeconds;
		_position = _position + _velocity * elapsedSeconds;
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionIdle* statePhysicsMeemoMinionIdle,double elapsedSeconds){
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionHit* statePhysicsMeemoMinionHit,double elapsedSeconds){
		_position = _position + _velocity * elapsedSeconds;
		if(_position[2] > 0){
			_position[2] = 0;
			statePhysicsMeemoMinionHit->destroy(this);
			delete statePhysicsMeemoMinionHit;
			_states.pop_front();
			if(_states.empty())
				_states.push_back(new StatePhysicsMeemoMinionIdle());
			_states.front()->start(this);
		}
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionWait* statePhysicsMeemoMinionWait,double elapsedSeconds){
		statePhysicsMeemoMinionWait->incrementElapsedSeconds(elapsedSeconds);
		double waitElapsedSeconds = statePhysicsMeemoMinionWait->getElapsedSeconds();
		double waitDuration = statePhysicsMeemoMinionWait->getDuration();
		if(waitElapsedSeconds > waitDuration){
			statePhysicsMeemoMinionWait->destroy(this);
			delete statePhysicsMeemoMinionWait;
			_states.pop_front();
			if(_states.empty())
				_states.push_back(new StatePhysicsMeemoMinionIdle());
			_states.front()->start(this);
		}
	}

	void PhysicsMeemoMinion::step(StatePhysicsMeemoMinionPullLever* statePhysicsMeemoMinionPullLever,double elapsedSeconds){
		_velocity = _velocity + cg::Vector3d(0,0,-PhysicsParameters::GRAVITY) * elapsedSeconds;
		_position = _position + _velocity * elapsedSeconds;
		if(_position[2] < 0){
			_position[2] = 0;
			statePhysicsMeemoMinionPullLever->destroy(this);
			delete statePhysicsMeemoMinionPullLever;
			_states.pop_front();
			if(_states.empty())
				_states.push_back(new StatePhysicsMeemoMinionIdle());
			_states.front()->start(this);
		}
	}

	void PhysicsMeemoMinion::moveTo(StatePhysicsMeemoMinionMoveToPath* state,const cg::Vector3d& moveToPosition) { 
		_position[2]=0.0;
		_moveToPosition = moveToPosition;
		cg::Vector3d moveToVector = _moveToPosition - _position;
		double moveToVectorLength = length(moveToVector);

		if(moveToVectorLength < _stopDistance){
			state->setMoving(false);
		}
		else{
			cg::Vector3d moveToDirection(_moveToPosition - _position);
			moveToDirection = normalize(moveToDirection);
			rotate(moveToDirection);
			state->setMoving(true);
		}
	}

	bool PhysicsMeemoMinion::isCloseToMoveToPosition(){
		double unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		using namespace Utilities::Math;
		double deltaX = _moveToPosition[0]-_position[0];
		double deltaY = _moveToPosition[1]-_position[1];
		double lengthCaptainVector = sqrt(square(deltaX) + square(deltaY));

		return lengthCaptainVector <= unitaryLength/4;
	}



	void PhysicsMeemoMinion::stepMoveTo(StatePhysicsMeemoMinionMoveToPath* statePhysicsMeemoMinionMoveToPath,double elapsedSeconds){
		if(isCloseToMoveToPosition()){
			statePhysicsMeemoMinionMoveToPath->setMoving(false);
		}
		else{
			cg::Vector3d newPosition = _position + _front * _stopDistance;
			if(antecipatesCollision(newPosition)){
				cg::Vector3d targetPositionVector = _targetPosition - _position;
				double targetPositionVectorLength = length(targetPositionVector);

				if(targetPositionVectorLength < _stopDistanceRelaxed){
					statePhysicsMeemoMinionMoveToPath->destroy(this);
					delete statePhysicsMeemoMinionMoveToPath;
					_states.pop_front();
					if(_states.empty())
						_states.push_front(new StatePhysicsMeemoMinionIdle());
					_states.front()->start(this);
				}
				else{
					_states.push_front(new StatePhysicsMeemoMinionWait(_waitDuration));
					_states.front()->start(this);
				}
			}
			else{
				_position += _front * _linearVelocity * elapsedSeconds;
			}
		}
	}

	bool PhysicsMeemoMinion::antecipatesCollision(const cg::Vector3d& position){
		bool collisionAnticipated = false;

		cg::Vector3d originalPosition(_position);
		_position = position;
		std::list<EventCollision*>* collisions = getCollisions();
		_position = originalPosition;

		if(!collisions->empty()){
			std::list<EventCollision*>::iterator iteratorCollisions;
			iteratorCollisions = collisions->begin();
			for(;iteratorCollisions != collisions->end(); iteratorCollisions++){
				EventCollision* eventCollision = *iteratorCollisions;
				if(eventCollision->isDetectable()){
					EventType::Type collisionType = eventCollision->type();
					if(collisionType != EventType::COLLISION_FINISH_LINE){
						collisionAnticipated = true;
						break;
					}
				}
			}
		}

		Utilities::destroy(collisions);
		return collisionAnticipated;
	}
}