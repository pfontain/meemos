#include "LocationEcphory.h"

namespace meemo{

	LocationEcphory::LocationEcphory(MemoryStorage* memoryStorage,avt::Physical* physicalAgent){
		_memoryStorage = memoryStorage;
		_physicalAgent = physicalAgent;
		_shortTermMemoryDuration = cg::Properties::instance()->getInt("SHORT_TERM_MEMORY_DURATION");
	}

	void LocationEcphory::generateMemoryRetrievalEvents(){
		std::list<MemoryTrace*>::iterator memoryTracesIterator;
		std::list<MemoryTrace*>::iterator memoryTracesEnd;

		memoryTracesIterator = _memoryStorage->getMemoryTracesIterator();
		memoryTracesEnd = _memoryStorage->getMemoryTracesEnd();

		for(; memoryTracesIterator != memoryTracesEnd; memoryTracesIterator++){
			MemoryTrace* memoryTrace = *memoryTracesIterator;
			if(ecphoricMatch(memoryTrace)){
				if(!isEventInShortTermMemory(memoryTrace)){
					appraisal::Event* eventRetrieved = memoryTrace->getEvent();
					Location agentLocation(_physicalAgent->getPosition());
					avt::EventMemoryRetrieval* eventMemoryRetrieval = new avt::EventMemoryRetrieval(eventRetrieved->copy(),agentLocation);
					notify(eventMemoryRetrieval);
					appraisal::ActiveEmotion* memoryTraceEmotion = memoryTrace->getCausedEmotion();
					_memoryStorage->addRetrievedMemoryTraceToShortTermMemory(eventRetrieved->copy(),new appraisal::ActiveEmotion(*memoryTraceEmotion));
				}
			}
		}

		avt::EventNotifier::step();
	}

	bool LocationEcphory::ecphoricMatch(MemoryTrace* memoryTrace){
		appraisal::Event* event = memoryTrace->getEvent();
		Location eventLocation = event->getLocation();
		Location agentLocation(_physicalAgent->getPosition());
		float squaredDistance = eventLocation.squaredDistance(agentLocation);

		int locationEcphoryDistance = cg::Properties::instance()->getInt("LOCATION_ECPHORY_DISTANCE");
		int squaredLocationEcphoryDistance = locationEcphoryDistance * locationEcphoryDistance;

		if(squaredDistance < squaredLocationEcphoryDistance)
			return true;
		else
			return false;
	}

	bool LocationEcphory::isEventInShortTermMemory(MemoryTrace* memoryTrace){
		appraisal::ActiveEmotion* memoryTraceEmotion = memoryTrace->getCausedEmotion();
		long activationTime = memoryTraceEmotion->getActivationTime();
		long currenTime = Utilities::Time::getTime();
		long elapsedTimeSinceActivation = currenTime - activationTime;
		bool actualEventInShortTermMemory = elapsedTimeSinceActivation < _shortTermMemoryDuration;
		if(actualEventInShortTermMemory)
			return true;
		else{
			bool memoryTraceEventInShortTermMemory = false;
			std::list<MemoryTrace*>* retrievedMemoryTracesInShortTermMemory = _memoryStorage->getRetrievedMemoryTracesInShortTermMemory();
			std::list<MemoryTrace*>::iterator iteratorRetrievedMemoryTracesInShortTermMemory;
			iteratorRetrievedMemoryTracesInShortTermMemory = retrievedMemoryTracesInShortTermMemory->begin();
			while(iteratorRetrievedMemoryTracesInShortTermMemory != retrievedMemoryTracesInShortTermMemory->end()){
				MemoryTrace* memoryTraceShortMemory = *iteratorRetrievedMemoryTracesInShortTermMemory;
				appraisal::Event* memoryTraceShortMemoryEvent = memoryTraceShortMemory->getEvent();
				appraisal::Event* memoryTraceEvent = memoryTrace->getEvent();
				if(memoryTraceEvent->equal(memoryTraceShortMemoryEvent)){
					memoryTraceEventInShortTermMemory = true;
					break;
				}
				iteratorRetrievedMemoryTracesInShortTermMemory++;
			}
			return memoryTraceEventInShortTermMemory;
		}
	}
}