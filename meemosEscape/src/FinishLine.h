#ifndef _FinishLine_h_
#define _FinishLine_h_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "Texture.h"
#include "Physical.h"
#include "PhysicsRegistry.h"
#include "PhysicsFinishLine.h"
#include "Utilities.h"
#include "Debugable.h"
#include "DebugRegistry.h"

namespace avt{

	class FinishLine : 
		public cg::Entity,
		public cg::IDrawListener,
		public Debugable
	{

	public:
		FinishLine(float x, float y,float size,const std::string& id);
		~FinishLine();
		void init();
		void draw();

	private:
		int _modelDL;
		float _size;

		Material* _material;
		Physics* _physics;

	private:
		void makeModel();
		Physics* getPhysics();
	};
}


#endif