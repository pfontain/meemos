#include "Behaviour.h"

namespace meemo{
	Behaviour::Behaviour(EmotionalAgent* emotionalAgent){
		_emotionalAgent = emotionalAgent;
	}

	void Behaviour::init(){
		_emotionalState = _emotionalAgent->getEmotionalState();
		_physics = _emotionalAgent->getPhysics();
		_thoughtBalloonDuration = cg::Properties::instance()->getDouble("THOUGHT_BALLOON_DURATION");
	}

	void Behaviour::update(double elapsedSeconds){
		updateMoodColour();
		updateFace();
		updateThoughtBalloon(elapsedSeconds);
	}

	void Behaviour::updateMoodColour(){
		float mood = _emotionalState->getMood();
		if(mood < appraisal::EmotionalParameters::NEUTRAL_MOOD){
			float maximumMoodDeviation = appraisal::EmotionalParameters::MAXIMUM_MOOD_DEVIATION;
			float intensityPercentage = (mood + maximumMoodDeviation)/maximumMoodDeviation;
			_emotionalAgent->changeColourIntensity(intensityPercentage);
		}
	}

	void Behaviour::updateFace(){
		appraisal::ActiveEmotion* strongestEmotion = _emotionalState->getStrongestEmotion();
		if(strongestEmotion != NULL){
			appraisal::EmotionType::Type strongestEmotionType = strongestEmotion->getType();
			updateFace(strongestEmotionType);
		}
		else{
			_emotionalAgent->turnOnNeutralEyes();
			_emotionalAgent->turnOnNeutralMouth();
		}
	}

	void Behaviour::updateFace(appraisal::EmotionType::Type emotionType){
		switch(emotionType){
				case appraisal::EmotionType::JOY:
					_emotionalAgent->turnOnJoyEyes();
					_emotionalAgent->turnOnSmileMouth();
					break;
				case appraisal::EmotionType::PITY:
				case appraisal::EmotionType::DISTRESS:
					_emotionalAgent->turnOnDistressEyes();
					_emotionalAgent->turnOnDistressMouth();
					break;
				case appraisal::EmotionType::ANGER:
					_emotionalAgent->turnOnAngerEyes();
					_emotionalAgent->turnOnDistressMouth();
				default:
					_emotionalAgent->turnOnNeutralEyes();
					_emotionalAgent->turnOnNeutralMouth();
		}	
	}

	void Behaviour::updateThoughtBalloon(double elapsedSeconds){
		_thoughtBalloonTimer.update(elapsedSeconds);

		appraisal::ActiveEmotion* strongestEmotion = _emotionalState->getStrongestEmotion();
		if(strongestEmotion != NULL){
			appraisal::Event* event = strongestEmotion->getCause();
			if(event->type() == EventType::MEMORY_RETRIEVAL){
				avt::EventMemoryRetrieval* eventMemoryRetrieval = static_cast<avt::EventMemoryRetrieval*>(event);
				activateThoughtBalloon(eventMemoryRetrieval);
			}	
		}
		else
			setStateThoughtBalloonMonitored(avt::ThoughtBalloonState::OFF);
	}

	void Behaviour::activateThoughtBalloon(avt::EventMemoryRetrieval* event){
		EventType::Type retrievedEventType = event->getRetrievedEventType();
		appraisal::Event* retrievedEvent = event->getRetrievedEvent();

		avt::ThoughtBalloonState::State stateThoughtBalloon;

		if(retrievedEventType == EventType::COLLISION_ANVIL)
			stateThoughtBalloon = avt::ThoughtBalloonState::ANVIL_HIT;
		else if(retrievedEventType == EventType::COLLISION_TRAP_DOOR)
			stateThoughtBalloon = avt::ThoughtBalloonState::TRAPDOOR_FALL;
		else if(retrievedEventType == EventType::WITNESS){
			avt::EventWitness* eventWitness = static_cast<avt::EventWitness*>(retrievedEvent);
			EventType::Type witnessedEventType = eventWitness->getWitnessedEventType();
			if(witnessedEventType == EventType::COLLISION_ANVIL)
				stateThoughtBalloon = avt::ThoughtBalloonState::ANVIL_HIT;
			else if(witnessedEventType == EventType::COLLISION_TRAP_DOOR)
				stateThoughtBalloon = avt::ThoughtBalloonState::TRAPDOOR_FALL;
		} else {
			stateThoughtBalloon = avt::ThoughtBalloonState::OFF;
		}

		setStateThoughtBalloonMonitored(stateThoughtBalloon);
	}

	void Behaviour::setStateThoughtBalloonMonitored(avt::ThoughtBalloonState::State stateThoughtBalloon){
		bool stateChanged;
		stateChanged = _thoughtBalloonStateMonitor.update(stateThoughtBalloon);
		if(stateChanged){
			_emotionalAgent->setStateThoughtBalloon(stateThoughtBalloon);
			_thoughtBalloonTimer.setTimer(_thoughtBalloonDuration);
		} else if(_thoughtBalloonTimer.isOn()){
			_emotionalAgent->setStateThoughtBalloon(stateThoughtBalloon);
		} else{
			_emotionalAgent->setStateThoughtBalloon(avt::ThoughtBalloonState::OFF);
		}
	}
}