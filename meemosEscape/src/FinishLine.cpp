#include "FinishLine.h"

namespace avt{
	FinishLine::FinishLine(float x, float y,float size,const std::string& id): cg::Entity(id){
		_size = size;
		_physics = new PhysicsFinishLine();
		_physics->setPosition(x,y,_size/2.0 + Utilities::Math::smallPositive());
		_material = new MaterialStatic();
	}

	FinishLine::~FinishLine(){
		delete _physics;
		delete _material;
	}

	void FinishLine::init() {
		_material->setAmbient(0.9,0.8,0.1,1.0);
		_material->setDiffuse(0.9,0.8,0.1,1.0);
		_material->init();

		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0+15);

		DebugRegistry::getInstance()->add(this);

		makeModel();
	}

	void FinishLine::draw() {
		glPushMatrix();
		{
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
		drawDebug();
	}

	// TODO: Implement transparency by ordering primitive drawing
	void FinishLine::makeModel()
	{
		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			glDisable(GL_TEXTURE_2D);
			glPushMatrix();
			{
				glTranslatef(getX(),getY(),getZ());
				glEnable(GL_BLEND_DST);
				glBlendFunc(GL_ONE, GL_ONE);
				glutSolidCube(_size);
				glDisable(GL_BLEND);
			}
			glPopMatrix();
		}
		glEndList();
	}
	// DEBUG
	//glPushMatrix();
	//glCallList(_material._materialDL);
	//glTranslatef(getX(),getY(),getZ());
	//glColor4f(0.5, 0.5, 0.5, 0.5);
	//glutSolidSphere(_physics->_boundingSphereRadius,10,10);
	//glPopMatrix();

	Physics* FinishLine::getPhysics(void){
		return _physics;
	}
}