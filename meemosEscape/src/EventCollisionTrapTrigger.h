#ifndef _EventCollisionTrapTrigger_h_
#define _EventCollisionTrapTrigger_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionTrapTrigger: public EventCollision
	{	
	public:
		EventCollisionTrapTrigger(){ _detectable = false;}
		EventCollisionTrapTrigger(meemo::Location location): EventCollision(location){ _detectable = false;}

	public:
		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionTrapTrigger";
			if(isLocationSet())
				stringStream << "[" << _location << "]";
			return stringStream.str();
		}

		EventType::Type type(){
			return EventType::COLLISION_TRAP_TRIGGER;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionTrapTrigger(_location);
			else
				return new EventCollisionTrapTrigger();
		}
	};
}

#endif