#ifndef __StatePhysicsMeemoMinionPullLever_avt__
#define __StatePhysicsMeemoMinionPullLever_avt__

#include "StatePhysicsMeemoMinion.h"
#include "StatePhysicsMeemoMinionVisitor.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionPullLever: public StatePhysicsMeemoMinion{
	public:
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoMinionPullLever_avt__