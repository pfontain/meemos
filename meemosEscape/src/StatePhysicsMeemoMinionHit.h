#ifndef __StatePhysicsMeemoMinionHit_avt__
#define __StatePhysicsMeemoMinionHit_avt__

#include "StatePhysicsMeemoMinion.h"
#include "StatePhysicsMeemoMinionVisitor.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionHit: public StatePhysicsMeemoMinion{
	public:
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoMinionHit_avt__
