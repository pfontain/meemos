#include "MeemoMinionWithMemory.h"

namespace avt{
	MeemoMinionWithMemory::MeemoMinionWithMemory(float x, float y,int size,unsigned int idNumber,std::string id,AbstractLevel* level,AbstractGameLogic* gameLogic): MeemoMinion(x,y,size,idNumber,id,level,gameLogic){
		_behaviourMeemoMinionWithMemory = new meemo::BehaviourMeemoMinionWithMemory(this,this,level,gameLogic);
		_behaviourMeemoMinion = _behaviourMeemoMinionWithMemory;
		_behaviour = _behaviourMeemoMinionWithMemory;

		_mindWithMemory = new meemo::MindWithMemory(this,this);
		_mind = _mindWithMemory;
	}

	MeemoMinionWithMemory::~MeemoMinionWithMemory(){
		delete _behaviourMeemoMinionWithMemory;
		delete _mindWithMemory;
	}

	void MeemoMinionWithMemory::init() {
		MeemoMinion::init();
		meemo::LocationEcphory* locationEcphory = _mindWithMemory->getLocationEcphory();
		locationEcphory->registerSubscriber(_eventBuffer);
	}

	meemo::MemoryStorage* MeemoMinionWithMemory::getMemoryStorage(){
		return _mindWithMemory->getMemoryStorage();
	}

	bool MeemoMinionWithMemory::movePath(std::list<meemo::Location*>* locations){
		return MeemoMinion::movePath(locations);
	}

	avt::Path* MeemoMinionWithMemory::updatePath(avt::Path* path){
		path = _behaviourMeemoMinionWithMemory->updatePath(path);
		return path;
	}
}