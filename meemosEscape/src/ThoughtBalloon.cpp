#include "ThoughtBalloon.h"

namespace avt{
	ThoughtBalloon::ThoughtBalloon(Physical* physical){
		_physical = physical;
		_ellipseA = cg::Properties::instance()->getInt("BALLOON_ELLIPSE_A");
		_ellipseB = cg::Properties::instance()->getInt("BALLOON_ELLIPSE_B");
		_numberEdges = cg::Properties::instance()->getInt("BALLOON_NUMBER_EDGES");
		_ellipse1Position = cg::Properties::instance()->getVector2d("BALLOON_ELLIPSE_1_POSITION");
		_ellipse2Position = cg::Properties::instance()->getVector2d("BALLOON_ELLIPSE_2_POSITION");
		_ellipse3Position = cg::Properties::instance()->getVector2d("BALLOON_ELLIPSE_3_POSITION");
		_ellipseSizes = cg::Properties::instance()->getVector3d("BALLOON_ELLIPSE_SIZES");
		_thoughtSize = cg::Properties::instance()->getInt("BALLOON_SQUARE_SIDE_SIZE");
		_material = new MaterialStatic();
		_physics = new PhysicsDefault();
		_state = ThoughtBalloonState::OFF;
	}

	ThoughtBalloon::~ThoughtBalloon(){
		delete _material;
		delete _physics;
	}

	void ThoughtBalloon::init() {
		cg::Vector3d colour = cg::Properties::instance()->getVector3d("BALLOON_COLOUR");
		_material->setAmbient(colour[0],colour[1],colour[2],1.0);
		_material->init();

		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		std::string textureAnvilHitFileName = cg::Properties::instance()->getString("THOUGHT_BALLOON_ANVIL_HIT_TEXTURE");
		_textureAnvilHit.open(textureAnvilHitFileName);
		std::string textureTrapdoorFallFileName = cg::Properties::instance()->getString("THOUGHT_BALLOON_TRAPDOOR_FALL_TEXTURE");
		_textureTrapdoorFall.open(textureTrapdoorFallFileName);

		makeModels();
	}

	void ThoughtBalloon::setState(ThoughtBalloonState::State state){
		_state = state;
	}

	void ThoughtBalloon::update(const cg::Vector3d& observerPosition){
		cg::Vector3d physicalPosition(_physical->getPosition());
		_physics->setPosition(physicalPosition[0],physicalPosition[1],physicalPosition[2]);

		cg::Vector3d observerDirection;
		observerDirection[0]= observerPosition[0] - physicalPosition[0];
		observerDirection[1]= observerPosition[1] - physicalPosition[1];
		observerDirection[2]= 0.0;
		cg::Vector3d observerDirectionNormalized(normalize(observerDirection));

		_physics->rotate(observerDirectionNormalized);
		_physics->step();
	}

	void ThoughtBalloon::draw(){
		if(_state != ThoughtBalloonState::OFF){
			glPushMatrix();{
				_physics->applyTransforms();
				_material->draw();
				switch(_state){
					case ThoughtBalloonState::ANVIL_HIT: glCallList(_modelDLAnvilHit); break;
					case ThoughtBalloonState::TRAPDOOR_FALL: glCallList(_modelDLTrapDoorHit); break;
				}
			}glPopMatrix();
		}
	}

	void ThoughtBalloon::makeModels(void){
		_modelDLAnvilHit = glGenLists(1);
		assert(_modelDLAnvilHit != 0);
		glNewList(_modelDLAnvilHit,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureAnvilHit.id);
			lightsAndModel();
		}
		glEndList();		

		_modelDLTrapDoorHit = glGenLists(1);
		assert(_modelDLTrapDoorHit != 0);
		glNewList(_modelDLTrapDoorHit,GL_COMPILE);
		{	
			glBindTexture(GL_TEXTURE_2D, _textureTrapdoorFall.id);
			lightsAndModel();
		}
		glEndList();		
	}

	void ThoughtBalloon::lightsAndModel(void){
		GLfloat lightingAmbientArray[4];
		GLfloat* lightingAmbient = &(lightingAmbientArray[0]);
		glGetFloatv(GL_LIGHT_MODEL_AMBIENT,lightingAmbient);
		GLfloat lightingAmbientThoughtBalloon[] = {1,1,1,1.0};
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbientThoughtBalloon);

		makeModelThought();
		makeModelBalloon();

		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightingAmbient);
	}

	void ThoughtBalloon::makeModelThought(void){
		double halfSideSize = _thoughtSize/2;
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);
		{
			glNormal3f(1,0,0);
			glTexCoord2f(0,0);
			glVertex3f(Utilities::Math::smallPositive(),-halfSideSize+_ellipse1Position[0],-halfSideSize+_ellipse1Position[1]);
			glTexCoord2f(1,0);
			glVertex3f(Utilities::Math::smallPositive(),+halfSideSize+_ellipse1Position[0],-halfSideSize+_ellipse1Position[1]);
			glTexCoord2f(1,1);
			glVertex3f(Utilities::Math::smallPositive(),+halfSideSize+_ellipse1Position[0],+halfSideSize+_ellipse1Position[1]);
			glTexCoord2f(0,1);
			glVertex3f(Utilities::Math::smallPositive(),-halfSideSize+_ellipse1Position[0],+halfSideSize+_ellipse1Position[1]);	
		}
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	void ThoughtBalloon::makeModelBalloon(void){
		using namespace Utilities::Draw;
		glPushMatrix();{
			glTranslated(0,_ellipse1Position[0],_ellipse1Position[1]);
			drawEllipse(_ellipseA*_ellipseSizes[0],_ellipseB*_ellipseSizes[0],_numberEdges);
		}glPopMatrix();

		glPushMatrix();{
			glTranslated(0,_ellipse2Position[0],_ellipse2Position[1]);
			drawEllipse(_ellipseA*_ellipseSizes[1],_ellipseA*_ellipseSizes[1],_numberEdges);
		}glPopMatrix();

		glPushMatrix();{
			glTranslated(0,_ellipse3Position[0],_ellipse3Position[1]);
			drawEllipse(_ellipseA*_ellipseSizes[2],_ellipseA*_ellipseSizes[2],_numberEdges);
		}glPopMatrix();
	}
}