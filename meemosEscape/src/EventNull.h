#ifndef _EventNull_h_
#define _EventNull_h_

#include "Event.h"

namespace avt{
	/**
	 * Singleton pattern 
	 */
	class EventNull : public Event
	{	
	private:
		static EventNull* _instance;
	protected:
		EventNull(){};
	public:
		static Event* getInstance();	
		void destroy();
		Event* copy();
		static void deleteInstance();
		bool isNull();
		const std::string toString();
		EventType::Type type();

		bool equal(appraisal::Event* event);
		int match(appraisal::Event* event);
	};
}

#endif