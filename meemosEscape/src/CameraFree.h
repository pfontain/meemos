#ifndef _CameraFree_h_
#define _CameraFree_h_

#include "Camera.h"

namespace avt{
	class CameraFree : public Camera,
		public cg::IUpdateListener
	{
	public:
		CameraFree(const std::string& id,
			const cg::Vector3d& position,
			const cg::Vector3d& frontDirection,
			const cg::Vector3d& leftDirection,
			const cg::Vector3d& upDirection);
		void init();

	private:
		cg::Vector3d _lookingDirection;

	public:
		void setModelViewMatrix();
		void update(unsigned long elapsed_millis);
	};
}

#endif // _CameraFree_h_