#ifndef _EmotionalReactionRuleMemoryRetrieval_h_
#define _EmotionalReactionRuleMemoryRetrieval_h_

#include "EmotionalReactionRule.h"
#include "EventMemoryRetrieval.h"

namespace meemo{
	class EmotionalReactionRuleMemoryRetrieval: public appraisal::EmotionalReactionRule{
	public:
		EmotionalReactionRuleMemoryRetrieval(appraisal::EmotionalReactionRule* emotionalReactionRule);
		bool match(appraisal::Event* event);
	};
}

#endif // _EmotionalReactionRuleMemoryRetrieval_h_