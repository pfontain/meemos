#include "Path.h"

namespace avt{
	Path::Path(std::list<meemo::Location*>* locations){
		_locations = locations;
		_currentLocation = _locations->begin();
	}

	Path::~Path(void){
		Utilities::destroy(_locations);
	}

	meemo::Location* Path::getCurrentLocation(void){
		return *_currentLocation;
	}

	meemo::Location* Path::getTargetLocation(void){
		return _locations->back();
	}

	void Path::nextLocation(void){
		_currentLocation++;
	}

	bool Path::isEnd(void){
		return _currentLocation == _locations->end();
	}
}