#ifndef _ReactiveProcessDefault_h_
#define _ReactiveProcessDefault_h_

#include "ReactiveProcess.h"

namespace appraisal{
	class ReactiveProcessDefault: public ReactiveProcess{
	public:
		ReactiveProcessDefault(const std::string& nameSelf,EmotionalState* emotionalState);

	private:
		void generateActionBasedEmotions(Event* event, int praiseworthiness);
		void generateFortuneForAll(Event* event, int desirability, int desirabilityForOther, std::string other);
		void generateFortuneOfOtherEmotions(Event* event, int desirability, int desirabilityForOther,std::string target);
		void generateWellBeingEmotions(Event* event, int desirability);
	};
}

#endif // _ReactiveProcessDefault_h_