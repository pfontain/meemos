#ifndef _PhysicsTrapDoor_avt_
#define _PhysicsTrapDoor_avt_

#include "Physics.h"
#include "EventCollisionTrapDoor.h"

namespace avt{
	namespace PhysicsTrapDoorState{
		enum State{
			CLOSED,
			OPENING,
			OPEN,
			CLOSING,
		};
	}

	class PhysicsTrapDoor: public Physics{
	public:
		PhysicsTrapDoor(void);

	private:
		PhysicsTrapDoorState::State _state;
		float _openingAngularVelocity;
		float _openDuration;
		float _closingAngularVelocity;
		float _previousRotationAngleModule;
		float _timeCounter;
		float _halfEdgeSize;

	public:
		void setOpeningAngularVelocity(float velocity);
		void setClosingAngularVelocity(float velocity);
		void setOpenDuration(float velocity);
		void setEdgeSize(float edgeSize);

		EventCollision* getCollisionEvent();
		bool isColliding(Physics* collider);
		void step(double elapsedSeconds);
		void applyTransforms();

	private:		
		void transitionOpening(void);
		void transitionOpen(void);
		void transitionClosing(void);
		void transitionClosed(void);
		void stepOpening(double elapsedSeconds);
		void stepOpen(double elapsedSeconds);
		void stepClosing(double elapsedSeconds);
		void stepClosed(double elapsedSeconds);
	};
}

#endif