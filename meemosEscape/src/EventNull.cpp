#include "EventNull.h"

namespace avt{

	EventNull* EventNull::_instance = 0;

	Event* EventNull::getInstance(){ 
		if(!_instance)
			_instance = new EventNull();
		return _instance; 
	}

	void EventNull::destroy(){
		return EventNull::deleteInstance();
	}

	Event* EventNull::copy(){
		return EventNull::getInstance();
	}

	void EventNull::deleteInstance(){
		delete _instance;
		_instance = 0;
	}

	bool EventNull::isNull() { return true; }

	const std::string EventNull::toString() { return "EventNull"; }

	EventType::Type EventNull::type() { return EventType::NULL_EVENT; }

	bool EventNull::equal(appraisal::Event* event){ return Event::defaultEqual(event); }

	int EventNull::match(appraisal::Event* event){ return Event::defaultMatch(event); }
}