#ifndef _MeemoApplication_H_
#define _MeemoApplication_H_

#include "cg/cg.h"
#include "GameApplication.h"
#include "MenuController.h"
#include "Level.h"

namespace avt{
	class Level;

	class MeemoApplication : public GameApplication{
	public:
		MeemoApplication(void);

	private:
		int _menuWindowWidth;
		int _menuWindowHeight;
		Level* _currentLevel;

	public:
		void createEntities(void);
		void loadDebugLevel(void);
		void loadLevel(Level* level);
		void unloadLevel(void);
	};
}

#endif