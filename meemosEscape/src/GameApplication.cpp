#include "GameApplication.h"

namespace avt {

	GameApplication::GameApplication(std::string caption): cg::Application("config.ini"){
		_window.caption = caption;
		_isLevelLoaded = false;
		_isLevelMarkedForDeletion = false;
	}

	GameApplication::~GameApplication(void){
		TextureRegistry::deleteInstance();
	}

	bool GameApplication::islevelLoaded(){
		return _isLevelLoaded;
	}

	void GameApplication::addLevelEntity(cg::Entity* entity){
		entity->init();
		addEntity(entity);
	}

	void GameApplication::loadLevel(AbstractLevel* level){
		level->init();
		_currentLevel = level;
		_isLevelLoaded = true;
	}

	void GameApplication::unloadLevel(){
		delete _currentLevel;
		_isLevelLoaded = false;
	}

	void GameApplication::markLevelForDeletion(){
		_isLevelMarkedForDeletion = true;
	}

	bool GameApplication::isLevelForDeletion(){
		return _isLevelMarkedForDeletion;
	}


	void GameApplication::onDisplay() {
        glClearColor(0.01,0.01,0.01,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // 1st pass
        glEnable(GL_DEPTH_TEST);
		if(_isLevelLoaded){
			Scene* scene = _currentLevel->getActiveScene();
			scene->draw();
			Camera* camera = _currentLevel->getActiveCamera();
			camera->setModelViewMatrix();
		}
		cg::DrawNotifier::instance()->draw();
        // 2nd pass
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		setOverlayProjection();
		if(_isLevelLoaded){
			Scene* scene = _currentLevel->getActiveScene();
			scene->drawOverlay();
		}
		cg::DrawOverlayNotifier::instance()->drawOverlay();
        glDisable(GL_BLEND);
    }

	void GameApplication::onUpdate() {
		if(_isLevelMarkedForDeletion){
			unloadLevel();
			_isLevelMarkedForDeletion = false;
		}
		Application::onUpdate();
    }
}