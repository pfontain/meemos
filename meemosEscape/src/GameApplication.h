#ifndef _GameApplication_h_
#define _GameApplication_h_

#include "cg/cg.h"
#include "AbstractLevel.h"
#include "TextureRegistry.h"

namespace avt{
	class AbstractLevel;

	class GameApplication : public cg::Application {
	public:
		GameApplication(std::string caption);
		~GameApplication(void);

	private:
		AbstractLevel* _currentLevel;
	protected:
		bool _isLevelMarkedForDeletion;
		bool _isLevelLoaded;

	public:
		bool islevelLoaded();
		bool isLevelForDeletion();

	public:
		void addLevelEntity(cg::Entity* entity);
		virtual void loadLevel(AbstractLevel* level);
		virtual void unloadLevel();
		void markLevelForDeletion();

		void onDisplay();
		void onUpdate();
	};
}

#endif