// This file is an example for CGLib.
//
// CGLib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// CGLib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CGLib; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Copyright 2007 Carlos Martinho

#include "Scene.h"

namespace avt {

	Scene::Scene() : cg::Entity("Scene") {
	}
	Scene::~Scene() {
	}
	void Scene::init() {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		GLfloat lmodel_ambient[] = {0.5,0.5,0.5,1.0};
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	}
	void Scene::draw() {
		glEnable(GL_LIGHTING);
	}
	void Scene::drawOverlay() {
		glDisable(GL_LIGHTING);
	}
}