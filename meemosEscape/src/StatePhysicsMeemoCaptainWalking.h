#ifndef __StatePhysicsMeemoCaptainWalking_avt__
#define __StatePhysicsMeemoCaptainWalking_avt__

#include "StatePhysicsMeemoCaptain.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt{
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptainWalking: public StatePhysicsMeemoCaptain{	
	public:
		void step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds);
		void destroy();
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoCaptainWalking_avt__
