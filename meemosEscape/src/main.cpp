#define _CRTDBG_MAP_ALLOC

#include "cg/cg.h"
#include "MeemoApplication.h"
#include <time.h>
#include <stdlib.h>
#include <crtdbg.h>

int main(int argc,char** argv)
{

//BEGIN: MEMORY LEAK DETECTION
#ifdef _DEBUG
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
//  _CrtSetBreakAlloc(1266);
#endif
//END: MEMORY LEAK DETECTION
	srand ( time(NULL) );

	cg::Manager::instance()->runApp(new avt::MeemoApplication(),60,argc,argv);
	return 0;
}