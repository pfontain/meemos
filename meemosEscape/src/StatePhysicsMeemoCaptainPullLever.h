#ifndef __StatePhysicsMeemoCaptainPullLever_avt__
#define __StatePhysicsMeemoCaptainPullLever_avt__

#include "StatePhysicsMeemoCaptain.h"
#include "StatePhysicsMeemoCaptainVisitor.h"

namespace avt{
	class StatePhysicsMeemoCaptainVisitor;

	class StatePhysicsMeemoCaptainPullLever: public StatePhysicsMeemoCaptain{	
	public:
		void step(StatePhysicsMeemoCaptainVisitor* statePhysicsMeemoCaptainVisitor,double elapsedSeconds);
		void destroy();
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoCaptainPullLever_avt__
