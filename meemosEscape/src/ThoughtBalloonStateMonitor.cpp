#include "ThoughtBalloonStateMonitor.h"

namespace avt{
	ThoughtBalloonStateMonitor::ThoughtBalloonStateMonitor(void){
		_state = ThoughtBalloonState::OFF;
	}

	bool ThoughtBalloonStateMonitor::update(ThoughtBalloonState::State state){
		bool stateChanged = (_state != state);
		_state = state;
		return stateChanged;
	}

	ThoughtBalloonState::State ThoughtBalloonStateMonitor::getState(void){
		return _state;
	}
}