#include "EventWitnessNotifier.h"

namespace avt{

	void EventWitnessNotifier::registerWitnessSubscriber(EventWitnessSubscriber* eventSubscriber){
		_eventWitnessSubscribers.push_back(eventSubscriber);
	}

	void EventWitnessNotifier::unregisterWitnessSubscriber(EventWitnessSubscriber* eventSubscriber){
		_eventWitnessSubscribers.remove(eventSubscriber);
	}

	bool EventWitnessNotifier::notify(Event* event){
		bool isEventNew = EventNotifier::notify(event);

		if(isEventNew){
			std::list<EventWitnessSubscriber*>::iterator iteratorEventSubscriber = _eventWitnessSubscribers.begin();
			for(;iteratorEventSubscriber != _eventWitnessSubscribers.end(); iteratorEventSubscriber++){
				if(event->isLocationSet()){
					meemo::Location eventLocation(event->getLocation());
					EventWitnessSubscriber* eventWitness=(*iteratorEventSubscriber);
					meemo::EmotionalAgent* emotionalAgent = eventWitness->getAgent();

					if(emotionalAgent->canSenseEvent(eventLocation))
					{
						avt::Event* newEvent = new EventWitness(event->copy());
						eventWitness->onEvent(newEvent);
					}
				}
			}
		}

		return isEventNew;
	};
}


	//bool EventWitnessNotifier::isEventClose(meemo::Location& eventLocation,EventWitnessSubscriber* eventWitness){
	//	bool isEventClose;
	//	meemo::EmotionalAgent* emotionalAgentWitness = eventWitness->getAgent();
	//	meemo::Location eventWitnessLocation(emotionalAgentWitness->getLocation());
	//	float squaredDistanceBetweenEventAndWitness = eventLocation.squaredDistance(eventWitnessLocation);
	//	float witnessVisionRadius = emotionalAgentWitness->getVisionRadius();
	//	float squaredWitnessVisionRadius = Utilities::Math::square(witnessVisionRadius);

	//	if(squaredDistanceBetweenEventAndWitness < squaredWitnessVisionRadius)
	//		isEventClose = true;
	//	else
	//		isEventClose = false;

	//	return isEventClose;
	//}

	//bool EventWitnessNotifier::isEventWithinVisionAngle(meemo::Location& eventLocation,
	//												EventWitnessSubscriber* eventWitness){
	//	bool isEventWithinVisionAngle;
	//	meemo::EmotionalAgent* emotionalAgentWitness = eventWitness->getAgent();
	//	meemo::Location eventWitnessLocation(emotionalAgentWitness->getLocation());		
	//	cg::Vector3d eventWitnessLocationVector(eventWitnessLocation.getVector3d());
	//	cg::Vector3d eventLocationVector(eventLocation.getVector3d());
	//	cg::Vector3d eventDirectionVector;
	//	eventDirectionVector = normalize(eventLocationVector - eventWitnessLocationVector);
	//	cg::Vector3d eventWitnessVisionDirection(emotionalAgentWitness->getVisionDirection());
	//	float eventWitnessVisionAngle = emotionalAgentWitness->getVisionAngle();
	//	cg::Vector3d crossProductVisionEventDirection  = cross(eventWitnessVisionDirection,eventDirectionVector);
	//	double dotProductVisionEventDirection = dot(eventWitnessVisionDirection,eventDirectionVector);
	//	
	//	if(isEventInFront(crossProductVisionEventDirection,dotProductVisionEventDirection)){
	//		isEventWithinVisionAngle = true;
	//	}
	//	else if (isEventBehind(crossProductVisionEventDirection)){
	//		isEventWithinVisionAngle = isEventWitnessFishEyed(eventWitnessVisionAngle);
	//	}
	//	else{
	//		isEventWithinVisionAngle = isAngleVisionEventDirectionSmallEnough(dotProductVisionEventDirection,eventWitnessVisionAngle);
	//	}

	//	return isEventWithinVisionAngle;
	//}

	//bool EventWitnessNotifier::isEventInFront(cg::Vector3d& crossProductVisionEventDirection,float dotProductVisionEventDirection){
	//	return fabs(crossProductVisionEventDirection[2]) < Utilities::Math::smallPositive()
	//		&& dotProductVisionEventDirection > 0;
	//}

	//bool EventWitnessNotifier::isEventBehind(cg::Vector3d& crossProductVisionEventDirection){
	//	return fabs(crossProductVisionEventDirection[2]) < Utilities::Math::smallPositive();
	//}

	//bool EventWitnessNotifier::isEventWitnessFishEyed(float eventWitnessVisionAngle){
	//	return Utilities::Math::isZero(eventWitnessVisionAngle-360);
	//}

	//bool EventWitnessNotifier::isAngleVisionEventDirectionSmallEnough(float dotProductVisionEventDirection,float eventWitnessVisionAngle){
	//	double angleVisionEventDirection = acos(dotProductVisionEventDirection);
	//	double absoluteAngleVisionEventDirection = fabs(angleVisionEventDirection);
	//	double absoluteAngleVisionEventDirectionDegrees = absoluteAngleVisionEventDirection*cg::RADIANS_TO_DEGREES;

	//	return absoluteAngleVisionEventDirectionDegrees < eventWitnessVisionAngle/2;
	//}