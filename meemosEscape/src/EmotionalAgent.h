#ifndef _EmotionalAgent_h_
#define _EmotionalAgent_h_

#include "EmotionalState.h"
#include "MemoryStorage.h"
#include "Physics.h"
#include "ThoughtBalloon.h"

namespace meemo{
	class EmotionalAgent{
	public:
		virtual appraisal::EmotionalState* getEmotionalState(void) = 0;
		virtual avt::Physics* getPhysics(void) = 0;
		virtual const std::string& getId() const = 0;
		virtual bool canSenseEvent(Location& eventLocation) = 0;
		virtual bool isLocationVisible(Location& location) = 0;

		/**
		* Changes the agent's colour intensity according to a percentage.
		* 0.0 corresponds to the lowest intensity, and 1.0 to the highest
		* @param percentage - the new colour intensity. Values between
		* [0.0,1.0]
		*/
		virtual void changeColourIntensity(float intensity) = 0;
		virtual void turnOnNeutralEyes() = 0;
		virtual void turnOnJoyEyes() = 0;
		virtual void turnOnDistressEyes() = 0;
		virtual void turnOnNeutralMouth() = 0;
		virtual void turnOnSmileMouth() = 0;
		virtual void turnOnDistressMouth() = 0;
		virtual void turnOnAngerEyes() = 0;
		virtual void setStateThoughtBalloon(avt::ThoughtBalloonState::State state) = 0;
		virtual void activateSpeechBalloon(unsigned int speechNumber) = 0;
		virtual void turnOffSpeechBalloon(void) = 0;
	};
}

#endif // _EmotionalAgent_h_
