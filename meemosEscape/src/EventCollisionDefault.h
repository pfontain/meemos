#ifndef _EventCollisionDefault_h_
#define _EventCollisionDefault_h_

#include "EventCollision.h"

namespace avt{

	class EventCollisionDefault: public EventCollision
	{	
	public:
		EventCollisionDefault(){}
		EventCollisionDefault(meemo::Location location): EventCollision(location){}

		const std::string toString(){
			std::stringstream stringStream;
			stringStream << "EventCollisionDefault";
			if(isLocationSet())
			{
				stringStream << "[" << _location << "]";
			}
			return stringStream.str();
		}

		EventType::Type type(){
			return EventType::COLLISION_DEFAULT;
		}

		Event* copy(){
			if(isLocationSet())
				return new EventCollisionDefault(_location);
			else
				return new EventCollisionDefault();
		}
	};
}

#endif