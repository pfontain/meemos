#ifndef _UpdatePathAgent_h_
#define _UpdatePathAgent_h_

#include "Path.h"

namespace avt{
	class UpdatePathAgent{
	public:
		virtual avt::Path* updatePath(avt::Path* path) = 0;
	};
}

#endif // _UpdatePathAgent_h_
