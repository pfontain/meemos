#include "EventBuffer.h"

namespace avt{
	EventBuffer::EventBuffer(meemo::EmotionalAgent* agent){
		_agent = agent;
	}

	EventBuffer::~EventBuffer(){
		std::list<Event*>::iterator iteratorEvents = _events.begin();
		for(;iteratorEvents != _events.end(); iteratorEvents++){
			Event* event = *iteratorEvents;
			event->destroy();
			delete event;
		}

		std::list<Event*>::iterator iteratorTrashEvents = _trash.begin();
		for(;iteratorTrashEvents != _trash.end(); iteratorTrashEvents++){
			Event* event = *iteratorTrashEvents;
			event->destroy();
			delete event;
		}
	}

	meemo::EmotionalAgent* EventBuffer::getAgent(){
		return _agent;
	}

	Event* EventBuffer::pop(){
		Event* event;
		event = _events.front();
		_events.pop_front();
		_trash.push_back(event);
		return event;
	}

	void EventBuffer::push(Event* event){
		_events.push_back(event);
	}

	void EventBuffer::onEvent(Event* event){

		std::list<Event*>::iterator iteratorEvents = _events.begin();
		for(;iteratorEvents != _events.end(); iteratorEvents++)
			if((*iteratorEvents)->equal(event))
			{
				event->destroy();
				delete event;
				return;
			}
		push(event);
	}

	bool EventBuffer::empty(){
		return _events.empty();
	}

	void EventBuffer::clean(){
		std::list<Event*>::iterator iteratorTrashEvents = _trash.begin();
		for(;iteratorTrashEvents != _trash.end(); iteratorTrashEvents++){
			(*iteratorTrashEvents)->destroy();
			delete (*iteratorTrashEvents);
		}
		_trash.clear();
	}
}