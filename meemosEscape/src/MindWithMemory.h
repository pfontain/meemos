#ifndef _MindWithMemory_meemo_
#define _MindWithMemory_meemo_

#include "Mind.h"
#include "MemoryStorage.h"
#include "LocationEcphory.h"
#include "EmotionalReactionRuleMemoryRetrieval.h"
#include "ReactiveProcessMemoryRetrieval.h"

namespace meemo{
	class MindWithMemory: public Mind{
	public:
		MindWithMemory(avt::Physical* physicalAgent,meemo::EmotionalAgent* emotionalAgent);
		~MindWithMemory();

	private:
		LocationEcphory* _locationEcphory;
		MemoryStorage _memoryStorage;
		ReactiveProcessMemoryRetrieval* _reactiveProcessMemoryRetrieval;

	public:
		LocationEcphory* getLocationEcphory(void) const;
		MemoryStorage* getMemoryStorage(void);

	private:
		void initModules();
		void initEmotionalReactionRules();
		void updateAppraisal();
	};
}

#endif // _MindWithMemory_meemo_