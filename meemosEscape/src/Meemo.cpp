#include "Meemo.h"

namespace avt{
	Meemo::Meemo(std::string id,int size,AbstractLevel* level,AbstractGameLogic* gameLogic): cg::Entity(id){
		_gameLogic = gameLogic;
		_level = level;
		_size = size;
		_eventBuffer = new EventBuffer(this);
		_eyes = new Eyes(this);
		_mouth = new Mouth(this);
		_thoughtBalloon = new ThoughtBalloon(this);
		_speechBalloon = new SpeechBalloon(this);
		_material = new MaterialDynamic();

		_doorFound = false;
		_leverFound = false;
		_openDoorFound = false;
	}

	Meemo::~Meemo(){
		delete _eventBuffer;
		delete _eyes;
		delete _mouth;
		delete _thoughtBalloon;
		delete _speechBalloon;
		delete _material;
	}

	//fun�ao de inicializa�ao do meemo
	void Meemo::init() {
		this->meemoMesh.init();
		_eyes->init();
		_mouth->init();
		_thoughtBalloon->init();
		_speechBalloon->init();

		_visionRadius = cg::Properties::instance()->getFloat("MEEMO_VISION_RADIUS");
		_visionAngle = cg::Properties::instance()->getFloat("MEEMO_VISION_ANGLE");
		_hearingRadius = cg::Properties::instance()->getFloat("MEEMO_HEARING_RADIUS");
		initPhysics();
		_physics->registerSubscriber(_eventBuffer);

		makeModel();
		DebugRegistry::getInstance()->add(this);

		_mind->init();
		_behaviour->init();
		_physics->registerSubscriber(_mind);
	}

	void Meemo::draw(void) {
		glPushMatrix();
		{
			_physics->applyTransforms();
			_eyes->draw();
			_mouth->draw();
			_material->draw();
			glCallList(_modelDL);
		}
		glPopMatrix();
		_thoughtBalloon->draw();
		_speechBalloon->draw();

		drawDebug();
	}

	void Meemo::subscribe(EventWitnessNotifier* eventNotifier){
		eventNotifier->registerWitnessSubscriber(_eventBuffer);
		eventNotifier->registerWitnessSubscriber(_mind);
	}

	void Meemo::update(unsigned long elapsed_millis){
		double elapsedSeconds = elapsed_millis / (double)1000;

		_eventBuffer->clean();
		_mind->update();
		_behaviour->update(elapsedSeconds);
		Camera* activeCamera = _level->getActiveCamera();
		cg::Vector3d activeCameraPosition = activeCamera->getPosition();
		_thoughtBalloon->update(activeCameraPosition);
		_speechBalloon->update(elapsedSeconds,activeCameraPosition);
		_physics->generatePhysicalEvents();
		generateVisionEvents();
	}

	const cg::Vector3d Meemo::getPosition(void){
		return _physics->getPosition();
	}

	const cg::Quaterniond Meemo::getOrientation(void){
		return _physics->getOrientation();
	}

	appraisal::EmotionalState* Meemo::getEmotionalState(void){
		return _mind->getEmotionalState();
	}

	meemo::Mind* Meemo::getMind(){
		return _mind;
	}

	float Meemo::getVisionRadius(){
		return _visionRadius;
	}

	const cg::Vector3d Meemo::getVisionDirection(){
		cg::Quaterniond orientation(_physics->getOrientation());
		cg::Vector3d visionDirection(1,0,0);
		visionDirection = apply(orientation, visionDirection);
		return visionDirection;
	}

	float Meemo::getVisionAngle(){
		return _visionAngle;
	}

	const std::string& Meemo::getId() const { return _id; }

	Physics* Meemo::getPhysics(void){
		return _physics;
	}

	bool Meemo::canSenseEvent(meemo::Location& eventLocation){
		bool canSenseEvent = isLocationVisible(eventLocation) || isEventAudible(eventLocation);
		return canSenseEvent;
	}

	bool Meemo::isLocationVisible(meemo::Location& location){
		bool isLocationVisible = isWithinVisionRadius(location) && isWithinVisionAngle(location);
		return isLocationVisible;
	}

	void Meemo::turnOnNeutralEyes(){
		_eyes->neutralOn();
	}

	void Meemo::turnOnJoyEyes(){
		_eyes->joyOn();
	}

	void Meemo::turnOnDistressEyes(){
		_eyes->sadOn();
	}

	void Meemo::turnOnAngerEyes(){
		_eyes->angryOn();
	}

	void Meemo::turnOnNeutralMouth(){
		_mouth->neutralOn();
	}

	void Meemo::turnOnSmileMouth(){
		_mouth->smileOn();
	}

	void Meemo::turnOnDistressMouth(){
		_mouth->distressOn();
	}

	void Meemo::setStateThoughtBalloon(ThoughtBalloonState::State state){
		_thoughtBalloon->setState(state);
	}

	void Meemo::changeColourIntensity(float intensity){
		cg::Vector3d materialColour;

		materialColour[0]=intensity * _materialColourFirst[0] + (1-intensity) * _materialColourSecond[0];
		materialColour[1]=intensity * _materialColourFirst[1] + (1-intensity) * _materialColourSecond[1];
		materialColour[2]=intensity * _materialColourFirst[2] + (1-intensity) * _materialColourSecond[2];

		_material->setAmbient(materialColour[0],materialColour[1],materialColour[2],1.0);
		_material->setDiffuse(materialColour[0],materialColour[1],materialColour[2],1.0);
	}

	void Meemo::activateSpeechBalloon(unsigned int speechNumber){
		_speechBalloon->activate(speechNumber);
	}

	void Meemo::turnOffSpeechBalloon(void){
		_speechBalloon->turnOff();
	}

	void Meemo::makeModel() {
		_modelDL = glGenLists(1);
		assert(_modelDL != 0);
		glNewList(_modelDL,GL_COMPILE);
		{
			glBegin(GL_TRIANGLES);
			for(unsigned int iIndex = 0; iIndex < (unsigned int)(this->meemoMesh.number_faces*3); iIndex++){
				unsigned int pointIndice = this->meemoMesh._indices[iIndex];
				cg::Vector3d vertex = this->meemoMesh._points[pointIndice];
				cg::Vector3d normal = this->meemoMesh._normals[iIndex];

				glNormal3f(normal[2],normal[0],normal[1]);
				glVertex3f(vertex[2],vertex[0],vertex[1]);
			}
			glEnd();
		}
		glEndList();
	}

	void Meemo::initMaterial(){		
		_material->setAmbient(_materialColourFirst[0],_materialColourFirst[1],_materialColourFirst[2],1.0);
		_material->setDiffuse(_materialColourFirst[0],_materialColourFirst[1],_materialColourFirst[2],1.0);
		_material->init();
	}

	void Meemo::initPhysics(){
		_physics->setAxesScale(_size);
		PhysicsRegistry::getInstance()->add(_physics);
		_physics->setBoundingSphereRadius(_size/2.0);
		int unitaryLength = cg::Properties::instance()->getInt("UNITARY_LENGTH");
		_physics->setLinearVelocity(unitaryLength);
		_physics->setAngularVelocity(unitaryLength*2);
		_physics->setAxesScale(unitaryLength);
	}

	void Meemo::generateVisionEvents(void){
		Physical* door = _gameLogic->getDoor();
		cg::Vector3d doorPosition = door->getPosition();
		meemo::Location doorLocation(doorPosition);
		if(!_openDoorFound && _gameLogic->isDoorOpened() && isLocationVisible(doorLocation)){
			_openDoorFound = true;
			_doorFound = true;
			_eventBuffer->push(new EventSeeOpenDoor(doorLocation));
		}
		else if(!_doorFound && isLocationVisible(doorLocation)){
			_doorFound = true;
			_eventBuffer->push(new EventSeeDoor(doorLocation));
		}

		Physical* lever = _gameLogic->getLever();
		cg::Vector3d leverPosition = lever->getPosition();
		meemo::Location leverLocation(leverPosition);
		if(!_leverFound && isLocationVisible(leverLocation)){
			_leverFound = true;
			_eventBuffer->push(new EventSeeLever(leverLocation));
		}
	}

	bool Meemo::isWithinVisionRadius(meemo::Location& location){
		return isWithinRadius(location,_visionRadius);
	}

	bool Meemo::isWithinVisionAngle(meemo::Location& location){
		bool isLocationWithinVisionAngle;
		cg::Vector3d meemoPositionVector(_physics->getPosition());
		cg::Vector3d locationVector(location.getVector3d());
		cg::Vector3d locationDirectionVector;
		locationDirectionVector = normalize(locationVector - meemoPositionVector);
		cg::Vector3d visionDirection(getVisionDirection());
		cg::Vector3d crossProductVisionLocationDirection  = cross(visionDirection,locationDirectionVector);
		double dotProductVisionLocationDirection = dot(visionDirection,locationDirectionVector);
		
		if(isLocationInFront(crossProductVisionLocationDirection,dotProductVisionLocationDirection)){
			isLocationWithinVisionAngle = true;
		}
		else if (isLocationBehind(crossProductVisionLocationDirection)){
			isLocationWithinVisionAngle = isFishEyed();
		}
		else{
			isLocationWithinVisionAngle = isAngleVisionLocationDirectionSmallEnough(dotProductVisionLocationDirection);
		}

		return isLocationWithinVisionAngle;
	}

	bool Meemo::isLocationInFront(cg::Vector3d& crossProductVisionLocationDirection,float dotProductVisionLocationDirection){
		return fabs(crossProductVisionLocationDirection[2]) < Utilities::Math::smallPositive()
			&& dotProductVisionLocationDirection > 0;
	}

	bool Meemo::isLocationBehind(cg::Vector3d& crossProductVisionLocationDirection){
		return fabs(crossProductVisionLocationDirection[2]) < Utilities::Math::smallPositive();
	}

	bool Meemo::isFishEyed(){
		return Utilities::Math::isZero(_visionAngle-360);
	}

	bool Meemo::isAngleVisionLocationDirectionSmallEnough(float dotProductVisionLocationDirection){
		double angleVisionLocationDirection = acos(dotProductVisionLocationDirection);
		double absoluteAngleVisionLocationDirection = fabs(angleVisionLocationDirection);
		double absoluteAngleVisionLocationDirectionDegrees = absoluteAngleVisionLocationDirection*cg::RADIANS_TO_DEGREES;

		return absoluteAngleVisionLocationDirectionDegrees < _visionAngle/2;
	}

	bool Meemo::isEventAudible(meemo::Location& eventLocation){
		bool isEventAudible = isWithinHearingRadius(eventLocation);
		return isEventAudible;
	}

	bool Meemo::isWithinHearingRadius(meemo::Location& eventLocation){
		return isWithinRadius(eventLocation,_hearingRadius);
	}

	bool Meemo::isWithinRadius(meemo::Location& location,float radius){
		bool isLocationWhithinRadius;
		meemo::Location meemoLocation(_physics->getPosition());
		float squaredDistanceToLocation = location.squaredDistance(meemoLocation);
		float squaredRadius = Utilities::Math::square(radius);

		if(squaredDistanceToLocation < squaredRadius)
			isLocationWhithinRadius = true;
		else
			isLocationWhithinRadius = false;

		return isLocationWhithinRadius;
	}
}