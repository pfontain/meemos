#ifndef _Anvil_avt_
#define _Anvil_avt_

#include "cg/cg.h"
#include "MaterialStatic.h"
#include "PhysicsAnvil.h"
#include "DebugRegistry.h"

namespace avt{
	class Anvil: public cg::Entity,
		public cg::IDrawListener,
		public cg::IUpdateListener,
		public Debugable
	{
	public:
		Anvil(int x, int y,float size,const std::string& id);
		~Anvil();
		void init();

	private:
		int _modelDL;
		float _size;
		Material* _material;
		PhysicsAnvil* _physics;

	public:
		void draw();
		void update(unsigned long elapsed_millis);
		Physics* getPhysics();
		
	private:
		void makeModel();
	};
}

#endif