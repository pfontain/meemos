#include "StatePhysicsMeemoMinionWait.h"

namespace avt{
	StatePhysicsMeemoMinionWait::StatePhysicsMeemoMinionWait(double duration){
		_duration = duration;
		_elapsedSeconds = 0;
	}

	double StatePhysicsMeemoMinionWait::getDuration(void){
		return _duration;
	}

	double StatePhysicsMeemoMinionWait::getElapsedSeconds(void){
		return _elapsedSeconds;
	}

	void StatePhysicsMeemoMinionWait::incrementElapsedSeconds(double increment){
		_elapsedSeconds += increment;
	}

	void StatePhysicsMeemoMinionWait::start(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor){
		statePhysicsMeemoMinionVisitor->start(this);
	}

	void StatePhysicsMeemoMinionWait::step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds){
		statePhysicsMeemoMinionVisitor->step(this,elapsedSeconds);
	}

	void StatePhysicsMeemoMinionWait::destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion){}

	bool StatePhysicsMeemoMinionWait::canInterrupt(){
		return false;
	}
}