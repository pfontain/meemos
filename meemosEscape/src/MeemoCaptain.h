#ifndef _MeemoCaptain_h_
#define _MeemoCaptain_h_

#include "Meemo.h"
#include "MindWithMemory.h"
#include "PhysicsMeemoCaptain.h"
#include "Location.h"
#include "EventSeeDoor.h"

#include <string.h>

namespace avt{

	class MeemoCaptain : public Meemo
	{
	public:
		MeemoCaptain(float x, float y,int size, AbstractLevel* level,AbstractGameLogic* gameLogic);
		~MeemoCaptain(void);

	private:
		bool _actionLock;
		bool _actionIdLock;
		bool _alive;
		MeemoAction::Action _action;
		MeemoAction::Action _potentialAction;
		int _actionMeemoId;
		int _potentialActionMeemoId;
		PhysicsMeemoCaptain* _physicsMeemoCaptain;
		meemo::MindWithMemory* _mindWithMemory;

	public:
		void init(void);
		int getAction(void);
		int getActionId(void);
		void setNumberMeemoMinions(int numberMeemoMinions);
		void update(unsigned long elapsed_millis);
		void draw(void);

	protected:
		void initMaterial(void);

	private:
		void update(Event* event);
		void trigger(EventWitness* eventWitness);
		void trigger(EventCollisionAnvil* eventCollisionAnvil);
		void trigger(EventCollisionTrapDoor* eventCollisionTrapDoor);
		void trigger(EventCollisionHole* eventCollisionHole);
		void trigger(EventSeeOpenDoor* eventSeeOpenDoor);
		void trigger(EventSeeDoor* eventSeeDoor);
		void trigger(EventSeeLever* eventSeeLever);
		void updateKeyboardActions(void);
		void updateMinionNumberCommand(void);
		void updateActionCommand(void);
		void updateCompleteCommandCheck(void);
	};
}

#endif