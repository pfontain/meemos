#ifndef _PhysicsDefault_h
#define _PhysicsDefault_h

#include "Physics.h"

namespace avt {

	class PhysicsDefault : public Physics {	
	public:
		PhysicsDefault();

	private:
		bool _isGoUp;
		bool _isGoDown;

	public:
		void goUp();
		void goDown();
		void step(double elapsedSeconds);
		void step();
		EventCollision* getCollisionEvent();
		bool isColliding(Physics* physics);
		void rotate(const cg::Vector3d& direction);

		void setFront(const cg::Vector3d& frontDirection);
		void setLeft(const cg::Vector3d& leftDirection);
		void setUp(const cg::Vector3d& upDirection);
	};
}

#endif