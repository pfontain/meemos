#include "Map.h"

namespace avt{

	Map::Map(const std::pair<int,int>& dimensions){
		setDimensions(dimensions);
	}

	Map::Map(const Map& map){
		std::pair<int,int> dimensions = map._dimensions;
		setDimensions(dimensions);
		int sizeX = dimensions.first;
		int sizeY = dimensions.second;
		unsigned int totalSize = sizeX*sizeY;
		for(unsigned int i=0; i<totalSize; i++)
			(*_map)[i]=(*map._map)[i];
	}

	Map::Map(){
	}

	Map::~Map(){
		delete _map;
	}

	void Map::setDimensions(const std::pair<int,int>& dimensions){
		_dimensions = dimensions;
		int sizeX = dimensions.first;
		int sizeY = dimensions.second;
		_map = new std::vector<char>(sizeX*sizeY,' ');
	}

	const std::pair<unsigned int,unsigned int>& Map::getDimensions(){
		return _dimensions;
	}

	int Map::getSizeX(){
		int sizeX = _dimensions.first;
		return sizeX;
	}
	
	int Map::getSizeY(){
		int sizeY = _dimensions.second;
		return sizeY;
	}

	char& Map::operator[](const std::pair<int,int>& location){
		int x = location.first;
		int y = location.second;
		int sizeX = _dimensions.first;
		int sizeY = _dimensions.second;
		return (*_map)[x + y * sizeX];
	}

	char& Map::operator[](int position){
		return (*_map)[position];
	}

	int* Map::createIntegerMap(){
		unsigned int totalMapSize = _dimensions.first*_dimensions.second;
		int* integerMap = new int[totalMapSize];
		for(unsigned int i=0; i < totalMapSize; i++)
			if((*_map)[i] == 'w' || (*_map)[i] == 'h')
				integerMap[i] = 0;
			else
				integerMap[i] = 1;
		return integerMap;
	}

	int* Map::createIntegerMap(std::map<char,int>& mappingToInteger){
		unsigned int totalMapSize = _dimensions.first*_dimensions.second;
		int* integerMap = new int[totalMapSize];
		for(unsigned int i=0; i < totalMapSize; i++){
			char mapChar = (*_map)[i];
			bool charIsMapped;
			if(mappingToInteger.count(mapChar) == 1)
				charIsMapped = true;
			else
				charIsMapped = false;
			if(charIsMapped)
				integerMap[i] = mappingToInteger[mapChar];
			else 
				integerMap[i] = mappingToInteger[' '];
		}
		return integerMap;
	}

	int* Map::copyIntegerMap(int* integerMap,const std::pair<unsigned int,unsigned int>& dimensions){
		unsigned int mapSizeX = dimensions.first;
		unsigned int mapSizeY = dimensions.second;
		unsigned int totalMapSize = mapSizeX * mapSizeY;
		int* copyIntegerMap = new int[totalMapSize];

		for(unsigned int i=0; i < totalMapSize; i++)
			copyIntegerMap[i] = integerMap[i];
		
		return copyIntegerMap;
	}

	const std::string Map::toString(){
		unsigned int width = _dimensions.first;
		unsigned int height = _dimensions.second;
		std::stringstream stringStream;
		for(int row = height-1; row >= 0; row--){
			for(unsigned int column = 0; column < width; column++){
				stringStream << (*_map)[column + row * width];
			}
			stringStream << std::endl;
		}
		return stringStream.str();
	}

	const std::string Map::toStringIntegerMap(int* integerMap,unsigned int width,int height){
		std::stringstream stringStream;
		for(int row = height-1; row >= 0; row--){
			for(unsigned int column = 0; column < width; column++){
				stringStream.width(3);
				stringStream << std::right << integerMap[column + row * width];
			}
			stringStream << std::endl;
			stringStream << std::endl;
		}
		return stringStream.str();
	}
}