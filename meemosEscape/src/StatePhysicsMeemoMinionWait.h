#ifndef __StatePhysicsMeemoMinionWait_avt__
#define __StatePhysicsMeemoMinionWait_avt__

#include "StatePhysicsMeemoMinion.h"
#include "StatePhysicsMeemoMinionVisitor.h"

namespace avt{
	class StatePhysicsMeemoMinionVisitor;

	class StatePhysicsMeemoMinionWait: public StatePhysicsMeemoMinion{	
	public:
		StatePhysicsMeemoMinionWait(double duration);

	private:
		double _duration;
		double _elapsedSeconds;

	public:
		double getDuration(void);
		double getElapsedSeconds(void);
		void incrementElapsedSeconds(double increment);
		void start(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		void step(StatePhysicsMeemoMinionVisitor* statePhysicsMeemoMinionVisitor,double elapsedSeconds);
		void destroy(StatePhysicsMeemoMinionVisitor* physicsMeemoMinion);
		bool canInterrupt();
	};
}

#endif // __StatePhysicsMeemoMinionWait_avt__