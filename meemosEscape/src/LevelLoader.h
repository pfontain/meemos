#ifndef _LevelLoader_avt_
#define _LevelLoader_avt_

#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <ctype.h>
#include "cg/cg.h"
#include "Level.h"
#include "MeemoCaptain.h"
#include "MeemoMinion.h"
#include "MeemoMinionNoMemory.h"
#include "MeemoMinionWithMemory.h"
#include "Map.h"
#include "Wall.h"
#include "Hole.h"
#include "FloorTile.h"
#include "FinishLine.h"
#include "Anvil.h"
#include "TrapDoor.h"
#include "Lever.h"
#include "Door.h"

namespace avt{
	class Level;

	class LevelLoader
	{
	public:
		LevelLoader(Level* level);

	private:
		Level* _level;
		std::ifstream* _levelFile;
		Map* _map;
		unsigned int _numberWalls;
		unsigned int _numberHoles;
		unsigned int _numberFinishLines;
		unsigned int _numberAnvils;
		unsigned int _numberMeemoMinions;
		unsigned int _numberFloorTiles;
		unsigned int _numberTrapDoors;
		unsigned int _numberLevers;

	public:
		void load(const std::string& levelNameFile);

	private:
		void openLevelFile(const std::string& levelNameFile);
		void loadMapSize();
		void loadMapFromFile();
		void loadMeemoCaptainFromMap();
		void loadEntitiesFromMap();
		void addEntityChar(char lineChar,int x,int y);
		void addFloorTile(int x,int y);
		void addWall(int x,int y);
		void addHole(int x,int y);
		void addFinishLine(int x,int y);
		void addAnvil(int x,int y);
		void addTrapDoor(int x,int y);
		void addLever(int x,int y);
		void addDoor(int x,int y);
		void addMeemoMinion(int x,int y,char number);
		MeemoMinion* createMeemoMinion(int x,int y,unsigned int idNumber);
		void setMeemoMinionScript(MeemoMinion* meemoMinion);
		void setMeemoMinionScriptPositions(MeemoMinion* meemoMinion);
		void setMeemoMinionScriptWaitDurations(MeemoMinion* meemoMinion);
		void addMeemoCaptain(int x,int y);
		void closeLevelFile();
	};
}

#endif