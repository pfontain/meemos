TODOs:
- segfault when jumping while walking?
- Put MEMORY_RETRIEVAL_INTENSITY_BIAS in config.ini
- Change finish line draw

- TODO CM: Define Meemo Goals
- Unscript ultra scripted sensors

Sugest�es
- Andar diferente depois de atingido com bigorna
- necessita de notifica��o de eventos como ver a porta

- Meemos don't avoid non-static meemos?
- Unscript Event Desirable Reaction - Personality Module
- location trigger:
	- what distance?

- Meemo Minion: Wait State - time configurable
- Idle Animation : Eyes?
- MovePath: Start Move when in collision with other meemo minion
- Improve Pull Lever animation
- Unscript Pull Lever Action
- MovePath: Try to get closer to Meemo Captain
- Meemo 3: hard to be hit by Anvil
- Light Config Values
- Witness Event - Anvil Falling no Meemo Hurt (no Meemo Collision)
- BehaviourMeemoMinion: updateThoughtBalloon abstraction
- Warning when commands are issued to dead Meemo Minion
- TODO CM: Regras para event witnessed retrieval
- TODO CM: Potencialmente elementos com e sem mem�ria na mesma simula��o
- TODO CM: Fall & Death
- Anvil Mesh Model
- Double Anvil Collision: Decrease wait time
- Meemo Minion: Step away from danger - can be avoided in scenario creation
- Thought Balloon Quirky
- Additional Emotion Rules: Fear? Hope?
- Meemo Mesh Lighting Problem
- Limpar envio mensagens e ac��es nos Meemos
- make emotion time dependent on simulation time
- enable freeze time
- Meemo - Performance Memory Storage
- Meemo destructor eliminar do registo?
- Performance A*
- Automatic Level Loading Hidden Problem
- C�mara
	- deixar utilizador controlar a c�mara
	- foco d� significado emocional potencialmente indesejado
	- Automated Camera Movement