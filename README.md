# README #

### What is this repository for? ###

* Storing the code and content for Meemos Escape.
* Meemos Escape is and AI driven simulation with cartoon looking characters.

### How do I get set up? ###

* Install Visual Studio in a Windows machine.
* Open the solution.
* Compile and run the startup project.

### Who do I talk to? ###

* pfontain at ucsc.edu